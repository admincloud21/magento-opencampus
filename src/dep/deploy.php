<?php
namespace Deployer;

require 'recipe/magento2.php';

set('application', 'capabilia');

desc('Magento2 Set the necessary ownership / permissions');
task('magento:fix_permission', function () {
    $httpUser = get('http_user');
    $httpGroup = get('http_group');
    run("chmod +x {{release_path}}/bin/magento");
    run("chown -R {$httpGroup}:{$httpUser} {{release_path}}");
    run("chown -R {$httpGroup}:{$httpUser} {{current_path}}");
    run("chown -R {$httpGroup}:{$httpUser} {{current_path}}/var/*");
    run("sudo find {{current_path}}/var {{current_path}}/pub/static {{current_path}}/pub/media {{current_path}}/generated {{current_path}}/app/etc -type f -exec chmod g+w {} \;");
    run("sudo find {{current_path}}/var {{current_path}}/pub/static {{current_path}}/pub/media {{current_path}}/generated/ {{current_path}}/app/etc -type d -exec chmod g+ws {} \;");
    run("mkdir -p {{current_path}}/pub/media/tmp");
    run("sudo find {{current_path}}/pub/media/captcha {{current_path}}/pub/media/catalog {{current_path}}/pub/media/tmp -type d -exec chmod -R a+rwxs {} \;");
    run("chown -R {$httpGroup}:{$httpUser} {{current_path}}/pub/media/tmp");
});

task('reload:php-fpm', function () {
    run('sudo service php7.1-fpm reload');
});

after('deploy', 'reload:php-fpm');
after('rollback', 'reload:php-fpm');

set('repository', 'git@bitbucket.org:admincloud21/magento-2018-final.git');
set('composer_options', '{{composer_action}} --no-progress --no-interaction --optimize-autoloader --no-suggest');
set('default_stage', 'test-dev');
set('writable_mode', 'chmod');


inventory('hosts.yml');
