<?php

namespace BestWorlds\Capabilia\Block\Octavio;

use Magento\Framework\View\Element\Template;

class Session extends \Magento\Framework\View\Element\Template {

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;
    }

    public function getCapabiliaSessionData()
    {
        $sessionNames = [
            'UserId' 		=> 'userId',
            'TokenOctavio' 	=> 'authToken',
            'NombreUsuario'         => 'nombre',
            'ApellidoUsuario'       => 'apellido',
            'MailUsuario' 			=> 'email',
            'CalleUsuario'          => 'calle',
            'NumeroCalleUsuario'    => 'numero',
            'PisoUsuario'           => 'piso',
            'CiudadUsuario'         => 'ciudad',
            'CpUsuario'             => 'cp',
            'FechaNacUsuario'       => 'fechaDeNacimiento',
            'IdiomaUsuario'         => 'idioma',
            'PaisUsuario'           => 'pais',
            'ProvinciaUsuario'      => 'provincia'
        ];

        $sessionData = $this->customerSession->getCapabiliaSessionData();
        if ($sessionData) {
            foreach($sessionNames as $key => $value) {
                if (isset($sessionData[$value])) {
                    $sessionData[$key] = $sessionData[$value];
                    unset($sessionData[$value]);
                }

            }
        }

        return $sessionData;
    }
}