<?php namespace BestWorlds\Capabilia\Block\Sidebar;

use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Catalog\Model\ResourceModel\Product;
use Magento\Theme\Block\Html\Topmenu;

/**
 * Class:Category
 * Sebwite\Sidebar\Block
 *
 * @author      Sebwite
 * @package     Sebwite\Sidebar
 * @copyright   Copyright (c) 2015, Sebwite. All rights reserved
 */
class Category extends Topmenu
{
}