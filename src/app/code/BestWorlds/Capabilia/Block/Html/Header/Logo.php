<?php

namespace BestWorlds\Capabilia\Block\Html\Header;

use Magento\Framework\View\Element\Template;

class Logo extends \Magento\Theme\Block\Html\Header\Logo {

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\MediaStorage\Helper\File\Storage\Database $fileStorageHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\MediaStorage\Helper\File\Storage\Database $fileStorageHelper,
        array $data = []
    ) {
        parent::__construct($context, $fileStorageHelper, $data);
    }

    /**
     * Retrieve logo url
     *
     * @return int
     */
    public function getLogoUrl()
    {
        if (empty($this->_data['logo_url'])) {
            $this->_data['logo_url'] = $this->_scopeConfig->getValue(
                'design/header/logo_url',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
        }

        return $this->_data['logo_url'] ? $this->_data['logo_url'] : $this->getUrl('');
    }
}