<?php

namespace BestWorlds\Capabilia\Cron;

class SendToApi {

    protected $orderCollectionFactory;
    protected $_api;
    protected $logger;

    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \BestWorlds\Capabilia\Model\Api $api,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->_api = $api;
        $this->logger = $logger;
    }

    /**
     * Write to system.log
     *
     * @return void
    */
    public function execute() {
        $collection = $this->orderCollectionFactory->create();
        $collection->addAttributeToSelect('*')->addFieldToFilter('imported_to_api', ['null' => true]);
        foreach ($collection as $order) {
            $this->sendApi($order);
        }
    }

    public function sendApi($order)
    {
        try {
            /** lock order to avoid duplicated sending */
            $order->setImportedToApi(0);
            $order->save();
            $response = $this->_api->saveOrder($order);
            $order->setImportedToApi(1);
            $order->setApiResponse($response);
            $order->save();
        } catch (Exception $e) {
            /** unlock order */
            $order->setImportedToApi(null);
            $order->save();
            $this->logger->addDebug($e->getMessage());
        }
    }

}