<?php
namespace Bestworlds\Capabilia\Model\Checkout;

class LayoutProcessorPlugin
{
    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */

    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array  $jsLayout
    ) {
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['shipping-address-fieldset']['children']['street'] = [
            'children' => [
                [
                    'label' => __('Street')
                ],
                [
                    'label' => __('Number')
                ],
                [
                    'label' => __('Floor')
                ]
            ]
        ];
        return $jsLayout;
    }
}
