<?php

namespace BestWorlds\Capabilia\Model;

use Magento\Framework\Exception\InvalidEmailOrPasswordException;
use Magento\Framework\Exception\LocalizedException;


class Api {

    protected $_config;
    protected $_logger;
    protected $_idioma = ['14' => 'EN', '15' => 'ES', '16' => 'PT', '17' => 'CA'];

    public function __construct(
        \BestWorlds\Capabilia\Helper\Config $config,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_config = $config;
        $this->_logger = $logger;
    }

    /**
     * Idioma values:
     *  Catalan => CA
     *  Ingles => EN
     *  Español => ES
     *  Portugues => PT
     *
     * @param [type] $order
     * @return void
     */
    public function saveOrder($order)
    {
        $address = $order->getBillingAddress();
        $idiom = $this->_config->getOrquestradorConfig('idiom');
        $idiom = $idiom ? $idiom : 'EN';

        $orderData = [
            'order_id' => $order->getId(),
            'increment_id' => $order->getIncrementId(),
            'cliente' => $this->_config->getOrquestradorConfig('client'), // @todo: cliente is the instance, like capabilia, barcelona, etc
            'idioma'  => $idiom, // @todo create a configuration to get it
            'producto' => [],
            'usuario' => [
                'usuario_id' => $order->getCustomerId(),
                'nombre' => $order->getCustomerFirstname(),
                'apellido' => $order->getCustomerLastname(),
                'email' => $order->getCustomerEmail(),
                'calle' => $address->getStreet()[0],
                'numero' => isset($address->getStreet()[1]) ? $address->getStreet()[1] : '',
                'ciudad' => $address->getCity(),
                'provincia' => $address->getRegion(),
                'pais' => $address->getCountryId(),
                'telefono' => $address->getTelephone(),
                'movil' => $address->getFax()
            ]
        ];

        foreach ($order->getAllItems() as $item) {
            $product = $item->getProduct();
            $orderData['producto'][] = [
                'producto_id' => $product->getId(),
                'nombre' => $item->getName(),
                'precio' => $item->getOriginalPrice(),
                'precio_final' => $item->getPrice(),
                'cantidad' => $item->getQtyOrdered(),
                'descuento' => $item->getDiscountPercent(),
                'monto_descuento' => $item->getDiscountAmount(),
                'idioma' => $this->getIdioma($item->getImpartidoEn())
            ];
        }

        return $this->sendPost('/compras', $orderData);
    }


    private function getIdioma($idiomId)
    {
        if (!isset($this->_idioma[$idiomId])) {
            return null;
        }

        return $this->_idioma[$idiomId];
    }

    public function sendLogin($email, $pass)
    {
        $data = [
            'email' => $email,
            'password' => $pass
        ];

        $result = json_decode($this->sendPost('/login', $data), true);
        if (!$result) {
            throw new LocalizedException(
                __('Not possible to connect to login API')
            );
        }

        if (isset($result['error']) && $result['error'] == 'Unauthorized') {
            throw new InvalidEmailOrPasswordException(
                __($result['mensaje'])
            );
        }

        return $result;
    }

    public function sendRegister($fname, $lname, $pass, $email, $lang)
    {
        $data = [
            'nombre' => $fname,
            'apellido' => $lname,
            'password' => $pass,
            'email' => $email,
            'idioma' => $lang
        ];
        $result = json_decode($this->sendPost('/registro', $data), true);
        if (!$result) {
            throw new LocalizedException(
                __('Not possible to connect to login API')
            );
        }

        if (isset($result['error']) && $result['error'] == 'Unauthorized') {
            throw new InvalidEmailOrPasswordException(
                __($result['mensaje'])
            );
        }

        return $result;
    }

    public function sendPost($uri, $data)
    {
        $data_string = json_encode($data);
        
        $ch = curl_init($this->_config->getOrquestradorConfig('api_url') . $uri);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Auth-Token: ' . $this->_config->getOrquestradorConfig('auth_token'),
            'Cliente: ' .  $this->_config->getOrquestradorConfig('client')
        ));

        curl_setopt($ch, CURLOPT_TIMEOUT, 25);
        $result = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
        curl_close($ch);

        return $result;
    }

    public function getUser($userId, $authToken)
    {
        $result = $this->sendGet('/usuarios/' . $userId, $authToken);
        $result = json_decode($result, true);

        return $result;
    }

    public function sendGet($uri, $authToken = null)
    {
        $ch = curl_init($this->_config->getOrquestradorConfig('api_url') . $uri);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Auth-Token: ' . $authToken,
            'Cliente: ' .  $this->_config->getOrquestradorConfig('client')
        ));

        curl_setopt($ch, CURLOPT_TIMEOUT, 25);
        $result = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
        curl_close($ch);

        return $result;
    }
}
