<?php

namespace BestWorlds\Capabilia\Model\Plugin;

class Product {
    
    public function aroundGetProductUrl(\Magento\Catalog\Model\Product $subject, callable $proceed)
    {
        $returnValue = $subject->getCmsProductUrl();
        $returnValue = $returnValue ? $returnValue : $proceed(); 

        return $returnValue;
    }
}