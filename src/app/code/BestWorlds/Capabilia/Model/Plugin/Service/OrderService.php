<?php

namespace BestWorlds\Capabilia\Model\Plugin\Service;

class OrderService {
    
    protected $_api;
    protected $logger;

    /**
     * Observer Constructor
     * @param \BestWorlds\Capabilia\Model\Api $api
     * @param LoggerInterface $logger
     */
    public function __construct(
        \BestWorlds\Capabilia\Model\Api $api,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->logger = $logger;
        $this->_api = $api;
    }

    public function afterPlace(\Magento\Sales\Model\Service\OrderService $subject, $result)
    {
        try {
            /** lock order to avoid duplicated sending */
            $result->setImportedToApi(0);
            $result->save();
            $response = $this->_api->saveOrder($result);
            $result->setImportedToApi(1);
            $result->setApiResponse($response);
            $result->save();
        } catch (Exception $e) {
            /** unlock order */
            $result->setImportedToApi(null);
            $result->save();
            $this->logger->addDebug($e->getMessage());
        }
        
        return $result;
    }
}