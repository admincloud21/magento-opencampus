<?php

namespace BestWorlds\Capabilia\Model\Plugin;

use \Magento\Framework\Controller\ResultFactory;
use \Magento\Checkout\Model\Session;
use \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use \Magento\Catalog\Model\ProductRepository;
use \Magento\Store\Model\StoreManagerInterface;

class Success {
    
    protected $resultRedirect;
    protected $checkoutSession;
    protected $helperConfig;
    protected $categoryCollection;
    protected $productRepository;
    protected $storeManager;

    public function __construct(
        ResultFactory $result,
        Session $checkoutSession,
        CollectionFactory $categoryCollection,
        ProductRepository $productRepository,
        StoreManagerInterface $storeManager,
        \BestWorlds\Capabilia\Helper\Config $helperConfig
    ) {
        $this->helperConfig = $helperConfig;
        $this->checkoutSession = $checkoutSession;
        $this->resultRedirect = $result;
        $this->categoryCollection = $categoryCollection;
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
    }

    public function afterExecute(\Magento\Checkout\Controller\Onepage\Success $subject, $result)
    {
        if (
            $this->helperConfig->getGeneralConfig('redirect_success_enabled') 
            && $result instanceof \Magento\Framework\View\Result\Page\Interceptor
        ) {
            //get Events category ID from EN Store
            $categoryId = false;
            $collection = $this->categoryCollection
                ->create()
                ->setStore(1)
                ->addAttributeToFilter('name', 'Events')
                ->setPageSize(1);

            if ($collection->getSize()) {
                $categoryId = $collection->getFirstItem()->getId();
            }
            
            $redirect = $this->helperConfig->getGeneralConfig('redirect_success_url', $this->storeManager->getStore()->getId());
            if ($categoryId) {
                $order = $this->checkoutSession->getLastRealOrder();
                $orderItems = $order->getAllItems();
                $eventRedirect = true;
                foreach ($orderItems as $orderItem) {
                    $product = $this->productRepository->getById($orderItem->getProductId());
                    $categoryIds = $product->getCategoryIds();
                    if (!in_array($categoryId, $categoryIds)) {
                        $eventRedirect= false;
                        break;
                    }
                }
                if ($eventRedirect) {
                    $redirect = $this->helperConfig->getGeneralConfig('redirect_success_url_event', $this->storeManager->getStore()->getId());
                }
            }

            $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($redirect);

            return $resultRedirect;     
        }

        return $result;
    }
}