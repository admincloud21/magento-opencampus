<?php
namespace BestWorlds\Capabilia\Controller\Cart;

class Update extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    private $session;
    
	public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Checkout\Model\Session $session
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->session = $session;
        
        parent::__construct($context);
    }
    
	public function execute()
	{
        if (!$this->getRequest()->isAjax()) {
            return;
        }

        if (!$itemImpartidoEn = $this->getRequest()->getParam('item_impartido_en')) {
            return;
        }

        $quote = $this->session->getQuote();
        foreach($itemImpartidoEn as $itemId => $idiomId) {
            $item = $quote->getItemById($itemId);
            if (!$item) {
                continue;
            }

            $item->setImpartidoEn($idiomId);
            $item->save();
        }

        $result = $this->resultJsonFactory->create();
        $response = array(
            'message' => __('Idiom updated')
        );

        return $result->setData($response);
	}
}