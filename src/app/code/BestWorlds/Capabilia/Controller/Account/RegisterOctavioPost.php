<?php
namespace BestWorlds\Capabilia\Controller\Account;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Customer\Model\Session;
use Magento\Framework\UrlInterface;

class RegisterOctavioPost extends Action
{
    /**
     * @var Validator
     */
    private $formKeyValidator;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var Api
     */
    protected $api;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     */
    private $cookieMetadataManager;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Customer\Api\Data\CustomerInterface
     */
    private $customerInterface;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $urlInterface;


    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        Validator $formKeyValidator,
        \BestWorlds\Capabilia\Model\Api $api,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        Session $session,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
        \Magento\Framework\Stdlib\Cookie\PhpCookieManager $cookieMetadataManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Api\Data\CustomerInterface $customerInterface,
        \Magento\Framework\UrlInterface $urlInterface
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->formKeyValidator = $formKeyValidator;
        $this->api = $api;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->session = $session;
        $this->storeManager = $storeManager;
        $this->customerInterface = $customerInterface;
        $this->urlInterface = $urlInterface;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute() {
        $resultJson = $this->resultJsonFactory->create();
        $post = $this->getRequest()->getPostValue();
        if ($this->getRequest()->isPost() && $this->isAjax()) {
            $lang = 'EN';
            $currentUrl = strtoupper($this->urlInterface->getCurrentUrl());

            $spanish = strpos($currentUrl, '/ES/');
            $portuguese = strpos($currentUrl, '/PT/');
            $catalan = strpos($currentUrl, '/CA/');

            if ($spanish !== false) $lang = 'ES';
            if ($portuguese !== false) $lang = 'PT';
            if ($catalan !== false) $lang = 'CA';
            
            try{
                $registerResult = $this->api->sendRegister(
                    $post['fname'],
                    $post['lname'],
                    $post['pass'],
                    $post['email'],
                    $lang);
            }catch (\Exception $e) {
                return $resultJson->setData(['error' => 'Error creating account on API: ' . $e->getMessage()]);
            }

            try{
                $loginResult = $this->api->sendLogin(
                    $post['email'],
                    $post['pass']
                );
                $userData = $this->api->getUser($loginResult["userId"], $loginResult["authToken"]);
            }catch (\Exception $e) {
                return $resultJson->setData(['error' => 'Error login account on API: ' . $e->getMessage()]);
            }

            try{
                $sessionData = array_merge($userData, $loginResult);
                $this->session->setCapabiliaSessionData($sessionData);
                $customer = $this->customerRepositoryInterface->get($post['email']);
                $this->session->loginById($customer->getId());
                $this->session->setCustomerDataAsLoggedIn($customer);
                $this->session->regenerateId();
                if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
                    $metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
                    $metadata->setPath('/');
                    $this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
                }
                return $resultJson->setData(['response' => 'ok']);
            }catch (\Exception $e) {
                return $resultJson->setData(['error' => 'Error saving session on Magento: ' . $e->getMessage()]);
            }


        } else {
            return $resultJson->setData(['error' => 'There was an error, try again later']);
        }
    }


    /**
     * Retrieve cookie manager
     *
     * @deprecated 100.1.0
     * @return \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     */
    private function getCookieManager()
    {
        if (!$this->cookieMetadataManager) {
            $this->cookieMetadataManager = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\PhpCookieManager::class
            );
        }
        return $this->cookieMetadataManager;
    }

    /**
     * Retrieve cookie metadata factory
     *
     * @deprecated 100.1.0
     * @return \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private function getCookieMetadataFactory()
    {
        if (!$this->cookieMetadataFactory) {
            $this->cookieMetadataFactory = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory::class
            );
        }
        return $this->cookieMetadataFactory;
    }

    /*
     *  Check Request is Ajax or not
     * @return boolean
     * */
    protected function isAjax() {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
    }
}
