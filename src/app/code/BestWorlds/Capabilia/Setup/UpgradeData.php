<?php
namespace BestWorlds\Capabilia\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Framework\DB\Ddl\Table;
use Magento\Cms\Model\BlockFactory;

class UpgradeData implements UpgradeDataInterface
{
    private $eavSetupFactory;
    
    /**
     * @var QuoteSetupFactory
     */
    private $quoteSetupFactory;
 
    /**
     * @var SalesSetup
     */
    private $salesSetupFactory;

    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    private $cmsBlockFactory;

    public function __construct(
        EavSetupFactory $eavSetupFactory,
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory,
        QuoteSetupFactory $quoteSetupFactory,
        SalesSetupFactory $salesSetupFactory,
        BlockFactory $cmsBlockFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->quoteSetupFactory = $quoteSetupFactory;
        $this->salesSetupFactory = $salesSetupFactory;
        $this->cmsBlockFactory = $cmsBlockFactory;
    }
    
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        
        /** @var QuoteSetup $quoteSetup */
        $quoteSetup = $this->quoteSetupFactory->create(['setup' => $setup]);
 
        /** @var SalesSetup $salesSetup */
        $salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);
        
        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'installments_qty',
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Max Installments',
                    'input' => 'text',
                    'class' => '',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false,
                    'apply_to' => ''
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

            $customerSetup->addAttribute('customer',
                'fcm_tokens', [
                    'label' => 'FCM Tokens',
                    'type' => 'text',
                    'frontend_input' => 'text',
                    'required' => false,
                    'visible' => true,
                    'system'=> 0,
                    'position' => 105,
                ]
            );

            $fcmAttribute = $customerSetup->getEavConfig()->getAttribute('customer', 'fcm_tokens');
            $fcmAttribute->setData('used_in_forms',['adminhtml_customer']);
            $fcmAttribute->save();

            $customerSetup->addAttribute('customer',
                'app_settings', [
                    'label' => 'APP Settings',
                    'type' => 'text',
                    'frontend_input' => 'text',
                    'required' => false,
                    'visible' => true,
                    'system'=> 0,
                    'position' => 106,
                ]
            );

            $settingsAttribute = $customerSetup->getEavConfig()->getAttribute('customer', 'app_settings');
            $settingsAttribute->setData('used_in_forms',['adminhtml_customer']);
            $settingsAttribute->save();

            $customerSetup->addAttribute('customer',
                'idioma_de_cursado', [
                    'label' => 'Idioma de Cursado',
                    'type' => 'text',
                    'frontend_input' => 'text',
                    'required' => false,
                    'visible' => true,
                    'system'=> 0,
                    'position' => 108,
                ]
            );

            $fcmAttribute = $customerSetup->getEavConfig()->getAttribute('customer', 'idioma_de_cursado');
            $fcmAttribute->setData('used_in_forms',['adminhtml_customer']);
            $fcmAttribute->save();
        }

        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'idioma',
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Idiom',
                    'input' => 'select',
                    'class' => '',
                    'source' => 'BestWorlds\Capabilia\Model\Config\Source\Idioms',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false,
                    'apply_to' => ''
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'cms_product_url',
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Cms Product Url',
                    'input' => 'text',
                    'class' => '',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => ''
                ]
            );
        }
        
        if (version_compare($context->getVersion(), '1.0.5') < 0) {
            $attributeOptions = [
                'type'     => Table::TYPE_TEXT,
                'visible'  => true,
                'required' => false
            ];
            $quoteSetup->addAttribute('quote_item', 'impartido_en', $attributeOptions);
            $salesSetup->addAttribute('order_item', 'impartido_en', $attributeOptions);
        }

        if (version_compare($context->getVersion(), '1.0.6') < 0) {
            $attributeOptions = [
                'type'     => Table::TYPE_TEXT,
                'visible'  => true,
                'required' => false
            ];
            $salesSetup->addAttribute('order', 'imported_to_api', $attributeOptions);
            $salesSetup->addAttribute('order', 'api_response', $attributeOptions);

            $setup->getConnection()->query("update sales_order set imported_to_api=1");
        }
        
        if (version_compare($context->getVersion(), '1.0.7') < 0) :
            $blocks = [
                [
                    'title' => 'Catalog Header Background',
                    'identifier' => 'catalog_header_background',
                    'is_active' => 1,
                    'stores' => [0],
                    'content' => '
                        <header class="catalog_header" style="background-color: transparent; background-image: url({{media url="wysiwyg/catalogo_banner.jpg"}})">
                            <div class="content">
                                {{block class="Magento\\Cms\\Block\\Block" block_id="catalog_header_content"}}
                            </div>
                        </header>
                    '
                ],
                [
                    'title' => 'Catalog Header - EN',
                    'identifier' => 'catalog_header_content',
                    'is_active' => 1,
                    'stores' => [1],
                    'content' => '<h2></h2><h3><span style="color: #ff9800"></span></h3>
                    '
                ],
                [
                    'title' => 'Catalog Header - PT',
                    'identifier' => 'catalog_header_content',
                    'is_active' => 1,
                    'stores' => [4],
                    'content' => '<h2></h2><h3><span style="color: #ff9800"></span></h3>
                    '
                ],
                [
                    'title' => 'Catalog Header - ES',
                    'identifier' => 'catalog_header_content',
                    'is_active' => 1,
                    'stores' => [3],
                    'content' => '<h2></h2><h3><span style="color: #ff9800"></span></h3>
                    '
                ],
                [
                    'title' => 'Catalog Header - CA',
                    'identifier' => 'catalog_header_content',
                    'is_active' => 1,
                    'stores' => [5],
                    'content' => '<h2></h2><h3><span style="color: #ff9800"></span></h3>
                    '
                ]
            ];
            foreach ($blocks as $block) :
                $cmsBlock = $this->cmsBlockFactory->create()->load($block['title'], 'title');
                if ($cmsBlock->getId()) {
                    $cmsBlock->setContent($block['content']);
                    $cmsBlock->save();
                } else {
                    $this->cmsBlockFactory->create(["data" => $block])->save();
                }
            endforeach;
        endif; /* 1.0.7 */

        if (version_compare($context->getVersion(), '1.0.8') < 0) :
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'special_price_text',
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Special Price Text',
                    'input' => 'text',
                    'class' => '',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'position' => 10,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => true,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => ''
                ]
            );
        endif; /* 1.0.8 */

        $setup->endSetup();
    }
}
