<?php

namespace BestWorlds\Capabilia\Plugin\Block\Checkout;

use Magento\Checkout\Block\Checkout\LayoutProcessor as MageLayoutProcessor;

class LayoutProcessor
{
    public function afterProcess(MageLayoutProcessor $subject, $jsLayout)
    {

        $jsLayout['components']['checkout']['children']['steps']['children']
        ['billing-step']['children']['payment']['children']
        ['payments-list']['children']['checkmo-form']['children']
        ['form-fields']['children']['country_id']['sortOrder'] = 81;

        $jsLayout['components']['checkout']['children']['steps']['children']
        ['billing-step']['children']['payment']['children']
        ['payments-list']['children']['tco_api-form']['children']
        ['form-fields']['children']['country_id']['sortOrder'] = 81;

        $jsLayout['components']['checkout']['children']['steps']['children']
        ['billing-step']['children']['payment']['children']
        ['payments-list']['children']['checkmo-form']['children']
        ['form-fields']['children']['region_id']['sortOrder'] = 82;

        $jsLayout['components']['checkout']['children']['steps']['children']
        ['billing-step']['children']['payment']['children']
        ['payments-list']['children']['tco_api-form']['children']
        ['form-fields']['children']['region_id']['sortOrder'] = 82;

        $jsLayout['components']['checkout']['children']['steps']['children']
        ['billing-step']['children']['payment']['children']
        ['payments-list']['children']['checkmo-form']['children']
        ['form-fields']['children']['city']['sortOrder'] = 83;

        $jsLayout['components']['checkout']['children']['steps']['children']
        ['billing-step']['children']['payment']['children']
        ['payments-list']['children']['tco_api-form']['children']
        ['form-fields']['children']['city']['sortOrder'] = 83;

        return $jsLayout;
    }
}