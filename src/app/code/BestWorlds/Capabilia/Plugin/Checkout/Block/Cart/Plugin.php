<?php
namespace BestWorlds\Capabilia\Plugin\Checkout\Block\Cart;

class Plugin
{
    protected $_storeManager;
    protected $categoryRepository;
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository
    ) {
        $this->_storeManager = $storeManager;
        $this->categoryRepository = $categoryRepository;
    }

    public  function getCategoryUrl()
    {
        $category = $this->categoryRepository->get(3, $this->_storeManager->getStore()->getId());
        return $category->getUrl();
    }

    public function afterGetContinueShoppingUrl(\Magento\Checkout\Block\Cart $subject, $result)
    {
        if ($url = $this->getCategoryUrl()) {
            $result = $url;
        }
        return $result;
    }

}