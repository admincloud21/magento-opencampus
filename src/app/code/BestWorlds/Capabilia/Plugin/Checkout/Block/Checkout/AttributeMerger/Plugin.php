<?php
namespace BestWorlds\Capabilia\Plugin\Checkout\Block\Checkout\AttributeMerger;

class Plugin
{
    public function afterMerge(\Magento\Checkout\Block\Checkout\AttributeMerger $subject, $result)
    {
        if (array_key_exists('street', $result)) {
            $result['street']['children'][0]['additionalClasses'] = 'field';
            $result['street']['children'][0]['additionalClasses'] = 'required-label';
            $result['street']['children'][0]['validation']['required-entry'] = true;
            $result['street']['children'][0]['validation']['max_text_length'] = 50;
            $result['street']['children'][0]['label'] = __('Street');
            $result['street']['children'][1]['additionalClasses'] = 'field';
            $result['street']['children'][1]['additionalClasses'] = 'required-label';
            $result['street']['children'][1]['validation']['required-entry'] = true;
            $result['street']['children'][1]['validation']['max_text_length'] = 8;
            $result['street']['children'][1]['label'] = __('Number');
            $result['street']['children'][2]['additionalClasses'] = 'field';
            $result['street']['children'][2]['validation']['required-entry'] = false;
            $result['street']['children'][2]['label'] = __('Floor/Flat');
            $result['street']['children'][3]['additionalClasses'] = 'field';
            $result['street']['children'][3]['additionalClasses'] = 'required-label';
            $result['street']['children'][3]['validation']['required-entry'] = true;
            $result['street']['children'][3]['label'] = __('Neighborhood');
        }

        if (array_key_exists('fax', $result)) {
            $result['fax']['label'] = __('Mobile Number');
        }

        if (array_key_exists('postcode', $result)) {
            $result['postcode']['validation']['max_text_length'] = 10;
        }

        if (array_key_exists('region', $result)) {
            $result['region']['validation']['max_text_length'] = 20;
        }

        if (array_key_exists('city', $result)) {
            $result['city']['validation']['max_text_length'] = 20;
        }

        if (array_key_exists('firstname', $result)) {
          $result['firstname']['validation']['letters-with-basic-punc'] = true;
        }

        if (array_key_exists('lastname', $result)) {
          $result['lastname']['validation']['letters-with-basic-punc'] = true;
        }

        return $result;
    }
}