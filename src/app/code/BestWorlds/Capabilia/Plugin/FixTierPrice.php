<?php
namespace BestWorlds\Capabilia\Plugin;

class FixTierPrice {

    public function afterGetWebsiteId(\Magento\Store\Model\Store $subject, $result) 
    {
        if (is_string($result)) {
            return intval($result);
        }
        
        return $result;
    }
}