<?php
namespace BestWorlds\Capabilia\Plugin;

use Magento\Framework\App\Action\Context;
use Magento\Store\Model\StoreManagerInterface;

class AroundCheckout
{
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $resultRedirectFactory;

    public function __construct(
        Context $context,
        \Magento\Checkout\Model\Session $session,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->session = $session;
        $this->_storeManager = $storeManager;
        $this->messageManager = $context->getMessageManager();
        $this->_response = $context->getResponse();

    }

    public function aroundExecute(\Magento\Checkout\Controller\Index\Index $subject, \Closure $proceed)
    {
        $quote = $this->session->getQuote();

        foreach ($quote->getAllItems() as $itemId => $item) {
            if (!empty($item->getProduct()->getData('impartido_en')) && empty($item->getImpartidoEn())) {
                $this->messageManager->addError(__('Select an idiom'));
                $url = $this->_storeManager->getStore()->getCurrentUrl();
                $url = parse_url($url);
                $path = $url['path'];
                $path = explode('/', $path);
                $availableIdioms = ['es', 'pt'];
                $redirectUrl = '/checkout/cart';
                if (in_array($path[1], $availableIdioms)) {
                    $redirectUrl = '/' . $path[1] . '/checkout/cart';
                }
                $this->_response->setRedirect($redirectUrl);
            }
        }

        return $proceed();

    }
}