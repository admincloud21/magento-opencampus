<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace BestWorlds\Exto\Model\Paypal\Api;

/**
 * Class Recurring
 *
 * @method setToken
 * @method setProfileId
 * @method setAction
 * @method setAmount
 * @method setCurrencyCode
 * @method getAddress
 * @method setAddress
 * @method setBillingAddress
 * @method setSuppressShipping
 * @method getProfileId
 * @method getProfileStatus
 * @method setBillingPeriod
 * @method setBillingFrequency
 * @method setTotalBillingCycles
 * @method setMaxFailedAttempts
 * @method setProfileStartDate
 */
class Recurring extends \Exto\Sarp\Model\Paypal\Api\Recurring
{
    /**
     * @var array
     */
    protected $additionalGlobalMap = [
        'PROFILESTARTDATE' => 'profile_start_date',
        'BILLINGPERIOD' => 'billing_period',
        'BILLINGFREQUENCY' => 'billing_frequency',
        'TOTALBILLINGCYCLES' => 'total_billing_cycles',
        'MAXFAILEDPAYMENTS' => 'max_failed_attempts',
        'PROFILEID' => 'profile_id',
        'PROFILESTATUS' => 'profile_status',
        'DESC' => 'profile_description',
        'INITAMT' => 'initial_amount'
    ];

    /** @var array */
    protected $createRecurringPaymentsProfile = [
        'TOKEN',
        'PROFILESTARTDATE',
        'DESC',
        'BILLINGPERIOD',
        'BILLINGFREQUENCY',
        'TOTALBILLINGCYCLES',
        'AMT',
        'CURRENCYCODE',
        'SHIPPINGAMT',
        'TAXAMT',
        'EMAIL',
        'MAXFAILEDPAYMENTS',
        'INITAMT'
    ];

}