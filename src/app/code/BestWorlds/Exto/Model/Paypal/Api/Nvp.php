<?php

namespace BestWorlds\Exto\Model\Paypal\Api;

use Psr\Log\LoggerInterface;
use Exto\Sarp\Model\QuoteHelper;
use Magento\Checkout\Model\Session;
use Magento\Customer\Helper\Address;
use Magento\Payment\Model\Method\Logger;
use Magento\Directory\Model\RegionFactory;
use Magento\Directory\Model\CountryFactory;
use Exto\Sarp\Model\Quote\VirtualManagement;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Paypal\Model\Api\ProcessableExceptionFactory;
use Magento\Framework\Exception\LocalizedExceptionFactory;

/**
 * Class Nvp
 */
class Nvp extends \Exto\Sarp\Model\Paypal\Api\Nvp
{

    /** @var \Magento\Checkout\Model\Session */
    protected $_checkoutSession;

    /**
     * Nvp constructor.
     * @param Address $customerAddress
     * @param LoggerInterface $logger
     * @param Logger $customLogger
     * @param ResolverInterface $localeResolver
     * @param RegionFactory $regionFactory
     * @param CountryFactory $countryFactory
     * @param ProcessableExceptionFactory $processableExceptionFactory
     * @param LocalizedExceptionFactory $frameworkExceptionFactory
     * @param CurlFactory $curlFactory
     * @param VirtualManagement $virtualManagement
     * @param QuoteHelper $quoteHelper
     * @param Session $_checkoutSession
     * @param array $data
     */
    public function __construct(
        Address $customerAddress,
        LoggerInterface $logger,
        Logger $customLogger,
        ResolverInterface $localeResolver,
        RegionFactory $regionFactory,
        CountryFactory $countryFactory,
        ProcessableExceptionFactory $processableExceptionFactory,
        LocalizedExceptionFactory $frameworkExceptionFactory,
        CurlFactory $curlFactory,
        VirtualManagement $virtualManagement,
        QuoteHelper $quoteHelper,
        Session $_checkoutSession,
        array $data = []
    ) {
        parent::__construct(
            $customerAddress,
            $logger,
            $customLogger,
            $localeResolver,
            $regionFactory,
            $countryFactory,
            $processableExceptionFactory,
            $frameworkExceptionFactory,
            $curlFactory,
            $virtualManagement,
            $quoteHelper,
            $data
        );

        $this->_checkoutSession = $_checkoutSession;
        $this->virtualManagement = $virtualManagement;
        $this->quoteHelper = $quoteHelper;
    }

    /**
     * {@inheritdoc}
     * @return $this
     */
    public function callSetExpressCheckout()
    {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->_checkoutSession->getQuote();
        if (!$this->quoteHelper->isQuoteContainRecurringItems($quote)) {
            \Magento\Paypal\Model\Api\Nvp::callSetExpressCheckout();
            return $this;
        }
        $this->recurringSetExpressCheckoutFlag = true;
        /** Call directly to the PayPal Class Method */
        \Magento\Paypal\Model\Api\Nvp::callSetExpressCheckout();
        $this->recurringSetExpressCheckoutFlag = false;
        return $this;
    }

    /**
     * @param array $request
     * @return $this
     */
    protected function processRequestDataForRecurring(&$request)
    {
        $newRequestData = [];
        $quote = $this->_checkoutSession->getQuote();
        $recurringPaymentId = 0;

        /** @var \Magento\Quote\Model\Quote $quote */
        if ($this->quoteHelper->isQuoteContainRecurringItems($quote)) {
            $request['L_BILLINGTYPE' . $recurringPaymentId] = 'RecurringPayments';
            $quote = $this->_checkoutSession->getQuote();
            $allVisibleItems = $quote->getAllVisibleItems();
            $billingAgreement = $this->quoteHelper::getBillingAgreementForQuote($quote);
            $request['L_BILLINGAGREEMENTDESCRIPTION' . $recurringPaymentId] = $billingAgreement;
            $recurringPaymentId++;
        } else {
            $newRequestData = array_merge(
                $newRequestData,
                $this->convertRequestKeysToRecurring($request, $quote)
            );
        }

        $this->removeNonRecurringKeys($request);
        $request = array_merge($request, $newRequestData);
        return $this;
    }
}
