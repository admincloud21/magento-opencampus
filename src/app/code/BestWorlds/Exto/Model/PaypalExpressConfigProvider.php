<?php
namespace BestWorlds\Exto\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Checkout\Model\Session;
use \Magento\Checkout\Model\Cart;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;

class PaypalExpressConfigProvider implements ConfigProviderInterface
{

    protected $_checkoutSession;
    protected $_cart;
    protected $_priceHelper;


    /**
     * ConfigProvider constructor.
     * @param Session $_checkoutSession
     */
    public function __construct(
        Session $_checkoutSession,
        Cart $_cart,
        PriceHelper $_priceHelper
    )
    {
        $this->_checkoutSession = $_checkoutSession;
        $this->_cart = $_cart;
        $this->_priceHelper = $_priceHelper;
    }

    public function getInstallmentsQty($quote)
    {
        $maxInstallments = [];
        foreach ($quote->getAllVisibleItems() as $item) {
            $maxInstallments[] = max(intval($item->getProduct()->getInstallmentsQty()), 1);
        }
        
        return min($maxInstallments);
    }
    
    public function getTemplateOptionsValuesConfig() 
    {
        $options = [];
        $quote = $this->_cart->getQuote();
        $installmentsQty = $this->getInstallmentsQty($quote);
        for($i = 1; $i <= $installmentsQty; $i++) {
            $installmentAmount = round($quote->getBaseSubtotalWithDiscount() / $i, 2);
            $options[] = [
                'value' => $i, 
                'template_option_text' => __('%1x of %2', $i, $this->_priceHelper->currency($installmentAmount, true, false))
            ];
        }

        return $options;

    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $config = [
            'payment' => [
                'extoRecurrence' => [
                    'template_options_values' => $this->getTemplateOptionsValuesConfig()
                ]
            ],
        ];

        return $config;
    }
}
