<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace BestWorlds\Exto\Model;

use Magento\Quote\Model\Quote as QuoteEntity;
use Magento\Quote\Model\Quote\Item as QuoteItem;
use Exto\Sarp\Model\Product\Type\Subscription as SarpProductType;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class QuoteHelper
 */
class QuoteHelper extends \Exto\Sarp\Model\QuoteHelper
{

    protected $subscriptionTemplateOptions;

     /**
     * @param QuoteEntity $quote
     *
     * @return bool
     */
    public function isQuoteContainRecurringItems($quote)
    {
        if ((int) $quote->getPayment()->getAdditionalInformation('installment_qty') > 1) {
            return true;
        }

        return false;
    }

    public function getSubscriptionTemplateOption($id)
    {
        $templateOptionId = intval($id);

        if (!isset($this->subscriptionTemplateOptions[$templateOptionId])) {
            try {
                $this->subscriptionTemplateOptions[$templateOptionId] = $this->subscriptionTemplateOptionRepository->get($templateOptionId);
            } catch (NoSuchEntityException $e) {
                $this->subscriptionTemplateOptions[$templateOptionId] = null;
            }
        }
        
        return $this->subscriptionTemplateOptions[$templateOptionId];
    }

    public static function getBillingAgreementForQuote($quote)
    {
        $billingFrequency = '1';
        $billingPeriod = 'month';
        $billingCycles = $quote->getPayment()->getAdditionalInformation('installment_qty');

        return __(
            'Bill each %1 %2 for %3 times',
            $billingFrequency,
            $billingPeriod,
            $billingCycles
        )->render();
    }
}

