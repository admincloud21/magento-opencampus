<?php

namespace BestWorlds\Exto\Model\Quote;

use Magento\Quote\Model\Quote as QuoteEntity;

/**
 * Class QuoteManagement
 */
class QuoteManagement extends \Exto\Sarp\Model\Quote\QuoteManagement
{

    /**
     * @param QuoteEntity $quote
     * @param array $orderData
     *
     * @return \Magento\Framework\Model\AbstractExtensibleModel|\Magento\Sales\Api\Data\OrderInterface|null|object
     * @throws \Exception
     */
    public function submit(QuoteEntity $quote, $orderData = [])
    {
        if (!$this->quoteHelper->isQuoteContainRecurringItems($quote)) {
            return \Magento\Quote\Model\QuoteManagement::submit($quote, $orderData);//go to original steps if no sarp products in cart
        }
        
        if (!$quote->getCustomerIsGuest()) {
            if ($quote->getCustomerId()) {
                $this->_prepareCustomerQuote($quote);
            }
            $this->customerManagement->populateCustomerInfo($quote);
        }

        $standardQuote = null;
        $successProfileIds = [];
        $profile = $this->submitProfile($quote);
        $successProfileIds[] = $profile->getId();

        $this->checkoutSession->setLastRecurringProfileIds($successProfileIds);

        return $this->submitQuote($quote, $orderData);//place order after sarp profiles
    }

    /**
     * @param QuoteEntity $quote
     *
     * @return \Exto\Sarp\Api\Data\ProfileInterface
     */
    protected function submitProfile(QuoteEntity $quote)
    {
        $profile = $this->profileService->createProfileFromQuote($quote);
        $profile = $this->profileService->createProfileOnExternalService($profile, $quote);
        $profile = $this->profileService->saveProfile($profile);
        //$this->profileService->sendNewProfileNotification($profile); DO NOT SEND THE SUBSCRIPTION E-MAIL
        return $profile;
    }
}
