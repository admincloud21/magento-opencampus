<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace BestWorlds\Exto\Model;

use Magento\Quote\Model\Quote as QuoteEntity;
use Exto\Sarp\Api\Data\ProfileInterface as ProfileInterface;
use Magento\Paypal\Model\Express\Checkout;
use Exto\Sarp\Model\Source\Profile\Status;

/**
 * Class ProfileService
 */
class ProfileService extends \Exto\Sarp\Model\ProfileService
{

    /**
     * @param QuoteEntity $quote
     *
     * @return ProfileInterface
     */
    public function createProfileFromQuote(QuoteEntity $quote)
    {
        $billingPeriod = 'month';
        $startDate = $this->convertStartDate(null);
        $installmentQty = $quote->getPayment()->getAdditionalInformation('installment_qty');
        $billingTitle = $this->quoteHelper::getBillingAgreementForQuote($quote);
        /** @var ProfileInterface $profile */
        $profile = $this->profileFactory->create();
        $profile
            ->setTitle($billingTitle)
            ->setCustomerId($quote->getCustomerId())
            ->setCustomerName($quote->getCustomerFirstname() . ' ' . $quote->getCustomerLastname())
            ->setCustomerEmail($quote->getCustomerEmail()) 
            ->setBillingStartDate($startDate)
            ->setBillingFrequency('1')
            ->setBillingPeriod($billingPeriod)
            ->setBillingCycles($installmentQty - 1) // There is a initial amount
            ->setMaxPaymentFailures('3')
            ->setDefaultOrderStatus('complete')
        ;
        $quoteData = $this->cleanupArray($quote->getData());
        $billingAddressData = $this->cleanupArray($quote->getBillingAddress()->getData());
        $shippingAddressData = $this->cleanupArray($quote->getShippingAddress()->getData());
        $profile
            ->setQuoteData($quoteData)
            ->setBillingAddressData($billingAddressData)
            ->setShippingAddressData($shippingAddressData)
        ;
        $quoteItemsData = [];
        foreach ($quote->getItemsCollection() as $item) {
            /** @var QuoteItem $item */
            $itemData = $item->getData();
            $buyRequest = $item->getOptionByCode('info_buyRequest');
            if (null !== $buyRequest) {
                $itemData['info_buyRequest'] = $buyRequest->getValue();
            }
            $quoteItemsData[] = $itemData;
        }
        $quoteItemsData = $this->cleanupArray($quoteItemsData);
        $profile->setQuoteItemsData($quoteItemsData);
        return $profile;
    }

     /**
     * @param ProfileInterface $profile
     * @param QuoteEntity $quote
     *
     * @return ProfileInterface
     */
    public function createProfileOnExternalService(ProfileInterface $profile, QuoteEntity $quote)
    {
        $payment = $quote->getPayment();
        $token = $payment->getAdditionalInformation(
            Checkout::PAYMENT_INFO_TRANSPORT_TOKEN
        );
        /** @var \Magento\Paypal\Model\Cart $paypalCart */
        $paypalCart = $this->paypalCartFactory->create(['salesModel' => $quote]);
        $monthlyAmount = round($quote->getBaseSubtotalWithDiscount() / $payment->getAdditionalInformation('installment_qty'), 2);

        /** @var \Exto\Sarp\Model\Paypal\Api\Recurring $api */
        $api = $this->paypalRecurringApiFactory->create();
        $api->setToken(
            $token
        )->setInitialAmount(
            $monthlyAmount
        )
        ->setAmount(
            $monthlyAmount
        )->setCurrencyCode(
            $quote->getCurrency()->getBaseCurrencyCode()
        )->setPaypalCart(
            $paypalCart
        )->setProfileDescription(
            $this->quoteHelper::getBillingAgreementForQuote($quote)
        );
        $this->exportDataFromProfileToApi($profile, $api);

        if ($quote->getIsVirtual()) {
            $api->setAddress($quote->getBillingAddress())->setSuppressShipping(true);
        } else {
            $api->setAddress($quote->getShippingAddress());
            $api->setBillingAddress($quote->getBillingAddress());
        }

        $api->callCreateRecurringPaymentsProfile();
        $profile->setExternalServiceProfileId($api->getProfileId());
        $profile->setStatus(Status::PENDING_VALUE);
        return $profile;
    }

    /**
     * @param \DateTime|null $startDate
     *
     * @return string
     */
    protected function convertStartDate($startDate)
    {
        if (null === $startDate) {
            $startDate = new \DateTime('now');
            $startDate->modify('+1 month');
        }
        return $startDate->format('Y-m-d G:i:s');
    }

    /**
     * @param array $array
     * @return array
     */
    protected function cleanupArray($array)
    {
        if (!$array) {
            return [];
        }
        foreach ($array as $key => $value) {
            if (is_object($value)) {
                unset($array[$key]);
            } elseif (is_array($value)) {
                $array[$key] = $this->cleanupArray($array[$key]);
            }
        }
        return $array;
    }
}