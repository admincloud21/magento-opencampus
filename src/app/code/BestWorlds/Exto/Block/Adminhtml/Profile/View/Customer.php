<?php

namespace BestWorlds\Exto\Block\Adminhtml\Profile\View;

/**
 * Class Customer
 */
class Customer extends \Exto\Sarp\Block\Adminhtml\Profile\View\Customer
{

    /**
     * @return string
     */
    public function getShippingAddress()
    {
        $data = $this->getProfile()->getShippingAddressData();
        if (!$data['address_id']) {
            return __("No shipping address");
        }

        return parent::getShippingAddress();
    }

    /**
     * Retrieve module name of block
     *
     * @return string
     */
    public function getModuleName()
    {
        if (!$this->_getData('module_name')) {
            $this->setData('module_name', 'Exto_Sarp');
        }
        return $this->_getData('module_name');
    }
}
