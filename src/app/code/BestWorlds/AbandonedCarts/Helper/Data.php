<?php

namespace BestWorlds\AbandonedCarts\Helper;

use Magento\Catalog\Model\ProductRepository;
use Magento\Directory\Model\Currency;
use Magento\Framework\App\Helper\AbstractHelper;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\RequestInterface;

class Data extends AbstractHelper
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Currency
     */
    private $currency;

    /**
     * @var RequestInterface
     */
    private $requestInterface;

    /**
     * @param ProductRepository $productRepository
     * @param Currency $currency
     * @param LoggerInterface $logger
     * @param RequestInterface $requestInterface
     */
    public function __construct(
        ProductRepository $productRepository,  
        Currency $currency,
        LoggerInterface $logger,
        RequestInterface $requestInterface
    ) {        
        $this->productRepository = $productRepository;
        $this->logger = $logger;
        $this->currency = $currency;
        $this->requestInterface = $requestInterface;
    }

    public function sendData($abandonedCart)
    {
        $this->logger->info("====Quote sent to hubspot====");
        $this->logger->info(print_r($abandonedCart->getData(),true));

        $lifeCicleStage = $this->scopeConfig->getValue('capabilia/hubspot/lifeciclestage');
        $origenLead = $this->scopeConfig->getValue('capabilia/hubspot/originlead');
        $instancia =  $this->scopeConfig->getValue('capabilia/hubspot/instance');
        $testMode = $this->scopeConfig->getValue('capabilia/hubspot/testmode');
        $testEmail = $this->scopeConfig->getValue('capabilia/hubspot/testmail');

        $skus = explode(',', $abandonedCart->getData('prod_skus'));
        $data = [
            'properties' => [],
            [
                'property' => 'lifecyclestage',
                'value' => $lifeCicleStage
            ],
            [
                'property' => 'utm_origen',
                'value' => $origenLead
            ],
            [
                'property' => 'origen',
                'value' => 'Web'
            ],
            [
                'property' => 'instancia',
                'value' => $instancia
            ]
        ];

        if ($testMode) {
            if($this->requestInterface->getParam('debug')){
                echo '<pre>';
                var_export($abandonedCart->getData());
                echo '======================';
            }
            $email = $testEmail;
        } else {
            $email = $abandonedCart->getData('customer_email');
        }

        //Create/Update contact
        $hubspotResult = $this->hubspotConnect('contacts/v1/contact/createOrUpdate/email/' . $email . '/', 'POST', $data);
        if($skus){
           $hubspotIdsString = $this->getHubspotProductIds($skus);
        }
        $abandonedCart->setHubspotsIdString($hubspotIdsString);
        
        //New Deal
        $newdeal = $this->createDeal($hubspotResult, $abandonedCart);
        if ($newdeal['dealId']) {

            foreach ($skus as $s) {
                $product = $this->productRepository->get($s);
                if ($product->getId() && $product->getData('hubspot_id')) {
                    $line_item = [
                        [
                            'name' => 'hs_product_id',
                            'value' => $product->getData('hubspot_id')
                        ]
                    ];
                    $new_line_item = $this->hubspotConnect('crm-objects/v1/objects/line_items', 'POST', $line_item);
                    $this->logger->info(print_r($line_item,true));
                    $asociationDealProductData = [
                        "fromObjectId" => $newdeal['dealId'],
                        "toObjectId" => $new_line_item['objectId'],
                        "category" => "HUBSPOT_DEFINED",
                        "definitionId" => 19
                    ];
                    
                    //Asociate products
                    $asociatedProduct = $this->hubspotConnect('crm-associations/v1/associations', 'PUT', $asociationDealProductData);

                }
            }
        }
        $this->logger->info("====END Quote sent to hubspot====");
    }

    public function createDeal($hubspotResult, $abandoned)
    {
        $quoteSubTotal =  $this->currency->format($abandoned->getData('grand_total'), ['display'=>\Zend_Currency::NO_SYMBOL],false);
        $dealstage = $this->scopeConfig->getValue('capabilia/hubspot/dealstage');
        
        $deal = [
            'associations' => [
                'associatedVids' => [
                    $hubspotResult["vid"]
                ]
            ],
            'properties' => [
                [
                    'value' => 'Magento Abandoned Cart: '.$abandoned->getData('customer_firstname').' '. $abandoned->getData('customer_lastname'),
                    'name' => 'dealname'
                ],
                [
                    'value' => $dealstage,
                    'name' => 'dealstage'
                ],
                [
                    'value' => 'Comment',
                    'name' => 'description'
                ],
                [
                    'value' => 'FCB',
                    'name' => 'instancia'
                ],
                [
                    'value' => 'si',
                    'name' => 'carrito_abandonado'
                ],
                [
                    'value' => $abandoned->getHubspotsIdString(),
                    'name' => 'codigo_del_producto'
                ],
                [
                    'value' => $quoteSubTotal,
                    'name' => 'monto_total_carrito_abandonado'
                ]
            ],
        ];
        $newdeal = $this->hubspotConnect('deals/v1/deal', 'POST', $deal);
        return $newdeal;
    }

    function hubspotConnect($url, $method, $body)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, "https://api.hubapi.com/" . $url . "?hapikey=" . $this->scopeConfig->getValue('capabilia/hubspot/api_key'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        
        if ($method == 'GET') {
            
        } else if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        } else if ($method == 'DELETE') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        } else if ($method == 'PATCH') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        } else if ($method == 'PUT') {
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        }

        $returndata = curl_exec($ch);
        curl_close($ch);
        $returndata = json_decode($returndata, true);
        return $returndata;
    }

    protected function getHubspotProductIds($skus){
        $hubspotIds = [];
        $hubspotIdsString = '';
        foreach ($skus as $s) {
            $product = $this->productRepository->get($s);
            if($product->getId() && $product->getData('hubspot_id')) {
                $hubspotIds[] = $product->getData('hubspot_id');
            }
        }
        if(count($hubspotIds)>=1){
            $hubspotIdsString = implode(",", $hubspotIds);
        }
         return $hubspotIdsString;
    }
}