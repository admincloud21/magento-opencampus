<?php
namespace BestWorlds\AbandonedCarts\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $date)
    {
        $this->_pageFactory = $pageFactory;
        $this->_date = $date;
        return parent::__construct($context);
    }

    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        if($customerSession->isLoggedIn()) {
            echo  $todayDateTime = $this->_date->date()->format('Y-m-d H:i:s');
            $hubspot = $objectManager->create('BestWorlds\AbandonedCarts\Cron\Hubspot');
            $hubspot->execute();
            exit;
        }

        exit;
    }
}