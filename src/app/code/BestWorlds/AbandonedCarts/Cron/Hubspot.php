<?php

namespace BestWorlds\AbandonedCarts\Cron;

use BestWorlds\AbandonedCarts\Helper\Data as HubspotData;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Reports\Model\ResourceModel\Quote\CollectionFactory;
use Psr\Log\LoggerInterface;

class Hubspot
{
    /**
     * @var CollectionFactory
     */
    private $quotesFactory;

    /**
     * @var TimezoneInterface
     */
    private $date;

    /**
     * @var HubspotData
     */
    private $hubspotHelper;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param Context $context
     * @param Data $backendHelper
     * @param CollectionFactory $quotesFactory
     * @param TimezoneInterface $date
     * @param HubspotData $hubspotHelper
     * @param ScopeConfigInterface $scopeConfig
     * @param LoggerInterface $logger
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        CollectionFactory $quotesFactory,
        TimezoneInterface $date,
        HubspotData $hubspotHelper,
        ScopeConfigInterface $scopeConfig,
        LoggerInterface $logger,
        array $data = []
    ) {
        $this->quotesFactory = $quotesFactory;
        $this->date = $date;
        $this->hubspotHelper = $hubspotHelper;
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
    }

    public function execute()
    {
        $this->logger->info("====Hubspot cron started====");
        $limit = $this->scopeConfig->getValue('capabilia/hubspot/testlimit');
        $collection = $this->quotesFactory->create();
        $joinConditions = 'main_table.entity_id = quote_item.quote_id';
        $collection->getSelect()->join(['quote_item'],
            $joinConditions,
            [])->group('main_table.entity_id');
        $today = $this->date->date()->format('Y-m-d');

        // $todayDateTime = $this->date->date()->modify('-1 hour')->format('Y-m-d H:i:s');
        //$collection->getSelect()->where("main_table.created_at between '".$today." 00:00:00' AND '".$today." 23:59:59'" );
        //$collection->getSelect()->where("main_table.updated_at < '".$todayDateTime."' and main_table.updated_at between '".$today." 00:00:00' AND '".$today." 23:59:59'");
        //$today = '2020-06-23';

        $collection->getSelect()->where("main_table.updated_at between '".$today." 00:00:00' AND '".$today." 23:59:59'");

        $collection->getSelect()->columns('group_concat(sku) as prod_skus');
        $collection->getSelect()->having('main_table.is_active = 1 and customer_email is not  null');
        if ($limit) {
            $collection->getSelect()->limit($limit);
        }
        $query =  (string)$collection->getSelect();
        $this->logger->info($query);

        foreach ($collection as $abandoned){
            if($abandoned->getData('customer_email')){
                $this->hubspotHelper->sendData($abandoned);
            }
        }

        $this->logger->info("====Hubspot cron end====");

        return $this;
    }
}