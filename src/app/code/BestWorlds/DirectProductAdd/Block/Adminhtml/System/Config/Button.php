<?php

namespace BestWorlds\DirectProductAdd\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Button extends Field
{

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $_urlInterface;

    public function __construct(
        Context $context,
        \Magento\Framework\UrlInterface $urlInterface,
        array $data = []
    )
    {
        $this->_urlInterface = $urlInterface;
        parent::__construct($context, $data);
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $url = $this->_urlInterface->getUrl('bwcouponhash');
        $buttonHtml = '<input id="generatehash" type="button" value="' . __('Get Coupon Hash') . '" >
                  <script> require([\'jquery\'], function($){
                  $("#generatehash").on(\'click\', function () {
                        $.ajax({
                        url: \'' . $url . '\',
                        type: \'POST\',
                           data: {coupon: $(\'#sukhvirdirectproductadd_general_bwcoupon\').val()},
                            success: function (response) {
                               $(\'#sukhvirdirectproductadd_general_bwcoupon\').val(response.coupon)
                            }
                           });
                        });
                  });
                  </script>';

        return $buttonHtml;
    }

}