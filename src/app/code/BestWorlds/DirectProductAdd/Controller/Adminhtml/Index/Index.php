<?php
namespace BestWorlds\DirectProductAdd\Controller\Adminhtml\Index;

use Magento\Framework\App\Request\Http;
use Magento\Framework\Encryption\EncryptorInterface;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\App\Request\Http\Request
     */
    private $request;

    /**
     * @var Magento\Framework\Encryption\EncryptorInterface
     */
    private $encryptor;


    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $jsonResultFactory;


    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory,
        EncryptorInterface $encryptor,
        Http $request
    ) {
        parent::__construct($context);
        $this->encryptor = $encryptor;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->request = $request;
    }


    public function execute()
    {
        $coupon = $this->request->getParam('coupon');
        if($coupon !==''){
        $data['coupon'] = urlencode($this->encryptor->encrypt($coupon));

        } else{
            $data['coupon'] = '';
        }
        $result = $this->jsonResultFactory->create();
        $result->setData($data);
        return $result;
    }

    /**
     * Check Permission.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}