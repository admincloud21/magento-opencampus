<?php

namespace BestWorlds\DirectProductAdd\Plugin;


use Magento\Framework\App\Action\Context;
use \Magento\Framework\App\Request\Http;
use \Magento\Catalog\Model\ProductRepository;
use \Magento\Checkout\Model\Cart;
use \Magento\Framework\Data\Form\FormKey;
use Magento\Framework\Encryption\EncryptorInterface;


class Redirect
{

    /**
     * @var \Magento\Framework\App\Request\Http\Request
     */
    protected $_request;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */

    protected $_productRepository;

    /**
     * @var \Magento\Checkout\Helper\Cart
     */

    protected $_cart;

    /**
     * @var \Magento\Framework\Data\Form\FormKey
     */
    protected $formKey;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    public function __construct(
        Context $context,
        Http $request,
        ProductRepository $productRepository,
        Cart $cart,
        FormKey $formKey,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Checkout\Helper\Cart $cartHelper,
        EncryptorInterface $encryptor,
        \Magento\SalesRule\Model\ResourceModel\Coupon\CollectionFactory $salesRuleCoupon
    )
    {
        $this->_request = $request;
        $this->_productRepository = $productRepository;
        $this->_cart = $cart;
        $this->formKey = $formKey;
        $this->_scopeConfig = $scopeConfig;
        $this->_response = $context->getResponse();
        $this->_urlInterface = $urlInterface;
        $this->_cartHelper = $cartHelper;
        $this->encryptor = $encryptor;
        $this->salesRuleCoupon = $salesRuleCoupon;
    }


    public function aroundExecute(\Sukhvir\DirectProductAdd\Controller\Add\ToCart $subject, \Closure $proceed)
    {

        $product_id = $this->_request->getParam('id');
        $coupon = urldecode($this->encryptor->decrypt($this->_request->getParam('couponcode')));
        try {
            $this->addToCart($product_id, $coupon);
        } catch (\Exception $e) {
            $this->_response->setRedirect("/");
        }


    }


    public function addToCart($product_id, $coupon)
    {
        if (strpos($product_id, ",") !== FALSE) {
            $carr = explode(",", $product_id);
            if (is_array($carr)) {
                foreach ($carr as $p) {
                    $p = trim($p);
                    $q = 1;
                    $this->toCart($p, $q);
                }
                if ($coupon !== "") {
                    $this->addCoupon($coupon);
                }
            }
        } else {
            $p = trim($product_id);
            $q = 1;
            $this->toCart($p, $q);
            if ($coupon !== "") {
                $this->addCoupon($coupon);
            }
        }
        $url = $this->_urlInterface->getUrl('');
        $redirect = $this->_scopeConfig->getValue('sukhvirdirectproductadd/general/redirects', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if ($redirect == 'checkout/') {
            $url = $this->_urlInterface->getUrl('checkout');
        }

        if ($redirect == 'checkout/cart/') {
            $url = $this->_cartHelper->getCartUrl();
        }
        $this->_response->setRedirect($url);
    }

    private function toCart($p, $q)
    {
        if (strpos($p, "-") !== FALSE) {
            $aq = explode("-", $p);
            $p = $aq[0];
            $q = (int)$aq[1];
        }
        $params = array(
            'product' => $p,
            'qty' => $q
        );
        $_product = $this->_productRepository->getById($p);
        $this->_cart->addProduct($_product, $params);

        $this->_cart->save();
    }

    private function addCoupon($coupon)
    {
        $collection = $this->salesRuleCoupon->create();
        $collection->addFieldToFilter('code', $coupon);

        if ($collection->count() == 1) {
            $this->_cart->getQuote()->setTotalsCollectedFlag(false);
            $this->_cart->getQuote()->setCouponCode($coupon)
                ->collectTotals()
                ->save();
            $this->_cart->save();
        }
    }
}