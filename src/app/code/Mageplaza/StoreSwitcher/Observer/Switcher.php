<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_StoreSwitcher
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\StoreSwitcher\Observer;

use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Store\Api\StoreCookieManagerInterface;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Store\App\Response\Redirect;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreIsInactiveException;
use Magento\Store\Model\StoreManagerInterface;
use Mageplaza\StoreSwitcher\Block\Switcher as BlockSwitcher;
use Mageplaza\StoreSwitcher\Helper\Data as HelperData;
use Mageplaza\StoreSwitcher\Model\Config\Source\ActionType;
use Mageplaza\StoreSwitcher\Model\Config\Source\ChangeType;
use Mageplaza\StoreSwitcher\Model\RuleFactory;

/**
 * Class Switcher
 * @package Mageplaza\StoreSwitcher\Observer
 */
class Switcher implements ObserverInterface
{
    /**
     * @var \Mageplaza\StoreSwitcher\Model\RuleFactory
     */
    protected $_ruleFactory;

    /**
     * @var \Mageplaza\StoreSwitcher\Helper\Data
     */
    protected $_helperData;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManagerInterface;

    /**
     * @var \Magento\Store\App\Response\Redirect
     */
    protected $_redirect;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magento\Store\Api\StoreCookieManagerInterface
     */
    protected $storeCookieManager;

    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $httpContext;

    /**
     * @var \Magento\Store\Api\StoreRepositoryInterface
     */
    protected $storeRepository;

    /**
     * @var \Mageplaza\StoreSwitcher\Block\Switcher
     */
    protected $_blockSwitcher;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    protected $cookieMetadataFactory;

    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    protected $cookieManager;

    /**
     * Switcher constructor.
     *
     * @param \Magento\Store\Model\StoreManagerInterface $storeManagerInterface
     * @param \Mageplaza\StoreSwitcher\Model\RuleFactory $ruleFactory
     * @param \Mageplaza\StoreSwitcher\Helper\Data $helperData
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Store\Api\StoreCookieManagerInterface $storeCookieManager
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param \Magento\Store\Api\StoreRepositoryInterface $storeRepository
     * @param \Magento\Store\App\Response\Redirect $redirect
     * @param \Mageplaza\StoreSwitcher\Block\Switcher $blockSwitcher
     * @param \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager
     * @param \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory
     */
    public function __construct(
        StoreManagerInterface $storeManagerInterface,
        RuleFactory $ruleFactory,
        HelperData $helperData,
        RequestInterface $request,
        StoreCookieManagerInterface $storeCookieManager,
        HttpContext $httpContext,
        StoreRepositoryInterface $storeRepository,
        Redirect $redirect,
        BlockSwitcher $blockSwitcher,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory
    )
    {
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_ruleFactory = $ruleFactory;
        $this->_helperData = $helperData;
        $this->_request = $request;
        $this->storeCookieManager = $storeCookieManager;
        $this->httpContext = $httpContext;
        $this->storeRepository = $storeRepository;
        $this->_redirect = $redirect;
        $this->_blockSwitcher = $blockSwitcher;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->cookieManager = $cookieManager;
    }

    /**
     * @param Observer $observer
     *
     * @throws NoSuchEntityException
     * @throws StoreIsInactiveException
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     */
    public function execute(Observer $observer)
    {
        if (!$this->_helperData->isEnabled()) {
            return;
        }


        $rule = $this->_helperData->getMatchingRule();
        if (!$rule) {
            return;
        }


        $this->processSaveStore($observer);
        $changeStore = $this->_helperData->getCookieByName('change_store') ? $this->_helperData->getCookieByName('change_store') : 0;
        $storeSwitcherCode = $this->_storeManagerInterface->getStore($rule->getStoreRedirected())->getCode();

        if ($rule->getRedirectType() == ActionType::REDIRECT_STORE_CURRENCY) {
            if ($rule->getChangeType() == ChangeType::MANUALLY) {
                $this->_helperData->deleteCookieByName('change_store');
            } else if ($rule->getChangeType() == ChangeType::AUTOMATIC && $changeStore == 0) {
                $this->_helperData->setCookie('change_store', 1);
                $this->_helperData->setCookie('mpstoreswitcher_laststore_' . $this->_helperData->getHttpHost(), $storeSwitcherCode);
                $this->_helperData->deleteCookieByName('is_switcher');
                $this->setStore($observer, $storeSwitcherCode);
            }
        } else if ($rule->getRedirectUrl() && $rule->getRedirectType() == ActionType::REDIRECT_URL) {
            $url = $rule->getRedirectUrl();
            $currentUrl = $this->_storeManagerInterface->getStore()->getCurrentUrl(false);
            if ($url != strtok($currentUrl, "?") && $url != $currentUrl) {
                $observer->getResponse()->setRedirect($url);
            }
        }
    }

    /**
     * @param $observer
     *
     * @throws NoSuchEntityException
     * @throws StoreIsInactiveException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     */
    public function processSaveStore($observer)
    {
        if (!$this->_helperData->getSaveSwitchedStoreConfig()) {
            return;
        }

        $lastStoreCode = $this->_helperData->getCookieByName('mpstoreswitcher_laststore_' . $this->_helperData->getHttpHost());
        $storeSwitched = $this->_helperData->getCookieByName('mpstoreswitcher_store_switched');
        $currentStoreCode = $this->_storeManagerInterface->getStore()->getCode();

        if ($lastStoreCode && in_array($lastStoreCode, $this->getListStoreCode())) {
            if ($storeSwitched) {
                $this->saveStore($storeSwitched);
            } else if ($currentStoreCode != $lastStoreCode) {
                $this->setStore($observer, $lastStoreCode);
            }
        }
    }

    /**
     * Get All Store Code
     *
     * @return array
     */
    public function getListStoreCode()
    {
        $list = [];
        $stores = $this->_storeManagerInterface->getStores();

        foreach ($stores as $store) {
            $list[] = $store->getCode();
        }

        return $list;
    }

    /**
     * @param $observer
     * @param $storeCode
     *
     * @throws NoSuchEntityException
     * @throws StoreIsInactiveException
     */
    public function setStore($observer, $storeCode)
    {
        $currentActiveStore = $this->_storeManagerInterface->getStore();
        $store = $this->storeRepository->getActiveStoreByCode($storeCode);

        $defaultStoreView = $this->_storeManagerInterface->getDefaultStoreView();

        if ($defaultStoreView->getId() == $store->getId()) {
            $this->storeCookieManager->deleteStoreCookie($store);
        } else {
            $this->httpContext->setValue(Store::ENTITY, $store->getCode(), $defaultStoreView->getCode());
            $this->storeCookieManager->setStoreCookie($store);
        }

        if ($store->isUseStoreInUrl()) {
            $url = rtrim($currentActiveStore->getBaseUrl(), '/') . $this->_request->getRequestString();
            $observer->getResponse()->setRedirect(
                str_replace(
                    $currentActiveStore->getBaseUrl(),
                    $store->getBaseUrl(),
                    $url
                )
            );
        } else {
            $observer->getResponse()->setRedirect($this->_redirect->getRedirectUrl());
        }
    }

    /**
     * @param $storeCode
     *
     * @throws NoSuchEntityException
     * @throws StoreIsInactiveException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     */
    public function saveStore($storeCode)
    {
        $store = $this->storeRepository->getActiveStoreByCode($storeCode);

        $cookieMetadata = $this->cookieMetadataFactory->createPublicCookieMetadata()
            ->setHttpOnly(true)
            ->setPath($store->getStorePath());

        $this->cookieManager->setPublicCookie('store', $store->getCode(), $cookieMetadata);
    }
}
