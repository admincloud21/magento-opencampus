<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_StoreSwitcher
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\StoreSwitcher\Block;

use Magento\Framework\View\Element\Template;
use Mageplaza\StoreSwitcher\Helper\Data as HelperData;
use Mageplaza\StoreSwitcher\Model\Config\Source\ActionType;
use Mageplaza\StoreSwitcher\Model\Config\Source\ChangeType;

/**
 * Class Switcher
 * @package Mageplaza\StoreSwitcher\Block
 */
class Switcher extends Template
{
    /**
     * @var \Mageplaza\StoreSwitcher\Helper\Data
     */
    protected $_helperData;

    /**
     * Switcher constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Mageplaza\StoreSwitcher\Helper\Data $helperData
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        HelperData $helperData,
        array $data = []
    )
    {
        $this->_helperData = $helperData;

        parent::__construct($context, $data);
    }

    /**
     * Get Country name of store switched
     *
     * @return string
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getSwitcherCountryLabel()
    {
        $rule = $this->_helperData->getMatchingRule();
        $switcherCountry = $this->_storeManager->getStore($rule->getStoreRedirected())->getName();

        return $switcherCountry;
    }

    /**
     * get label Redirect popup
     *
     * @return \Magento\Framework\Phrase
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getPopupSwitcherLabel()
    {
        $currentCountry = $this->_storeManager->getStore()->getName();

        return __('You are in %1, would you like to switch to the %2?', $currentCountry, $this->getSwitcherCountryLabel());
    }

    /**
     * Get Allow visitors save switched store view config
     *
     * @return mixed
     */
    public function getSaveSwitchedStoreConfig()
    {
        return $this->_helperData->getSaveSwitchedStoreConfig();
    }

    /**
     * Check to show redirect popup
     *
     * @return bool
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function checkRedirectPopup()
    {
        $rule = $this->_helperData->getMatchingRule();

        return ($rule &&
            $rule->getRedirectType() == ActionType::REDIRECT_STORE_CURRENCY &&
            $rule->getChangeType() == ChangeType::MANUALLY &&
            $rule->getStoreRedirected() &&
            $rule->getStoreRedirected() != $this->_storeManager->getStore()->getId()
        );
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoreSwitcherCode()
    {
        $rule = $this->_helperData->getMatchingRule();

        return $this->_storeManager->getStore($rule->getStoreRedirected())->getCode();
    }

    /**
     * Get data save popup
     *
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getDataSave()
    {
        $data = [
            'lastStore' => $this->_storeManager->getStore()->getCode(),
            'baseUrl'   => $this->_helperData->getHttpHost()
        ];

        return HelperData::jsonEncode($data);
    }
}