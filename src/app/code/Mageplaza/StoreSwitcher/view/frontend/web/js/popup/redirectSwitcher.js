/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_StoreSwitcher
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'jquery',
    'jquery/ui',
    'jquery/jquery.cookie'
], function ($) {
    'use strict';

    $.widget('mageplaza.storeSwitcher', {

        /**
         * @inheritDoc
         */
        _create: function () {
            this._redirectPopup();
        },

        _redirectPopup: function () {
            var _this = this,
                date = new Date(),
                isSwitcher = $.cookie('is_switcher'),
                mpStoreSwitcherPopup = $('#mpstoreswitcher-redirect-popup'),
                storeCode = mpStoreSwitcherPopup.attr('data-store-code');

            date.setTime(date.getTime() + (60 * 60 * 24 * 1000));

            if (isSwitcher) {
                mpStoreSwitcherPopup.hide();
            } else {
                mpStoreSwitcherPopup.show();
            }

            $('.mp-redirect-yes').on('click', function () {
                mpStoreSwitcherPopup.hide();
                $.cookie('mpstoreswitcher_laststore_' + _this.options.baseUrl, storeCode, {expires: date});
                $.cookie('is_switcher', 1, {expires: date});
                location.reload();
            });

            $('.mp-redirect-no').on('click', function () {
                mpStoreSwitcherPopup.hide();
                $.cookie('is_switcher', 0, {expires: date});
            });
        }
    });

    return $.mageplaza.storeSwitcher;
});

