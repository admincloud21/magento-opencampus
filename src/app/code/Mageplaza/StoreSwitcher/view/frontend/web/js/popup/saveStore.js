/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_StoreSwitcher
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'jquery',
    'jquery/jquery.cookie'
], function ($) {
    'use strict';

    $.widget('mageplaza.storeSwitcher', {
        /**
         * @inheritDoc
         */
        _create: function () {
            this._savePopup();
        },

        _savePopup: function () {
            var _this = this,
                date = new Date(),
                popup = $('#mpstoreswitcher-save-store-popup'),
                mainContent = $('#maincontent');

            if (mainContent.length) {
                popup.appendTo(mainContent);
            }

            date.setTime(date.getTime() + (60 * 60 * 1000 * 24));

            $('.switcher-language .switcher-option a, .switcher-store .switcher-option a').each(function () {
                var el = $(this);
                el.on('click', function (e) {
                    var dataPost = JSON.parse(el.attr('data-post')),
                        url = dataPost.action,
                        data = dataPost.data,
                        storeCode = data["___store"],
                        params = '',
                        storeUrl = '';

                    if (url.indexOf('___from_store') === -1) {
                        $.each(data, function (key, value) {
                            params += key + '=' + value + '&';
                        });
                        storeUrl = url + '?' + params.substring(0, params.length - 1);
                    } else {
                        storeUrl = url;
                    }

                    popup.show();
                    e.stopPropagation();

                    $('.mp-save-store-yes').on('click', function () {
                        $.cookie('mpstoreswitcher_laststore_' + _this.options.baseUrl, storeCode, {expires: date});
                        $.cookie('mpstoreswitcher_store_switched', null);
                        window.location = storeUrl;
                    });

                    $('.mp-save-store-no').on('click', function () {
                        var laststore = $.cookie('mpstoreswitcher_laststore_' + _this.options.baseUrl);

                        if (!laststore) {
                            $.cookie('mpstoreswitcher_laststore_' + _this.options.baseUrl, _this.options.lastStore, {expires: date});
                        }

                        $.cookie('mpstoreswitcher_store_switched', storeCode);
                        window.location = storeUrl;
                    })
                });
            });

            $('.mpstoreswitcher-btn-close-save-popup').on('click', function () {
                popup.hide();
            })
        }
    });

    return $.mageplaza.storeSwitcher;
});

