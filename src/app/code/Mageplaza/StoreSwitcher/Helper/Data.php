<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_StoreSwitcher
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\StoreSwitcher\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Mageplaza\Core\Helper\AbstractData;
use Mageplaza\GeoIP\Helper\Address as GeoIpAddress;
use Mageplaza\StoreSwitcher\Model\Config\Source\PageType;
use Mageplaza\StoreSwitcher\Model\RuleFactory;

/**
 * Class Data
 * @package Mageplaza\StoreSwitcher\Helper
 */
class Data extends AbstractData
{
    const CONFIG_MODULE_PATH = 'mpstoreswitcher';

    /**
     * @var \Mageplaza\StoreSwitcher\Model\RuleFactory
     */
    protected $_ruleFactory;

    /**
     * @var \Mageplaza\GeoIP\Helper\Address
     */
    protected $_geoIp;

    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    protected $_cookieManager;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    protected $_cookieMetadataFactory;

    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    protected $_sessionManager;

    /**
     * Data constructor.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Mageplaza\GeoIP\Helper\Address $geoIp
     * @param \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager
     * @param \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory
     * @param \Magento\Framework\Session\SessionManagerInterface $sessionManager
     * @param \Mageplaza\StoreSwitcher\Model\RuleFactory $ruleFactory
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,
        GeoIpAddress $geoIp,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        SessionManagerInterface $sessionManager,
        RuleFactory $ruleFactory
    )
    {
        $this->_geoIp = $geoIp;
        $this->_cookieManager = $cookieManager;
        $this->_cookieMetadataFactory = $cookieMetadataFactory;
        $this->_sessionManager = $sessionManager;
        $this->_ruleFactory = $ruleFactory;

        parent::__construct($context, $objectManager, $storeManager);
    }

    /**
     * @param $rule
     *
     * @return bool
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function checkCountry($rule)
    {
        $geoIpData = $this->_geoIp->getGeoIpData();
        if ($geoIpData) {
            $currentCountry = $geoIpData['country_id'];
            $countries = explode(',', $rule->getCountries());

            return in_array($currentCountry, $countries);
        }

        return false;
    }

    /**
     * @param $rule
     *
     * @return bool
     */
    public function checkPageType($rule)
    {
        $type = $rule->getPageType();
        $actionName = $this->_request->getFullActionName();

        if ($type == PageType::HOME_PAGE) {
            if ($actionName === 'cms_index_index') {
                return true;
            }

            return false;
        } else if ($type == PageType::SPECIFIC_PAGES) {
            $includePaths = $rule->getIncludePath();
            $excludePaths = $rule->getExcludePath();

            if ($this->checkPaths($excludePaths)) {
                return false;
            }

            if (!$this->checkPaths($includePaths)) {
                return false;
            }

            if ($this->checkPaths($includePaths) && !$this->checkPaths($excludePaths)) {
                return true;
            }
        }

        return true;
    }

    /**
     * @param $paths
     *
     * @return bool
     */
    public function checkPaths($paths)
    {
        if ($paths) {
            $currentPath = $this->_request->getRequestUri();

            $arrayPaths = explode("\n", $paths);
            $pathsUrl = array_map('trim', $arrayPaths);
            foreach ($pathsUrl as $path) {
                if ($path && strpos($currentPath, $path) !== false) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param $rule
     *
     * @return bool
     */
    public function checkExcludeIPs($rule)
    {
        $excludeIps = explode("\n", $rule->getExcludeIps());
        $currentIp = $this->getIpAddress();

        foreach ($excludeIps as $excludeIp) {
            if ($this->checkIp($currentIp, $excludeIp)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return string
     */
    public function getIpAddress()
    {
        $server = $this->_getRequest()->getServer();

        if (!empty($server['HTTP_CLIENT_IP'])) {
            $serverIp = $server['HTTP_CLIENT_IP'];
        } else if (!empty($server['HTTP_X_FORWARDED_FOR'])) {
            $serverIp = $server['HTTP_X_FORWARDED_FOR'];
        } else {
            $serverIp = $server['REMOTE_ADDR'];
        }

        $ipArr = explode(',', $serverIp);
        $serverIp = $ipArr[count($ipArr) - 1];

        return trim($serverIp);
    }

    /**
     * @param $ipAddr
     * @param $range
     *
     * @return bool
     */
    public function checkIp($ipAddr, $range)
    {
        if (strpos($range, '*') !== false) {
            $low = $high = $range;
            if (strpos($range, '-') !== false) {
                list($low, $high) = explode('-', $range, 2);
            }
            $low = str_replace('*', '0', $low);
            $high = str_replace('*', '255', $high);
            $range = $low . '-' . $high;
        }
        if (strpos($range, '-') !== false) {
            list($low, $high) = explode('-', $range, 2);

            return $this->ipCompare($ipAddr, $low, 1) && $this->ipcompare($ipAddr, $high, -1);
        }

        return $this->ipCompare($ipAddr, $range);
    }

    /**
     * @param $ip1
     * @param $ip2
     * @param int $op
     *
     * @return bool
     */
    private function ipCompare($ip1, $ip2, $op = 0)
    {
        $ip1Arr = explode('.', $ip1);
        $ip2Arr = explode('.', $ip2);

        for ($i = 0; $i < 4; $i++) {
            if ($ip1Arr[$i] < $ip2Arr[$i]) {
                return ($op == -1);
            }
            if ($ip1Arr[$i] > $ip2Arr[$i]) {
                return ($op == 1);
            }
        }

        return ($op === 0);
    }

    /**
     * get Matching Rule
     *
     * @return mixed
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function getMatchingRule()
    {
        $rules = $this->_ruleFactory->create()->getCollection()->setOrder('priority', 'ASC');
        foreach ($rules as $rule) {
            if ($rule->getStatus() && $this->checkCountry($rule) && $this->checkPageType($rule) && $this->checkExcludeIPs($rule)) {
                return $rule;
            }
        }

        return [];
    }

    /**
     * @param $name
     *
     * @return null|string
     */
    public function getCookieByName($name)
    {
        return $this->_cookieManager->getCookie($name);
    }

    /**
     * @param $name
     * @param $value
     * @param int $duration
     *
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     */
    public function setCookie($name, $value, $duration = 86400)
    {
        $metadata = $this->_cookieMetadataFactory
            ->createPublicCookieMetadata()
            ->setDuration($duration)
            ->setPath($this->_sessionManager->getCookiePath())
            ->setDomain($this->_sessionManager->getCookieDomain());

        $this->_cookieManager->setPublicCookie(
            $name,
            $value,
            $metadata
        );
    }

    /**
     * @param $name
     *
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     */
    public function deleteCookieByName($name)
    {
        $this->_cookieManager->deleteCookie(
            $name,
            $this->_cookieMetadataFactory
                ->createCookieMetadata()
                ->setPath($this->_sessionManager->getCookiePath())
                ->setDomain($this->_sessionManager->getCookieDomain())
        );
    }

    /**
     * Get Allow visitors save switched store view config
     *
     * @return mixed
     */
    public function getSaveSwitchedStoreConfig()
    {
        return $this->getConfigGeneral('save_switched_store');
    }

    /**
     * Get Http host
     *
     * @return mixed
     */
    public function getHttpHost()
    {
        $url = str_replace('.', '_', $this->_getRequest()->getHttpHost());

        return $url;
    }
}