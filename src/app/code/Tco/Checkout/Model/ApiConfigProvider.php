<?php

namespace Tco\Checkout\Model;

use Magento\Payment\Model\CcGenericConfigProvider;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Framework\View\Asset\Source as Source;
use \Magento\Checkout\Model\Cart;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;

class ApiConfigProvider extends CcGenericConfigProvider
{
    protected $_methodCodes = [
        \Tco\Checkout\Model\Api::CODE
    ];

    protected $_methodCode = "tco_api";
    protected $_paymentHelper;
    protected $_assetSource;

    public function __construct(
        \Magento\Payment\Model\CcConfig $ccConfig,
        PaymentHelper $paymentHelper,
        Source $assetSource,
        Cart $_cart,
        PriceHelper $_priceHelper
    ) {
        parent::__construct($ccConfig, $paymentHelper, $this->_methodCodes);
        $this->_paymentHelper = $paymentHelper;
        $this->_assetSource = $assetSource;
        $this->_cart = $_cart;
        $this->_priceHelper = $_priceHelper;
    }

    public function getInstallmentsQty($quote)
    {
        $maxInstallments = [];
        foreach ($quote->getAllVisibleItems() as $item) {
            $maxInstallments[] = max(intval($item->getProduct()->getInstallmentsQty()), 1);
        }
        
        return min($maxInstallments);
    }
    
    public function getInstallmentsValuesConfig() 
    {
        $options = [];
        $quote = $this->_cart->getQuote();
        $installmentsQty = $this->getInstallmentsQty($quote);
        for($i = 1; $i <= $installmentsQty; $i++) {
            $installmentAmount = round($quote->getBaseSubtotalWithDiscount() / $i, 2);
            $options[] = [
                'value' => $i, 
                'installments' => __('%1x of %2', $i, $this->_priceHelper->currency($installmentAmount, true, false))
            ];
        }

        return $options;

    }

    public function getConfig()
    {
        $config = parent::getConfig();
        $config['payment']['tco_api']['sellerId'] = $this->methods[$this->_methodCode]->getSellerId();
        $config['payment']['tco_api']['publishableKey'] = $this->methods[$this->_methodCode]->getPublishableKey();
        $config['payment']['tco_api']['publicKeyType'] = $this->methods[$this->_methodCode]->getPublicKeyType();
        $config['payment']['tco_api']['redirectUrl'] = $this->methods[$this->_methodCode]->getRedirectUrl();
        $config['payment']['tco_api']['processingError'] = '{"message":"Payment Authorization Failed: Please verify your information and try again, or try another payment method."}';
        $config['payment']['tco_api']['installments'] = $this->getInstallmentsValuesConfig();
        return $config;
    }

}
