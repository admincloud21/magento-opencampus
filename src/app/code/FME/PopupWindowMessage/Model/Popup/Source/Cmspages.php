<?php

namespace FME\PopupWindowMessage\Model\Popup\Source;

class Cmspages extends \FME\PopupWindowMessage\Model\Popup implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return $this->getCMSPage();
    }//end toOptionArray()
}//end class
