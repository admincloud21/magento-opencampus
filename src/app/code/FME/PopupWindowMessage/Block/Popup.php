<?php
/**
 * FME Extensions
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the fmeextensions.com license that is
 * available through the world-wide-web at this URL:
 * https://www.fmeextensions.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  FME
 * @package   FME_PopupWindowMessage
 * @author    Dara Baig  (support@fmeextensions.com)
 * @copyright Copyright (c) 2018 FME (http://fmeextensions.com/)
 * @license   https://fmeextensions.com/LICENSE.txt
 */
namespace FME\PopupWindowMessage\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\ObjectManagerInterface;

class Popup extends Template
{
    public $popupHelper;
    protected $_customerSession;
    protected $_customerGroupCollection;
    protected $scopeConfig;
    protected $collectionFactory;
    protected $objectManager;
    protected $request;
    protected $date;
    protected $_messageManager;
    protected $registry;
    protected $rule;
        
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Group $customerGroupCollection,
        \FME\PopupWindowMessage\Model\ResourceModel\Popup\
        CollectionFactory $collectionFactory,
        \FME\PopupWindowMessage\Helper\Popup $popupHelper,
        \FME\PopupWindowMessage\Model\PopupFactory $rule,
        ObjectManagerInterface $objectManager,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->_customerSession = $customerSession;
        $this->_customerGroupCollection = $customerGroupCollection;
        $this->popupHelper = $popupHelper;
        $this->context = $context;
        $this->objectManager = $objectManager;
        $this->request = $request;
        $this->registry = $registry;
        $this->rule = $rule;
        $this->_messageManager = $messageManager;
                
        parent::__construct($context);
    }
    
    public function getPopupCollection()
    {
               
        $filters = $this->getRequest()->getPostValue();
        $collection = $this->collectionFactory->create();
        $collection = $collection->addFieldToFilter('is_active', 1);

        return $collection;
    }

    public function getCurrentProduct()
    {
        return $this->registry->registry('current_product');
    }

    // public function getCurrentCategory()
    // {
    //     return $this->registry->registry('current_category');
    // }

    
    public function getConditionsStatusForProducts($popupId)
    {

        $rule = $this->rule->create()->load($popupId);
        if ($rule->getConditions()->validate($this->getCurrentProduct())) {
                    return true;
        }
    }

    // public function getConditionsStatusForCategory($popupId)
    // { 
    //     print_r($rule->getConditions()->validate($this->getCurrentCategory()));
    //     exit();
    //     $rule = $this->rule->create()->load($popupId);
    //     if($rule->getConditions()->validate($this->getCurrentProduct())){
    //         return true;

    //     }
    // }
    public function getSerializeFormData()
    {
        $filters = $this->getRequest()->getPostValue();
        return json_encode($filters);
    }

    public function getCustomerGroup()
    {
        $currentGroupId = $this->_customerSession->getCustomer()->getGroupId();
        $collection = $this->_customerGroupCollection->load($currentGroupId);
        $CustomerGroupName = $collection->getCustomerGroupCode();
        return $currentGroupId;
    }
}
