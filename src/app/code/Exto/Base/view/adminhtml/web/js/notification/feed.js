/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery'
], function ($) {
    'use strict';

    /**
     * Update feed.
     *
     * @param {String} url
     * @return {Promise}
     */
    function updateFeed(url) {
        return $.ajax({
            url: url,
            type: 'GET',
            showLoader: false
        });
    }

    return function (config) {
        $(document).ready(function () {
            updateFeed(config.url);
        });
    };
});
