<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class TrackingConfig.
 */
class TrackingConfig
{
    /**
     * @var string
     */
    private static $xmlPathIsEnabled = 'exto_base/tracking/is_enabled';

    /**
     * @var string
     */
    private static $xmlPathTrackUrl = 'exto_base/tracking/track_url';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var ConfigResource
     */
    private $configResource;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ConfigResource $configResource
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ConfigResource $configResource
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->configResource = $configResource;
    }

    /**
     * Is tracking enabled.
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->scopeConfig->isSetFlag(self::$xmlPathIsEnabled);
    }

    /**
     * Set is tracking enabled.
     *
     * @param bool $isEnabled
     * @return $this
     */
    public function setIsEnabled($isEnabled)
    {
        $this->configResource->save(self::$xmlPathIsEnabled, (int)$isEnabled);

        return $this;
    }

    /**
     * Get track url.
     *
     * @return string
     */
    public function getTrackUrl()
    {
        return $this->scopeConfig->getValue(self::$xmlPathTrackUrl);
    }
}
