<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class NotificationConfig.
 */
class NotificationConfig
{
    /**
     * @var string
     */
    private static $xmlPathSubscriptionsSegments = 'exto_base/notifications/subscribe_segments';

    /**
     * @var string
     */
    private static $xmlPathFrequency = 'exto_base/notifications/frequency';

    /**
     * @var string
     */
    private static $xmlPathFeedUrl = 'exto_base/notifications/feed_url';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var ConfigResource
     */
    private $configResource;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ConfigResource $configResource
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ConfigResource $configResource
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->configResource = $configResource;
    }

    /**
     * Get frequency.
     *
     * @return int
     */
    public function getFrequency()
    {
        return (int)$this->scopeConfig->getValue(self::$xmlPathFrequency) * 3600;
    }

    /**
     * Get feed url.
     *
     * @return string
     */
    public function getFeedUrl()
    {
        return $this->scopeConfig->getValue(self::$xmlPathFeedUrl);
    }

    /**
     * Get subscription segments.
     *
     * @return string[]
     */
    public function getSubscriptionSegments()
    {
        return (array)$this->scopeConfig->getValue(self::$xmlPathSubscriptionsSegments);
    }

    /**
     * Set subscription segments.
     *
     * @param array $segments
     * @return $this
     */
    public function setSubscriptionSegments(array $segments)
    {
        $this->configResource->save(self::$xmlPathSubscriptionsSegments, implode($segments, ','));

        return $this;
    }
}
