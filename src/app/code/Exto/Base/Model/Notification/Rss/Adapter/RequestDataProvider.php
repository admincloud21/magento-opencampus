<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Notification\Rss\Adapter;

use Exto\Base\Api\ModuleListInterface;
use Exto\Base\Model\Config\ModuleConfig;
use Exto\Base\Model\Config\NotificationConfig;

/**
 * Class RequestDataProvider.
 */
class RequestDataProvider
{
    /**
     * @var NotificationConfig
     */
    private $notificationConfig;

    /**
     * @var ModuleConfig
     */
    private $moduleConfig;

    /**
     * @var ModuleListInterface
     */
    private $moduleList;

    /**
     * @param NotificationConfig $notificationConfig
     * @param ModuleConfig $moduleConfig
     * @param ModuleListInterface $moduleList
     */
    public function __construct(
        NotificationConfig $notificationConfig,
        ModuleConfig $moduleConfig,
        ModuleListInterface $moduleList
    ) {
        $this->notificationConfig = $notificationConfig;
        $this->moduleConfig = $moduleConfig;
        $this->moduleList = $moduleList;
    }

    /**
     * Get request data.
     *
     * @return array
     */
    public function getRequestData()
    {
        $data = [];

        if ($this->notificationConfig->getSubscriptionSegments()) {
            $data['severity'] = $this->notificationConfig->getSubscriptionSegments();
        }

        $modules = $this->moduleList->getModules();
        foreach ($modules as $module) {
            $data['module'][$module->getId()] = [
                'installation_date' => $this->moduleConfig->getInstallationDate($module)
            ];
        }

        return $data;
    }
}
