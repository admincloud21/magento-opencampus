<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Tracking;

use Psr\Log\LoggerInterface;
use Exto\Base\Api\Data\ModuleMetadataInterface;
use Exto\Base\Api\TrackingInterface;
use Exto\Base\Model\Config\TrackingConfig;

/**
 * Class Tracking.
 */
class Tracking implements TrackingInterface
{
    /**
     * @var TrackingConfig
     */
    private $trackingConfig;

    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param TrackingConfig $trackingConfig
     * @param StorageInterface $storage
     * @param LoggerInterface $logger
     */
    public function __construct(
        TrackingConfig $trackingConfig,
        StorageInterface $storage,
        LoggerInterface $logger
    ) {
        $this->trackingConfig = $trackingConfig;
        $this->storage = $storage;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function track(ModuleMetadataInterface $module, $action, array $data = [])
    {
        if ($this->trackingConfig->isEnabled()) {
            try {
                $this->storage->addTrack($module, $action, $data);
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage(), $e->getTrace());
            }
        }

        return $this;
    }
}
