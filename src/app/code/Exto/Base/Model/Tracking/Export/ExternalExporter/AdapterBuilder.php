<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Tracking\Export\ExternalExporter;

use Magento\Framework\HTTP\Adapter\Curl;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use Exto\Base\Model\System\HttpAdapter\ConfigProvider;

/**
 * Class AdapterBuilder.
 */
class AdapterBuilder
{
    /**
     * @var CurlFactory
     */
    private $curlFactory;

    /**
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    private $requestData = [];

    /**
     * @param CurlFactory $curlFactory
     * @param ConfigProvider $configProvider
     */
    public function __construct(
        CurlFactory $curlFactory,
        ConfigProvider $configProvider
    ) {
        $this->curlFactory = $curlFactory;
        $this->configProvider = $configProvider;
    }

    /**
     * Set url.
     *
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Set request data.
     *
     * @param array $requestData
     * @return AdapterBuilder
     */
    public function setRequestData(array $requestData)
    {
        $this->requestData = $requestData;

        return $this;
    }

    /**
     * Build http adapter.
     *
     * @return Curl
     */
    public function build()
    {
        $curl = $this->curlFactory->create();

        $curl->setConfig($this->configProvider->getConfig());

        $requestBody = http_build_query($this->requestData);

        $curl->write(
            \Zend_Http_Client::POST,
            $this->url,
            '1.0',
            ['Connection: close'],
            $requestBody
        );

        return $curl;
    }
}
