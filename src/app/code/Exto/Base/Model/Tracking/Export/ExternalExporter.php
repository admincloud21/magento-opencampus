<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Tracking\Export;

use Exto\Base\Exception\Tracking\ExportException;
use Exto\Base\Model\Config\TrackingConfig;
use Exto\Base\Model\Tracking\Export\ExternalExporter\AdapterBuilder;

/**
 * Class ExternalExporter.
 */
class ExternalExporter implements ExporterInterface
{
    /**
     * @var AdapterBuilder
     */
    private $adapterBuilder;

    /**
     * @var TrackingConfig
     */
    private $trackingConfig;

    /**
     * @param AdapterBuilder $adapterBuilder
     * @param TrackingConfig $trackingConfig
     */
    public function __construct(
        AdapterBuilder $adapterBuilder,
        TrackingConfig $trackingConfig
    ) {
        $this->adapterBuilder = $adapterBuilder;
        $this->trackingConfig = $trackingConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function export(array $tracks)
    {
        try {
            $requestData = [
                'data' => @serialize($tracks)
            ];
            $adapter = $this->adapterBuilder->setRequestData($requestData)
                ->setUrl($this->trackingConfig->getTrackUrl())
                ->build();

            $adapter->read();
        } catch (\Exception $e) {
            throw new ExportException(__('Something went wrong'));
        }
    }
}
