<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Tracking\Storage;

use Exto\Base\Api\Data\ModuleMetadataInterface;
use Exto\Base\Model\Tracking\StorageInterface;

/**
 * Class ArrayUtil.
 */
class ArrayUtil
{
    /**
     * Add track to registry array.
     *
     * @param array $registry
     * @param ModuleMetadataInterface $module
     * @param string $action
     * @param array $data
     * @return array
     */
    public function addTrack(array $registry, ModuleMetadataInterface $module, $action, array $data = [])
    {
        $moduleId = (string)$module->getId();
        $action = (string)$action;
        $data = json_encode($data);
        $dataHash = $this->getDataHash($data);

        if (!isset($registry[$moduleId][$action][$dataHash][StorageInterface::KEY_CALL_COUNT])) {
            $registry[$moduleId][$action][$dataHash] = [
                StorageInterface::KEY_CALL_COUNT => 0,
                StorageInterface::KEY_CALL_DATA => $data
            ];
        }

        $registry[$moduleId][$action][$dataHash][StorageInterface::KEY_CALL_COUNT]++;

        return $registry;
    }

    /**
     * Add tracks to registry array.
     *
     * @param array $registry
     * @param array $tracks
     * @return array
     */
    public function addTracks(array $registry, array $tracks)
    {
        foreach ($tracks as $moduleId => $moduleTracks) {
            foreach ($moduleTracks as $action => $actionTracks) {
                foreach ($actionTracks as $trackId => $actionTrack) {
                    if (!isset($registry[$moduleId][$action][$trackId])) {
                        $registry[$moduleId][$action][$trackId] = [
                            StorageInterface::KEY_CALL_COUNT => 0,
                            StorageInterface::KEY_CALL_DATA => $actionTrack[StorageInterface::KEY_CALL_DATA]
                        ];
                    }

                    $registry[$moduleId][$action][$trackId][StorageInterface::KEY_CALL_COUNT]++;
                }
            }
        }

        return $registry;
    }

    /**
     * Get module tracks from registry array.
     *
     * @param array $registry
     * @param ModuleMetadataInterface $module
     * @return array
     */
    public function getModuleTracks(array $registry, ModuleMetadataInterface $module)
    {
        $tracks = [];

        if (isset($registry[$module->getId()])) {
            $tracks = (array)$registry[$module->getId()];
        }

        return $tracks;
    }

    /**
     * Get data hash.
     *
     * @param string $data
     * @return int
     */
    private function getDataHash($data)
    {
        return crc32($data);
    }
}
