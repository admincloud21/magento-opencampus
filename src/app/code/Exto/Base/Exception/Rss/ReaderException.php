<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Exception\Rss;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class ReaderException.
 */
class ReaderException extends LocalizedException
{
}
