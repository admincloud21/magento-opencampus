<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Controller\Adminhtml\Config;

use Magento\Framework\App\Config\ReinitableConfigInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Exto\Base\Model\Config as ModuleConfig;
use Exto\Base\Model\Config\NotificationConfig;
use Exto\Base\Model\Config\TrackingConfig;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Save.
 */
class Save extends Action
{
    /**
     * @var ReinitableConfigInterface
     */
    private $coreConfig;

    /**
     * @var ModuleConfig
     */
    private $moduleConfig;

    /**
     * @var NotificationConfig
     */
    private $notificationConfig;

    /**
     * @var TrackingConfig
     */
    private $trackingConfig;

    /**
     * @param Context $context
     * @param ModuleConfig $moduleConfig
     * @param NotificationConfig $notificationConfig
     * @param TrackingConfig $trackingConfig
     * @param ReinitableConfigInterface $coreConfig
     */
    public function __construct(
        Context $context,
        ModuleConfig $moduleConfig,
        NotificationConfig $notificationConfig,
        TrackingConfig $trackingConfig,
        ReinitableConfigInterface $coreConfig
    ) {
        parent::__construct($context);
        $this->coreConfig = $coreConfig;
        $this->moduleConfig = $moduleConfig;
        $this->notificationConfig = $notificationConfig;
        $this->trackingConfig = $trackingConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        try {
            $subscriptionSegments = $this->getRequest()->getParam('subscription_segments', []);
            $isTrackingEnable = (bool)$this->getRequest()->getParam('send_anonymous_data', false);

            $this->notificationConfig->setSubscriptionSegments((array)$subscriptionSegments);
            $this->trackingConfig->setIsEnabled($isTrackingEnable);
            $this->moduleConfig->setIsConfigured(true);

            $this->coreConfig->reinit();

            $resultData = [
                'success' => true
            ];
        } catch (LocalizedException $e) {
            $resultData = [
                'error' => true,
                'message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $resultData = [
                'error' => true,
                'message' => __('Something went wrong.')
            ];
        }

        return $result->setData($resultData);
    }
}
