<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Api;

use Exto\Base\Api\Data\ModuleMetadataInterface;

/**
 * Module management interface.
 *
 * @api
 */
interface ModuleManagementInterface
{
    /**
     * Install module.
     *
     * @param ModuleMetadataInterface $module
     * @return $this
     */
    public function install(ModuleMetadataInterface $module);
}
