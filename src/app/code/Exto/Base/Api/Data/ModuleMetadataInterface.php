<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Api\Data;

/**
 * Module Metadata interface.
 *
 * @api
 */
interface ModuleMetadataInterface
{
    /**
     * Get module id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get module name.
     *
     * @return string
     */
    public function getName();
}
