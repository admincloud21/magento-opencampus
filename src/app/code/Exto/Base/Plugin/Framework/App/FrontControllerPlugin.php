<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Plugin\Framework\App;

use Psr\Log\LoggerInterface;
use Magento\Framework\App\FrontControllerInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Exto\Base\Model\Config\TrackingConfig;
use Exto\Base\Model\Tracking\StorageInterface;

/**
 * Class FrontControllerPlugin.
 */
class FrontControllerPlugin
{
    /**
     * @var TrackingConfig
     */
    private $trackingConfig;

    /**
     * @var StorageInterface
     */
    private $tempStorage;

    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param TrackingConfig $trackingConfig
     * @param StorageInterface $tempStorage
     * @param StorageInterface $storage
     * @param LoggerInterface $logger
     */
    public function __construct(
        TrackingConfig $trackingConfig,
        StorageInterface $tempStorage,
        StorageInterface $storage,
        LoggerInterface $logger
    ) {
        $this->trackingConfig = $trackingConfig;
        $this->tempStorage = $tempStorage;
        $this->storage = $storage;
        $this->logger = $logger;
    }

    /**
     * After dispatch plugin.
     *
     * @param FrontControllerInterface $subject
     * @param ResponseInterface|ResultInterface $result
     * @return ResponseInterface|ResultInterface
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterDispatch(FrontControllerInterface $subject, $result)
    {
        if ($this->trackingConfig->isEnabled()) {
            try {
                $this->storage->addTracks(
                    $this->tempStorage->getTracks()
                );
                $this->tempStorage->clear();
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage(), $e->getTrace());
            }
        }

        return $result;
    }
}
