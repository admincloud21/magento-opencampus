<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Cron\Tracking;

use Psr\Log\LoggerInterface;
use Exto\Base\Exception\Tracking\ExportException;
use Exto\Base\Model\Config\TrackingConfig;
use Exto\Base\Model\Tracking\Export\ExporterInterface;
use Exto\Base\Model\Tracking\StorageInterface;

/**
 * Class TracksExport.
 */
class TracksExport
{
    /**
     * @var TrackingConfig
     */
    private $trackingConfig;

    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * @var ExporterInterface
     */
    private $exporter;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param TrackingConfig $trackingConfig
     * @param StorageInterface $storage
     * @param ExporterInterface $exporter
     * @param LoggerInterface $logger
     */
    public function __construct(
        TrackingConfig $trackingConfig,
        StorageInterface $storage,
        ExporterInterface $exporter,
        LoggerInterface $logger
    ) {
        $this->trackingConfig = $trackingConfig;
        $this->storage = $storage;
        $this->exporter = $exporter;
        $this->logger = $logger;
    }

    /**
     * Execute export tracks.
     *
     * @return void
     */
    public function execute()
    {
        if (!$this->trackingConfig->isEnabled()) {
            return;
        }

        try {
            $this->exporter->export($this->storage->getTracks());
            $this->storage->clear();
        } catch (ExportException $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }
}
