<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Sarp\Setup;

use Exto\Base\Api\Data\ModuleMetadataInterface;

/**
 * Class ModuleMetadata.
 */
class ModuleMetadata implements ModuleMetadataInterface
{
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 'Exto_Sarp';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Exto Sarp';
    }
}
