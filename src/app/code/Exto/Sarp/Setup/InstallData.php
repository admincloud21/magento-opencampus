<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Setup;

use Magento\Catalog\Model\Product as ProductModel;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute as EavAttribute;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Exto\Sarp\Model\Product\ProductManagement;
use Exto\Sarp\Model\Source\Subscription\Template;
use Exto\Base\Api\ModuleManagementInterface;

/**
 * Class InstallData
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * @var string
     */
    protected $entityTypeId = ProductModel::ENTITY;

    /**
     * @var string
     */
    protected $sarpInfoGroupName = 'Subscription Information';

    /**
     * @var int
     */
    protected $sarpInfoGroupSortOrder = 100;

    /**
     * @var ModuleManagementInterface
     */
    private $moduleManagement;

    /**
     * @var ModuleMetadata
     */
    private $moduleMetadata;

    /**
     * InstallData constructor.
     *
     * @param EavSetupFactory $eavSetupFactory
     * @param ModuleManagementInterface $moduleManagement
     * @param ModuleMetadata $moduleMetadata
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        ModuleManagementInterface $moduleManagement,
        ModuleMetadata $moduleMetadata
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->moduleManagement = $moduleManagement;
        $this->moduleMetadata = $moduleMetadata;
    }

    /**
     * {@inheritdoc}
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        if ($attributeSetId = $eavSetup->getAttributeSet($this->entityTypeId, 'Default', 'attribute_set_id')) {
            $eavSetup
                ->addAttributeGroup(
                    $this->entityTypeId,
                    $attributeSetId,
                    $this->sarpInfoGroupName,
                    $this->sarpInfoGroupSortOrder
                )
                ->updateAttributeGroup(
                    $this->entityTypeId,
                    $attributeSetId,
                    $this->sarpInfoGroupName,
                    'attribute_group_code',
                    'exto-sarp-info'
                )
                ->updateAttributeGroup(
                    $this->entityTypeId,
                    $attributeSetId,
                    $this->sarpInfoGroupName,
                    'tab_group_code',
                    'basic'
                )
            ;
        }
        if (!$eavSetup->getAttribute($this->entityTypeId, ProductManagement::TEMPLATE_PRODUCT_ATTRIBUTE_CODE)) {
            $eavSetup
                ->addAttribute(
                    $this->entityTypeId,
                    ProductManagement::TEMPLATE_PRODUCT_ATTRIBUTE_CODE,
                    [
                        'type' => 'int',
                        'label' => 'Enable subscription',
                        'input' => 'select',
                        'required' => false,
                        'source' => Template::class,
                        'global' => EavAttribute::SCOPE_GLOBAL,
                        'user_defined' => false,
                        'searchable' => false,
                        'filterable' => false,
                        'visible_in_advanced_search' => false,
                        'used_in_product_listing' => false,
                        'used_for_sort_by' => false,
                        'apply_to' => '',
                        'group' => $this->sarpInfoGroupName,
                        'sort_order' => 1,
                    ]
                );
        }

        $setup->endSetup();

        $this->moduleManagement->install($this->moduleMetadata);
    }
}
