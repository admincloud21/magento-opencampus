<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        
        $table = $installer->getConnection()->newTable($installer->getTable('exto_sarp_profile'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['primary' => 1, 'nullable' => false, 'identity' => 1]
            )
            ->addColumn(
                'profile_id',
                Table::TYPE_TEXT,
                null,
                []
            )
            ->addColumn(
                'customer_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => 1]
            )
            ->addColumn(
                'customer_name',
                Table::TYPE_TEXT,
                null,
                []
            )
            ->addColumn(
                'customer_email',
                Table::TYPE_TEXT,
                null,
                []
            )
            ->addColumn(
                'external_service_profile_id',
                Table::TYPE_TEXT,
                null,
                []
            )
            ->addColumn(
                'status',
                Table::TYPE_TEXT,
                null,
                []
            )
            ->addColumn(
                'title',
                Table::TYPE_TEXT,
                null,
                ['nullable' => 1]
            )
            ->addColumn(
                'created_date',
                Table::TYPE_DATETIME,
                null,
                []
            )
            ->addColumn(
                'completed_date',
                Table::TYPE_DATETIME,
                null,
                ['nullable' => 1]
            )
            ->addColumn(
                'billing_start_date',
                Table::TYPE_DATETIME,
                null,
                ['nullable' => 1]
            )
            ->addColumn(
                'max_payment_failures',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => 1]
            )
            ->addColumn(
                'default_order_status',
                Table::TYPE_TEXT,
                null,
                []
            )
            ->addColumn(
                'billing_frequency',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => 1]
            )
            ->addColumn(
                'billing_period',
                Table::TYPE_TEXT,
                null,
                []
            )
            ->addColumn(
                'billing_cycles',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => 1]
            )
            ->addColumn(
                'last_successful_date',
                Table::TYPE_DATETIME,
                null,
                ['nullable' => 1]
            )
            ->addColumn(
                'quote_data',
                Table::TYPE_TEXT,
                null,
                []
            )
            ->addColumn(
                'quote_items_data',
                Table::TYPE_TEXT,
                null,
                []
            )
            ->addColumn(
                'billing_address_data',
                Table::TYPE_TEXT,
                null,
                []
            )
            ->addColumn(
                'shipping_address_data',
                Table::TYPE_TEXT,
                null,
                []
            )
            ->addColumn(
                'product_ids',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false]
            )
            ->addColumn(
                'is_deleted',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => '0', 'nullable' => false]
            );

        $installer->getConnection()->createTable($table);

        $table = $installer->getConnection()->newTable($installer->getTable('exto_sarp_profile_message'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => 1, 'nullable' => false, 'primary' => 1, 'identity' => 1]
            )
            ->addColumn(
                'profile_id',
                Table::TYPE_INTEGER,
                null,
                []
            )
            ->addColumn(
                'created_at',
                Table::TYPE_DATETIME,
                null,
                []
            )
            ->addColumn(
                'message',
                Table::TYPE_TEXT,
                null,
                ['nullable' => 1]
            )
            ->addIndex(
                'exto_sarp_profile_message_sarp_profile',
                ['profile_id'],
                ['type' => 'index']
            )
            ->addForeignKey(
                'exto_sarp_profile_message_sarp_profile',
                'profile_id',
                $installer->getTable('exto_sarp_profile'),
                'id',
                'CASCADE'
            );

        $installer->getConnection()->createTable($table);

        $table = $installer->getConnection()->newTable($installer->getTable('exto_sarp_profile_order'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => 1, 'nullable' => false, 'primary' => 1, 'identity' => 1]
            )
            ->addColumn(
                'profile_id',
                Table::TYPE_INTEGER,
                null,
                []
            )
            ->addColumn(
                'order_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => 1]
            )
            ->addIndex(
                'exto_sarp_profile_order_sarp_profile',
                ['profile_id'],
                ['type' => 'index']
            )
            ->addForeignKey(
                'exto_sarp_profile_order_sarp_profile',
                'profile_id',
                $installer->getTable('exto_sarp_profile'),
                'id',
                'CASCADE'
            );

        $installer->getConnection()->createTable($table);

        $table = $installer->getConnection()->newTable($installer->getTable('exto_sarp_subscription_template'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => 1, 'nullable' => false, 'primary' => 1, 'identity' => 1]
            )
            ->addColumn(
                'title',
                Table::TYPE_TEXT,
                null,
                []
            )
            ->addColumn(
                'is_subscription_only',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => 1]
            )
            ->addColumn(
                'is_choose_billing_date',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => 1]
            )
            ->addColumn(
                'max_payment_failures',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => 1]
            )
            ->addColumn(
                'default_order_status',
                Table::TYPE_TEXT,
                null,
                []
            )
            ->addColumn(
                'option_style',
                Table::TYPE_TEXT,
                null,
                []
            )
            ->addColumn(
                'created_at',
                Table::TYPE_DATETIME,
                null,
                []
            )
            ->addColumn(
                'is_deleted',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => '0', 'nullable' => false]
            );

        $installer->getConnection()->createTable($table);

        $table = $installer->getConnection()->newTable($installer->getTable('exto_sarp_subscription_template_option'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => 1, 'nullable' => false, 'primary' => 1, 'identity' => 1]
            )
            ->addColumn(
                'template_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => 1]
            )
            ->addColumn(
                'title',
                Table::TYPE_TEXT,
                null,
                []
            )
            ->addColumn(
                'billing_frequency',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => 1]
            )
            ->addColumn(
                'billing_period',
                Table::TYPE_TEXT,
                null,
                []
            )
            ->addColumn(
                'billing_cycles',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => 1]
            )
            ->addIndex(
                'exto_sarp_template_option_sarp_subscription',
                ['template_id'],
                ['type' => 'index']
            )
            ->addForeignKey(
                'exto_sarp_template_option_sarp_subscription',
                'template_id',
                $installer->getTable('exto_sarp_subscription_template'),
                'id',
                'CASCADE'
            );

        $installer->getConnection()->createTable($table);

        $table = $installer->getConnection()->newTable($installer->getTable('exto_sarp_template_option_translate'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => 1, 'nullable' => false, 'primary' => 1, 'identity' => 1]
            )
            ->addColumn(
                'option_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => 1]
            )
            ->addColumn(
                'store_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => 1]
            )
            ->addColumn(
                'value',
                Table::TYPE_TEXT,
                null,
                []
            )
            ->addIndex(
                'exto_sarp_option_translate_sarp_option',
                ['option_id'],
                ['type' => 'index']
            )
            ->addForeignKey(
                'exto_sarp_option_translate_sarp_option',
                'option_id',
                $installer->getTable('exto_sarp_subscription_template_option'),
                'id',
                'CASCADE'
            );

        $installer->getConnection()->createTable($table);
        
        $installer->endSetup();
    }
}
