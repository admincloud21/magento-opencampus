<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Sarp\Observer;

use Exto\Sarp\Model\Product\ProductManagement;
use Exto\Sarp\Model\QuoteHelper;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Exception\LocalizedException;
use Exto\Sarp\Model\ResourceModel\SubscriptionTemplateRepository;

/**
 * Class RestrictNonRecurringPaymentMethods
 */
class CatalogProductTypePrepareOptions implements ObserverInterface
{
    /**
     * @var SubscriptionTemplateRepository $subscriptionTemplateRepository
     */
    protected $subscriptionTemplateRepository;

    /**
     * @param SubscriptionTemplateRepository $subscriptionTemplateRepository
     */
    public function __construct(
        SubscriptionTemplateRepository $subscriptionTemplateRepository
    ) {
        $this->subscriptionTemplateRepository = $subscriptionTemplateRepository;
    }

    /**
     * @param EventObserver $observer
     *
     * @throws LocalizedException
     */
    public function execute(EventObserver $observer)
    {
        $event = $observer->getEvent();
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $event->getProduct();
        /** @var \Magento\Framework\DataObject $buyRequest */
        $buyRequest = $event->getBuyRequest();

        try {
            $template = $this->subscriptionTemplateRepository->get(
                $product->getData(ProductManagement::TEMPLATE_PRODUCT_ATTRIBUTE_CODE)
            );
        } catch (\Exception $e) {
            $template = null;
        }

        if (null !== $template && $template->getIsSubscriptionOnly()
            && null === $buyRequest->getExtoSarpTemplateOption()) {
            throw new LocalizedException(__('Please specify product option(s).'));
        }

        $product->addCustomOption(
            QuoteHelper::BUY_REQUEST_TEMPLATE_OPTION,
            $buyRequest->getExtoSarpTemplateOption(),
            $product
        );
        $product->addCustomOption(
            QuoteHelper::BUY_REQUEST_FIRST_BILLING_DATE,
            $buyRequest->getExtoSarpStartBillingDate(),
            $product
        );
    }
}
