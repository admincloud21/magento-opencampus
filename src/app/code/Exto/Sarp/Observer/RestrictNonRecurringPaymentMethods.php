<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Checkout\Model\Session as CheckoutSession;
use Exto\Sarp\Model\QuoteHelper;
use Magento\Paypal\Model\Config;

/**
 * Class RestrictNonRecurringPaymentMethods
 */
class RestrictNonRecurringPaymentMethods implements ObserverInterface
{
    /** @var \Magento\Checkout\Model\Session */
    protected $checkoutSession;

    /** @var \Exto\Sarp\Model\QuoteHelper */
    protected $quoteHelper;

    /**
     * RestrictNonRecurringPaymentMethods constructor.
     * @param CheckoutSession $checkoutSession
     * @param QuoteHelper $quoteHelper
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        QuoteHelper $quoteHelper
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->quoteHelper = $quoteHelper;
    }

    /**
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        $event = $observer->getEvent();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $event->getQuote();
        if (null === $quote) {
            $quote = $this->checkoutSession->getQuote();
        }
        /** @var \Magento\Payment\Model\Method\AbstractMethod $methodInstance */
        $methodInstance = $event->getMethodInstance();
        /** @var \Magento\Framework\DataObject $result */
        $result = $observer->getEvent()->getResult();
        if (!$this->isMethodAvailable($methodInstance, $quote)) {
            $result->setData('is_available', false);
        }
    }

    /**
     * @param \Magento\Payment\Model\Method\AbstractMethod $methodInstance
     * @param \Magento\Quote\Model\Quote $quote
     * @return bool
     */
    protected function isMethodAvailable($methodInstance, $quote)
    {
        if (!$this->quoteHelper->isQuoteContainRecurringItems($quote)) {
            return true;
        }
        if (in_array($methodInstance->getCode(), $this->getRecurringPaymentMethodsList())) {
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    protected function getRecurringPaymentMethodsList()
    {
        return [
            Config::METHOD_WPP_EXPRESS
        ];
    }
}
