<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Helper\Catalog\Product;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Catalog\Helper\Product\Configuration\ConfigurationInterface;
use Magento\Framework\App\Helper\Context;
use Exto\Sarp\Model\QuoteHelper;
use Exto\Sarp\Model\ResourceModel\SubscriptionTemplateOptionRepository;
use Magento\Catalog\Helper\Product\Configuration as ProductConfiguration;
use Magento\Catalog\Model\Product\Configuration\Item\ItemInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\ResourceModel\Quote;

/**
 * Class Configuration
 */
class Configuration extends AbstractHelper implements ConfigurationInterface
{
    /** @var \Magento\Catalog\Helper\Product\Configuration */
    protected $productConfig = null;

    /** @var \Exto\Sarp\Model\QuoteHelper */
    protected $quoteHelper;

    /** @var \Exto\Sarp\Model\ResourceModel\SubscriptionTemplateOptionRepository */
    protected $subscriptionTemplateOptionRepository;

    /**
     * Configuration constructor.
     * @param Context $context
     * @param ProductConfiguration $productConfig
     * @param QuoteHelper $quoteHelper
     * @param SubscriptionTemplateOptionRepository $subscriptionTemplateOptionRepository
     */
    public function __construct(
        Context $context,
        ProductConfiguration $productConfig,
        QuoteHelper $quoteHelper,
        SubscriptionTemplateOptionRepository $subscriptionTemplateOptionRepository
    ) {
        parent::__construct($context);
        $this->productConfig = $productConfig;
        $this->quoteHelper = $quoteHelper;
        $this->subscriptionTemplateOptionRepository = $subscriptionTemplateOptionRepository;
    }

    /**
     * Retrieves product options
     *
     * @param ItemInterface $item
     * @return array
     */
    public function getOptions(ItemInterface $item)
    {
        $options = $this->productConfig->getOptions($item);
        return array_merge($options, $this->getRecurringOptions($item));
    }

    /**
     * @param ItemInterface $item
     *
     * @return array
     */
    public function getRecurringOptions(ItemInterface $item)
    {
        $options = [];
        $templateOption = $item->getOptionByCode(
            QuoteHelper::BUY_REQUEST_TEMPLATE_OPTION
        );
        $templateOptionId = intval($templateOption->getValue());
        try {
            $templateDataModel = $this->subscriptionTemplateOptionRepository->get($templateOptionId);
            $options[] = [
                'label' => __('Subscribe for regular delivery'),
                'value' => $templateDataModel->getTitle()
            ];
        } catch (NoSuchEntityException $e) {
            return [];
        }
        $startDateOption = $item->getOptionByCode(
            QuoteHelper::BUY_REQUEST_FIRST_BILLING_DATE
        );
        if (null !== $startDateOption) {
            $startDateAsDatetime = $this->quoteHelper->getQuoteItemStartDateByOptionValue(
                $startDateOption->getValue()
            );
            if (null === $startDateAsDatetime) {
                $startDateAsDatetime = new \DateTime();
            }
            $options[] = [
                'label' => __('Start billing date'),
                'value' => $this->quoteHelper->getSubscriptionStartDateDescriptionByDatetime(
                    $startDateAsDatetime
                )
            ];
        }
        return $options;
    }
}
