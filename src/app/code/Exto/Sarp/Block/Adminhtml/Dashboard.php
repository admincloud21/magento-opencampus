<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Block\Adminhtml;

use Exto\Sarp\Model\Product\ProductManagement;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Backend\Block\Template;
use Exto\Sarp\Model\ResourceModel\ProfileOrder\CollectionFactory as ProfileOrderCollectionFactory;
use Exto\Sarp\Model\ResourceModel\SubscriptionTemplate\CollectionFactory as SubscriptionTemplateCollectionFactory;
use Exto\Sarp\Model\Source\Profile\Status;
use Magento\Backend\Block\Template\Context;

/**
 * Class Dashboard
 */
class Dashboard extends Template
{
    /**
     * @var \Exto\Sarp\Model\ResourceModel\ProfileOrder\CollectionFactory
     */
    protected $profileOrderCollectionFactory;

    /**
     * @var \Exto\Sarp\Model\ResourceModel\SubscriptionTemplate\CollectionFactory
     */
    protected $templateCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var \Exto\Sarp\Model\ResourceModel\Profile\CollectionFactory
     */
    protected $profileCollectionFactory;

    /**
     * @var \Exto\Sarp\Model\Source\Profile\Status
     */
    protected $profileStatusSource;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * Dashboard constructor.
     * @param \Exto\Sarp\Model\ResourceModel\ProfileOrder\CollectionFactory $profileOrderCollectionFactory
     * @param \Exto\Sarp\Model\ResourceModel\SubscriptionTemplate\CollectionFactory $templateCollectionFactory
     * @param \Exto\Sarp\Model\ResourceModel\Profile\CollectionFactory $profileCollectionFactory
     * @param \Exto\Sarp\Model\Source\Profile\Status $profileStatusSource
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param PriceCurrencyInterface $priceCurrency
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        ProfileOrderCollectionFactory $profileOrderCollectionFactory,
        SubscriptionTemplateCollectionFactory $templateCollectionFactory,
        \Exto\Sarp\Model\ResourceModel\Profile\CollectionFactory $profileCollectionFactory,
        Status $profileStatusSource,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        PriceCurrencyInterface $priceCurrency,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->profileOrderCollectionFactory = $profileOrderCollectionFactory;
        $this->templateCollectionFactory = $templateCollectionFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->profileCollectionFactory = $profileCollectionFactory;
        $this->profileStatusSource = $profileStatusSource;
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * @return bool
     */
    public function isModuleEnabled()
    {
        return true;
    }

    /**
     * @return int
     */
    public function getTemplateCount()
    {
        /** @var \Exto\Sarp\Model\ResourceModel\SubscriptionTemplate\Collection $templateCollection */
        $templateCollection = $this->templateCollectionFactory->create();
        return $templateCollection->getSize();
    }

    /**
     * @return int
     */
    public function getProductCount()
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
        $productCollection = $this->productCollectionFactory->create();

        $productCollection
            ->addAttributeToSelect(ProductManagement::TEMPLATE_PRODUCT_ATTRIBUTE_CODE)
            ->addFieldToFilter([
                [
                    'attribute' => ProductManagement::TEMPLATE_PRODUCT_ATTRIBUTE_CODE,
                    'gt'        => 0
                ]
            ]);
        return $productCollection->getSize();
    }

    /**
     * @return array
     */
    public function getProfileData()
    {
        /** @var \Exto\Sarp\Model\ResourceModel\Profile\Collection $profileCollection */
        $profileCollection =  $this->profileCollectionFactory->create();
        $profileStatuses = $this->profileStatusSource->getOptionArray();
        $result = [];
        foreach ($profileStatuses as $key => $title) {
            $count = $profileCollection->getProfileCountByStatus($key);
            $result[$key] = [
                'title' => $title,
                'count' => $count
            ];
        }
        return $result;
    }

    /**
     * @return array
     */
    public function getTotalData()
    {
        /** @var \Exto\Sarp\Model\ResourceModel\ProfileOrder\Collection $profileOrderCollection */
        $profileOrderCollection = $this->profileOrderCollectionFactory->create();
        $today = $profileOrderCollection->getTotalForSubscriptionInPeriod(
            new \DateTime('today 00:00:00'),
            new \DateTime('now')
        );
        $yesterday = $profileOrderCollection->getTotalForSubscriptionInPeriod(
            new \DateTime('yesterday 00:00:00'),
            new \DateTime('yesterday 23:59:59')
        );
        $thisWeek = $profileOrderCollection->getTotalForSubscriptionInPeriod(
            new \DateTime('Monday this week 00:00:00'),
            new \DateTime('now')
        );
        $thisMonth = $profileOrderCollection->getTotalForSubscriptionInPeriod(
            new \DateTime('first day of this month 00:00:00'),
            new \DateTime('now')
        );
        $lastMonth = $profileOrderCollection->getTotalForSubscriptionInPeriod(
            new \DateTime('first day of last month 00:00:00'),
            new \DateTime('last day of last month 23:59:59')
        );
        $allTime = $profileOrderCollection->getTotalForSubscriptionInPeriod();
        $result = [
            'today' => $today,
            'yesterday' => $yesterday,
            'this_week' => $thisWeek,
            'this_month' => $thisMonth,
            'last_month' => $lastMonth,
            'all_time' => $allTime
        ];

        return $result;
    }

    /**
     * @return string
     */
    public function getTemplateUrl()
    {
        return $this->getUrl('exto_sarp/template/index', []);
    }

    /**
     * @param string $status
     * @return string
     */
    public function getProfileUrlByStatus($status)
    {
        return $this->getUrl('exto_sarp/dashboard/profileFilter', ['status' => $status]);
    }

    /**
     * @return string
     */
    public function getProductUrl()
    {
        return $this->getUrl('exto_sarp/dashboard/productFilter', ['is_subscription' => 1]);
    }

    /**
     * @param int $price
     * @return float
     */
    public function formatPrice($price)
    {
        return $this->priceCurrency->format(
            $price,
            false,
            PriceCurrencyInterface::DEFAULT_PRECISION,
            $this->_storeManager->getDefaultStoreView()
        );
    }
}
