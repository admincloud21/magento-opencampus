<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Block\Adminhtml\Template;

use Magento\Backend\Block\Widget\Form\Container;

/**
 * Class Edit
 */
class Edit extends Container
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Exto_Sarp';
        $this->_controller = 'adminhtml_template';
        $this->_mode = 'edit';

        parent::_construct();
    }

    /**
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('exto_sarp/template/index');
    }
}
