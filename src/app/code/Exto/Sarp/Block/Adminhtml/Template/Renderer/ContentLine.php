<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Block\Adminhtml\Template\Renderer;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Data\Form\Element\Renderer\RendererInterface;
use Magento\Backend\Block\TemplateFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class ContentLine
 */
class ContentLine implements RendererInterface
{
    /**
     * @var string
     */
    protected $_template = 'Exto_Sarp::template/renderer/content/line.phtml';

    /**
     * @var TemplateFactory
     */
    protected $blockFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * ContentLine constructor.
     * @param TemplateFactory $block
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        TemplateFactory $block,
        StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
        $this->blockFactory = $block;
    }

    /**
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $allStores = $this->storeManager->getStores();
        return $this->blockFactory
            ->create()
            ->setStores($allStores)
            ->setFieldCode($element->getFieldCode())
            ->setFormName($element->getFormName())
            ->setOptionValues($element->getOptionValues())
            ->setTemplate($this->_template)
            ->toHtml();
    }
}
