<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Block\Adminhtml\Template\Edit\Tab;

use Exto\Sarp\Model\ResourceModel\TemplateOptionTranslate;
use Exto\Sarp\Model\ResourceModel\TemplateOptionTranslateRepository;
use Magento\Ui\Component\Layout\Tabs\TabInterface;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Backend\Block\Widget\Form\Renderer\Fieldset;
use Exto\Sarp\Model\ResourceModel\SubscriptionTemplateOptionRepository;
use Exto\Sarp\Block\Adminhtml\Template\Renderer\ContentLine;
use Exto\Sarp\Model\Source\Template\BillingPeriod;
use Magento\Backend\Block\Template;

/**
 * Class Option
 */
class Option extends Generic implements TabInterface
{
    /**
     * @var SubscriptionTemplateOptionRepository
     */
    protected $templateOptionRepository;

    /**
     * @var TemplateOptionTranslateRepository
     */
    protected $templateOptionTranslateRepository;

    /**
     * @var ContentLine
     */
    protected $renderer;

    /**
     * @var BillingPeriod
     */
    protected $billingPeriodSource;

    /**
     * Option constructor.
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param SubscriptionTemplateOptionRepository $templateOptionRepository
     * @param TemplateOptionTranslateRepository $templateOptionTranslateRepository
     * @param ContentLine $renderer
     * @param BillingPeriod $billingPeriodSource
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        SubscriptionTemplateOptionRepository $templateOptionRepository,
        TemplateOptionTranslateRepository $templateOptionTranslateRepository,
        ContentLine $renderer,
        BillingPeriod $billingPeriodSource,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->templateOptionRepository = $templateOptionRepository;
        $this->renderer = $renderer;
        $this->billingPeriodSource = $billingPeriodSource;
        $this->templateOptionTranslateRepository = $templateOptionTranslateRepository;
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Billing Options');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Billing Options');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * @return null
     */
    public function getTabClass()
    {
        return null;
    }

    /**
     * @return null
     */
    public function getTabUrl()
    {
        return null;
    }

    /**
     * @return bool
     */
    public function isAjaxLoaded()
    {
        return false;
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        $formName = 'template_form';
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('template_');

        $id = $this->getRequest()->getParam('id', null);
        $optionCount = 0;
        if (null !== $id) {
            /** @var \Exto\Sarp\Api\Data\SubscriptionTemplateOptionSearchResultsInterface $templateOptions */
            $templateOptions = $this->templateOptionRepository->getListByTemplateId($id);

            /** @var \Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterface $option */
            foreach ($templateOptions->getItems() as $option) {
                $optionCount++;
                $this->addOptionToForm($form, $option, $formName, null);
            }
        }

        $fieldset = $form->addFieldset(
            'add_option_fieldset',
            [
                'legend' => __(''),
            ]
        );

        $stores = $this->_storeManager->getStores();
        $addOptionHtml = $this->getLayout()->createBlock(
            Template::class
        )
            ->setTemplate('Exto_Sarp::template/renderer/option/add.phtml')
            ->setOptionCount($optionCount)
            ->setFormName($formName)
            ->setPlaceholder(__('This can be empty, default from Title'))
            ->setBillingPeriods($this->billingPeriodSource->getOptionArray())
            ->setStores($stores)
            ->toHtml();

        $fieldset->addField(
            'add_option',
            'label',
            [
                'name' => 'add_option',
                'label' => __(''),
                'title' => __(''),
                'after_element_html' => $addOptionHtml
            ]
        );
        $this->setForm($form);

        return $this;
    }

    /**
     * @param \Magento\Framework\Data\Form $form
     * @param \Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterface $option
     * @param string $formName
     * @param string $fieldsetId
     * @return \Magento\Framework\Data\Form
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function addOptionToForm($form, $option, $formName, $fieldsetId = 'template_options_fieldset')
    {
        /** @var \Magento\Framework\Data\Form $form */
        $fieldset = $form->addFieldset(
            $fieldsetId . $option->getId(),
            ['legend' => '']
        );

        $fieldset->addField(
            'id_' . $option->getId(),
            'hidden',
            [
                'name' => 'options['.$option->getId().'][id]',
                'value' => $option->getId(),
                'data-form-part' => $formName
            ]
        );

        $fieldset->addField(
            'title_'. $option->getId(),
            'text',
            [
                'name' => 'options['.$option->getId().'][title]',
                'label' => __('Title'),
                'title' => __('Title'),
                'required' => true,
                'value' => $option->getTitle(),
                'data-form-part' => $formName
            ]
        );
        $optionTranslateList = $this->templateOptionTranslateRepository->getListByOptionId($option->getId());

        $translateData = [];
        /** @var \Exto\Sarp\Api\Data\TemplateOptionTranslateInterface $translate */
        foreach ($optionTranslateList->getItems() as $translate) {
            $translateData[$translate->getStoreId()] = $translate->getValue();
        }
        $fieldset
            ->addField(
                'titles_' . $option->getId(),
                'text',
                [
                    'name' => 'options['.$option->getId().'][titles]',
                    'label' => __(''),
                    'title' => __(''),
                    'data-form-part' => $formName
                ]
            )
            ->setFieldCode('options['.$option->getId().'][titles]')
            ->setPlaceholder(__('This can be empty, default from Title'))
            ->setFormName($formName)
            ->setOptionValues($translateData)
            ->setRenderer($this->renderer)
        ;

        $billingPeriodHtml = $this->getLayout()->createBlock(
            Template::class
        )
            ->setTemplate('Exto_Sarp::template/renderer/option/billing.phtml')
            ->setFormName($formName)
            ->setFieldCode('options['.$option->getId().']')
            ->setBillingFrequency($option->getBillingFrequency())
            ->setBillingPeriod($option->getBillingPeriod())
            ->setBillingCycles($option->getBillingCycles())
            ->setBillingPeriods($this->billingPeriodSource->getOptionArray())
            ->toHtml();

        $fieldset->addField(
            'billing_period_' . $option->getId(),
            'label',
            [
                'name' => 'billing_period_' . $option->getId(),
                'label' => __('Bill Each'),
                'title' => __('Bill Each'),
                'after_element_html' => $billingPeriodHtml
            ]
        );

        $removeButtonHtml = $this->getLayout()->createBlock(
            Template::class
        )
            ->setTemplate('Exto_Sarp::template/renderer/option/remove.phtml')
            ->toHtml();

        $fieldset->addField(
            'remove_option_'.$option->getId(),
            'label',
            [
                'name' => 'remove_option_'.$option->getId(),
                'label' => __(''),
                'title' => __(''),
                'after_element_html' => $removeButtonHtml
            ]
        );

        return $form;
    }
}
