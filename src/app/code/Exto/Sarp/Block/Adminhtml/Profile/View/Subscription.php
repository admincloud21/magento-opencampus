<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Block\Adminhtml\Profile\View;

use Exto\Sarp\Block\Adminhtml\Profile\View;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Exto\Sarp\Model\ResourceModel\ProfileRepository;
use Magento\Catalog\Helper\Product;
use Magento\Backend\Block\Template\Context;

/**
 * Class Subscription
 */
class Subscription extends View
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var Product
     */
    protected $productHelper;

    /**
     * Subscription constructor.
     * @param Context $context
     * @param ProfileRepository $profileRepository
     * @param PriceCurrencyInterface $priceCurrencyInterface
     * @param ProductRepository $productRepository
     * @param Product $productHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        ProfileRepository $profileRepository,
        PriceCurrencyInterface $priceCurrencyInterface,
        ProductRepository $productRepository,
        Product $productHelper,
        array $data = []
    ) {
        parent::__construct($context, $profileRepository, $priceCurrencyInterface, $data);
        $this->productRepository = $productRepository;
        $this->productHelper = $productHelper;
    }

    /**
     * @return array
     */
    public function getSubscriptionData()
    {
        $itemsData = $this->getProfile()->getQuoteItemsData();
        $subscriptionData = [];
        $subscriptionData['items'] = [];
        foreach ($itemsData as $item) {
            try {
                $product = $this->productRepository->getById($item['product_id']);
            } catch (\Exception $e) {
                continue;
            }
            $subscriptionData['items'][$item['product_id']]['thumbnail'] = $this->productHelper->getImageUrl($product);
            $subscriptionData['items'][$item['product_id']]['url'] = $this->getUrl(
                'catalog/product/edit',
                ['id' => $product->getId()]
            );
            $subscriptionData['items'][$item['product_id']]['name'] = $product->getName();
            $subscriptionData['items'][$item['product_id']]['qty'] = $item['qty'];
            $subscriptionData['items'][$item['product_id']]['price'] = $item['price'];
        }
        $shippingData = $this->getProfile()->getShippingAddressData();
        $subscriptionData['subtotal'] = $shippingData['base_subtotal'];
        $subscriptionData['tax_total'] = $shippingData['base_tax_amount'];
        $subscriptionData['shipping_total'] = $shippingData['base_shipping_amount'];
        $subscriptionData['grand_total'] = $shippingData['base_grand_total'];

        return $subscriptionData;
    }
}
