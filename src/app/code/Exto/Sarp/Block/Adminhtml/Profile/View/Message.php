<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Block\Adminhtml\Profile\View;

use Exto\Sarp\Block\Adminhtml\Profile\View;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Exto\Sarp\Model\ResourceModel\ProfileRepository;
use Exto\Sarp\Model\ResourceModel\ProfileMessageRepository;
use Magento\Backend\Block\Template\Context;

/**
 * Class Message
 */
class Message extends View
{
    /**
     * @var ProfileMessageRepository
     */
    protected $profileMessageRepository;

    /**
     * Message constructor.
     * @param Context $context
     * @param ProfileRepository $profileRepository
     * @param PriceCurrencyInterface $priceCurrencyInterface
     * @param ProfileMessageRepository $profileMessageRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        ProfileRepository $profileRepository,
        PriceCurrencyInterface $priceCurrencyInterface,
        ProfileMessageRepository $profileMessageRepository,
        array $data = []
    ) {
        parent::__construct($context, $profileRepository, $priceCurrencyInterface, $data);
        $this->profileMessageRepository = $profileMessageRepository;
    }

    /**
     * @return array
     */
    public function getMessageLog()
    {
        $profileId = $this->getProfile()->getId();
        return $this->profileMessageRepository->getListByProfileId($profileId)->getItems();
    }
}
