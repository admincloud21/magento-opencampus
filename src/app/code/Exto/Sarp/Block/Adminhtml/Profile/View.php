<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Block\Adminhtml\Profile;

use Magento\Framework\Pricing\PriceCurrencyInterface;
use Exto\Sarp\Model\ResourceModel\ProfileRepository;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Template;
use Exto\Sarp\Model\Source\Profile\Status;

/**
 * Class View
 */
class View extends Template
{
    /**
     * @var ProfileRepository
     */
    protected $profileRepository;
    
    /**
     * @var \Exto\Sarp\Api\Data\ProfileInterface
     */
    protected $profile;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * View constructor.
     * @param Context $context
     * @param ProfileRepository $profileRepository
     * @param PriceCurrencyInterface $priceCurrency
     * @param array $data
     */
    public function __construct(
        Context $context,
        ProfileRepository $profileRepository,
        PriceCurrencyInterface $priceCurrency,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->profileRepository = $profileRepository;
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * @return \Exto\Sarp\Api\Data\ProfileInterface
     */
    public function getProfile()
    {
        if (null === $this->profile) {
            $this->profile = $this->profileRepository->get(
                $this->getRequest()->getParam('id', null)
            );
        }
        return $this->profile;
    }
    
    /**
     * @param int $price
     * @return float
     */
    public function formatPrice($price)
    {
        return $this->priceCurrency->format(
            $price,
            false,
            PriceCurrencyInterface::DEFAULT_PRECISION,
            $this->_storeManager->getDefaultStoreView()
        );
    }

    /**
     * Retrieve formatting date
     *
     * @param null|string|\DateTime $date
     * @param int $format
     * @param bool $showTime
     * @param null|string $timezone
     * @return string
     */
    public function prepareDate(
        $date = null,
        $format = \IntlDateFormatter::MEDIUM,
        $showTime = false,
        $timezone = null
    ) {
        return parent::formatDate($date, $format, $showTime, $timezone);
    }

    /**
     * @return array
     */
    public function getButtons()
    {
        $buttons = [];
        if (
            $this->getProfile()->getStatus() !== Status::PAUSED_VALUE
            && $this->getProfile()->getStatus() !== Status::CANCELED_VALUE
            && $this->getProfile()->getStatus() !== Status::COMPLETED_VALUE
        ) {
            $buttons[] = [
                'url' => $this->getUrl('*/*/changeStatus', [
                    'id' => $this->getProfile()->getId(),
                    'status' => Status::PAUSED_VALUE
                ]),
                'label' => __('Suspend')
            ];
        } elseif ($this->getProfile()->getStatus() === Status::PAUSED_VALUE) {
            $buttons[] = [
                'url' => $this->getUrl('*/*/changeStatus', [
                    'id' => $this->getProfile()->getId(),
                    'status' => Status::RESUME_ACTION
                ]),
                'label' => __('Activate')
            ];
        }

        if (
            $this->getProfile()->getStatus() == Status::ACTIVE_VALUE
            || $this->getProfile()->getStatus() == Status::SUSPEND_ACTION
        ) {
            $buttons[] = [
                'url' => $this->getUrl('*/*/cancel', ['id' => $this->getProfile()->getId()]),
                'label' => __('Cancel')
            ];
        }
        return $buttons;
    }
}
