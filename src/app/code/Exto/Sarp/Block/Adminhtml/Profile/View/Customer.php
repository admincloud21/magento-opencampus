<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Block\Adminhtml\Profile\View;

use Exto\Sarp\Block\Adminhtml\Profile\View;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Exto\Sarp\Model\ResourceModel\ProfileRepository;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Backend\Block\Template\Context;

/**
 * Class Customer
 */
class Customer extends View
{
    /**
     * @var CustomerRepository
     */
    protected $customerRepository;

    /**
     * @var CountryFactory
     */
    protected $countryFactory;
    
    /**
     * Customer constructor.
     * @param Context $context
     * @param ProfileRepository $profileRepository
     * @param PriceCurrencyInterface $priceCurrencyInterface
     * @param CustomerRepository $customerRepository
     * @param CountryFactory $countryFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        ProfileRepository $profileRepository,
        PriceCurrencyInterface $priceCurrencyInterface,
        CustomerRepository $customerRepository,
        CountryFactory $countryFactory,
        array $data = []
    ) {
        parent::__construct($context, $profileRepository, $priceCurrencyInterface, $data);
        $this->customerRepository = $customerRepository;
        $this->countryFactory = $countryFactory;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
        return $this->getProfile()->getCustomerName();
    }

    /**
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->getProfile()->getCustomerEmail();
    }

    /**
     * @return string
     */
    public function getCustomerUrl()
    {
        $url = '#';
        if ($this->getProfile()->getCustomerId()) {
            $url = $this->getUrl('customer/index/edit', ['id' => $this->getProfile()->getCustomerId()]);
        }
        return $url;
    }

    /**
     * @return string
     */
    public function getBillingAddress()
    {
        $data = $this->getProfile()->getBillingAddressData();
        $billingData[] = $data['street'];
        $billingData[] = $data['city'];
        $billingData[] = $data['region'];
        $billingData[] = $data['postcode'];
        $countryModel = $this->countryFactory->create();
        $countryModel->loadByCode($data['country_id']);
        $billingData[] = $countryModel->getName();

        $billingDataHtml = '';
        $billingDataHtml .= implode(', ', $billingData);
        $billingDataHtml .= '<br>T: ' . $data['telephone'] .' ';
        $billingDataHtml .= 'F: ' . $data['fax'] .' ';
        return $billingDataHtml;
    }

    /**
     * @return string
     */
    public function getShippingAddress()
    {
        $data = $this->getProfile()->getShippingAddressData();
        $shippingData[] = $data['street'];
        $shippingData[] = $data['city'];
        $shippingData[] = $data['region'];
        $shippingData[] = $data['postcode'];
        try {
            $countryModel = $this->countryFactory->create();
            $countryModel->loadByCode($data['country_id']);
            $shippingData[] = $countryModel->getName();
        } catch (\Exception $e) {}
        
        $shippingDataHtml = 'Ship to: <br>';
        $shippingDataHtml .= $data['firstname'] . ' ' . $data['lastname'];
        $shippingDataHtml .= ' <' . $data['email'] . '>';
        $shippingDataHtml .= implode(', ', $shippingData);
        $shippingDataHtml .= '<br>T: ' . $data['telephone'] .' ';
        $shippingDataHtml .= 'F: ' . $data['fax'] .' ';
        return $shippingDataHtml;
    }
}
