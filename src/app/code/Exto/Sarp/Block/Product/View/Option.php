<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Block\Product\View;

use Exto\Sarp\Model\Product\ProductManagement;
use Exto\Sarp\Block\Product\View\Element\Html\Date;
use Exto\Sarp\Model\ResourceModel as SarpResourceModel;
use Magento\Catalog\Block\Product\View;
use Magento\Catalog\Block\Product\Context;
use Magento\Framework\Url\EncoderInterface as UrlEncoder;
use Magento\Framework\Json\EncoderInterface as JsonEncoder;
use Magento\Framework\Stdlib\StringUtils;
use Magento\Catalog\Helper\Product as ProductHelper;
use Magento\Catalog\Model\ProductTypes\ConfigInterface;
use Magento\Framework\Locale\FormatInterface;
use Magento\Customer\Model\Session;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Exto\Sarp\Model\Source\Subscription\Template as SarpTemplate;
use Exto\Sarp\Model\Source\Template\OptionStyle;
use Exto\Sarp\Model\Config as Config;

/**
 * Class Option
 */
class Option extends View
{
    /**
     * @var \Magento\Catalog\Model\Product\Media\Config
     */
    protected $mediaConfig;

    /**
     * @var \Magento\Framework\View\Element\Html\Date
     */
    protected $dateElement;

    /**
     * @var \Exto\Sarp\Model\ResourceModel\SubscriptionTemplateRepository
     */
    protected $templateRepository;

    /**
     * @var \Exto\Sarp\Model\ResourceModel\SubscriptionTemplateOption\CollectionFactory
     */
    protected $templateOptionCollectionFactory;

    /**
     * @var Config
     */
    protected $config;

    /**
     * Option constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Url\EncoderInterface $urlEncoder
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Catalog\Helper\Product $productHelper
     * @param \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig
     * @param \Magento\Framework\Locale\FormatInterface $localeFormat
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Exto\Sarp\Model\ResourceModel\SubscriptionTemplateOption\CollectionFactory $optionCollectionFactory
     * @param \Exto\Sarp\Model\ResourceModel\SubscriptionTemplateRepository $templateRepository
     * @param Config $config
     * @param Element\Html\Date $dateElement
     * @param array $data
     */
    public function __construct(
        Context $context,
        UrlEncoder $urlEncoder,
        JsonEncoder $jsonEncoder,
        StringUtils $string,
        ProductHelper $productHelper,
        ConfigInterface $productTypeConfig,
        FormatInterface $localeFormat,
        Session $customerSession,
        ProductRepositoryInterface $productRepository,
        PriceCurrencyInterface $priceCurrency,
        SarpResourceModel\SubscriptionTemplateOption\CollectionFactory $optionCollectionFactory,
        SarpResourceModel\SubscriptionTemplateRepository $templateRepository,
        Config $config,
        Date $dateElement,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $urlEncoder,
            $jsonEncoder,
            $string,
            $productHelper,
            $productTypeConfig,
            $localeFormat,
            $customerSession,
            $productRepository,
            $priceCurrency,
            $data
        );

        $this->dateElement = $dateElement;
        $this->templateRepository = $templateRepository;
        $this->templateOptionCollectionFactory = $optionCollectionFactory;
        $this->config = $config;
    }

    /**
     * @return \Magento\Framework\Phrase|mixed
     */
    public function getSubscriptionLabel()
    {
        $label = $this->config->getBlockName();
        if (!$label) {
            $label = __('Subscribe for regular delivery');
        }
        return $label;
    }

    /**
     * @return bool
     */
    public function isSelectOptionStyle()
    {
        $result = true;
        /** @var \Exto\Sarp\Api\Data\SubscriptionTemplateInterface|null $template */
        $template = $this->getSarpTemplate();
        if (null !== $template) {
            if ($template->getOptionStyle() !== OptionStyle::SELECT_VALUE) {
                $result = false;
            }
        }
        return $result;
    }

    /**
     * @return \Magento\Framework\Phrase|mixed
     */
    public function getStartBillingLabel()
    {
        $label = $this->config->getStartDateLabel();
        if (!$label) {
            $label = __('Start billing from:');
        }
        return $label;
    }

    /**
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateInterface|null
     */
    protected function getSarpTemplate()
    {
        $productTemplateId = $this->getProduct()->getData(ProductManagement::TEMPLATE_PRODUCT_ATTRIBUTE_CODE);
        try {
            $template = $this->templateRepository->get($productTemplateId);
        } catch (\Exception $e) {
            $template = null;
        }
        return $template;
    }

    /**
     * @return array
     */
    public function getTemplateOptions()
    {
        $storeId = $this->_storeManager->getStore()->getId();
        $template = $this->getSarpTemplate();
        $options = [];
        if (null !== $template) {
            $templateOptionsCollection = $this->templateOptionCollectionFactory->create();
            $options = $templateOptionsCollection->getTemplateOptionsByTemplateId($template->getId(), $storeId);
            if (!$template->getIsSubscriptionOnly()) {
                $noSubscription = SarpTemplate::getNoSubscriptionOption();
                $options = $noSubscription + $options;
            }
        }

        return $options;
    }

    /**
     * @return bool
     */
    public function isDisplayDatePicker()
    {
        $template = $this->getSarpTemplate();
        $result = false;
        if (null !== $template && $template->getIsChooseBillingDate()) {
            $result = true;
        }
        return $result;
    }

    /**
     * @return string
     */
    public function getDatePickerHtml()
    {
        $this->dateElement->setData([
            'name' => 'exto_sarp_start_billing_date',
            'id' => 'exto_sarp_start_billing_date',
            'date_format' => $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT),
            'image' => $this->getViewFileUrl('Magento_Theme::calendar.png'),
            'years_range' => '',
            'min_date' => '0d',
            'change_month' => 'true',
            'change_year' => 'false',
            'show_on' => 'both'
        ]);
        return $this->dateElement->getHtml();
    }
}
