<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Block\Product\View\Element\Html;

use Magento\Framework\Phrase;

/**
 * Class Date
 */
class Date extends \Magento\Framework\View\Element\Html\Date
{
    /**
     * @return string
     */
    protected function _toHtml()
    {
        $html = '<input type="text" name="' . $this->getName() . '" id="' . $this->getId() . '" ';
        $html .= 'value="' . $this->escapeHtml($this->getValue()) . '" ';
        $html .= 'class="' . $this->getClass() . '" ' . $this->getExtraParams() . '/> ';
        $calendarYearsRange = $this->getYearsRange();
        $changeMonth = $this->getChangeMonth();
        $changeYear = $this->getChangeYear();
        $maxDate = $this->getMaxDate();
        $minDate = $this->getMinDate();
        $showOn = $this->getShowOn();

        $html .= '<script type="text/javascript">
            require(["jquery", "mage/calendar"], function($){
                    $("#' . $this->getId() . '").calendar({
                        showsTime: false,
                        timeFormat: "",
                        dateFormat: "' . $this->getDateFormat() . '",
                        buttonImage: "' . $this->getImage() . '",
                        yearRange: "' .$calendarYearsRange . '",
                        buttonText: "' . (string)new Phrase('Select Date')
            . '"' . ($maxDate ? ', maxDate: "' . $maxDate . '"' : '') .
            ($minDate ? ', minDate: "' . $minDate . '"' : '') .
            ($changeMonth === null ? '' : ', changeMonth: ' . $changeMonth) .
            ($changeYear === null ? '' : ', changeYear: ' . $changeYear) .
            ($showOn ? ', showOn: "' . $showOn . '"' : '') .
            '})
            });
            </script>';

        return $html;
    }
}
