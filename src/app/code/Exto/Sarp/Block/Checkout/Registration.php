<?php
namespace Exto\Sarp\Block\Checkout;

use Magento\Framework\View\Element\Template;
use Exto\Sarp\Model\ResourceModel\ProfileRepository;

/**
 * Class Registration
 */
class Registration extends \Magento\Checkout\Block\Registration
{
    /** @var ProfileRepository */
    protected $profileRepository;

    /**
     * @param Template\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Model\Registration $registration
     * @param \Magento\Customer\Api\AccountManagementInterface $accountManagement
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Sales\Model\Order\Address\Validator $addressValidator
     * @param ProfileRepository $profileRepository
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Registration $registration,
        \Magento\Customer\Api\AccountManagementInterface $accountManagement,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Model\Order\Address\Validator $addressValidator,
        ProfileRepository $profileRepository,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $checkoutSession,
            $customerSession,
            $registration,
            $accountManagement,
            $orderRepository,
            $addressValidator,
            $data
        );
        $this->profileRepository = $profileRepository;
    }

    /**
     * @return {@inheritdoc}
     */
    public function toHtml()
    {
        if (!$this->checkoutSession->getLastRecurringProfileIds()) {
            return '';
        }
        return parent::toHtml();
    }

    /**
     * {@inheritdoc}
     */
    protected function validateAddresses()
    {
        return true;
    }

    /**
     * @return string
     */
    public function getEmailAddress()
    {
        $ids = $this->checkoutSession->getLastRecurringProfileIds();
        $profile = $this->profileRepository->get(array_shift($ids));
        $addressData = $profile->getBillingAddressData();
        return $addressData['email'];
    }
}