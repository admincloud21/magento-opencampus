<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Block\Checkout\Onepage;

use Magento\Framework\View\Element\Template\Context;
use Magento\Checkout\Model\Session;
use Exto\Sarp\Model\ResourceModel\ProfileRepository;

/**
 * Class Success
 */
class Success extends \Magento\Framework\View\Element\Template
{
    /** @var ProfileRepository */
    protected $profileRepository;
    
    /** @var \Magento\Checkout\Model\Session */
    protected $checkoutSession;

    /**
     * Success constructor.
     * @param Context $context
     * @param Session $checkoutSession
     * @param ProfileRepository $profileRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $checkoutSession,
        ProfileRepository $profileRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->checkoutSession = $checkoutSession;
        $this->profileRepository = $profileRepository;
    }

    /**
     * @return \Exto\Sarp\Api\Data\ProfileInterface[]
     */
    public function getProfileList()
    {
        $ids = $this->checkoutSession->getLastRecurringProfileIds();
        if (null === $ids) {
            return [];
        }
        /** @var \Exto\Sarp\Api\Data\ProfileSearchResultsInterface $profiles */
        $profiles = $this->profileRepository->getListByIds($ids);
        return $profiles->getItems();
    }

    public function getCheckoutSession()
    {
        return $this->checkoutSession;
    }

    /**
     * @return bool
     */
    public function isCanShowMessages()
    {
        return count($this->getProfileList()) > 0;
    }

    /**
     * @return string
     */
    public function getProfileIdsHtml()
    {
        $profileHtmlList = [];
        foreach ($this->getProfileList() as $profile) {
            $profileHtmlList[] = '<span><a href="' . $this->getProfileUrl($profile) . '">'
                . $this->escapeHtml($profile->getProfileId())
                . '</a></span>'
            ;
        }
        return join(', ', $profileHtmlList);
    }

    /**
     * @param \Exto\Sarp\Model\Profile $profile
     *
     * @return string
     */
    public function getProfileUrl($profile)
    {
        return $this->getUrl('exto_sarp/customer_profile/view', ['id' => $profile->getId()]);
    }
}
