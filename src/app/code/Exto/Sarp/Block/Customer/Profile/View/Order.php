<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Block\Customer\Profile\View;

use Exto\Sarp\Block\Customer\Profile\View;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Exto\Sarp\Model\ResourceModel\ProfileRepository;
use Magento\Sales\Model\Order\Config;
use Magento\Framework\View\Element\Template;
use Exto\Sarp\Model\Profile\OrderManagement;

/**
 * Class Order
 */
class Order extends View
{
    /**
     * @var OrderManagement
     */
    protected $orderManagement;
    
    /**
     * @var Config
     */
    protected $orderConfig;

    /**
     * Order constructor.
     * @param Template\Context $context
     * @param ProfileRepository $profileRepository
     * @param PriceCurrencyInterface $priceCurrencyInterface
     * @param OrderManagement $orderManagement
     * @param Config $orderConfig
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        ProfileRepository $profileRepository,
        PriceCurrencyInterface $priceCurrencyInterface,
        OrderManagement $orderManagement,
        Config $orderConfig,
        array $data = []
    ) {
        parent::__construct($context, $profileRepository, $priceCurrencyInterface, $data);
        $this->orderManagement = $orderManagement;
        $this->orderConfig = $orderConfig;
    }

    /**
     * @return array
     */
    public function getOrderData()
    {
        $profileId = $this->getProfile()->getId();
        return $this->orderManagement->getOrderListByProfileId($profileId)->getItems();
    }

    /**
     * @param int $orderId
     * @return string
     */
    public function getOrderUrl($orderId)
    {
        return $this->getUrl('sales/order/view', ['order_id' => $orderId]);
    }

    /**
     * @param string $statusCode
     * @return string
     */
    public function getOrderStatus($statusCode)
    {
        return $this->orderConfig->getStatusLabel($statusCode);
    }
}
