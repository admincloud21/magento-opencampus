<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Block\Customer\Profile;

use Magento\Framework\View\Element\Template as Template;
use Magento\Customer\Model\Session;
use Magento\Theme\Block\Html\Pager;
use Exto\Sarp\Model\Profile;
use Exto\Sarp\Model\ResourceModel\Profile\Collection as ProfileCollection;
use Exto\Sarp\Model\ResourceModel\Profile\CollectionFactory as ProfileCollectionFactory;
use Exto\Sarp\Model\Source\Profile\Status as ProfileStatus;

/**
 * Class Grid
 */
class Grid extends Template
{
    /** @var ProfileCollection */
    protected $profileCollectionFactory;

    /** @var \Magento\Customer\Model\Session */
    protected $customerSession;

    /**
     * @var ProfileCollection|null
     */
    protected $collection = null;

    /**
     * Grid constructor.
     * @param Template\Context $context
     * @param ProfileCollectionFactory $profileCollectionFactory
     * @param Session $customerSession
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        ProfileCollectionFactory $profileCollectionFactory,
        Session $customerSession,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->profileCollectionFactory = $profileCollectionFactory;
        $this->customerSession = $customerSession;
    }

    /**
     * @return Profile[]
     */
    public function getItems()
    {
        return $this->getCollection()->getItems();
    }

    /**
     * @return bool
     */
    public function hasItems()
    {
        return $this->getCollection()->getSize() > 0;
    }

    /**
     * @param Profile $profile
     * @return string
     */
    public function getStatusLabel(Profile $profile)
    {
        $options = ProfileStatus::getOptionArray();
        if (!array_key_exists($profile->getStatus(), $options)) {
            return __('Unknown');
        }
        return $options[$profile->getStatus()];
    }

    /**
     * @param Profile $profile
     * @return string
     */
    public function getChangeStatusUrl(Profile $profile)
    {
        $statusCode = $this->getAvailableStatusCode($profile);
        return $this->getUrl(
            'exto_sarp/customer_profile/changeStatus',
            ['id' => $profile->getId(), 'status' => $statusCode]
        );
    }

    /**
     * @param Profile $profile
     * @return string|null
     */
    public function getChangeStatusTitle(Profile $profile)
    {
        $statusCode = $this->getAvailableStatusCode($profile);
        $status = null;
        switch ($statusCode) {
            case ProfileStatus::ACTIVE_VALUE:
                $status = __('Activate');
                break;
            case ProfileStatus::PAUSED_VALUE:
                $status = __('Pause');
                break;
        }
        return $status;
    }

    /**
     * @param Profile $profile
     * @return string
     */
    public function getViewUrl(Profile $profile)
    {
        return $this->getUrl(
            'exto_sarp/customer_profile/view',
            ['id' => $profile->getId()]
        );
    }

    /**
     * @param Profile $profile
     * @return string
     */
    public function getCancelUrl(Profile $profile)
    {
        return $this->getUrl(
            'exto_sarp/customer_profile/cancel',
            ['id' => $profile->getId()]
        );
    }

    /**
     * @param Profile $profile
     * @return string|null
     */
    public function getAvailableStatusCode(Profile $profile)
    {
        $status = null;
        switch ($profile->getStatus()) {
            case ProfileStatus::ACTIVE_VALUE:
            case ProfileStatus::PENDING_VALUE:
                $status = ProfileStatus::PAUSED_VALUE;
                break;
            case ProfileStatus::PAUSED_VALUE:
                $status = ProfileStatus::ACTIVE_VALUE;
                break;
        }
        return $status;
    }

    /**
     * @param Profile $profile
     * @return bool
     */
    public function isCanChangeStatus(Profile $profile)
    {
        return null !== $this->getAvailableStatusCode($profile);
    }

    /**
     * @param Profile $profile
     * @return bool
     */
    public function isCanCancel(Profile $profile)
    {
        switch ($profile->getStatus()) {
            case ProfileStatus::ACTIVE_VALUE:
            case ProfileStatus::SUSPEND_ACTION:
                return true;
        }
        return false;
    }

    /**
     * @param string $date
     * @return string
     */
    public function dateFormat($date)
    {
        return $this->formatDate($date, \IntlDateFormatter::MEDIUM);
    }

    /**
     * @return ProfileCollection
     */
    protected function getCollection()
    {
        if (null === $this->collection) {
            /** @var ProfileCollection $collection */
            $this->collection = $this->profileCollectionFactory->create();
            $this->collection->addCustomerIdFilter($this->customerSession->getCustomerId());
        }
        return $this->collection;
    }

    /**
     * @return string
     */
    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        $toolbar = $this->getLayout()->createBlock(
            Pager::class,
            'exto_sarp_profile_list.toolbar'
        )->setCollection(
            $this->getCollection()
        );
        $this->setChild('toolbar', $toolbar);
        return parent::_prepareLayout();
    }
}
