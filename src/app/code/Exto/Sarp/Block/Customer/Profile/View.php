<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Block\Customer\Profile;

use Magento\Framework\Profiler\Driver\Standard\Stat;
use Magento\Framework\View\Element\Template;
use Exto\Sarp\Model\ResourceModel\ProfileRepository;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Exto\Sarp\Model\Source\Profile\Status;

/**
 * Class View
 */
class View extends Template
{
    /** @var ProfileRepository */
    protected $profileRepository;

    /** @var PriceCurrencyInterface */
    protected $priceCurrency;

    /** @var \Exto\Sarp\Api\Data\ProfileInterface */
    protected $profile = null;

    /**
     * View constructor.
     * @param Template\Context $context
     * @param ProfileRepository $profileRepository
     * @param PriceCurrencyInterface $priceCurrency
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        ProfileRepository $profileRepository,
        PriceCurrencyInterface $priceCurrency,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->profileRepository = $profileRepository;
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * @return \Exto\Sarp\Api\Data\ProfileInterface
     */
    public function getProfile()
    {
        if (null === $this->profile) {
            $this->profile = $this->profileRepository->get(
                $this->getRequest()->getParam('id', null)
            );
        }
        return $this->profile;
    }

    /**
     * @param int $price
     * @return float
     */
    public function formatPrice($price)
    {
        return $this->priceCurrency->format(
            $price,
            false,
            PriceCurrencyInterface::DEFAULT_PRECISION,
            $this->_storeManager->getStore()
        );
    }

    /**
     * Retrieve formatting date
     *
     * @param null|string|\DateTime $date
     * @param int $format
     * @param bool $showTime
     * @param null|string $timezone
     * @return string
     */
    public function prepareDate(
        $date = null,
        $format = \IntlDateFormatter::MEDIUM,
        $showTime = false,
        $timezone = null
    ) {
        return parent::formatDate($date, $format, $showTime, $timezone);
    }

    /**
     * @return string
     */
    public function getCancelUrl()
    {
        return $this->getUrl(
            'exto_sarp/customer_profile/cancel',
            ['id' => $this->getProfile()->getId()]
        );
    }

    /**
     * @return string|null
     */
    public function getAvailableStatusCode()
    {
        $status = null;
        switch ($this->getProfile()->getStatus()) {
            case Status::ACTIVE_VALUE:
            case Status::PENDING_VALUE:
                $status = Status::PAUSED_VALUE;
                break;
            case Status::PAUSED_VALUE:
                $status = Status::ACTIVE_VALUE;
                break;
        }
        return $status;
    }

    /**
     * @return bool
     */
    public function isCanChangeStatus()
    {
        return null !== $this->getAvailableStatusCode();
    }

    /**
     * @return bool
     */
    public function isCanCancel()
    {
        switch ($this->getProfile()->getStatus()) {
            case Status::ACTIVE_VALUE:
            case Status::SUSPEND_ACTION:
                return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getChangeStatusUrl()
    {
        $statusCode = $this->getAvailableStatusCode();
        return $this->getUrl(
            'exto_sarp/customer_profile/changeStatus',
            ['id' => $this->getProfile()->getId(), 'status' => $statusCode]
        );
    }

    /**
     * @return string|null
     */
    public function getChangeStatusTitle()
    {
        $statusCode = $this->getAvailableStatusCode();
        $status = null;
        switch ($statusCode) {
            case Status::ACTIVE_VALUE:
                $status = __('Activate');
                break;
            case Status::PAUSED_VALUE:
                $status = __('Pause');
                break;
        }
        return $status;
    }
}
