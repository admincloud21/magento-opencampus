<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Block\Customer\Profile\View;

use Exto\Sarp\Block\Customer\Profile\View;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Exto\Sarp\Model\ResourceModel\ProfileRepository;
use Exto\Sarp\Model\Source\Profile\Status;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\Order\Config;

/**
 * Class Profile
 */
class Profile extends View
{
    /**
     * @var Status
     */
    protected $statusSource;

    /**
     * @var Config
     */
    protected $orderConfig;

    /**
     * Profile constructor.
     * @param Template\Context $context
     * @param ProfileRepository $profileRepository
     * @param PriceCurrencyInterface $priceCurrencyInterface
     * @param Status $statusSource
     * @param Config $orderConfig
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        ProfileRepository $profileRepository,
        PriceCurrencyInterface $priceCurrencyInterface,
        Status $statusSource,
        Config $orderConfig,
        array $data = []
    ) {
        parent::__construct($context, $profileRepository, $priceCurrencyInterface, $data);
        $this->statusSource = $statusSource;
        $this->orderConfig = $orderConfig;
    }
    
    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->getProfile()->getTitle();
    }

    /**
     * @return string
     */
    public function getStatusCode()
    {
        return $this->getProfile()->getStatus();
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        $optionArray = Status::getOptionArray();
        $status = __('Unknown');
        if (array_key_exists($this->getProfile()->getStatus(), $optionArray)) {
            $status =  $optionArray[$this->getProfile()->getStatus()];
        }
        return $status;
    }

    /**
     * @return string
     */
    public function getProfileId()
    {
        return $this->getProfile()->getProfileId();
    }

    /**
     * @return string
     */
    public function getPaypalId()
    {
        return $this->getProfile()->getExternalServiceProfileId();
    }

    /**
     * @return string
     */
    public function getFirstBillingDate()
    {
        $date = $this->getProfile()->getBillingStartDate();
        return $this->getProfileFormattedDate($date);
    }

    /**
     * @return string
     */
    public function getLastSuccessfulBillingDate()
    {
        $date = $this->getProfile()->getLastSuccessfulDate();
        return $this->getProfileFormattedDate($date);
    }

    /**
     * @param string|null $date
     * @return string
     */
    private function getProfileFormattedDate($date)
    {
        if (null !== $date) {
            $result = $this->prepareDate($date);
        } else {
            $result = '';
        }
        return $result;
    }

    /**
     * @return int
     */
    public function getMaxPaymentFailures()
    {
        return $this->getProfile()->getMaxPaymentFailures();
    }

    /**
     * @return string
     */
    public function getDefaultOrderStatus()
    {
        return $this->getProfile()->getDefaultOrderStatus();
    }

    /**
     * @param string $statusCode
     * @return string
     */
    public function getOrderStatus($statusCode)
    {
        return $this->orderConfig->getStatusLabel($statusCode);
    }
}
