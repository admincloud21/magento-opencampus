<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Api;

/**
 * @api
 */
interface ProfileRepositoryInterface
{
    /**
     * Create profile service
     *
     * @param \Exto\Sarp\Api\Data\ProfileInterface $profile
     * @return \Exto\Sarp\Api\Data\ProfileInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Exto\Sarp\Api\Data\ProfileInterface $profile);

    /**
     * Get info about profile by profile id
     *
     * @param int $profileId
     * @return \Exto\Sarp\Api\Data\ProfileInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($profileId);

    /**
     * Delete profile
     *
     * @param \Exto\Sarp\Api\Data\ProfileInterface $profile
     * @return void
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function delete(\Exto\Sarp\Api\Data\ProfileInterface $profile);

    /**
     * Retrieve list of profiles
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Exto\Sarp\Api\Data\ProfileSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Retrieve list of profiles by profile ids
     *
     * @param array $ids
     * @return \Exto\Sarp\Api\Data\ProfileSearchResultsInterface
     */
    public function getListByIds($ids);
}
