<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Api\Data;

/**
 * @api
 */
interface SubscriptionTemplateInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{
    
    const ID = 'id';
    const TITLE = 'title';
    const IS_SUBSCRIPTION_ONLY = 'is_subscription_only';
    const IS_CHOOSE_BILLING_DATE = 'is_choose_billing_date';
    const MAX_PAYMENT_FAILURES = 'max_payment_failures';
    const DEFAULT_ORDER_STATUS = 'default_order_status';
    const OPTION_STYLE = 'option_style';
    const CREATED_AT = 'created_at';
    const IS_DELETED = 'is_deleted';

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * @return int
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsSubscriptionOnly();

    /**
     * @param int $isSubscriptionOnly
     * @return $this
     */
    public function setIsSubscriptionOnly($isSubscriptionOnly);

    /**
     * @return int
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsChooseBillingDate();

    /**
     * @param int $isChooseBillingDate
     * @return $this
     */
    public function setIsChooseBillingDate($isChooseBillingDate);

    /**
     * @return int
     */
    public function getMaxPaymentFailures();

    /**
     * @param int $maxPaymentFailures
     * @return $this
     */
    public function setMaxPaymentFailures($maxPaymentFailures);

    /**
     * @return string
     */
    public function getDefaultOrderStatus();

    /**
     * @param string $defaultOrderStatus
     * @return $this
     */
    public function setDefaultOrderStatus($defaultOrderStatus);

    /**
     * @return string
     */
    public function getOptionStyle();

    /**
     * @param string $optionStyle
     * @return $this
     */
    public function setOptionStyle($optionStyle);

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsDeleted();

    /**
     * @param bool $isDeleted
     * @return $this
     */
    public function setIsDeleted($isDeleted);
}
