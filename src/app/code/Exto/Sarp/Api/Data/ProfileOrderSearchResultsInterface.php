<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Api\Data;

/**
 * @api
 */
interface ProfileOrderSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get attributes list.
     *
     * @return \Exto\Sarp\Api\Data\ProfileOrderInterface[]
     */
    public function getItems();

    /**
     * Set attributes list.
     *
     * @param \Exto\Sarp\Api\Data\ProfileOrderInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
