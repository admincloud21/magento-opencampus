<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Api\Data;

/**
 * @api
 */
interface SubscriptionTemplateOptionInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{
    
    const ID = 'id';
    const TEMPLATE_ID = 'template_id';
    const TITLE = 'title';
    const BILLING_FREQUENCY = 'billing_frequency';
    const BILLING_PERIOD = 'billing_period';
    const BILLING_CYCLES = 'billing_cycles';

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getTemplateId();

    /**
     * @param int $templateId
     * @return $this
     */
    public function setTemplateId($templateId);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * @return int
     */
    public function getBillingFrequency();

    /**
     * @param int $billingFrequency
     * @return $this
     */
    public function setBillingFrequency($billingFrequency);

    /**
     * @return string
     */
    public function getBillingPeriod();

    /**
     * @param string $billingPeriod
     * @return $this
     */
    public function setBillingPeriod($billingPeriod);

    /**
     * @return int
     */
    public function getBillingCycles();

    /**
     * @param int $billingCycles
     * @return $this
     */
    public function setBillingCycles($billingCycles);
}
