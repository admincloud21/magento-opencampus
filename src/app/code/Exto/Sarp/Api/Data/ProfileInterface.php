<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Api\Data;

/**
 * @api
 */
interface ProfileInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{
    const ID = 'id';
    const PROFILE_ID = 'profile_id';
    const CUSTOMER_ID = 'customer_id';
    const CUSTOMER_NAME = 'customer_name';
    const CUSTOMER_EMAIL = 'customer_email';
    const EXTERNAL_SERVICE_PROFILE_ID = 'external_service_profile_id';
    const STATUS = 'status';
    const TITLE = 'title';
    const CREATED_DATE = 'created_date';
    const COMPLETED_DATE = 'completed_date';
    const BILLING_START_DATE = 'billing_start_date';
    const LAST_SUCCESSFUL_DATE = 'last_successful_date';
    const MAX_PAYMENT_FAILURES = 'max_payment_failures';
    const DEFAULT_ORDER_STATUS = 'default_order_status';
    const BILLING_FREQUENCY = 'billing_frequency';
    const BILLING_PERIOD = 'billing_period';
    const BILLING_CYCLES = 'billing_cycles';
    const QUOTE_DATA = 'quote_data';
    const QUOTE_ITEMS_DATA = 'quote_items_data';
    const BILLING_ADDRESS_DATA = 'billing_address_data';
    const SHIPPING_ADDRESS_DATA = 'shipping_address_data';
    const PRODUCT_IDS = 'product_ids';
    const IS_DELETED = 'is_deleted';

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getProfileId();

    /**
     * @param string $profileId
     * @return $this
     */
    public function setProfileId($profileId);

    /**
     * @return int
     */
    public function getCustomerId();

    /**
     * @param int|null $customerId
     * @return $this
     */
    public function setCustomerId($customerId);

    /**
     * @return string
     */
    public function getCustomerName();

    /**
     * @param string $customerName
     * @return $this
     */
    public function setCustomerName($customerName);

    /**
     * @return string
     */
    public function getCustomerEmail();

    /**
     * @param string $customerEmail
     * @return $this
     */
    public function setCustomerEmail($customerEmail);

    /**
     * @return string
     */
    public function getExternalServiceProfileId();

    /**
     * @param string $externalServiceProfileId
     * @return $this
     */
    public function setExternalServiceProfileId($externalServiceProfileId);

    /**
     * @return string
     */
    public function getStatus();

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * @return string
     */
    public function getCreatedDate();

    /**
     * @param string $createdDate
     * @return $this
     */
    public function setCreatedDate($createdDate);

    /**
     * @return string
     */
    public function getCompletedDate();

    /**
     * @param string $completedDate
     * @return $this
     */
    public function setCompletedDate($completedDate);

    /**
     * @return string
     */
    public function getBillingStartDate();

    /**
     * @param string $billingStartDate
     * @return $this
     */
    public function setBillingStartDate($billingStartDate);
    
    /**
     * @return string
     */
    public function getLastSuccessfulDate();

    /**
     * @param string $lastSuccessfulDate
     * @return $this
     */
    public function setLastSuccessfulDate($lastSuccessfulDate);

    /**
     * @return int
     */
    public function getMaxPaymentFailures();

    /**
     * @param int $maxPaymentFailures
     * @return $this
     */
    public function setMaxPaymentFailures($maxPaymentFailures);

    /**
     * @return string
     */
    public function getDefaultOrderStatus();

    /**
     * @param string $defaultOrderStatus
     * @return $this
     */
    public function setDefaultOrderStatus($defaultOrderStatus);

    /**
     * @return int
     */
    public function getBillingFrequency();

    /**
     * @param int $billingFrequency
     * @return $this
     */
    public function setBillingFrequency($billingFrequency);

    /**
     * @return string
     */
    public function getBillingPeriod();

    /**
     * @param string $billingPeriod
     * @return $this
     */
    public function setBillingPeriod($billingPeriod);

    /**
     * @return int
     */
    public function getBillingCycles();

    /**
     * @param int $billingCycles
     * @return $this
     */
    public function setBillingCycles($billingCycles);

    /**
     * @return mixed[]|null
     */
    public function getQuoteData();

    /**
     * @param mixed[]|null $quoteData
     * @return $this
     */
    public function setQuoteData($quoteData);

    /**
     * @return mixed[]|null
     */
    public function getQuoteItemsData();

    /**
     * @param mixed[]|null $quoteItemsData
     * @return $this
     */
    public function setQuoteItemsData($quoteItemsData);

    /**
     * @return mixed[]|null
     */
    public function getBillingAddressData();

    /**
     * @param mixed[]|null $billingAddressData
     * @return $this
     */
    public function setBillingAddressData($billingAddressData);

    /**
     * @return mixed[]|null
     */
    public function getShippingAddressData();

    /**
     * @param mixed[]|null $shippingAddressData
     * @return $this
     */
    public function setShippingAddressData($shippingAddressData);

    /**
     * @return string
     */
    public function getProductIds();

    /**
     * @param string $productIds
     * @return $this
     */
    public function setProductIds($productIds);

    /**
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsDeleted();

    /**
     * @param bool $isDeleted
     * @return $this
     */
    public function setIsDeleted($isDeleted);
}
