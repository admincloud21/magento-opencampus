<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Api;

/**
 * @api
 */
interface SubscriptionTemplateOptionRepositoryInterface
{
    /**
     * Create subscriptionTemplateOption service
     *
     * @param \Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterface $subscriptionTemplateOption
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterface $subscriptionTemplateOption);

    /**
     * Get info about subscriptionTemplateOption by subscriptionTemplateOption id
     *
     * @param int $subscriptionTemplateOptionId
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($subscriptionTemplateOptionId);

    /**
     * Delete subscriptionTemplateOption
     *
     * @param \Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterface $subscriptionTemplateOption
     * @return void
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function delete(\Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterface $subscriptionTemplateOption);

    /**
     * Retrieve list of template options
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateOptionSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Retrieve list of template options by template id
     *
     * @param int $templateId
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateOptionSearchResultsInterface
     */
    public function getListByTemplateId($templateId);
}
