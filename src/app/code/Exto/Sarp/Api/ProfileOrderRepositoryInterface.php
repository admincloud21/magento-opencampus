<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Api;

/**
 * @api
 */
interface ProfileOrderRepositoryInterface
{
    /**
     * Create profileOrder service
     *
     * @param \Exto\Sarp\Api\Data\ProfileOrderInterface $profileOrder
     * @return \Exto\Sarp\Api\Data\ProfileOrderInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Exto\Sarp\Api\Data\ProfileOrderInterface $profileOrder);

    /**
     * Get info about profileOrder by profileOrder id
     *
     * @param int $profileOrderId
     * @return \Exto\Sarp\Api\Data\ProfileOrderInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($profileOrderId);

    /**
     * Delete profileOrder
     *
     * @param \Exto\Sarp\Api\Data\ProfileOrderInterface $profileOrder
     * @return void
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function delete(\Exto\Sarp\Api\Data\ProfileOrderInterface $profileOrder);

    /**
     * Retrieve list of profile orders
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Exto\Sarp\Api\Data\ProfileOrderSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Retrieve list of profile orders by profile id
     *
     * @param int $id
     * @return \Exto\Sarp\Api\Data\ProfileMessageSearchResultsInterface
     */
    public function getListByProfileId($id);
}
