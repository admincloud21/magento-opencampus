<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Api;

/**
 * @api
 */
interface TemplateOptionTranslateRepositoryInterface
{
    /**
     * Create templateOptionTranslate service
     *
     * @param \Exto\Sarp\Api\Data\TemplateOptionTranslateInterface $templateOptionTranslate
     * @return \Exto\Sarp\Api\Data\TemplateOptionTranslateInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Exto\Sarp\Api\Data\TemplateOptionTranslateInterface $templateOptionTranslate);

    /**
     * Get info about templateOptionTranslate by templateOptionTranslate id
     *
     * @param int $templateOptionTranslateId
     * @return \Exto\Sarp\Api\Data\TemplateOptionTranslateInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($templateOptionTranslateId);

    /**
     * Get info about templateOptionTranslate by templateOptionTranslate id and store id
     *
     * @param int $templateOptionTranslateId
     * @param int $storeId
     * @return \Exto\Sarp\Api\Data\TemplateOptionTranslateInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByIdAndStore($templateOptionTranslateId, $storeId);

    /**
     * Delete templateOptionTranslate
     *
     * @param \Exto\Sarp\Api\Data\TemplateOptionTranslateInterface $templateOptionTranslate
     * @return void
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function delete(\Exto\Sarp\Api\Data\TemplateOptionTranslateInterface $templateOptionTranslate);

    /**
     * Retrieve list of translate options
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Exto\Sarp\Api\Data\TemplateOptionTranslateSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
    
    /**
     * Retrieve list of translates by option id
     *
     * @param int $optionId
     * @return \Exto\Sarp\Api\Data\TemplateOptionTranslateSearchResultsInterface
     */
    public function getListByOptionId($optionId);
}
