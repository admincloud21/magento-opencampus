<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Api;

/**
 * @api
 */
interface OrderManagementInterface
{
    /**
     * Get all orders for current profile id
     *
     * @api
     * @param int $profileId
     * @return \Magento\Sales\Api\Data\OrderSearchResultInterface
     */
    public function getOrderListByProfileId($profileId);
}
