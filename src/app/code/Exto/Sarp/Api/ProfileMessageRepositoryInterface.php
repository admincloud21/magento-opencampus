<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Api;

/**
 * @api
 */
interface ProfileMessageRepositoryInterface
{
    /**
     * Create profileMessage service
     *
     * @param \Exto\Sarp\Api\Data\ProfileMessageInterface $profileMessage
     * @return \Exto\Sarp\Api\Data\ProfileMessageInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Exto\Sarp\Api\Data\ProfileMessageInterface $profileMessage);

    /**
     * Get info about profileMessage by profileMessage id
     *
     * @param int $profileMessageId
     * @return \Exto\Sarp\Api\Data\ProfileMessageInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($profileMessageId);

    /**
     * Delete profileMessage
     *
     * @param \Exto\Sarp\Api\Data\ProfileMessageInterface $profileMessage
     * @return void
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function delete(\Exto\Sarp\Api\Data\ProfileMessageInterface $profileMessage);

    /**
     * Retrieve list of profile messages
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Exto\Sarp\Api\Data\ProfileMessageSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Retrieve list of profile messages by profile id
     *
     * @param int $id
     * @return \Exto\Sarp\Api\Data\ProfileMessageSearchResultsInterface
     */
    public function getListByProfileId($id);
}
