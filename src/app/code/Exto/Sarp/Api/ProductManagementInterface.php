<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Api;

/**
 * @api
 */
interface ProductManagementInterface
{
    /**
     * Get all products
     *
     * @api
     * @return \Magento\Catalog\Api\Data\ProductSearchResultsInterface
     */
    public function getProductList();
}
