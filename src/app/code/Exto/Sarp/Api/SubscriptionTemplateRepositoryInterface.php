<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Api;

/**
 * @api
 */
interface SubscriptionTemplateRepositoryInterface
{
    /**
     * Create subscriptionTemplate service
     *
     * @param \Exto\Sarp\Api\Data\SubscriptionTemplateInterface $subscriptionTemplate
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Exto\Sarp\Api\Data\SubscriptionTemplateInterface $subscriptionTemplate);

    /**
     * Get info about subscriptionTemplate by subscriptionTemplate id
     *
     * @param int $subscriptionTemplateId
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($subscriptionTemplateId);

    /**
     * Delete subscriptionTemplate
     *
     * @param \Exto\Sarp\Api\Data\SubscriptionTemplateInterface $subscriptionTemplate
     * @return void
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function delete(\Exto\Sarp\Api\Data\SubscriptionTemplateInterface $subscriptionTemplate);

    /**
     * Retrieve list of templates
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Retrieve list of all templates
     * 
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateSearchResultsInterface
     */
    public function getAllList();
}
