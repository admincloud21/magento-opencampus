<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Ui\DataProvider\Product;

use Exto\Sarp\Model\Product\ProductManagement;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

/**
 * Class ProductDataProvider
 */
class ProductDataProvider extends \Magento\Catalog\Ui\DataProvider\Product\ProductDataProvider
{
    /**
     * ProductDataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param array $addFieldStrategies
     * @param array $addFilterStrategies
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        array $addFieldStrategies = [],
        array $addFilterStrategies = [],
        array $meta = [],
        array $data = []
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $collectionFactory,
            $addFieldStrategies,
            $addFilterStrategies,
            $meta,
            $data
        );

        $this->collection->addAttributeToSelect(ProductManagement::TEMPLATE_PRODUCT_ATTRIBUTE_CODE, 'left');
        $this->collection->addFieldToFilter([
            [
                'attribute' => ProductManagement::TEMPLATE_PRODUCT_ATTRIBUTE_CODE,
                'neq' => 'exto'
            ]
        ]);
        $this->collection->getSelect()->reset(\Magento\Framework\DB\Select::WHERE);
        $this->collection->getSelect()->columns(['is_subscription' => "(IF (at_exto_sarp_template.value > 0, 1, 0))"]);
    }
}
