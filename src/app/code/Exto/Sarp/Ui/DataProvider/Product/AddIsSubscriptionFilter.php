<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Ui\DataProvider\Product;

use Exto\Sarp\Model\Product\ProductManagement;
use Magento\Framework\Data\Collection;
use Magento\Ui\DataProvider\AddFilterToCollectionInterface;

/**
 * Class AddIsSubscriptionToCollection
 */
class AddIsSubscriptionFilter implements AddFilterToCollectionInterface
{
    /**
     * @param Collection $collection
     * @param string $field
     * @param null $condition
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    public function addFilter(Collection $collection, $field, $condition = null)
    {
        $value = array_pop($condition);
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection->addAttributeToSelect(ProductManagement::TEMPLATE_PRODUCT_ATTRIBUTE_CODE, 'left');
        if ($value == 1) {
            $collection->addFieldToFilter([
                [
                    'attribute' => ProductManagement::TEMPLATE_PRODUCT_ATTRIBUTE_CODE,
                    'gt'        => 0
                ]
            ]);
        } else {
            $collection->addFieldToFilter([
                [
                    'attribute' => ProductManagement::TEMPLATE_PRODUCT_ATTRIBUTE_CODE,
                    'eq'        => 0
                ],
                [
                    'attribute' => ProductManagement::TEMPLATE_PRODUCT_ATTRIBUTE_CODE,
                    'null'      => 1
                ],
            ]);
        }
    }
}
