<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Ui\Component\Form;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\UrlInterface;
use Exto\Sarp\Model\Product\Type\Subscription;

/**
 * Class Template
 */
class Template extends AbstractModifier
{
    const SORT_ORDER = 100;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var LocatorInterface
     */
    protected $locator;

    /**
     * @var ArrayManager
     */
    protected $arrayManager;

    /**
     * Template constructor.
     * @param LocatorInterface $locator
     * @param UrlInterface $urlBuilder
     * @param ArrayManager $arrayManager
     */
    public function __construct(
        LocatorInterface $locator,
        UrlInterface $urlBuilder,
        ArrayManager $arrayManager
    ) {
        $this->locator = $locator;
        $this->urlBuilder = $urlBuilder;
        $this->arrayManager = $arrayManager;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        if (null === $this->arrayManager->get('exto-sarp-info', $meta)) {
            return $meta;
        }
        $path = 'exto-sarp-info/children/container_exto_sarp_template/children/exto_sarp_template'
            .'/arguments/data/config';
        $extoSarpTemplateConfig = $this->arrayManager->get(
            $path,
            $meta
        );

        $extoSarpTemplateConfig['tooltip'] = [
            'link' => $this->urlBuilder->getUrl('exto_sarp/template/new'),
            'description' => __(
                'Configure subscription templates here'
            ),
        ];

        $meta = $this->arrayManager->set(
            $path,
            $meta,
            $extoSarpTemplateConfig
        );
        return $meta;
    }
}
