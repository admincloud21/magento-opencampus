<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Ui\Component\Listing\Columns\Profile;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class CustomerData
 */
class CustomerData extends Column
{
    /**
     * CustomerData constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory
        $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $item['customer'] = $this->getLink(
                    $item['customer_id'],
                    $item['customer']
                );
            }
        }
        return $dataSource;
    }

    /**
     * @param int $customerId
     * @param string $customerData
     * @return string
     */
    protected function getLink($customerId, $customerData)
    {
        if (null !== $customerId) {
            $url = $this->context->getUrl('customer/index/edit', ['id' => $customerId]);
            $result = '<a href="' . $url . '">' . $customerData . '</a>';
        } else {
            $result = $customerData;
        }

        return $result;
    }
}
