<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Ui\Component\Listing\Columns\Profile;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;
use Exto\Sarp\Model\Source\Profile\Status;

/**
 * Class Actions
 */
class Actions extends Column
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * Actions constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $item[$this->getData('name')]['edit'] = [
                    'href' => $this->urlBuilder->getUrl(
                        'exto_sarp/profile/view',
                        ['id' => $item['id']]
                    ),
                    'label' => __('Edit'),
                    'hidden' => false,
                ];
                if ($item['status'] == Status::PENDING_VALUE
                    || $item['status'] == Status::ACTIVE_VALUE
                ) {
                    $item[$this->getData('name')]['status'] = [
                        'href' => $this->urlBuilder->getUrl(
                            'exto_sarp/profile/changeStatus',
                            ['id' => $item['id'], 'status' => Status::SUSPEND_ACTION]
                        ),
                        'label' => __('Suspend'),
                        'hidden' => false,
                    ];
                } elseif ($item['status'] == Status::PAUSED_VALUE) {
                    $item[$this->getData('name')]['status'] = [
                        'href' => $this->urlBuilder->getUrl(
                            'exto_sarp/profile/changeStatus',
                            ['id' => $item['id'], 'status' => Status::RESUME_ACTION]
                        ),
                        'label' => __('Resume'),
                        'hidden' => false,
                    ];
                } elseif ($item['status'] != Status::CANCELED_VALUE) {
                    $item[$this->getData('name')]['delete'] = [
                        'href' => $this->urlBuilder->getUrl(
                            'exto_sarp/profile/delete',
                            ['id' => $item['id']]
                        ),
                        'label' => __('Delete'),
                        'hidden' => false,
                    ];
                }
            }
        }

        return $dataSource;
    }
}
