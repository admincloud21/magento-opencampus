<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Ui\Component\Listing\Columns\Profile;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Paypal
 */
class Paypal extends Column
{
    /**
     * Paypal constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $item['external_service_profile_id'] = $this->getLink($item['external_service_profile_id']);
            }
        }
        return $dataSource;
    }

    /**
     * @param string $paypalId
     * @return string
     */
    protected function getLink($paypalId)
    {
        $url = 'https://www.paypal.com/cgi-bin/webscr?cmd=_profile-recurring-payments&encrypted_profile_id='. $paypalId;
        return '<a href="' . $url . '">' . $paypalId . '</a>';
    }
}
