Exto Subscriptions and Recurring Billing module [www.exto.io](https://exto.io)
=============
Exto subscriptions extension for Magento 2 allows customers to purchase subscription products and set up automatic recurring payments. This increases customer lifetime value by automating repetitive purchases and charges, and decreasing the likelihood of customer churn.

A merchant can organize a subscription process with configurable subscription templates, while the extension accepts checkout with any combination of subscription and non-subscription items, has a failure-proof recurring billing system, and more.