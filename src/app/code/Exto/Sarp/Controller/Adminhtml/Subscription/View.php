<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Controller\Adminhtml\Subscription;

/**
 * Class View
 */
class View extends \Exto\Sarp\Controller\Adminhtml\Profile\View
{
    const ADMIN_RESOURCE = 'Exto_Sarp::exto_sarp_subscription';
}
