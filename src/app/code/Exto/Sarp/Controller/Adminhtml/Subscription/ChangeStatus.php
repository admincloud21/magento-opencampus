<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Controller\Adminhtml\Subscription;

/**
 * Class ChangeStatus
 */
class ChangeStatus extends \Exto\Sarp\Controller\Adminhtml\Profile\ChangeStatus
{
    const ADMIN_RESOURCE = 'Exto_Sarp::exto_sarp_subscription';
}
