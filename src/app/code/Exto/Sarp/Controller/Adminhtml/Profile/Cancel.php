<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Controller\Adminhtml\Profile;

use Magento\Backend\App\Action;
use Exto\Sarp\Model\ResourceModel\ProfileRepository;
use Exto\Sarp\Model\ProfileService;
use Magento\Framework\Exception\NoSuchEntityException;
use Exto\Sarp\Model\Source\Profile\Status;

/**
 * Class Cancel
 */
class Cancel extends Action
{
    const ADMIN_RESOURCE = 'Exto_Sarp::exto_sarp_profile';
    
    /** @var \Exto\Sarp\Model\ResourceModel\ProfileRepository */
    protected $profileRepository;

    /** @var \Exto\Sarp\Model\ProfileService */
    protected $profileService;

    /**
     * Delete constructor.
     * @param Action\Context $context
     * @param ProfileRepository $profileRepository
     * @param ProfileService $profileService
     */
    public function __construct(
        Action\Context $context,
        ProfileRepository $profileRepository,
        ProfileService $profileService
    ) {
        parent::__construct($context);
        $this->profileRepository = $profileRepository;
        $this->profileService = $profileService;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id', null);
        try {
            $profile = $this->profileRepository->get($id);
        } catch (NoSuchEntityException $e) {
            $profile = null;
            $this->messageManager->addErrorMessage(__('This profile no longer exists.'));
        }
        if ($profile) {
            try {
                $profile->setStatus(Status::CANCELED_VALUE);
                $this->profileService->changeProfileStatusOnExternalService($profile);
                $this->profileRepository->save($profile);
                $this->messageManager->addSuccessMessage(__('Profile was successfully cancelled.'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/view', ['id' => $id]);
            }
        }
        return $resultRedirect->setPath('*/*/');
    }
}
