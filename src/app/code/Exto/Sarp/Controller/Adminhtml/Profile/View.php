<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Controller\Adminhtml\Profile;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;
use Exto\Sarp\Model\ResourceModel\ProfileRepository;

/**
 * Class View
 */
class View extends Action
{
    const ADMIN_RESOURCE = 'Exto_Sarp::exto_sarp_profile';
    
    /**
     * @var \Exto\Sarp\Model\ResourceModel\ProfileRepository
     */
    protected $profileRepository;

    /**
     * View constructor.
     * @param Context $context
     * @param ProfileRepository $profileRepository
     */
    public function __construct(
        Context $context,
        ProfileRepository $profileRepository
    ) {
        $this->profileRepository = $profileRepository;
        parent::__construct($context);
    }
    
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id', null);
        try {
            $profile = $this->profileRepository->get($id);
        } catch (\Exception $e) {
            $profile = null;
        }

        if (!$profile) {
            $this->messageManager->addErrorMessage(__('This profile no longer exists.'));
            /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index');
        }
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('View Profile'));

        return $resultPage;
    }
}
