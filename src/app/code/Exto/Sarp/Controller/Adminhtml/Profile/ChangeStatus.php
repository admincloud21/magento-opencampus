<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Controller\Adminhtml\Profile;

use Magento\Backend\App\Action;
use Exto\Sarp\Model\ResourceModel\ProfileRepository;
use Exto\Sarp\Model\ProfileService;
use Exto\Sarp\Model\ResourceModel\ProfileOrderRepository;
use Exto\Sarp\Model\Source\Profile\Status;

/**
 * Class ChangeStatus
 */
class ChangeStatus extends Action
{
    const ADMIN_RESOURCE = 'Exto_Sarp::exto_sarp_profile';
    
    /**
     * @var \Exto\Sarp\Model\ResourceModel\ProfileRepository
     */
    protected $profileRepository;

    /**
     * @var \Exto\Sarp\Model\ProfileService
     */
    protected $profileService;

    /**
     * @var ProfileOrderRepository
     */
    protected $profileOrderRepository;

    /**
     * ChangeStatus constructor.
     * @param Action\Context $context
     * @param ProfileRepository $profileRepository
     * @param ProfileService $profileService
     * @param ProfileOrderRepository $profileOrderRepository
     */
    public function __construct(
        Action\Context $context,
        ProfileRepository $profileRepository,
        ProfileService $profileService,
        ProfileOrderRepository $profileOrderRepository
    ) {
        parent::__construct($context);
        $this->profileRepository = $profileRepository;
        $this->profileService = $profileService;
        $this->profileOrderRepository = $profileOrderRepository;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id');
        $status = $this->getRequest()->getParam('status', null);
        if (null === $status) {
            $this->messageManager->addErrorMessage(__('Status not found.'));
            return $resultRedirect->setPath('*/*/');
        }

        if ($id) {
            try {
                $profile = $this->profileRepository->get($id);
                switch ($status) {
                    case Status::SUSPEND_ACTION:
                    case Status::PAUSED_VALUE:
                        $statusValue = Status::PAUSED_VALUE;
                        break;
                    case Status::RESUME_ACTION:
                        $profileOrderCount = $this
                            ->profileOrderRepository
                            ->getListByProfileId($profile->getId())
                            ->getTotalCount();
                        if ($profileOrderCount && $profileOrderCount > 0) {
                            $statusValue = Status::ACTIVE_VALUE;
                        } else {
                            $statusValue = Status::PENDING_VALUE;
                        }
                        break;
                    default:
                        throw new \Exception(__('Status not found.'));
                }
                $profile->setStatus($statusValue);
                $this->profileService->changeProfileStatusOnExternalService($profile);
                $this->profileRepository->save($profile);
                $this->profileService->sendStatusChangedNotification($profile);
                $this->messageManager->addSuccessMessage(__('Profile was successfully updated.'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/view', ['id' => $id]);
            }
        } else {
            $this->messageManager->addErrorMessage(__('This profile no longer exists.'));
        }
        return $resultRedirect->setPath('*/*/');
    }
}
