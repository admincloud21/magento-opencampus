<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Controller\Adminhtml\Dashboard;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Api\BookmarkManagementInterface;
use Magento\Ui\Api\BookmarkRepositoryInterface;
use Magento\Framework\Json\EncoderInterface as JsonEncoder;

/**
 * Class ProfileFilter
 */
class ProfileFilter extends Action
{
    const ADMIN_RESOURCE = 'Exto_Sarp::exto_sarp_dashboard';
    
    /**
     * @var \Magento\Ui\Api\BookmarkManagementInterface
     */
    protected $bookmarkManagement;

    /**
     * @var \Magento\Ui\Api\BookmarkRepositoryInterface
     */
    protected $bookmarkRepository;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $jsonEncode;

    /**
     * ProfileFilter constructor.
     * @param Context $context
     * @param BookmarkManagementInterface $bookmarkManagement
     * @param BookmarkRepositoryInterface $bookmarkRepository
     * @param JsonEncoder $jsonEncode
     */
    public function __construct(
        Context $context,
        BookmarkManagementInterface $bookmarkManagement,
        BookmarkRepositoryInterface $bookmarkRepository,
        JsonEncoder $jsonEncode
    ) {
        parent::__construct($context);
        $this->bookmarkManagement = $bookmarkManagement;
        $this->bookmarkRepository = $bookmarkRepository;
        $this->jsonEncode = $jsonEncode;
    }

    /**
     * @return $this
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        /** @var \Magento\Ui\Api\Data\BookmarkInterface $currentBookmark */
        $currentBookmark = $this->bookmarkManagement->getByIdentifierNamespace('current', 'profile_listing');
        $currentConfig = $currentBookmark->getConfig();
        $currentConfig['current']['filters']['applied'] = $params;
        $currentBookmark->setConfig($this->jsonEncode->encode($currentConfig));

        $this->bookmarkRepository->save($currentBookmark);
        return $this->resultRedirectFactory->create()->setPath('exto_sarp/profile/index');
    }
}
