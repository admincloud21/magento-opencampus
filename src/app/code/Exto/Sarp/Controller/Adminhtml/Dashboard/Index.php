<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Controller\Adminhtml\Dashboard;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Exto\Sarp\Model\Product\ProductManagement;
use Exto\Sarp\Api\SubscriptionTemplateRepositoryInterface;
use Magento\Backend\App\Action;

/**
 * Class Index
 */
class Index extends Action
{
    const ADMIN_RESOURCE = 'Exto_Sarp::exto_sarp_dashboard';

    /**
     * @var \Exto\Sarp\Api\SubscriptionTemplateRepositoryInterface
     */
    protected $templateRepository;

    /**
     * @var ProductManagement
     */
    protected $productManagement;

    /**
     * Index constructor.
     * @param Context $context
     * @param ProductManagement $productManagement
     * @param SubscriptionTemplateRepositoryInterface $templateRepository
     */
    public function __construct(
        Context $context,
        ProductManagement $productManagement,
        SubscriptionTemplateRepositoryInterface $templateRepository
    ) {
        parent::__construct($context);
        $this->productManagement = $productManagement;
        $this->templateRepository = $templateRepository;
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Dashboard'));
        
        if ($this->isDisplayNoTemplateNotification()) {
            $url = $this->getUrl('exto_sarp/template/index');
            $message = __(
                'There are no subscription templates, <a href="%1">start with adding some</a>',
                $url
            );
            $this->messageManager->addComplexWarningMessage(
                'addWarningMessage',
                [
                    'text' => $message->render()
                ]
            );
        }

        if ($this->isDisplayNoProductNotification()) {
            $url = $this->getUrl('catalog/product/index');
            $message = __(
                'You have subscription templates, now <a href="%1">enable subscription for some of your products</a>.',
                $url
            );
            $this->messageManager->addComplexWarningMessage(
                'addWarningMessage',
                [
                    'text' => $message->render()
                ]
            );
        }

        return $resultPage;
    }

    /**
     * @return bool
     */
    private function isDisplayNoTemplateNotification()
    {
        $templateList = $this->templateRepository->getAllList();
        $result = true;
        foreach ($templateList->getItems() as $template) {
            //Not display message, if collection has one template with is_deleted = 0|null
            if (!$template->getIsDeleted()) {
                $result = false;
            }
        }
        return $result;
    }

    /**
     * @return bool
     */
    private function isDisplayNoProductNotification()
    {
        if ($this->isDisplayNoTemplateNotification()) {
            return false;
        }
        $sarpProductCount = $this->productManagement->getProductList()->getTotalCount();
        if ($sarpProductCount > 0) {
            return false;
        }
        return true;
    }
}
