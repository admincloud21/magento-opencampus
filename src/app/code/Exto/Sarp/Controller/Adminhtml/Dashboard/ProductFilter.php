<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Controller\Adminhtml\Dashboard;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Api\BookmarkManagementInterface;
use Magento\Ui\Api\BookmarkRepositoryInterface;
use Magento\Ui\Api\Data\BookmarkInterfaceFactory;
use Magento\Framework\Json\EncoderInterface as JsonEncoder;
use Magento\Authorization\Model\UserContextInterface;

/**
 * Class ProductFilter
 */
class ProductFilter extends Action
{
    const ADMIN_RESOURCE = 'Exto_Sarp::exto_sarp_dashboard';
    
    /**
     * @var \Magento\Ui\Api\BookmarkManagementInterface
     */
    protected $bookmarkManagement;

    /**
     * @var \Magento\Ui\Api\BookmarkRepositoryInterface
     */
    protected $bookmarkRepository;

    /**
     * @var BookmarkInterfaceFactory
     */
    protected $bookmarkDataFactory;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $jsonEncode;

    /**
     * @var \Magento\Authorization\Model\UserContextInterface
     */
    protected $userContext;

    /**
     * ProductFilter constructor.
     * @param Context $context
     * @param BookmarkManagementInterface $bookmarkManagement
     * @param BookmarkRepositoryInterface $bookmarkRepository
     * @param BookmarkInterfaceFactory $bookmarkDataFactory
     * @param JsonEncoder $jsonEncode
     * @param UserContextInterface $userContext
     */
    public function __construct(
        Context $context,
        BookmarkManagementInterface $bookmarkManagement,
        BookmarkRepositoryInterface $bookmarkRepository,
        BookmarkInterfaceFactory $bookmarkDataFactory,
        JsonEncoder $jsonEncode,
        UserContextInterface $userContext
    ) {
        parent::__construct($context);
        $this->bookmarkManagement = $bookmarkManagement;
        $this->bookmarkRepository = $bookmarkRepository;
        $this->bookmarkDataFactory = $bookmarkDataFactory;
        $this->jsonEncode = $jsonEncode;
        $this->userContext = $userContext;
    }

    /**
     * @return $this
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $identifier = 'current';
        $namespace = 'product_listing';
        /** @var \Magento\Ui\Api\Data\BookmarkInterface $currentBookmark */
        $currentBookmark = $this->bookmarkManagement->getByIdentifierNamespace($identifier, $namespace);
        if ($currentBookmark) {
            $currentConfig = $currentBookmark->getConfig();
            $currentConfig['current']['filters']['applied'] = $params;
            $currentBookmark->setConfig($this->jsonEncode->encode($currentConfig));
        } else {
            $currentBookmark = $this->bookmarkDataFactory->create();
            $currentBookmark
                ->setIdentifier($identifier)
                ->setNamespace($namespace)
                ->setUserId($this->userContext->getUserId());
            ;

            $currentConfig = $currentBookmark->getConfig();
            $currentConfig['current']['filters']['applied'] = $params;
            $currentBookmark->setConfig($this->jsonEncode->encode($currentConfig));
        }
        $this->bookmarkRepository->save($currentBookmark);
        return $this->resultRedirectFactory->create()->setPath('catalog/product/index');
    }
}
