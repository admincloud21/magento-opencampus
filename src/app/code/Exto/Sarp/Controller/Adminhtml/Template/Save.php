<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Controller\Adminhtml\Template;

use Magento\Backend\App\Action;
use Magento\Framework\Message\Error;
use Magento\Framework\Api\DataObjectHelper;
use Exto\Sarp\Model\ResourceModel\SubscriptionTemplateRepository;
use Exto\Sarp\Api\Data\SubscriptionTemplateInterfaceFactory;
use Exto\Sarp\Model\ResourceModel\SubscriptionTemplateOptionRepository;
use Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterfaceFactory;
use Exto\Sarp\Model\ResourceModel\TemplateOptionTranslateRepository;
use Exto\Sarp\Api\Data\TemplateOptionTranslateInterfaceFactory;
use Exto\Sarp\Api\Data\SubscriptionTemplateInterface;
use Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterface;
use Exto\Sarp\Api\Data\TemplateOptionTranslateInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Save
 */
class Save extends Action
{
    const ADMIN_RESOURCE = 'Exto_Sarp::exto_sarp_template';
    
    /**
     * @var \Exto\Sarp\Model\ResourceModel\SubscriptionTemplateRepository
     */
    protected $templateRepository;

    /**
     * @var \Exto\Sarp\Api\Data\SubscriptionTemplateInterfaceFactory
     */
    protected $templateDataFactory;

    /**
     * @var \Exto\Sarp\Model\ResourceModel\SubscriptionTemplateOptionRepository
     */
    protected $optionRepository;

    /**
     * @var \Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterfaceFactory
     */
    protected $optionDataFactory;

    /**
     * @var \Exto\Sarp\Model\ResourceModel\TemplateOptionTranslateRepository
     */
    protected $optionTranslateRepository;

    /**
     * @var \Exto\Sarp\Api\Data\TemplateOptionTranslateInterfaceFactory
     */
    protected $optionTranslateDataFactory;

    /**
     * Data object helper
     *
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param DataObjectHelper $dataObjectHelper
     * @param SubscriptionTemplateRepository $subscriptionTemplateRepository
     * @param SubscriptionTemplateInterfaceFactory $subscriptionTemplateInterfaceFactory
     * @param SubscriptionTemplateOptionRepository $optionRepository
     * @param SubscriptionTemplateOptionInterfaceFactory $optionDataFactory
     * @param TemplateOptionTranslateRepository $optionTranslateRepository
     * @param TemplateOptionTranslateInterfaceFactory $optionTranslateDataFactory
     */
    public function __construct(
        Action\Context $context,
        DataObjectHelper $dataObjectHelper,
        SubscriptionTemplateRepository $subscriptionTemplateRepository,
        SubscriptionTemplateInterfaceFactory $subscriptionTemplateInterfaceFactory,
        SubscriptionTemplateOptionRepository $optionRepository,
        SubscriptionTemplateOptionInterfaceFactory $optionDataFactory,
        TemplateOptionTranslateRepository $optionTranslateRepository,
        TemplateOptionTranslateInterfaceFactory $optionTranslateDataFactory
    ) {
        parent::__construct($context);
        $this->templateRepository = $subscriptionTemplateRepository;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->templateDataFactory = $subscriptionTemplateInterfaceFactory;
        $this->optionRepository = $optionRepository;
        $this->optionDataFactory = $optionDataFactory;
        $this->optionTranslateRepository = $optionTranslateRepository;
        $this->optionTranslateDataFactory = $optionTranslateDataFactory;
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();

        if (!$data) {
            $this->messageManager->addErrorMessage(__('Data not found.'));
            return $resultRedirect->setPath('*/*/index');
        }

        $template = $this->templateDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $template,
            $data,
            SubscriptionTemplateInterface::class
        );

        try {
            //save template
            $template = $this->templateRepository->save($template);
            $optionsData = isset($data['options']) ? $data['options']: [];
            //delete options
            $optionList = $this->optionRepository->getListByTemplateId($template->getId());
            foreach ($optionList->getItems() as $option) {
                if (!in_array($option->getId(), array_keys($optionsData))) {
                    $this->optionRepository->delete($option);
                }
            }
            //save options
            foreach ($optionsData as $optionData) {
                $optionData['template_id'] = $template->getId();
                $option = $this->optionDataFactory->create();

                $this->dataObjectHelper->populateWithArray(
                    $option,
                    $optionData,
                    SubscriptionTemplateOptionInterface::class
                );
                $option = $this->optionRepository->save($option);
                //save option translates
                $translates = $optionData['titles'];
                foreach ($translates as $key => $translateLabel) {
                    $translateData = [
                        'option_id' => $option->getId(),
                        'store_id' => $key,
                        'value' => $translateLabel
                    ];

                    $translate = $this->optionTranslateDataFactory->create();
                    $this->dataObjectHelper->populateWithArray(
                        $translate,
                        $translateData,
                        TemplateOptionTranslateInterface::class
                    );
                    $this->optionTranslateRepository->save($translate);
                }
            }
            $this->messageManager->addSuccessMessage(__('Template was successfully saved.'));
        } catch (LocalizedException $e) {
            $this->addSessionErrorMessages($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the template.'));
        }
        $this->_getSession()->setFormData($data);
        if (array_key_exists('id', $data)) {
            $resultRedirect->setPath('*/*/edit', ['id' => $data['id']]);
        } else {
            $resultRedirect->setPath('*/*/index');
        }
        return $resultRedirect;
    }

    /**
     * Add error messages
     *
     * @param string $messages
     * @return $this
     */
    protected function addSessionErrorMessages($messages)
    {
        $messages = (array)$messages;
        $session = $this->_getSession();

        $callback = function ($error) use ($session) {
            if (!$error instanceof Error) {
                $error = new Error($error);
            }
            $this->messageManager->addMessage($error);
        };
        array_walk_recursive($messages, $callback);
        return $this;
    }
}
