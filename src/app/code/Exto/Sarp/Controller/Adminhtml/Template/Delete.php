<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Controller\Adminhtml\Template;

use Magento\Backend\App\Action;
use Exto\Sarp\Model\ResourceModel\SubscriptionTemplateRepository;

/**
 * Class Delete
 */
class Delete extends Action
{
    const ADMIN_RESOURCE = 'Exto_Sarp::exto_sarp_template';
    
    /**
     * @var \Exto\Sarp\Model\ResourceModel\SubscriptionTemplateRepository
     */
    protected $templateRepository;

    /**
     * Delete constructor.
     * @param Action\Context $context
     * @param SubscriptionTemplateRepository $templateRepository
     */
    public function __construct(
        Action\Context $context,
        SubscriptionTemplateRepository $templateRepository
    ) {
        parent::__construct($context);
        $this->templateRepository = $templateRepository;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $template = $this->templateRepository->get($id);
                $template->setIsDeleted(true);
                $this->templateRepository->save($template);
                $this->messageManager->addSuccessMessage(__('Template was successfully deleted.'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        } else {
            $this->messageManager->addErrorMessage(__('This template no longer exists.'));
        }
        return $resultRedirect->setPath('*/*/');
    }
}
