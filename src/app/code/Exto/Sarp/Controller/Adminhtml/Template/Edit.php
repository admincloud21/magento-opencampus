<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Controller\Adminhtml\Template;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Exto\Sarp\Model\ResourceModel\SubscriptionTemplateRepository;

/**
 * Class Edit
 */
class Edit extends Action
{
    const ADMIN_RESOURCE = 'Exto_Sarp::exto_sarp_template';

    /**
     * @var \Exto\Sarp\Model\ResourceModel\SubscriptionTemplateRepository
     */
    protected $templateRepository;

    /**
     * Edit constructor.
     * @param Action\Context $context
     * @param SubscriptionTemplateRepository $templateRepository
     */
    public function __construct(
        Action\Context $context,
        SubscriptionTemplateRepository $templateRepository
    ) {
        $this->templateRepository = $templateRepository;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id', null);
        try {
            $template = $this->templateRepository->get($id);
        } catch (\Exception $e) {
            $template = null;
        }

        if (!$template && null !== $id) {
            $this->messageManager->addErrorMessage(__('This subscription template no longer exists.'));
            /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index');
        }

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        if ($template) {
            $resultPage->getConfig()->getTitle()->prepend(__('Edit Template "%1"', $template->getTitle()));
        } else {
            $resultPage->getConfig()->getTitle()->prepend(__('New Template'));
        }
        return $resultPage;
    }
}
