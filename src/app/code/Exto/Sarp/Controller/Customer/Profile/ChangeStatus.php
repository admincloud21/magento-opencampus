<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Controller\Customer\Profile;

use Magento\Framework\App\Action;
use Magento\Customer\Model\Session;
use Exto\Sarp\Controller\Customer\Profile;
use Exto\Sarp\Model\ResourceModel\ProfileRepository;
use Exto\Sarp\Model\ProfileService;
use Magento\Framework\Exception\NoSuchEntityException;
use Exto\Sarp\Model\Source\Profile\Status;
use Exto\Sarp\Model\ResourceModel\ProfileOrderRepository;

/**
 * Class ChangeStatus
 */
class ChangeStatus extends Profile
{
    /** @var \Exto\Sarp\Model\ResourceModel\ProfileRepository */
    protected $profileRepository;

    /** @var \Exto\Sarp\Model\ProfileService */
    protected $profileService;

    /** @var ProfileOrderRepository */
    protected $profileOrderRepository;

    /**
     * ChangeStatus constructor.
     * @param Action\Context $context
     * @param Session $customerSession
     * @param ProfileRepository $profileRepository
     * @param ProfileService $profileService
     * @param ProfileOrderRepository $profileOrderRepository
     */
    public function __construct(
        Action\Context $context,
        Session $customerSession,
        ProfileRepository $profileRepository,
        ProfileService $profileService,
        ProfileOrderRepository $profileOrderRepository
    ) {
        parent::__construct($context, $customerSession);
        $this->profileRepository = $profileRepository;
        $this->profileService = $profileService;
        $this->profileOrderRepository = $profileOrderRepository;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id', null);
        $status = $this->getRequest()->getParam('status', null);
        if (null === $status) {
            $this->messageManager->addErrorMessage(__('Status not found.'));
        }
        try {
            $profile = $this->profileRepository->get($id);
        } catch (NoSuchEntityException $e) {
            $profile = null;
            $this->messageManager->addErrorMessage(__('This profile no longer exists.'));
        }
        if ($profile) {
            try {
                if ($status == Status::ACTIVE_VALUE) {
                    $profileOrderCount = $this
                        ->profileOrderRepository
                        ->getListByProfileId($profile->getId())
                        ->getTotalCount();
                    if ($profileOrderCount && $profileOrderCount > 0) {
                        $status = Status::ACTIVE_VALUE;
                    } else {
                        $status = Status::PENDING_VALUE;
                    }
                }
                $profile->setStatus($status);
                $this->profileService->changeProfileStatusOnExternalService($profile);
                $this->profileRepository->save($profile);
                $this->profileService->sendStatusChangedNotification($profile);
                $this->messageManager->addSuccessMessage(__('Profile was successfully updated.'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/view', ['id' => $id]);
            }
        }
        return $resultRedirect->setPath('*/*/');
    }
}
