<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Controller\Customer\Profile;

use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Framework\Controller\ResultFactory;
use Exto\Sarp\Api\ProfileRepositoryInterface;
use Exto\Sarp\Controller\Customer\Profile;
use Magento\Framework\Exception\NoSuchEntityException;
use Exto\Sarp\Model\Source\Profile\Status;

/**
 * Class View
 */
class View extends Profile
{
    /** @var ProfileRepositoryInterface */
    protected $profileRepository;

    /**
     * View constructor.
     * @param Context $context
     * @param Session $customerSession
     * @param ProfileRepositoryInterface $profileRepository
     * @param Status $statusSource
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        ProfileRepositoryInterface $profileRepository,
        Status $statusSource
    ) {
        parent::__construct($context, $customerSession);
        $this->profileRepository = $profileRepository;
        $this->statusSource = $statusSource;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $profileId = $this->_request->getParam('id', null);
        try {
            $profile = $this->profileRepository->get($profileId);
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage(__('Profile has not been found'));
            return $this->redirectToIndex();
        }
        if ($profile->getCustomerId() !== $this->customerSession->getCustomerId()
            && $profile->getCustomerEmail() !== $this->customerSession->getCustomer()->getEmail()) {
            return $this->redirectToIndex();
        }

        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        if ($navigationBlock = $resultPage->getLayout()->getBlock('customer_account_navigation')) {
            /** @var \Magento\Framework\View\Element\Html\Links $navigationBlock */
            $navigationBlock->setActive('exto_sarp/customer_profile/index');
        }
        $statusOptions = $this->statusSource->getOptionArray();
        $status = __('Unknown');
        if (array_key_exists($profile->getStatus(), $statusOptions)) {
            $status = $statusOptions[$profile->getStatus()];
        }
        $resultPage->getConfig()->getTitle()->set(
            __(
                'Profile #%1 - %2',
                $profile->getProfileId(),
                $status
            )
        );
        return $resultPage;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    protected function redirectToIndex()
    {
        $redirect = $this->resultRedirectFactory->create();
        $redirect->setPath('exto_sarp/customer_profile/index');
        return $redirect;
    }
}
