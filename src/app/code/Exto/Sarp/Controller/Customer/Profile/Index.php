<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Controller\Customer\Profile;

use Magento\Framework\Controller\ResultFactory;
use Exto\Sarp\Controller\Customer\Profile;

/**
 * Class Index
 */
class Index extends Profile
{
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        if ($navigationBlock = $resultPage->getLayout()->getBlock('customer_account_navigation')) {
            /** @var \Magento\Framework\View\Element\Html\Links $navigationBlock */
            $navigationBlock->setActive('exto_sarp/customer_profile/index');
        }
        $resultPage->getConfig()->getTitle()->set(__('My Subscriptions'));
        return $resultPage;
    }
}
