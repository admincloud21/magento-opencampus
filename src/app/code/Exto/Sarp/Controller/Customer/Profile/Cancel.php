<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Controller\Customer\Profile;

use Magento\Framework\App\Action;
use Magento\Customer\Model\Session;
use Exto\Sarp\Controller\Customer\Profile;
use Exto\Sarp\Api\ProfileRepositoryInterface;
use Exto\Sarp\Model\ProfileService;
use Magento\Framework\Exception\NoSuchEntityException;
use Exto\Sarp\Model\Source\Profile\Status;

/**
 * Class Cancel
 */
class Cancel extends Profile
{
    /** @var \Exto\Sarp\Model\ResourceModel\ProfileRepository */
    protected $profileRepository;

    /** @var \Exto\Sarp\Model\ProfileService */
    protected $profileService;

    /**
     * Delete constructor.
     * @param Action\Context $context
     * @param Session $customerSession
     * @param ProfileRepositoryInterface $profileRepository
     * @param ProfileService $profileService
     */
    public function __construct(
        Action\Context $context,
        Session $customerSession,
        ProfileRepositoryInterface $profileRepository,
        ProfileService $profileService
    ) {
        parent::__construct($context, $customerSession);
        $this->profileRepository = $profileRepository;
        $this->profileService = $profileService;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id', null);
        try {
            $profile = $this->profileRepository->get($id);
        } catch (NoSuchEntityException $e) {
            $profile = null;
            $this->messageManager->addErrorMessage(__('This profile no longer exists.'));
        }
        if ($profile) {
            try {
                $profile->setStatus(Status::CANCELED_VALUE);
                $this->profileService->changeProfileStatusOnExternalService($profile);
                $this->profileRepository->save($profile);
                $this->messageManager->addSuccessMessage(__('Profile was successfully cancelled.'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/view', ['id' => $id]);
            }
        }
        return $resultRedirect->setPath('*/*/');
    }
}
