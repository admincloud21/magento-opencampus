<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model;

use Magento\Quote\Model\Quote as QuoteEntity;
use Magento\Quote\Model\Quote\Item as QuoteItem;
use Exto\Sarp\Model\ResourceModel\ProfileRepository;
use Exto\Sarp\Model\ResourceModel\ProfileOrderRepository;
use Exto\Sarp\Api\Data\ProfileInterface as ProfileInterface;
use Exto\Sarp\Api\Data\ProfileOrderInterface as ProfileOrderInterface;
use Exto\Sarp\Api\Data\ProfileOrderInterfaceFactory as ProfileOrderInterfaceFactory;
use Exto\Sarp\Api\Data\ProfileInterfaceFactory as ProfileInterfaceFactory;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\Quote\ItemFactory;
use Exto\Sarp\Model\Paypal\Api\RecurringFactory;
use Magento\Paypal\Model\CartFactory;
use Exto\Sarp\Model\Quote\ProfileManagement;
use Magento\Paypal\Model\Express\Checkout;
use Magento\Paypal\Model\Config as PaypalConfig;
use Exto\Sarp\Model\Source\Profile\Status;
use Psr\Log\LoggerInterface;

/**
 * Class ProfileService
 */
class ProfileService
{
    /** @var ProfileInterfaceFactory */
    protected $profileFactory;

    /** @var ProfileRepository */
    protected $profileRepository;

    /** @var ProfileOrderInterfaceFactory */
    protected $profileOrderFactory;

    /** @var ProfileOrderRepository */
    protected $profileOrderRepository;

    /** @var \Exto\Sarp\Model\Paypal\Api\RecurringFactory */
    protected $paypalRecurringApiFactory;

    /** @var \Magento\Paypal\Model\CartFactory */
    protected $paypalCartFactory;

    /** @var QuoteHelper */
    protected $quoteHelper;

    /** @var \Magento\Quote\Model\QuoteFactory */
    protected $quoteFactory;

    /** @var \Magento\Quote\Model\Quote\ItemFactory */
    protected $quoteItemFactory;

    /** @var \Exto\Sarp\Model\Quote\ProfileManagement */
    protected $profileManagement;

    /** @var  Mail\Sender */
    protected $emailSender;

    /** @var  LoggerInterface */
    protected $logger;

    /**
     * ProfileService constructor.
     * @param ProfileInterfaceFactory $profileFactory
     * @param ProfileRepository $profileRepository
     * @param ProfileOrderInterfaceFactory $profileOrderFactory
     * @param ProfileOrderRepository $profileOrderRepository
     * @param QuoteFactory $quoteFactory
     * @param ItemFactory $quoteItemFactory
     * @param RecurringFactory $paypalRecurringApiFactory
     * @param QuoteHelper $quoteHelper
     * @param CartFactory $paypalCartFactory
     * @param ProfileManagement $profileManagement
     * @param Mail\Sender $emailSender
     * @param LoggerInterface $logger
     */
    public function __construct(
        ProfileInterfaceFactory $profileFactory,
        ProfileRepository $profileRepository,
        ProfileOrderInterfaceFactory $profileOrderFactory,
        ProfileOrderRepository $profileOrderRepository,
        QuoteFactory $quoteFactory,
        ItemFactory $quoteItemFactory,
        RecurringFactory $paypalRecurringApiFactory,
        QuoteHelper $quoteHelper,
        CartFactory $paypalCartFactory,
        ProfileManagement $profileManagement,
        Mail\Sender $emailSender,
        LoggerInterface $logger
    ) {
        $this->profileFactory = $profileFactory;
        $this->profileRepository = $profileRepository;
        $this->profileOrderFactory = $profileOrderFactory;
        $this->profileOrderRepository = $profileOrderRepository;
        $this->paypalRecurringApiFactory = $paypalRecurringApiFactory;
        $this->paypalCartFactory = $paypalCartFactory;
        $this->quoteHelper = $quoteHelper;
        $this->quoteFactory = $quoteFactory;
        $this->quoteItemFactory = $quoteItemFactory;
        $this->profileManagement = $profileManagement;
        $this->emailSender = $emailSender;
        $this->logger = $logger;
    }

    /**
     * @param QuoteEntity $quote
     *
     * @return ProfileInterface
     */
    public function createProfileFromQuote(QuoteEntity $quote)
    {
        $allVisibleItems = $quote->getAllVisibleItems();
        $firstQuoteItem = array_shift($allVisibleItems);
        $billingPeriod = $this->quoteHelper->getQuoteItemPeriodOption($firstQuoteItem);
        $startDate = $this->quoteHelper->getQuoteItemStartDateOption($firstQuoteItem);
        /** @var ProfileInterface $profile */
        $profile = $this->profileFactory->create();
        $profile
            ->setTitle($this->quoteHelper->getQuoteItemSubscriptionTitle($firstQuoteItem))
            ->setCustomerId($quote->getCustomerId())
            ->setCustomerName($quote->getCustomerFirstname() . ' ' . $quote->getCustomerLastname())
            ->setCustomerEmail($quote->getCustomerEmail())
            ->setBillingStartDate($this->convertStartDate($startDate))
            ->setBillingFrequency($this->quoteHelper->getQuoteItemFrequencyOption($firstQuoteItem))
            ->setBillingPeriod($billingPeriod)
            ->setBillingCycles($this->quoteHelper->getQuoteItemCyclesOption($firstQuoteItem))
            ->setMaxPaymentFailures($this->quoteHelper->getQuoteItemMaxPaymentsFailures($firstQuoteItem))
            ->setDefaultOrderStatus($this->quoteHelper->getQuoteItemDefaultOrderStatus($firstQuoteItem))
        ;
        $quoteData = $this->cleanupArray($quote->getData());
        $billingAddressData = $this->cleanupArray($quote->getBillingAddress()->getData());
        $shippingAddressData = $this->cleanupArray($quote->getShippingAddress()->getData());
        $profile
            ->setQuoteData($quoteData)
            ->setBillingAddressData($billingAddressData)
            ->setShippingAddressData($shippingAddressData)
        ;
        $quoteItemsData = [];
        foreach ($quote->getItemsCollection() as $item) {
            /** @var QuoteItem $item */
            $itemData = $item->getData();
            $buyRequest = $item->getOptionByCode('info_buyRequest');
            if (null !== $buyRequest) {
                $itemData['info_buyRequest'] = $buyRequest->getValue();
            }
            $quoteItemsData[] = $itemData;
        }
        $quoteItemsData = $this->cleanupArray($quoteItemsData);
        $profile->setQuoteItemsData($quoteItemsData);
        return $profile;
    }

    /**
     * @param ProfileInterface $profile
     * @param QuoteEntity $quote
     *
     * @return ProfileInterface
     */
    public function createProfileOnExternalService(ProfileInterface $profile, QuoteEntity $quote)
    {
        $allVisibleItems = $quote->getAllVisibleItems();
        $firstQuoteItem = array_shift($allVisibleItems);
        $payment = $quote->getPayment();
        $token = $payment->getAdditionalInformation(
            Checkout::PAYMENT_INFO_TRANSPORT_TOKEN
        );
        /** @var \Magento\Paypal\Model\Cart $paypalCart */
        $paypalCart = $this->paypalCartFactory->create(['salesModel' => $quote]);

        /** @var \Exto\Sarp\Model\Paypal\Api\Recurring $api */
        $api = $this->paypalRecurringApiFactory->create();
        $api->setToken(
            $token
        )->setAmount(
            $quote->getBaseSubtotalWithDiscount()
        )->setCurrencyCode(
            $quote->getCurrency()->getBaseCurrencyCode()
        )->setPaypalCart(
            $paypalCart
        )->setProfileDescription(
            $this->quoteHelper->getBillingAgreementForQuoteItem($firstQuoteItem)
        );
        $this->exportDataFromProfileToApi($profile, $api);

        if ($quote->getIsVirtual()) {
            $api->setAddress($quote->getBillingAddress())->setSuppressShipping(true);
        } else {
            $api->setAddress($quote->getShippingAddress());
            $api->setBillingAddress($quote->getBillingAddress());
        }

        $api->callCreateRecurringPaymentsProfile();
        $profile->setExternalServiceProfileId($api->getProfileId());
        $profile->setStatus(Status::PENDING_VALUE);
        return $profile;
    }

    /**
     * @param ProfileInterface $profile
     *
     * @return $this
     */
    public function changeProfileStatusOnExternalService(ProfileInterface $profile)
    {
        /** @var \Exto\Sarp\Model\Paypal\Api\Recurring $api */
        $api = $this->paypalRecurringApiFactory->create();
        $api->setProfileId($profile->getExternalServiceProfileId());
        $api->setAction($this->exportStatusToAction($profile->getStatus()));
        $api->callManageRecurringPaymentsProfileStatus();
        return $this;
    }

    /**
     * @param ProfileInterface $profile
     *
     * @return ProfileInterface
     */
    public function saveProfile(ProfileInterface $profile)
    {
        return $this->profileRepository->save($profile);
    }

    /**
     * @param ProfileInterface $profile
     *
     * @return $this
     */
    public function sendNewProfileNotification(ProfileInterface $profile)
    {
        try {
            $this->emailSender->sendNewSubscriptionNotification($profile->getId());
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }
        return $this;
    }

    /**
     * @param ProfileInterface $profile
     *
     * @return $this
     */
    public function sendStatusChangedNotification(ProfileInterface $profile)
    {
        try {
            $this->emailSender->sendUpdateSubscriptionStatusNotification($profile->getId());
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }
        return $this;
    }

    /**
     * @param ProfileInterface $profile
     *
     * @return $this
     */
    public function sendProfileEndNotification(ProfileInterface $profile)
    {
        try {
            $this->emailSender->sendSubscriptionEndNotification($profile->getId());
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }
        return $this;
    }

    /**
     * @param \Exto\Sarp\Api\Data\ProfileInterface $profile
     * @param array $itemListTotalData
     *
     * @return \Magento\Sales\Api\Data\OrderInterface
     * @throws \Exception
     */
    public function createOrder($profile, $itemListTotalData)
    {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteFactory->create();
        $quote->setData($profile->getQuoteData());
        $quote->getBillingAddress()->setData($profile->getBillingAddressData());
        $quote->getShippingAddress()->setData($profile->getShippingAddressData());
        $quote->getShippingAddress()->setCollectShippingRates(true);
        $quote->getShippingAddress()->collectShippingRates();
        $quote->getPayment()->setMethod(PaypalConfig::METHOD_WPP_EXPRESS);
        $quote->getPayment()->setQuote($quote);

        foreach ($profile->getQuoteItemsData() as $itemData) {
            $itemTotalData = array_shift($itemListTotalData);
            $price = $itemTotalData['total'] - $itemTotalData['tax'] - $itemTotalData['shipping'];
            /** @var \Magento\Quote\Model\Quote\Item $quoteItem */
            $quoteItem = $this->quoteItemFactory->create();
            $quoteItem->setData($itemData);
            $quoteItem->setPrice($price)
                ->setBasePrice($price)
                ->setRowTotal($price)
                ->setBaseRowTotal($price)
                ->setTaxAmount($itemTotalData['tax'])
                ->setShippingAmount($itemTotalData['shipping'])
            ;
            $quote->addItem($quoteItem);
        }
        return $this->profileManagement->submit($quote);
    }

    /**
     * @param ProfileInterface $profile
     * @param \Magento\Sales\Model\Order $order
     *
     * @return $this
     */
    public function addOrderRelation($profile, $order)
    {
        /** @var ProfileOrderInterface $profileOrder */
        $profileOrder = $this->profileOrderFactory->create();
        $profileOrder->setOrderId($order->getId());
        $profileOrder->setProfileId($profile->getId());
        $this->profileOrderRepository->save($profileOrder);
        return $this;
    }

    /**
     * @param ProfileInterface $profile
     *
     * @return string|null
     */
    public function estimateCompleteDate($profile)
    {
        if ($profile->getStatus() === Status::COMPLETED_VALUE) {
            return $profile->getLastSuccessfulDate();
        }
        if (!in_array($profile->getStatus(), [Status::ACTIVE_VALUE, Status::PENDING_VALUE])) {
            return null;
        }

        $orderList = $this->profileOrderRepository->getListByProfileId($profile->getId());
        $pendingCycleCount = $profile->getBillingCycles() - $orderList->getTotalCount();
        $completeDate = new \DateTime($profile->getBillingStartDate());
        if ($profile->getLastSuccessfulDate()) {
            $completeDate = new \DateTime($profile->getLastSuccessfulDate());
        }
        $unit = '';
        switch ($profile->getBillingPeriod()) {
            case Source\Template\BillingPeriod::DAY_VALUE:
                $unit = 'D';
                break;
            case Source\Template\BillingPeriod::WEEK_VALUE:
                $unit = 'W';
                break;
            case Source\Template\BillingPeriod::MONTH_VALUE:
                $unit = 'M';
                break;
            case Source\Template\BillingPeriod::YEAR_VALUE:
                $unit = 'Y';
                break;
        }
        $count = $pendingCycleCount * $profile->getBillingFrequency();
        $dateInterval = new \DateInterval('P' . abs($count) . $unit);
        if ($count > 0) {
            $completeDate->add($dateInterval);
        } else {
            $completeDate->sub($dateInterval);
        }
        return $completeDate->format('Y-m-d H:i:s');
    }

    /**
     * @param string $status
     *
     * @return string
     * @throws \Exception
     */
    protected function exportStatusToAction($status)
    {
        switch ($status) {
            case Status::ACTIVE_VALUE:
            case Status::PENDING_VALUE:
                return 'Reactivate';
                break;
            case Status::PAUSED_VALUE:
                return 'Suspend';
                break;
            case Status::CANCELED_VALUE:
                return 'Cancel';
                break;
            case Status::COMPLETED_VALUE:
                throw new \Exception('Profile is expired.');
                break;
        }
    }

    /**
     * @param \DateTime|null $startDate
     *
     * @return string
     */
    protected function convertStartDate($startDate)
    {
        if (null === $startDate) {
            $startDate = new \DateTime('now');
        }
        return $startDate->format('Y-m-d G:i:s');
    }

    /**
     * @param int $billingPeriod
     *
     * @return string
     */
    protected function exportBillingPeriod($billingPeriod)
    {
        return $billingPeriod; //convert in _filterPeriodUnit
    }

    /**
     * @param ProfileInterface $profile
     * @param \Exto\Sarp\Model\Paypal\Api\Recurring $api
     *
     * @return $this
     */
    protected function exportDataFromProfileToApi(ProfileInterface $profile, &$api)
    {
        $api->setBillingPeriod($this->exportBillingPeriod($profile->getBillingPeriod()));
        $api->setBillingFrequency($profile->getBillingFrequency());
        $api->setTotalBillingCycles($profile->getBillingCycles());
        $api->setMaxFailedAttempts($profile->getMaxPaymentFailures());
        $api->setProfileStartDate($profile->getBillingStartDate());
        return $this;
    }

    /**
     * @param array $array
     * @return array
     */
    private function cleanupArray($array)
    {
        if (!$array) {
            return [];
        }
        foreach ($array as $key => $value) {
            if (is_object($value)) {
                unset($array[$key]);
            } elseif (is_array($value)) {
                $array[$key] = $this->cleanupArray($array[$key]);
            }
        }
        return $array;
    }
}
