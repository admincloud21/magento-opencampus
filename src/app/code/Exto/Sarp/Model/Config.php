<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model;

class Config
{
    const XML_PATH_GENERAL_MANAGE_STOCK = 'exto_sarp/general/manage_stock';
    const XML_PATH_WORDING_BLOCK_LABEL = 'exto_sarp/wording/subscription_block_label';
    const XML_PATH_WORDING_START_DATE_LABEL = 'exto_sarp/wording/subscription_start_date_label';
    const XML_PATH_EMAIL_NEW = 'exto_sarp/email_template/new_subscription';
    const XML_PATH_EMAIL_STATUS_UPDATE = 'exto_sarp/email_template/subscription_status_update';
    const XML_PATH_EMAIL_END = 'exto_sarp/email_template/subscription_ends';

    /** @var \Magento\Framework\App\Config\ScopeConfigInterface */
    protected $scopeConfig;

    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $storeManager;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * @param null $storeId
     *
     * @return mixed
     */
    public function isCanManageStock($storeId = null)
    {
        if (null === $storeId) {
            $storeId = $this->storeManager->getStore()->getId();
        }
        return !!$this->scopeConfig->getValue(
            self::XML_PATH_GENERAL_MANAGE_STOCK,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @param null $storeId
     *
     * @return mixed
     */
    public function getBlockName($storeId = null)
    {
        if (null === $storeId) {
            $storeId = $this->storeManager->getStore()->getId();
        }
        return !!$this->scopeConfig->getValue(
            self::XML_PATH_WORDING_BLOCK_LABEL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @param null $storeId
     *
     * @return mixed
     */
    public function getStartDateLabel($storeId = null)
    {
        if (null === $storeId) {
            $storeId = $this->storeManager->getStore()->getId();
        }
        return !!$this->scopeConfig->getValue(
            self::XML_PATH_WORDING_START_DATE_LABEL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @param null $storeId
     *
     * @return string
     */
    public function getNewEmailTemplate($storeId = null)
    {
        if (null === $storeId) {
            $storeId = $this->storeManager->getStore()->getId();
        }
        return $this->scopeConfig->getValue(
            self::XML_PATH_EMAIL_NEW,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @param null $storeId
     *
     * @return string
     */
    public function getStatusUpdateEmailTemplate($storeId = null)
    {
        if (null === $storeId) {
            $storeId = $this->storeManager->getStore()->getId();
        }
        return $this->scopeConfig->getValue(
            self::XML_PATH_EMAIL_STATUS_UPDATE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @param null $storeId
     *
     * @return string
     */
    public function getEndEmailTemplate($storeId = null)
    {
        if (null === $storeId) {
            $storeId = $this->storeManager->getStore()->getId();
        }
        return $this->scopeConfig->getValue(
            self::XML_PATH_EMAIL_END,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

}