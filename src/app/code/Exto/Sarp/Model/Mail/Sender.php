<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Mail;

use Exto\Sarp\Model\ResourceModel\ProfileRepository;
use Magento\Framework\Mail\Template\TransportBuilder;
use Psr\Log\LoggerInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Url;
use Magento\Framework\App\Area;
use Exto\Sarp\Model\Config as Config;

/**
 * Class Sender
 */
class Sender
{
    const EMAIL_SENDER = 'sales';

    /**
     * @var string
     */
    protected $prefixSubject = '';

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /** @var ProfileRepository */
    protected $profileRepository;

    /**
     * @var \Magento\Framework\Url
     */
    protected $frontendUrlBuilder;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Sender constructor.
     * @param TransportBuilder $transportBuilder
     * @param LoggerInterface $logger
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param ProfileRepository $profileRepository
     * @param Url $frontendUrlBuilder
     * @param Config $config
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        TransportBuilder $transportBuilder,
        LoggerInterface $logger,
        CustomerRepositoryInterface $customerRepositoryInterface,
        ProfileRepository $profileRepository,
        Url $frontendUrlBuilder,
        Config $config,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->logger = $logger;
        $this->transportBuilder = $transportBuilder;
        $this->customerRepository = $customerRepositoryInterface;
        $this->profileRepository = $profileRepository;
        $this->frontendUrlBuilder = $frontendUrlBuilder;
        $this->config = $config;
        $this->storeManager = $storeManager;
    }

    /**
     * @param int $profileId
     * @return array
     */
    private function prepareEmailData($profileId)
    {
        try {
            /** @var \Exto\Sarp\Api\Data\ProfileInterface $profile */
            $profile = $this->profileRepository->get($profileId);
            $quoteData = $profile->getQuoteData();
            $storeId = $quoteData['store_id'];
        } catch (\Exception $e) {
            $this->logger->addDebug($e->getMessage());
            return [];
        }

        $viewUrl = $this->frontendUrlBuilder->getUrl('exto_sarp/customer_profile/view', ['id' => $profile->getId()]);
        $statusOptionArray = \Exto\Sarp\Model\Source\Profile\Status::getOptionArray();
        $emailData = [
            'store_id' => $storeId,
            'store' => $this->storeManager->getStore($storeId),
            'to' => $profile->getCustomerEmail(),
            'customer_name' => $profile->getCustomerName(),
            'customer_subscription_view_link' => $viewUrl,
            'profile' => $profile,
            'subscription_id' => $profile->getProfileId(),
            'status' => $statusOptionArray[$profile->getStatus()]
        ];
        return $emailData;
    }

    /**
     * @param int $profileId
     * @return $this
     */
    public function sendNewSubscriptionNotification($profileId)
    {
        $emailData = $this->prepareEmailData($profileId);
        $template = $this->config->getNewEmailTemplate($emailData['store_id']);
        $emailData['template_id'] = $template;
        $this->sendEmail($emailData);
        return $this;
    }

    /**
     * @param int $profileId
     * @return $this
     */
    public function sendUpdateSubscriptionStatusNotification($profileId)
    {
        $emailData = $this->prepareEmailData($profileId);
        $template = $this->config->getStatusUpdateEmailTemplate($emailData['store_id']);
        $emailData['template_id'] = $template;
        $this->sendEmail($emailData);
        return $this;
    }

    /**
     * @param int $profileId
     * @return $this
     */
    public function sendSubscriptionEndNotification($profileId)
    {
        $emailData = $this->prepareEmailData($profileId);
        $template = $this->config->getEndEmailTemplate($emailData['store_id']);
        $emailData['template_id'] = $template;
        $this->sendEmail($emailData);
        return $this;
    }

    /**
     * @param array $emailData
     * @return $this
     */
    public function sendEmail($emailData)
    {
        $this->transportBuilder
            ->setTemplateIdentifier($emailData['template_id'])
            ->setTemplateOptions([
                'area' => Area::AREA_FRONTEND,
                'store' => $emailData['store_id']
            ])
            ->setTemplateVars($emailData)
            ->setFrom(self::EMAIL_SENDER)
            ->addTo($emailData['to'], $emailData['customer_name'])
        ;
        $transport = $this->transportBuilder->getTransport();
        $transport->sendMessage();
        return $this;
    }
}
