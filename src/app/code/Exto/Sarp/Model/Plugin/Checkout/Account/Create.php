<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Plugin\Checkout\Account;

use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Create
 */
class Create
{
    /** @var \Magento\Checkout\Model\Session */
    protected $checkoutSession;

    /** @var \Magento\Customer\Model\Session */
    protected $customerSession;

    /** @var \Magento\Framework\Controller\Result\JsonFactory */
    protected $jsonFactory;

    /** @var \Magento\Framework\Message\ManagerInterface */
    protected $messageManager;

    /** @var \Exto\Sarp\Model\Profile\CustomerManagement */
    protected $profileCustomerManagement;

    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Exto\Sarp\Model\Profile\CustomerManagement $profileCustomerManagement
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Exto\Sarp\Model\Profile\CustomerManagement $profileCustomerManagement
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->customerSession = $customerSession;
        $this->jsonFactory = $jsonFactory;
        $this->profileCustomerManagement = $profileCustomerManagement;
        $this->messageManager = $messageManager;
    }

    /**
     * @throws AlreadyExistsException
     * @throws NoSuchEntityException
     * @throws \Exception
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function aroundExecute(
        \Magento\Checkout\Controller\Account\Create $subject,
        \Closure $proceed
    ) {
        if ($this->customerSession->isLoggedIn()) {
            return $proceed();
        }
        if (!$this->checkoutSession->getLastRecurringProfileIds()) {
            return $proceed();
        }
        $profileIdList = $this->checkoutSession->getLastRecurringProfileIds();
        $profileId = array_shift($profileIdList);
        try {
            $this->profileCustomerManagement->create($profileId);
            return $this->jsonFactory->create()->setData(
                [
                    'errors' => false,
                    'message' => __('A letter with further instructions will be sent to your email.')
                ]
            );
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, $e->getMessage());
            throw $e;
        }
    }
}
