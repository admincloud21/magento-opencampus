<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Plugin\Checkout\Session;

/**
 * Class SuccessValidator
 */
class SuccessValidator
{

    /** @var \Magento\Checkout\Model\Session */
    protected $checkoutSession;

    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @return bool
     */
    public function aroundIsValid(
        \Magento\Checkout\Model\Session\SuccessValidator $subject,
        \Closure $proceed
    ) {
        if ($this->checkoutSession->getLastRecurringProfileIds()) {
            return true;
        }
        return $proceed();
    }
}
