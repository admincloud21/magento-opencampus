<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Plugin\Checkout\Block;

/**
 * Class SuccessValidator
 */
class Registration
{
    /** @var \Magento\Checkout\Model\Session */
    protected $checkoutSession;

    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @return string
     */
    public function aroundToHtml(
        \Magento\Checkout\Block\Registration $subject,
        \Closure $proceed
    ) {
        if (
            $subject instanceof \Exto\Sarp\Block\Checkout\Registration
            || $this->checkoutSession->getLastOrderId()
            || !$this->checkoutSession->getLastRecurringProfileIds()
        ) {
            return $proceed();
        }
        return '';
    }
}
