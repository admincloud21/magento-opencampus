<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Sarp\Model\Plugin\Catalog\Helper\Product;

/**
 * Class Configuration
 */
class Configuration
{
    /** @var \Exto\Sarp\Helper\Catalog\Product\Configuration */
    protected $catalogProductConfigurationHelper;

    /**
     * @param \Exto\Sarp\Helper\Catalog\Product\Configuration $catalogProductConfigurationHelper
     */
    public function __construct(
        \Exto\Sarp\Helper\Catalog\Product\Configuration $catalogProductConfigurationHelper
    ) {
        $this->catalogProductConfigurationHelper = $catalogProductConfigurationHelper;
    }

    /**
     * @param \Magento\Catalog\Helper\Product\Configuration $subject
     * @param callable $proceed
     * @param \Magento\Catalog\Model\Product\Configuration\Item\ItemInterface $item
     *
     * @return array
     */
    public function aroundGetCustomOptions(
        \Magento\Catalog\Helper\Product\Configuration $subject,
        \Closure $proceed,
        \Magento\Catalog\Model\Product\Configuration\Item\ItemInterface $item
    ) {
        $result = $proceed($item);
        $recurringOptions = $this->catalogProductConfigurationHelper->getRecurringOptions($item);
        $result = array_merge($result, $recurringOptions);
        return $result;
    }
}
