<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Sarp\Model\Plugin\Catalog\Product;

use Exto\Sarp\Model\Product\ProductManagement;

/**
 * Class View
 */
class View
{
    /** @var ProductManagement */
    protected $productManagement;

    /**
     * @param ProductManagement $productManagement
     */
    public function __construct(
        ProductManagement $productManagement
    ) {
        $this->productManagement = $productManagement;
    }

    /**
     * @return bool
     */
    public function aroundHasOptions(
        \Magento\Catalog\Block\Product\View $subject,
        \Closure $proceed
    ) {
        $product = $subject->getProduct();
        if ($this->productManagement->hasRecurringOptions($product)) {
            return true;
        }
        return $proceed();
    }
}
