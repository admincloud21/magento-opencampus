<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Exto\Sarp\Model;

/**
 * Class SubscriptionTemplate
 */
class SubscriptionTemplateRegistry
{
    /**
     * @var SubscriptionTemplateFactory
     */
    private $subscriptionTemplateFactory;

    /**
     * SubscriptionTemplate registry by id
     * @var array
     */
    private $subscriptionTemplateRegistryById = [];

    /**
     * SubscriptionTemplate resource
     * @var ResourceModel\SubscriptionTemplate
     */
    private $subscriptionTemplateResource;

    /**
     * SubscriptionTemplateRegistry constructor.
     * @param SubscriptionTemplateFactory $subscriptionTemplateFactory
     * @param ResourceModel\SubscriptionTemplate $subscriptionTemplateResource
     */
    public function __construct(
        SubscriptionTemplateFactory $subscriptionTemplateFactory,
        ResourceModel\SubscriptionTemplate $subscriptionTemplateResource
    ) {
        $this->subscriptionTemplateResource = $subscriptionTemplateResource;
        $this->subscriptionTemplateFactory = $subscriptionTemplateFactory;
    }

    /**
     * @param int $subscriptionTemplateId
     * @return SubscriptionTemplate
     * @throws NoSuchEntityException
     */
    public function retrieve($subscriptionTemplateId)
    {
        if (!isset($this->subscriptionTemplateRegistryById[$subscriptionTemplateId])) {
            /** @var SubscriptionTemplate $subscriptionTemplate */
            $subscriptionTemplate = $this->subscriptionTemplateFactory->create();
            $this->subscriptionTemplateResource->load($subscriptionTemplate, $subscriptionTemplateId);
            if (!$subscriptionTemplate->getId()) {
                throw NoSuchEntityException::singleField('subscriptionTemplateId', $subscriptionTemplateId);
            } else {
                $this->subscriptionTemplateRegistryById[$subscriptionTemplateId] = $subscriptionTemplate;
            }
        }
        return $this->subscriptionTemplateRegistryById[$subscriptionTemplateId];
    }

    /**
     * @param int $subscriptionTemplateId
     * @return void
     */
    public function remove($subscriptionTemplateId)
    {
        if (isset($this->subscriptionTemplateRegistryById[$subscriptionTemplateId])) {
            unset($this->subscriptionTemplateRegistryById[$subscriptionTemplateId]);
        }
    }

    /**
     * @param SubscriptionTemplate $subscriptionTemplate
     * @return $this
     */
    public function push(SubscriptionTemplate $subscriptionTemplate)
    {
        $this->subscriptionTemplateRegistryById[$subscriptionTemplate->getId()] = $subscriptionTemplate;
        return $this;
    }
}
