<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model;

use Magento\Framework\Profiler;
use Exto\Sarp\Api\Data\ProfileInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Exto\Sarp\Model\ResourceModel\Profile as ProfileResourceModel;
use Exto\Sarp\Model\ResourceModel\Profile\Collection as ProfileCollection;
use Exto\Sarp\Api\Data\ProfileInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

/**
 * Class Profile
 */
class Profile extends AbstractModel
{
    /**
     * @var \Exto\Sarp\Api\Data\ProfileInterfaceFactory
     */
    private $profileDataFactory;

    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * Profile constructor.
     * @param Context $context
     * @param Registry $registry
     * @param ProfileResourceModel $resource
     * @param ProfileCollection $resourceCollection
     * @param ProfileInterfaceFactory $profileDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ProfileResourceModel $resource,
        ProfileCollection $resourceCollection,
        ProfileInterfaceFactory $profileDataFactory,
        DataObjectHelper $dataObjectHelper,
        array $data = []
    ) {
        $this->profileDataFactory = $profileDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Initialize resource mode
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ProfileResourceModel::class);
    }

    /**
     * Retrieve Profile model with data
     *
     * @return \Exto\Sarp\Api\Data\ProfileInterface
     */
    public function getDataModel()
    {
        $data = $this->getData();
        /** @var ProfileInterface $dataObject */
        $dataObject = $this->profileDataFactory->create();
        $dataObject->setQuoteData($data[ProfileInterface::QUOTE_DATA])
            ->setQuoteItemsData($data[ProfileInterface::QUOTE_ITEMS_DATA])
            ->setBillingAddressData($data[ProfileInterface::BILLING_ADDRESS_DATA])
            ->setShippingAddressData($data[ProfileInterface::SHIPPING_ADDRESS_DATA])
        ;
        unset($data[ProfileInterface::QUOTE_DATA]);
        unset($data[ProfileInterface::QUOTE_ITEMS_DATA]);
        unset($data[ProfileInterface::BILLING_ADDRESS_DATA]);
        unset($data[ProfileInterface::SHIPPING_ADDRESS_DATA]);
        $this->dataObjectHelper->populateWithArray(
            $dataObject,
            $data,
            ProfileInterface::class
        );
        return $dataObject;
    }
}
