<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Quote;

use Magento\Quote\Model\Quote as QuoteEntity;
use Magento\Quote\Model\Quote\Item as QuoteItem;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\QuoteRepository;
use Exto\Sarp\Model\QuoteHelper;

/**
 * Class VirtualManagement
 */
class VirtualManagement
{
    /** @var \Magento\Quote\Model\QuoteFactory */
    protected $quoteFactory;

    /** @var \Magento\Quote\Model\QuoteRepository */
    protected $quoteRepository;

    /** @var QuoteEntity[] */
    protected $cacheVirtualQuote = [];

    /**
     * VirtualManagement constructor.
     * @param QuoteFactory $quoteFactory
     * @param QuoteRepository $quoteRepository
     * @param QuoteHelper $quoteHelper
     */
    public function __construct(
        QuoteFactory $quoteFactory,
        QuoteRepository $quoteRepository,
        QuoteHelper $quoteHelper
    ) {
        $this->quoteFactory = $quoteFactory;
        $this->quoteHelper = $quoteHelper;
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * @param QuoteEntity $realQuote
     *
     * @return mixed
     */
    public function getVirtualQuoteList(QuoteEntity $realQuote)
    {
        $cacheKey = $this->getQuoteCacheKey($realQuote);
        if (array_key_exists($cacheKey, $this->cacheVirtualQuote)) {
            return $this->cacheVirtualQuote[$cacheKey];
        }
        $processedItemIdList = [];
        foreach ($realQuote->getAllVisibleItems() as $item) {
            if (in_array($item->getId(), $processedItemIdList)) {
                continue;
            }
            $processedItemIdList[] = $item->getId();
            $virtualQuote = $this->getEmptyCart($realQuote);
            $this->addRealItemToVirtualQuote($item, $virtualQuote);
            foreach ($realQuote->getAllVisibleItems() as $itemToCompare) {
                if (in_array($itemToCompare->getId(), $processedItemIdList)) {
                    continue;
                }
                if (!$this->isIdenticalItems($item, $itemToCompare)) {
                    continue;
                }
                $processedItemIdList[] = $itemToCompare->getId();
                $this->addRealItemToVirtualQuote($itemToCompare, $virtualQuote);
            }
            $virtualQuote->getShippingAddress()->unsetData('cached_items_all')->unsetData('item_qty');
            $virtualQuote->getShippingAddress()->setCollectShippingRates(true);
            $virtualQuote->collectTotals();
            $this->cacheVirtualQuote[$cacheKey][] = $virtualQuote;
        }
        return $this->cacheVirtualQuote[$cacheKey];
    }

    /**
     * @param QuoteEntity $realQuote
     * @param QuoteEntity $virtualQuote
     *
     * @return $this
     */
    public function removeVirtualItems(QuoteEntity $realQuote, QuoteEntity $virtualQuote)
    {
        foreach ($virtualQuote->getAllVisibleItems() as $item) {
            $realQuote->removeItem($item->getData('real_id'));
        }
        $this->quoteRepository->save($realQuote);
        return $this;
    }

    /**
     * @param QuoteItem $item
     * @param QuoteItem $itemToCompare
     *
     * @return bool
     */
    protected function isIdenticalItems(QuoteItem $item, QuoteItem $itemToCompare)
    {
        $isItemRecurring = $this->quoteHelper->isRecurringQuoteItem($item);
        $isItemToCompareRecurring = $this->quoteHelper->isRecurringQuoteItem($itemToCompare);
        if (!$isItemRecurring && !$isItemToCompareRecurring) {
            return true;
        }
        if ($isItemRecurring !== $isItemToCompareRecurring) {
            return false;
        }
        $itemFrequencyOption = $this->quoteHelper->getQuoteItemFrequencyOption($item);
        $itemToCompareFrequencyOption = $this->quoteHelper->getQuoteItemFrequencyOption($itemToCompare);
        if ($itemFrequencyOption !== $itemToCompareFrequencyOption) {
            return false;
        }
        $itemPeriodOption = $this->quoteHelper->getQuoteItemPeriodOption($item);
        $itemToComparePeriodOption = $this->quoteHelper->getQuoteItemPeriodOption($itemToCompare);
        if ($itemPeriodOption !== $itemToComparePeriodOption) {
            return false;
        }
        $itemFrequencyOption = $this->quoteHelper->getQuoteItemCyclesOption($item);
        $itemToCompareFrequencyOption = $this->quoteHelper->getQuoteItemCyclesOption($itemToCompare);
        if ($itemFrequencyOption !== $itemToCompareFrequencyOption) {
            return false;
        }
        $itemStartDateOption = $this->quoteHelper->getQuoteItemStartDateOption($item);
        $itemToCompareStartDateOption = $this->quoteHelper->getQuoteItemStartDateOption($itemToCompare);
        if ($itemStartDateOption !== $itemToCompareStartDateOption) {
            return false;
        }
        return true;
    }

    /**
     * @param QuoteEntity $realQuote
     *
     * @return QuoteEntity
     */
    protected function getEmptyCart(QuoteEntity $realQuote)
    {
        /** @var QuoteEntity $virtualQuote */
        $virtualQuote = $this->quoteFactory->create();
        $virtualQuote->setStoreId($realQuote->getStoreId());
        $virtualQuote->setCustomer($realQuote->getCustomer());
        $virtualQuote->setCustomerIsGuest(0);
        $virtualQuote->setBillingAddress(clone $realQuote->getBillingAddress());
        $virtualQuote->getBillingAddress()->unsetData('cached_items_all')->unsetData('item_qty');
        $virtualQuote->setShippingAddress(clone $realQuote->getShippingAddress());
        $virtualQuote->getShippingAddress()->unsetData('cached_items_all')->unsetData('item_qty');
        $virtualQuote->setCurrency($realQuote->getCurrency());
        $virtualQuote->setData('real_quote', $realQuote);
        if ($virtualQuote->getCouponCode()) {
            $virtualQuote->setCouponCode($realQuote->getCouponCode());
        }
        $virtualQuote->getPayment()->setQuote($virtualQuote);
        $virtualQuote->getPayment()->setMethod($realQuote->getPayment()->getMethod());
        $virtualQuote->getPayment()->setAdditionalInformation(
            $realQuote->getPayment()->getAdditionalInformation()
        );
        return $virtualQuote;
    }

    /**
     * @param QuoteItem $item
     * @param QuoteEntity $virtualQuote
     *
     * @return $this
     */
    protected function addRealItemToVirtualQuote(QuoteItem $item, QuoteEntity $virtualQuote)
    {
        $newItem = clone $item;
        $newItem->setData('real_id', $item->getId());
        $virtualQuote->addItem($newItem);
        if ($item->getHasChildren()) {
            foreach ($item->getChildren() as $child) {
                $newChild = clone $child;
                $newChild->setData('real_id', $child->getId());
                $newChild->setParentItem($newItem);
                $virtualQuote->addItem($newChild);
            }
        }
        return $this;
    }

    /**
     * @param QuoteEntity $realQuote
     *
     * @return string
     */
    protected function getQuoteCacheKey(QuoteEntity $realQuote)
    {
        return $realQuote->getId() . '-' . $realQuote->getUpdatedAt();
    }
}
