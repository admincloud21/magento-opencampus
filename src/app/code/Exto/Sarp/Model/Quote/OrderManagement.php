<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Quote;

use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Model\Service\OrderService;
use Magento\Sales\Api\Data\OrderInterface;

/**
 * Class OrderManagement
 */
class OrderManagement extends OrderService implements OrderManagementInterface
{
    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return \Magento\Sales\Api\Data\OrderInterface
     * @throws \Exception
     */
    public function place(OrderInterface $order)
    {
        try {
            //do not place the order
            return $this->orderRepository->save($order);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
