<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Quote;

use Magento\Quote\Model\Quote as QuoteEntity;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Quote\Model\Quote\Address\ToOrder as ToOrderConverter;
use Magento\Quote\Model\Quote\Address\ToOrderAddress as ToOrderAddressConverter;
use Magento\Quote\Model\Quote\Item\ToOrderItem as ToOrderItemConverter;
use Magento\Quote\Model\Quote\Payment\ToOrderPayment as ToOrderPaymentConverter;
use Magento\Quote\Model\CustomerManagement;
use Magento\Quote\Model\QuoteValidator;
use Magento\Sales\Api\Data\OrderInterfaceFactory as OrderFactory;
use Magento\Sales\Api\OrderManagementInterface as SalesOrderManagement;
use Magento\Authorization\Model\UserContextInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Quote\Model\Quote\AddressFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Quote\Model\QuoteFactory;
use Exto\Sarp\Model\ProfileService;
use Exto\Sarp\Model\QuoteHelper;

/**
 * Class QuoteManagement
 */
class QuoteManagement extends \Magento\Quote\Model\QuoteManagement
{
    /** @var VirtualManagement */
    protected $virtualManagement;

    /** @var \Exto\Sarp\Model\ProfileService */
    protected $profileService;

    /** @var \Exto\Sarp\Model\QuoteHelper */
    protected $quoteHelper;

    /**
     * QuoteManagement constructor.
     * @param EventManager $eventManager
     * @param QuoteValidator $quoteValidator
     * @param OrderFactory $orderFactory
     * @param SalesOrderManagement $orderManagement
     * @param CustomerManagement $customerManagement
     * @param ToOrderConverter $quoteAddressToOrder
     * @param ToOrderAddressConverter $quoteAddressToOrderAddress
     * @param ToOrderItemConverter $quoteItemToOrderItem
     * @param ToOrderPaymentConverter $quotePaymentToOrderPayment
     * @param UserContextInterface $userContext
     * @param CartRepositoryInterface $quoteRepository
     * @param CustomerRepositoryInterface $customerRepository
     * @param CustomerFactory $customerModelFactory
     * @param AddressFactory $quoteAddressFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param StoreManagerInterface $storeManager
     * @param CheckoutSession $checkoutSession
     * @param CustomerSession $customerSession
     * @param AccountManagementInterface $accountManagement
     * @param QuoteFactory $quoteFactory
     * @param VirtualManagement $virtualManagement
     * @param ProfileService $profileService
     * @param QuoteHelper $quoteHelper
     */
    public function __construct(
        EventManager $eventManager,
        QuoteValidator $quoteValidator,
        OrderFactory $orderFactory,
        SalesOrderManagement $orderManagement,
        CustomerManagement $customerManagement,
        ToOrderConverter $quoteAddressToOrder,
        ToOrderAddressConverter $quoteAddressToOrderAddress,
        ToOrderItemConverter $quoteItemToOrderItem,
        ToOrderPaymentConverter $quotePaymentToOrderPayment,
        UserContextInterface $userContext,
        CartRepositoryInterface $quoteRepository,
        CustomerRepositoryInterface $customerRepository,
        CustomerFactory $customerModelFactory,
        AddressFactory $quoteAddressFactory,
        DataObjectHelper $dataObjectHelper,
        StoreManagerInterface $storeManager,
        CheckoutSession $checkoutSession,
        CustomerSession $customerSession,
        AccountManagementInterface $accountManagement,
        QuoteFactory $quoteFactory,
        VirtualManagement $virtualManagement,
        ProfileService $profileService,
        QuoteHelper $quoteHelper
    ) {
        parent::__construct(
            $eventManager,
            $quoteValidator,
            $orderFactory,
            $orderManagement,
            $customerManagement,
            $quoteAddressToOrder,
            $quoteAddressToOrderAddress,
            $quoteItemToOrderItem,
            $quotePaymentToOrderPayment,
            $userContext,
            $quoteRepository,
            $customerRepository,
            $customerModelFactory,
            $quoteAddressFactory,
            $dataObjectHelper,
            $storeManager,
            $checkoutSession,
            $customerSession,
            $accountManagement,
            $quoteFactory
        );
        $this->virtualManagement = $virtualManagement;
        $this->profileService = $profileService;
        $this->quoteHelper = $quoteHelper;
    }

    /**
     * @param QuoteEntity $quote
     * @param array $orderData
     *
     * @return \Magento\Framework\Model\AbstractExtensibleModel|\Magento\Sales\Api\Data\OrderInterface|null|object
     * @throws \Exception
     */
    public function submit(QuoteEntity $quote, $orderData = [])
    {
        if (!$this->quoteHelper->isQuoteContainRecurringItems($quote)) {
            return parent::submit($quote, $orderData);//go to original steps if no sarp products in cart
        }
        $standardQuote = null;
        $successProfileIds = [];
        foreach ($this->virtualManagement->getVirtualQuoteList($quote) as $virtualQuote) {
            /** @var QuoteEntity $virtualQuote*/
            if (!$this->quoteHelper->isQuoteContainRecurringItems($virtualQuote)) {
                $standardQuote = $virtualQuote;
                continue;
            }
            $profile = $this->submitProfile($virtualQuote);
            $this->virtualManagement->removeVirtualItems($quote, $virtualQuote);
            $successProfileIds[] = $profile->getId();
        }
        $this->checkoutSession->setLastRecurringProfileIds($successProfileIds);
        if (null !== $standardQuote) {
            $quote->getShippingAddress()->unsetData('cached_items_all')->unsetData('item_qty');
            $quote->getShippingAddress()->getCollectShippingRates(true);
            $quote->setTotalsCollectedFlag(false)->collectTotals();
            return $this->submitQuote($quote, $orderData);//place order after sarp profiles
        }
        if (!$quote->getCustomerIsGuest()) {
            if ($quote->getCustomerId()) {
                $this->_prepareCustomerQuote($quote);
            }
            $this->customerManagement->populateCustomerInfo($quote);
        }
        if (count($successProfileIds) > 0) {
            $this->checkoutSession ->setRedirectUrl(
                $quote->getPayment()->getOrderPlaceRedirectUrl()
            );
            $quote->setIsActive(false);
            $this->quoteRepository->save($quote);
        }
        return null;
    }

    /**
     * @param QuoteEntity $quote
     *
     * @return \Exto\Sarp\Api\Data\ProfileInterface
     */
    protected function submitProfile(QuoteEntity $quote)
    {
        $profile = $this->profileService->createProfileFromQuote($quote);
        $profile = $this->profileService->createProfileOnExternalService($profile, $quote);
        $profile = $this->profileService->saveProfile($profile);
        $this->profileService->sendNewProfileNotification($profile);
        return $profile;
    }
}
