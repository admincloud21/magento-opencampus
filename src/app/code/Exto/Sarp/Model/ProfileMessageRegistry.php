<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Exto\Sarp\Model\ProfileMessageFactory;
use Exto\Sarp\Model\ResourceModel\ProfileMessage as ProfileMessageResourceModel;

/**
 * Class ProfileMessage
 */
class ProfileMessageRegistry
{
    /**
     * @var ProfileMessageFactory
     */
    private $profileMessageFactory;

    /**
     * ProfileMessage registry by id
     * @var array
     */
    private $profileMessageRegistryById = [];

    /**
     * ProfileMessage resource
     * @var ResourceModel\ProfileMessage
     */
    private $profileMessageResource;

    /**
     * ProfileMessageRegistry constructor.
     * @param \Exto\Sarp\Model\ProfileMessageFactory $profileMessageFactory
     * @param ProfileMessageResourceModel $profileMessageResource
     */
    public function __construct(
        ProfileMessageFactory $profileMessageFactory,
        ProfileMessageResourceModel $profileMessageResource
    ) {
        $this->profileMessageResource = $profileMessageResource;
        $this->profileMessageFactory = $profileMessageFactory;
    }

    /**
     * @param int $profileMessageId
     * @return ProfileMessage
     * @throws NoSuchEntityException
     */
    public function retrieve($profileMessageId)
    {
        if (!isset($this->profileMessageRegistryById[$profileMessageId])) {
            /** @var ProfileMessage $profileMessage */
            $profileMessage = $this->profileMessageFactory->create();
            $this->profileMessageResource->load($profileMessage, $profileMessageId);
            if (!$profileMessage->getId()) {
                throw NoSuchEntityException::singleField('profileMessageId', $profileMessageId);
            } else {
                $this->profileMessageRegistryById[$profileMessageId] = $profileMessage;
            }
        }
        return $this->profileMessageRegistryById[$profileMessageId];
    }

    /**
     * @param int $profileMessageId
     * @return void
     */
    public function remove($profileMessageId)
    {
        if (isset($this->profileMessageRegistryById[$profileMessageId])) {
            unset($this->profileMessageRegistryById[$profileMessageId]);
        }
    }

    /**
     * @param ProfileMessage $profileMessage
     * @return $this
     */
    public function push(ProfileMessage $profileMessage)
    {
        $this->profileMessageRegistryById[$profileMessage->getId()] = $profileMessage;
        return $this;
    }
}
