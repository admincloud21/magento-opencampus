<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model;

use Magento\Framework\Profiler;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Exto\Sarp\Model\ResourceModel\ProfileMessage as ProfileMessageResourceModel;
use Exto\Sarp\Model\ResourceModel\ProfileMessage\Collection as ProfileMessageCollection;
use Exto\Sarp\Api\Data\ProfileMessageInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Exto\Sarp\Api\Data\ProfileMessageInterface;

/**
 * Class ProfileMessage
 */
class ProfileMessage extends AbstractModel
{
    /**
     * @var \Exto\Sarp\Api\Data\ProfileMessageInterfaceFactory
     */
    private $profileMessageDataFactory;

    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * ProfileMessage constructor.
     * @param Context $context
     * @param Registry $registry
     * @param ProfileMessageResourceModel $resource
     * @param ProfileMessageCollection $resourceCollection
     * @param ProfileMessageInterfaceFactory $profileMessageDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ProfileMessageResourceModel $resource,
        ProfileMessageCollection $resourceCollection,
        ProfileMessageInterfaceFactory $profileMessageDataFactory,
        DataObjectHelper $dataObjectHelper,
        array $data = []
    ) {
        $this->profileMessageDataFactory = $profileMessageDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Initialize resource mode
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ProfileMessageResourceModel::class);
    }

    /**
     * Retrieve ProfileMessage model with data
     *
     * @return \Exto\Sarp\Api\Data\ProfileMessageInterface
     */
    public function getDataModel()
    {
        $data = $this->getData();
        $dataObject = $this->profileMessageDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $dataObject,
            $data,
            ProfileMessageInterface::class
        );
        return $dataObject;
    }
}
