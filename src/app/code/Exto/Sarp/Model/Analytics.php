<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model;

use Exto\Sarp\Model\ResourceModel\Profile\CollectionFactory as ProfileCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Framework\Data\Collection\AbstractDb;

/**
 * Class Analytics
 */
class Analytics
{
    /**
     * @var ResourceModel\Profile\CollectionFactory
     */
    protected $profileCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory
     */
    protected $customerCollectionFactory;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * Analytics constructor.
     * @param ProfileCollectionFactory $profileCollectionFactory
     * @param ProductCollectionFactory $productCollectionFactory
     * @param CustomerCollectionFactory $customerCollectionFactory
     * @param OrderCollectionFactory $orderCollectionFactory
     */
    public function __construct(
        ProfileCollectionFactory $profileCollectionFactory,
        ProductCollectionFactory $productCollectionFactory,
        CustomerCollectionFactory $customerCollectionFactory,
        OrderCollectionFactory $orderCollectionFactory
    ) {
        $this->profileCollectionFactory = $profileCollectionFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->orderCollectionFactory = $orderCollectionFactory;
    }

    /**
     * @param \DateTime|null $from
     * @param \DateTime|null $to
     * @return array
     */
    public function getProfileListByDate(\DateTime $from = null, \DateTime $to = null)
    {
        /** @var \Exto\Sarp\Model\ResourceModel\Profile\Collection $profileCollection */
        $profileCollection = $this->profileCollectionFactory->create();
        $this->addDateConditions($profileCollection, 'main_table.created_date', $from, $to);

        return [
            'list' => $profileCollection->getItems(),
            'size' => $profileCollection->getSize()
        ];
    }

    /**
     * @param int $customerGroupId
     * @return array
     */
    public function getProfileListByCustomerGroup($customerGroupId)
    {
        /** @var \Exto\Sarp\Model\ResourceModel\Profile\Collection $profileCollection */
        $profileCollection = $this->profileCollectionFactory->create();
        $profileCollection->getSelect()->join(
            ['customer' => $profileCollection->getTable('customer_entity')],
            "main_table.customer_id = customer.entity_id AND customer.group_id = {$customerGroupId}",
            []
        );

        return [
            'list' => $profileCollection->getItems(),
            'size' => $profileCollection->getSize()
        ];
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection
     * @return array
     */
    public function getProfileListByProductList(ProductCollection $productCollection)
    {
        /** @var \Exto\Sarp\Model\ResourceModel\Profile\Collection $profileCollection */
        $profileCollection = $this->profileCollectionFactory->create();

        $productIds = $productCollection->getAllIds();
        $conditions = [];
        foreach ($productIds as $productId) {
            $conditions[] = ['finset' => [$productId]];
        }
        $profileCollection->addFieldToFilter('product_ids', $conditions);

        return [
            'list' => $profileCollection->getItems(),
            'size' => $profileCollection->getSize()
        ];
    }

    /**
     * @param string $statusCode
     * @return array
     */
    public function getProfileListByProfileStatus($statusCode)
    {
        /** @var \Exto\Sarp\Model\ResourceModel\Profile\Collection $profileCollection */
        $profileCollection = $this->profileCollectionFactory->create();
        $profileCollection->addFieldToFilter('status', ['eq' => $statusCode]);
        return [
            'list' => $profileCollection->getItems(),
            'size' => $profileCollection->getSize()
        ];
    }

    /**
     * @param string $statusCode
     * @return array
     */
    public function getProfileListByOrderStatus($statusCode)
    {
        /** @var \Exto\Sarp\Model\ResourceModel\Profile\Collection $profileCollection */
        $profileCollection = $this->profileCollectionFactory->create();

        $profileCollection->addOrderInfo();
        $profileCollection->addFieldToFilter('order_info.status', ['eq' => $statusCode]);
        return [
            'list' => $profileCollection->getItems(),
            'size' => $profileCollection->getSize()
        ];
    }

    /**
     * @param \DateTime|null $from
     * @param \DateTime|null $to
     * @return array
     */
    public function getOrderListByDate(\DateTime $from = null, \DateTime $to = null)
    {
        /** @var \Magento\Sales\Model\ResourceModel\Order\Collection $orderCollection */
        $orderCollection = $this->orderCollectionFactory->create();
        $this->joinProfileToOrder($orderCollection);
        $this->addDateConditions($orderCollection, 'profile.created_date', $from, $to);

        return [
            'list' => $orderCollection->getItems(),
            'size' => $orderCollection->getSize()
        ];
    }

    /**
     * @param int $customerGroupId
     * @return array
     */
    public function getOrderListByCustomerGroup($customerGroupId)
    {
        /** @var \Magento\Sales\Model\ResourceModel\Order\Collection $orderCollection */
        $orderCollection = $this->orderCollectionFactory->create();
        $this->joinProfileToOrder($orderCollection);
        $orderCollection->getSelect()->join(
            ['customer' => $orderCollection->getTable('customer_entity')],
            "profile.customer_id = customer.entity_id AND customer.group_id = {$customerGroupId}",
            []
        );

        return [
            'list' => $orderCollection->getItems(),
            'size' => $orderCollection->getSize()
        ];
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection
     * @return array
     */
    public function getOrderListByProductList(ProductCollection $productCollection)
    {
        /** @var \Magento\Sales\Model\ResourceModel\Order\Collection $orderCollection */
        $orderCollection = $this->orderCollectionFactory->create();
        $this->joinProfileToOrder($orderCollection);
        $productIds = $productCollection->getAllIds();
        $conditions = [];
        foreach ($productIds as $productId) {
            $conditions[] = ['finset' => [$productId]];
        }
        $orderCollection->addFieldToFilter('product_ids', $conditions);

        return [
            'list' => $orderCollection->getItems(),
            'size' => $orderCollection->getSize()
        ];
    }

    /**
     * @param string $statusCode
     * @return array
     */
    public function getOrderListByProfileStatus($statusCode)
    {
        /** @var \Magento\Sales\Model\ResourceModel\Order\Collection $orderCollection */
        $orderCollection = $this->orderCollectionFactory->create();
        $this->joinProfileToOrder($orderCollection);
        $orderCollection->addFieldToFilter('profile.status', ['eq' => $statusCode]);

        return [
            'list' => $orderCollection->getItems(),
            'size' => $orderCollection->getSize()
        ];
    }

    /**
     * @param string $statusCode
     * @return array
     */
    public function getOrderListByOrderStatus($statusCode)
    {
        /** @var \Magento\Sales\Model\ResourceModel\Order\Collection $orderCollection */
        $orderCollection = $this->orderCollectionFactory->create();
        $this->joinProfileToOrder($orderCollection);
        $orderCollection->addFieldToFilter('main_table.status', ['eq' => $statusCode]);

        return [
            'list' => $orderCollection->getItems(),
            'size' => $orderCollection->getSize()
        ];
    }

    /**
     * @param \DateTime|null $from
     * @param \DateTime|null $to
     * @return array
     */
    public function getCustomerListByDate(\DateTime $from = null, \DateTime $to = null)
    {
        /** @var \Magento\Customer\Model\ResourceModel\Customer\Collection $customerCollection */
        $customerCollection = $this->customerCollectionFactory->create();
        $customerCollection->getSelect()
            ->joinLeft(
                ['profile' => $customerCollection->getTable('exto_sarp_profile')],
                "e.entity_id = profile.customer_id",
                []
            );

        $customerCondition = $customerCollection->getConnection()
            ->prepareSqlCondition('profile.id', ['notnull' => true]);
        $customerCollection->getSelect()->where($customerCondition);
        $customerCollection->getSelect()->group('e.entity_id');
        $this->addDateConditions($customerCollection, 'profile.created_date', $from, $to);

        return [
            'list' => $customerCollection->getItems(),
            'size' => $customerCollection->getSize()
        ];
    }

    /**
     * @param \DateTime|null $from
     * @param \DateTime|null $to
     * @return array
     */
    public function getProductListByDate(\DateTime $from = null, \DateTime $to = null)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
        $productCollection = $this->productCollectionFactory->create();
        $productCollection->getSelect()
            ->joinLeft(
                ['profile' => $productCollection->getTable('exto_sarp_profile')],
                "FIND_IN_SET(e.entity_id, profile.product_ids)",
                []
            );
        $productCondition = $productCollection->getConnection()
            ->prepareSqlCondition('profile.id', ['notnull' => true]);
        $productCollection->getSelect()->where($productCondition);
        $productCollection->getSelect()->group('e.entity_id');
        $this->addDateConditions($productCollection, 'profile.created_date', $from, $to);
        return [
            'list' => $productCollection->getItems(),
            'size' => $productCollection->getSize()
        ];
    }

    /**
     * @param \Magento\Framework\Data\Collection\AbstractDb $collection
     * @param string $field
     * @param \DateTime|null $from
     * @param \DateTime|null $to
     * @return $this
     */
    private function addDateConditions(
        AbstractDb &$collection,
        $field,
        \DateTime $from = null,
        \DateTime $to = null
    ) {
        if (null === $from) {
            $from = new \DateTime();
        }
        if (null === $to) {
            $to = new \DateTime();
        }
        $fromDate = $from->format('Y-m-d H:i:s');
        $toDate = $to->format('Y-m-d H:i:s');

        $fromCondition = $collection->getConnection()
            ->prepareSqlCondition($field, [['gt' => $fromDate], ['null' => $fromDate]]);
        $toCondition = $collection->getConnection()
            ->prepareSqlCondition($field, [['lt' => $toDate], ['null' => $toDate]]);

        $collection->getSelect()->where($fromCondition);
        $collection->getSelect()->where($toCondition);

        return $this;
    }

    /**
     * @param \Magento\Framework\Data\Collection\AbstractDb $collection
     * @return $this
     */
    private function joinProfileToOrder(AbstractDb &$collection)
    {
        $collection->getSelect()
            ->joinLeft(
                ['profile_order' => $collection->getTable('exto_sarp_profile_order')],
                "main_table.entity_id = profile_order.order_id",
                []
            )->joinLeft(
                ['profile' => $collection->getTable('exto_sarp_profile')],
                "profile_order.profile_id = profile.id",
                []
            );
        $collection->addFieldToFilter('profile.id', ['notnull' => true]);
        $collection->getSelect()->group('main_table.entity_id');

        return $this;
    }
}
