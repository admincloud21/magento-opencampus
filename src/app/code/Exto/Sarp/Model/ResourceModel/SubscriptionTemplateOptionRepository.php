<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\ResourceModel;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Exto\Sarp\Api\SubscriptionTemplateOptionRepositoryInterface;
use Exto\Sarp\Model\SubscriptionTemplateOptionFactory;
use Exto\Sarp\Model\SubscriptionTemplateOptionRegistry;
use Exto\Sarp\Api\Data\SubscriptionTemplateOptionSearchResultsInterfaceFactory;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Exto\Sarp\Model\ResourceModel\SubscriptionTemplateOption;
use Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Class SubscriptionTemplateOptionRepository
 */
class SubscriptionTemplateOptionRepository implements SubscriptionTemplateOptionRepositoryInterface
{
    /**
     * @var \Exto\Sarp\Model\SubscriptionTemplateOptionFactory
     */
    protected $subscriptionTemplateOptionFactory;

    /**
     * @var \Exto\Sarp\Model\SubscriptionTemplateOptionRegistry
     */
    protected $subscriptionTemplateOptionRegistry;

    /**
     * @var \Exto\Sarp\Api\Data\SubscriptionTemplateOptionSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var \Magento\Framework\Api\ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;

    /**
     * @var SubscriptionTemplateOption
     */
    protected $subscriptionTemplateOptionResource;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchBuilder;

    /**
     * SubscriptionTemplateOptionRepository constructor.
     * @param SubscriptionTemplateOptionFactory $subscriptionTemplateOptionFactory
     * @param SubscriptionTemplateOptionRegistry $subscriptionTemplateOptionRegistry
     * @param SubscriptionTemplateOptionSearchResultsInterfaceFactory $searchResultsFactory
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param \Exto\Sarp\Model\ResourceModel\SubscriptionTemplateOption $subscriptionTemplateOptionResource
     * @param SearchCriteriaBuilder $searchBuilder
     */
    public function __construct(
        SubscriptionTemplateOptionFactory $subscriptionTemplateOptionFactory,
        SubscriptionTemplateOptionRegistry $subscriptionTemplateOptionRegistry,
        SubscriptionTemplateOptionSearchResultsInterfaceFactory $searchResultsFactory,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        SubscriptionTemplateOption $subscriptionTemplateOptionResource,
        SearchCriteriaBuilder $searchBuilder
    ) {
        $this->subscriptionTemplateOptionFactory = $subscriptionTemplateOptionFactory;
        $this->subscriptionTemplateOptionRegistry = $subscriptionTemplateOptionRegistry;
        $this->subscriptionTemplateOptionResource = $subscriptionTemplateOptionResource;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->searchBuilder = $searchBuilder;
    }

    /**
     * @param \Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterface $subscriptionTemplateOption
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterface
     * @throws \Exception
     */
    public function save(SubscriptionTemplateOptionInterface $subscriptionTemplateOption)
    {
        $subscriptionTemplateOptionData = $this->extensibleDataObjectConverter->toNestedArray(
            $subscriptionTemplateOption,
            [],
            SubscriptionTemplateOptionInterface::class
        );
        $subscriptionTemplateOptionModel = $this->subscriptionTemplateOptionFactory->create();
        $subscriptionTemplateOptionModel->addData($subscriptionTemplateOptionData);
        $subscriptionTemplateOptionModel->setId($subscriptionTemplateOption->getId());
        $this->subscriptionTemplateOptionResource->save($subscriptionTemplateOptionModel);
        $this->subscriptionTemplateOptionRegistry->push($subscriptionTemplateOptionModel);
        $savedObject = $this->get($subscriptionTemplateOptionModel->getId());
        return $savedObject;
    }

    /**
     * @param int $subscriptionTemplateOptionId
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($subscriptionTemplateOptionId)
    {
        $subscriptionTemplateOptionModel = $this
            ->subscriptionTemplateOptionRegistry
            ->retrieve($subscriptionTemplateOptionId);
        return $subscriptionTemplateOptionModel->getDataModel();
    }

    /**
     * @param \Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterface $subscriptionTemplateOption
     * @return $this
     */
    public function delete(SubscriptionTemplateOptionInterface $subscriptionTemplateOption)
    {
        $optionModel = $this->subscriptionTemplateOptionRegistry->retrieve($subscriptionTemplateOption->getId());
        $optionModel->getResource()->delete($optionModel);
        $this->subscriptionTemplateOptionRegistry->remove($subscriptionTemplateOption->getId());
        return $this;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateOptionSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Exto\Sarp\Api\Data\SubscriptionTemplateOptionSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $collection = $this->subscriptionTemplateOptionFactory->create()->getCollection();
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $fields[] = $filter->getField();
                $conditions[] = [$condition => $filter->getValue()];

            }
            if ($fields) {
                $collection->addFieldToFilter($fields, $conditions);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        $subscriptionTemplateOptions = [];
        foreach ($collection as $subscriptionTemplateOptionModel) {
            $subscriptionTemplateOptions[] = $subscriptionTemplateOptionModel->getDataModel();
        }
        $searchResults->setItems($subscriptionTemplateOptions);
        return $searchResults;
    }

    /**
     * @param int $templateId
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateOptionSearchResultsInterface
     */
    public function getListByTemplateId($templateId)
    {
        $criteria = $this->searchBuilder
            ->addFilter('template_id', $templateId, 'eq')
            ->create();
        return $this->getList($criteria);
    }
}
