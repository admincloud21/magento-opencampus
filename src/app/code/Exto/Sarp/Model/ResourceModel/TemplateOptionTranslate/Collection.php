<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\ResourceModel\TemplateOptionTranslate;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Exto\Sarp\Model\TemplateOptionTranslate as TemplateOptionTranslateModel;
use Exto\Sarp\Model\ResourceModel\TemplateOptionTranslate as TemplateOptionTranslateResourceModel;

/**
 * Class Collection
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    
    /**
     * Init collection and determine table names
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(TemplateOptionTranslateModel::class, TemplateOptionTranslateResourceModel::class);
    }

    /**
     * @param int $id
     * @return $this
     */
    public function addOptionFilter($id)
    {
        return $this->addFieldToFilter('option_id', ['eq' => $id]);
    }

    /**
     * @param int $storeId
     * @return $this
     */
    public function addStoreFilter($storeId)
    {
        return $this->addFieldToFilter('store_id', ['eq' => $storeId]);
    }
}
