<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\ResourceModel;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Exto\Sarp\Api\ProfileMessageRepositoryInterface;
use Exto\Sarp\Model\ProfileMessageFactory;
use Exto\Sarp\Model\ProfileMessageRegistry;
use Exto\Sarp\Api\Data\ProfileMessageSearchResultsInterfaceFactory;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Exto\Sarp\Model\ResourceModel\ProfileMessage;
use Exto\Sarp\Api\Data\ProfileMessageInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrderBuilder;

/**
 * Class ProfileMessageRepository
 */
class ProfileMessageRepository implements ProfileMessageRepositoryInterface
{
    /**
     * @var \Exto\Sarp\Model\ProfileMessageFactory
     */
    protected $profileMessageFactory;

    /**
     * @var \Exto\Sarp\Model\ProfileMessageRegistry
     */
    protected $profileMessageRegistry;

    /**
     * @var \Exto\Sarp\Api\Data\ProfileMessageSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var \Magento\Framework\Api\ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;

    /**
     * @var ProfileMessage
     */
    protected $profileMessageResource;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchBuilder;

    /**
     * @var SortOrderBuilder
     */
    protected $sortOrderBuilder;

    /**
     * ProfileMessageRepository constructor.
     * @param ProfileMessageFactory $profileMessageFactory
     * @param ProfileMessageRegistry $profileMessageRegistry
     * @param ProfileMessageSearchResultsInterfaceFactory $searchResultsFactory
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param \Exto\Sarp\Model\ResourceModel\ProfileMessage $profileMessageResource
     * @param SearchCriteriaBuilder $searchBuilder
     * @param SortOrderBuilder $sortOrderBuilder
     */
    public function __construct(
        ProfileMessageFactory $profileMessageFactory,
        ProfileMessageRegistry $profileMessageRegistry,
        ProfileMessageSearchResultsInterfaceFactory $searchResultsFactory,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        ProfileMessage $profileMessageResource,
        SearchCriteriaBuilder $searchBuilder,
        SortOrderBuilder $sortOrderBuilder
    ) {
        $this->profileMessageFactory = $profileMessageFactory;
        $this->profileMessageRegistry = $profileMessageRegistry;
        $this->profileMessageResource = $profileMessageResource;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->searchBuilder = $searchBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
    }

    /**
     * @param \Exto\Sarp\Api\Data\ProfileMessageInterface $profileMessage
     * @return \Exto\Sarp\Api\Data\ProfileMessageInterface
     * @throws \Exception
     */
    public function save(ProfileMessageInterface $profileMessage)
    {
        $profileMessageData = $this->extensibleDataObjectConverter->toNestedArray(
            $profileMessage,
            [],
            ProfileMessageInterface::class
        );

        $profileMessageModel = $this->profileMessageFactory->create();
        $profileMessageModel->addData($profileMessageData);
        $profileMessageModel->setId($profileMessage->getId());
        $this->profileMessageResource->save($profileMessageModel);
        $this->profileMessageRegistry->push($profileMessageModel);
        $savedObject = $this->get($profileMessageModel->getId());
        return $savedObject;
    }

    /**
     * @param int $profileMessageId
     * @return \Exto\Sarp\Api\Data\ProfileMessageInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($profileMessageId)
    {
        $profileMessageModel = $this->profileMessageRegistry->retrieve($profileMessageId);
        return $profileMessageModel->getDataModel();
    }

    /**
     * @param \Exto\Sarp\Api\Data\ProfileMessageInterface $profileMessage
     * @return $this
     */
    public function delete(ProfileMessageInterface $profileMessage)
    {
        $this->profileMessageRegistry->remove($profileMessage->getId());
        return $this;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $collection = $this->profileMessageFactory->create()->getCollection();
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $fields[] = $filter->getField();
                $conditions[] = [$condition => $filter->getValue()];

            }
            if ($fields) {
                $collection->addFieldToFilter($fields, $conditions);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        $profileMessages = [];
        foreach ($collection as $profileMessageModel) {
            $profileMessages[] = $profileMessageModel->getDataModel();
        }
        $searchResults->setItems($profileMessages);
        return $searchResults;
    }

    /**
     * @param int $profileId
     * @return \Exto\Sarp\Api\Data\ProfileMessageSearchResultsInterface
     */
    public function getListByProfileId($profileId)
    {
        $createdAtSort = $this->sortOrderBuilder
            ->setField('created_at')
            ->setDescendingDirection()
            ->create();
        $criteria = $this->searchBuilder
            ->addFilter('profile_id', $profileId, 'eq')
            ->addSortOrder($createdAtSort)
            ->create()
        ;
        return $this->getList($criteria);
    }
}
