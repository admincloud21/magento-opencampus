<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\ResourceModel;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrder;
use Exto\Sarp\Api\ProfileOrderRepositoryInterface;
use Exto\Sarp\Model\ProfileOrderFactory;
use Exto\Sarp\Model\ProfileOrderRegistry;
use Exto\Sarp\Api\Data\ProfileOrderSearchResultsInterfaceFactory;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Exto\Sarp\Model\ResourceModel\ProfileOrder;
use Exto\Sarp\Api\Data\ProfileOrderInterface;

/**
 * Class ProfileOrderRepository
 */
class ProfileOrderRepository implements ProfileOrderRepositoryInterface
{
    /**
     * @var \Exto\Sarp\Model\ProfileOrderFactory
     */
    protected $profileOrderFactory;

    /**
     * @var \Exto\Sarp\Model\ProfileOrderRegistry
     */
    protected $profileOrderRegistry;

    /**
     * @var \Exto\Sarp\Api\Data\ProfileOrderSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var \Magento\Framework\Api\ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;

    /**
     * @var ProfileOrder
     */
    protected $profileOrderResource;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchBuilder;

    /**
     * ProfileOrderRepository constructor.
     * @param ProfileOrderFactory $profileOrderFactory
     * @param ProfileOrderRegistry $profileOrderRegistry
     * @param ProfileOrderSearchResultsInterfaceFactory $searchResultsFactory
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param \Exto\Sarp\Model\ResourceModel\ProfileOrder $profileOrderResource
     * @param SearchCriteriaBuilder $searchBuilder
     */
    public function __construct(
        ProfileOrderFactory $profileOrderFactory,
        ProfileOrderRegistry $profileOrderRegistry,
        ProfileOrderSearchResultsInterfaceFactory $searchResultsFactory,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        ProfileOrder $profileOrderResource,
        SearchCriteriaBuilder $searchBuilder
    ) {
        $this->profileOrderFactory = $profileOrderFactory;
        $this->profileOrderRegistry = $profileOrderRegistry;
        $this->profileOrderResource = $profileOrderResource;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->searchBuilder = $searchBuilder;
    }

    /**
     * @param \Exto\Sarp\Api\Data\ProfileOrderInterface $profileOrder
     * @return \Exto\Sarp\Api\Data\ProfileOrderInterface
     * @throws \Exception
     */
    public function save(ProfileOrderInterface $profileOrder)
    {
        $profileOrderData = $this->extensibleDataObjectConverter->toNestedArray(
            $profileOrder,
            [],
            ProfileOrderInterface::class
        );
        $profileOrderModel = $this->profileOrderFactory->create();
        $profileOrderModel->addData($profileOrderData);
        $profileOrderModel->setId($profileOrder->getId());
        $this->profileOrderResource->save($profileOrderModel);
        $this->profileOrderRegistry->push($profileOrderModel);
        $savedObject = $this->get($profileOrderModel->getId());
        return $savedObject;
    }

    /**
     * @param int $profileOrderId
     * @return \Exto\Sarp\Api\Data\ProfileOrderInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($profileOrderId)
    {
        $profileOrderModel = $this->profileOrderRegistry->retrieve($profileOrderId);
        return $profileOrderModel->getDataModel();
    }

    /**
     * @param \Exto\Sarp\Api\Data\ProfileOrderInterface $profileOrder
     * @return $this
     */
    public function delete(ProfileOrderInterface $profileOrder)
    {
        $this->profileOrderRegistry->remove($profileOrder->getId());
        return $this;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Exto\Sarp\Api\Data\ProfileOrderSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $collection = $this->profileOrderFactory->create()->getCollection();
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $fields[] = $filter->getField();
                $conditions[] = [$condition => $filter->getValue()];

            }
            if ($fields) {
                $collection->addFieldToFilter($fields, $conditions);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        $profileOrders = [];
        foreach ($collection as $profileOrderModel) {
            $profileOrders[] = $profileOrderModel->getDataModel();
        }
        $searchResults->setItems($profileOrders);
        return $searchResults;
    }

    /**
     * @param int $profileId
     * @return \Exto\Sarp\Api\Data\ProfileOrderSearchResultsInterface
     */
    public function getListByProfileId($profileId)
    {
        $criteria = $this->searchBuilder
            ->addFilter('profile_id', $profileId, 'eq')
            ->create()
        ;
        return $this->getList($criteria);
    }

    /**
     * @param int $orderId
     * @return \Exto\Sarp\Api\Data\ProfileOrderSearchResultsInterface
     */
    public function getListByOrderId($orderId)
    {
        $criteria = $this->searchBuilder
            ->addFilter('order_id', $orderId, 'eq')
            ->create()
        ;
        return $this->getList($criteria);
    }
}
