<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Stdlib\DateTime;

/**
 * Class Profile
 */
class Profile extends AbstractDb
{
    /** @var array */
    protected $_serializableFields = [
        'quote_data' => [null, []],
        'quote_items_data' => [null, []],
        'billing_address_data' => [null, []],
        'shipping_address_data' => [null, []],
    ];

    /**
     * Profile constructor.
     * @param Context $context
     * @param null $connectionName
     */
    public function __construct(
        Context $context,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('exto_sarp_profile', 'id');
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _beforeSave(AbstractModel $object)
    {
        if (!$object->getId()) {
            $now = date(DateTime::DATETIME_PHP_FORMAT, time());
            $object->setCreatedDate($now);
            $object->setProfileId($this->getNewProfileId());
        }
        return parent::_beforeSave($object);
    }

    /**
     * @return string
     */
    protected function getNewProfileId()
    {
        $newId = $this->generateNewId();
        $result = $this->getConnection()->fetchOne($this->_getLoadSelect('profile_id', $newId, null));
        if (!$result) {
            return $newId;
        }
        return $this->getNewProfileId();
    }

    /**
     * @return string
     */
    protected function generateNewId()
    {
        $consonants = "BCDFGHJKLMNPQRSTVWXZ";
        $digits = "0123456789";
        $newId = "";
        for ($i = 0; $i < 3; $i++) {
            $newId .= $consonants[rand(0, strlen($consonants) - 1)];
        }
        for ($i = 0; $i < 9; $i++) {
            $newId .= $digits[rand(0, strlen($digits) - 1)];
        }
        return $newId;
    }
}
