<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\ResourceModel\SubscriptionTemplate;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\Data\Collection\EntityFactoryInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Exto\Sarp\Model\SubscriptionTemplate as SubscriptionTemplateModel;
use Exto\Sarp\Model\ResourceModel\SubscriptionTemplate as SubscriptionTemplateResourceModel;

/**
 * Class Collection
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Collection constructor.
     * @param EntityFactoryInterface $entityFactory
     * @param LoggerInterface $logger
     * @param FetchStrategyInterface $fetchStrategy
     * @param ManagerInterface $eventManager
     * @param AdapterInterface|null $connection
     * @param AbstractDb|null $resource
     */
    public function __construct(
        EntityFactoryInterface $entityFactory,
        LoggerInterface $logger,
        FetchStrategyInterface $fetchStrategy,
        ManagerInterface $eventManager,
        AdapterInterface $connection = null,
        AbstractDb $resource = null
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection);
        $this->addOnlyNotDeletedFilter();
    }
    
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(SubscriptionTemplateModel::class, SubscriptionTemplateResourceModel::class);
    }

    /**
     * @param string $valueField
     * @param string $labelField
     * @return array
     */
    public function toOptionHash($valueField = 'id', $labelField = 'name')
    {
        return $this->_toOptionHash($valueField, $labelField);
    }

    /**
     * @param string $valueField
     * @param string $labelField
     * @return array
     */
    public function toOptionArray($valueField = 'id', $labelField = 'name')
    {
        return $this->_toOptionArray($valueField, $labelField);
    }

    /**
     * Get only not deleted templates
     * @return $this
     */
    public function addOnlyNotDeletedFilter()
    {
        $this->addFieldToFilter('is_deleted', ['eq' => 0]);
        return $this;
    }
}
