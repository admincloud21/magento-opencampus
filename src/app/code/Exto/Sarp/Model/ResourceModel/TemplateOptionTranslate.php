<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Exto\Sarp\Model\ResourceModel\TemplateOptionTranslate\CollectionFactory as TemplateOptionTranslateCollectionFactory;
use Magento\Framework\Model\AbstractModel;

/**
 * Class TemplateOptionTranslate
 */
class TemplateOptionTranslate extends AbstractDb
{
    /**
     * @var TemplateOptionTranslate\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * TemplateOptionTranslate constructor.
     * @param Context $context
     * @param TemplateOptionTranslate\CollectionFactory $collectionFactory
     * @param null $connectionName
     */
    public function __construct(
        Context $context,
        TemplateOptionTranslateCollectionFactory $collectionFactory,
        $connectionName = null
    ) {
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context, $connectionName);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('exto_sarp_template_option_translate', 'id');
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     * @param int $id
     * @param int $storeId
     * @return $this|null
     */
    public function loadByStore(AbstractModel $object, $id, $storeId)
    {
        /** @var \Exto\Sarp\Model\ResourceModel\TemplateOptionTranslate\Collection $collection */
        $collection = $this->collectionFactory->create();
        $collection->addOptionFilter($id);
        $collection->addStoreFilter($storeId);
        $item = $collection->getFirstItem();
        try {
            $result = $this->load($object, $item->getId());
        } catch (\Exception $e) {
            $result = null;
        }
        return $result;
    }
}
