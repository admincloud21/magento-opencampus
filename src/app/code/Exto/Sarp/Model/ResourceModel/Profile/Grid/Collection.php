<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\ResourceModel\Profile\Grid;

use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Psr\Log\LoggerInterface as Logger;
use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;
use Exto\Sarp\Model\ResourceModel\Profile;

/**
 * Class Collection
 */
class Collection extends SearchResult
{
    /**
     * Collection constructor.
     * @param EntityFactory $entityFactory
     * @param Logger $logger
     * @param FetchStrategy $fetchStrategy
     * @param EventManager $eventManager
     * @param string $mainTable
     * @param string $resourceModel
     */
    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager,
        $mainTable = 'exto_sarp_profile',
        $resourceModel = Profile::class
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);

        $this->addFilterToMap('order_qty', 'main_table.order_qty');
        $this->addFilterToMap('order_total', 'main_table.order_total');
        $this->addFilterToMap('customer', 'main_table.customer');
        $this->addFilterToMap('status', 'main_table.status');
        $this->addFilterToMap('id', 'main_table.id');
        $this->addFilterToMap('profile_id', 'main_table.profile_id');
    }

    /**
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()->columns(
            ['customer' => 'CONCAT(main_table.customer_name, " &lt;", main_table.customer_email, "&gt;")']
        );
        $this->addOrderInfo();
        return $this;
    }

    /**
     * @param array|string $field
     * @param null $condition
     * @return $this
     */
    public function addFieldToFilter($field, $condition = null)
    {
        switch ($field) {
            case 'order_qty':
                $where = $this->_getConditionSql('IFNULL(t.order_qty, 0)', $condition);
                $this->getSelect()->where($where);
                return $this;
            case 'order_total':
                $where = $this->_getConditionSql('COALESCE(order_info.base_grand_total, 0)', $condition);
                $this->getSelect()->where($where);
                return $this;
            case 'customer':
                $where = $this->_getConditionSql(
                    'CONCAT(main_table.customer_name, " &lt;", main_table.customer_email, "&gt;")',
                    $condition
                );
                $this->getSelect()->where($where);
                return $this;
        }
        return parent::addFieldToFilter($field, $condition);
    }

    /**
     * @param string $field
     * @param string $direction
     * @return $this
     */
    public function setOrder($field, $direction = self::SORT_ORDER_DESC)
    {
        switch ($field) {
            case 'customer':
                $field = 'CONCAT(main_table.customer_name, " &lt;", main_table.customer_email, "&gt;")';
                break;
            case 'order_qty':
                $field = 'IFNULL(t.order_qty, 0)';
                break;
            case 'order_total':
                $field = 'COALESCE(order_info.base_grand_total, 0)';
                break;
        }
        return parent::setOrder($field, $direction);
    }

    /**
     * Join order info to collection
     * @return $this
     */
    public function addOrderInfo()
    {
        $this->getSelect()->joinLeft(
            ['profile_order' => $this->getTable('exto_sarp_profile_order')],
            'main_table.id = profile_order.profile_id',
            []
        )->joinLeft(
            new \Zend_Db_Expr(
                "(SELECT COUNT(order_id) as order_qty, profile_id"
                . " FROM {$this->getTable('exto_sarp_profile_order')}"
                . " GROUP BY profile_id)"
            ),
            "main_table.id = t.profile_id",
            ['order_qty' => "IFNULL(t.order_qty, 0)"]
        )->joinLeft(
            ['order_info' => $this->getTable('sales_order')],
            'profile_order.order_id = order_info.entity_id',
            [
                'order_total' => "COALESCE(order_info.base_grand_total, 0)",
            ]
        );
        $this->getSelect()->group('main_table.id');
        return $this;
    }
}
