<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\ResourceModel\Profile;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Exto\Sarp\Model\Profile as ProfileModel;
use Exto\Sarp\Model\ResourceModel\Profile as ProfileResourceModel;
use Magento\Framework\DB\Select;

/**
 * Class Collection
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    
    /**
     * Init collection and determine table names
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ProfileModel::class, ProfileResourceModel::class);
    }

    /**
     * @param string $statusCode
     * @return string
     */
    public function getProfileCountByStatus($statusCode)
    {
        $select = clone $this->getSelect();
        $condition = $this->getConnection()
            ->prepareSqlCondition('main_table.status', ['eq' => $statusCode]);
        $select->where($condition);
        $select->reset(Select::COLUMNS);
        $select->columns(['count' => 'COUNT(*)']);
        return $this->getConnection()->fetchOne($select);
    }
    
    /**
     * Join order info to collection
     * @return $this
     */
    public function addOrderInfo()
    {
        $this->getSelect()->joinLeft(
            ['profile_order' => $this->getTable('exto_sarp_profile_order')],
            'main_table.id = profile_order.profile_id',
            []
        )->joinLeft(
            ['order_info' => $this->getTable('sales_order')],
            'profile_order.order_id = order_info.entity_id',
            [
                'order_total' => "COALESCE(order_info.base_grand_total, 0)",
                'order_qty' => "COALESCE(COUNT(order_info.entity_id), 0)",
            ]
        );
        $this->getSelect()->group('main_table.id');
        return $this;
    }

    /**
     * @param int $customerId
     *
     * @return $this
     */
    public function addCustomerIdFilter($customerId)
    {
        return $this->addFieldToFilter('customer_id', $customerId);
    }
}
