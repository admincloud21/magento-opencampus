<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\ResourceModel\ProfileMessage;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Exto\Sarp\Model\ProfileMessage as ProfileMessageModel;
use Exto\Sarp\Model\ResourceModel\ProfileMessage as ProfileMessageResourceModel;

/**
 * Class Collection
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    
    /**
     * Init collection and determine table names
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ProfileMessageModel::class, ProfileMessageResourceModel::class);
    }
}
