<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\ResourceModel;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Exto\Sarp\Api\SubscriptionTemplateRepositoryInterface;
use Exto\Sarp\Model\SubscriptionTemplateFactory;
use Exto\Sarp\Model\SubscriptionTemplateRegistry;
use Exto\Sarp\Api\Data\SubscriptionTemplateSearchResultsInterfaceFactory;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Exto\Sarp\Model\ResourceModel\SubscriptionTemplate;
use Exto\Sarp\Api\Data\SubscriptionTemplateInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Class SubscriptionTemplateRepository
 */
class SubscriptionTemplateRepository implements SubscriptionTemplateRepositoryInterface
{
    /**
     * @var \Exto\Sarp\Model\SubscriptionTemplateFactory
     */
    protected $subscriptionTemplateFactory;

    /**
     * @var \Exto\Sarp\Model\SubscriptionTemplateRegistry
     */
    protected $subscriptionTemplateRegistry;

    /**
     * @var \Exto\Sarp\Api\Data\SubscriptionTemplateSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var \Magento\Framework\Api\ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;

    /**
     * @var SubscriptionTemplate
     */
    protected $subscriptionTemplateResource;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchBuilder;

    /**
     * SubscriptionTemplateRepository constructor.
     * @param SubscriptionTemplateFactory $subscriptionTemplateFactory
     * @param SubscriptionTemplateRegistry $subscriptionTemplateRegistry
     * @param SubscriptionTemplateSearchResultsInterfaceFactory $searchResultsFactory
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param \Exto\Sarp\Model\ResourceModel\SubscriptionTemplate $subscriptionTemplateResource
     * @param SearchCriteriaBuilder $searchBuilder
     */
    public function __construct(
        SubscriptionTemplateFactory $subscriptionTemplateFactory,
        SubscriptionTemplateRegistry $subscriptionTemplateRegistry,
        SubscriptionTemplateSearchResultsInterfaceFactory $searchResultsFactory,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        SubscriptionTemplate $subscriptionTemplateResource,
        SearchCriteriaBuilder $searchBuilder
    ) {
        $this->subscriptionTemplateFactory = $subscriptionTemplateFactory;
        $this->subscriptionTemplateRegistry = $subscriptionTemplateRegistry;
        $this->subscriptionTemplateResource = $subscriptionTemplateResource;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->searchBuilder = $searchBuilder;
    }

    /**
     * @param \Exto\Sarp\Api\Data\SubscriptionTemplateInterface $subscriptionTemplate
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateInterface
     * @throws \Exception
     */
    public function save(SubscriptionTemplateInterface $subscriptionTemplate)
    {
        $subscriptionTemplateData = $this->extensibleDataObjectConverter->toNestedArray(
            $subscriptionTemplate,
            [],
            SubscriptionTemplateInterface::class
        );

        $subscriptionTemplateModel = $this->subscriptionTemplateFactory->create();
        $subscriptionTemplateModel->addData($subscriptionTemplateData);
        $subscriptionTemplateModel->setId($subscriptionTemplate->getId());
        $this->subscriptionTemplateResource->save($subscriptionTemplateModel);
        $this->subscriptionTemplateRegistry->push($subscriptionTemplateModel);
        $savedObject = $this->get($subscriptionTemplateModel->getId());
        return $savedObject;
    }

    /**
     * @param int $subscriptionTemplateId
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($subscriptionTemplateId)
    {
        $subscriptionTemplateModel = $this->subscriptionTemplateRegistry->retrieve($subscriptionTemplateId);
        return $subscriptionTemplateModel->getDataModel();
    }

    /**
     * @param \Exto\Sarp\Api\Data\SubscriptionTemplateInterface $subscriptionTemplate
     * @return $this
     */
    public function delete(SubscriptionTemplateInterface $subscriptionTemplate)
    {
        $this->subscriptionTemplateRegistry->remove($subscriptionTemplate->getId());
        return $this;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $collection = $this->subscriptionTemplateFactory->create()->getCollection();
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $fields[] = $filter->getField();
                $conditions[] = [$condition => $filter->getValue()];

            }
            if ($fields) {
                $collection->addFieldToFilter($fields, $conditions);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        $subscriptionTemplates = [];
        foreach ($collection as $subscriptionTemplateModel) {
            $subscriptionTemplates[] = $subscriptionTemplateModel->getDataModel();
        }
        $searchResults->setItems($subscriptionTemplates);
        return $searchResults;
    }

    /**
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateSearchResultsInterface
     */
    public function getAllList()
    {
        $criteria = $this->searchBuilder->create();
        return $this->getList($criteria);
    }
}
