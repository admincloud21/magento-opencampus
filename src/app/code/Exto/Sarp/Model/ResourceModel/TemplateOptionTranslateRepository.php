<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\ResourceModel;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Exto\Sarp\Api\TemplateOptionTranslateRepositoryInterface;
use Exto\Sarp\Model\TemplateOptionTranslateFactory;
use Exto\Sarp\Model\TemplateOptionTranslateRegistry;
use Exto\Sarp\Api\Data\TemplateOptionTranslateSearchResultsInterfaceFactory;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Exto\Sarp\Model\ResourceModel\TemplateOptionTranslate;
use Exto\Sarp\Api\Data\TemplateOptionTranslateInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Class TemplateOptionTranslateRepository
 */
class TemplateOptionTranslateRepository implements TemplateOptionTranslateRepositoryInterface
{
    /**
     * @var \Exto\Sarp\Model\TemplateOptionTranslateFactory
     */
    protected $templateOptionTranslateFactory;

    /**
     * @var \Exto\Sarp\Model\TemplateOptionTranslateRegistry
     */
    protected $templateOptionTranslateRegistry;

    /**
     * @var \Exto\Sarp\Api\Data\TemplateOptionTranslateSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var \Magento\Framework\Api\ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;

    /**
     * @var TemplateOptionTranslate
     */
    protected $templateOptionTranslateResource;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchBuilder;

    /**
     * TemplateOptionTranslateRepository constructor.
     * @param TemplateOptionTranslateFactory $templateOptionTranslateFactory
     * @param TemplateOptionTranslateRegistry $templateOptionTranslateRegistry
     * @param TemplateOptionTranslateSearchResultsInterfaceFactory $searchResultsFactory
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param \Exto\Sarp\Model\ResourceModel\TemplateOptionTranslate $templateOptionTranslateResource
     * @param SearchCriteriaBuilder $searchBuilder
     */
    public function __construct(
        TemplateOptionTranslateFactory $templateOptionTranslateFactory,
        TemplateOptionTranslateRegistry $templateOptionTranslateRegistry,
        TemplateOptionTranslateSearchResultsInterfaceFactory $searchResultsFactory,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        TemplateOptionTranslate $templateOptionTranslateResource,
        SearchCriteriaBuilder $searchBuilder
    ) {
        $this->templateOptionTranslateFactory = $templateOptionTranslateFactory;
        $this->templateOptionTranslateRegistry = $templateOptionTranslateRegistry;
        $this->templateOptionTranslateResource = $templateOptionTranslateResource;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->searchBuilder = $searchBuilder;
    }

    /**
     * @param \Exto\Sarp\Api\Data\TemplateOptionTranslateInterface $templateOptionTranslate
     * @return \Exto\Sarp\Api\Data\TemplateOptionTranslateInterface
     * @throws \Exception
     */
    public function save(TemplateOptionTranslateInterface $templateOptionTranslate)
    {
        $templateOptionTranslateData = $this->extensibleDataObjectConverter->toNestedArray(
            $templateOptionTranslate,
            [],
            TemplateOptionTranslateInterface::class
        );
        $templateOptionTranslateModel = $this->templateOptionTranslateFactory->create();
        $templateOptionTranslateModel->addData($templateOptionTranslateData);
        $templateOptionTranslateModel->setId($templateOptionTranslate->getId());
        $this->templateOptionTranslateResource->save($templateOptionTranslateModel);
        $this->templateOptionTranslateRegistry->push($templateOptionTranslateModel);
        $savedObject = $this->get($templateOptionTranslateModel->getId());
        return $savedObject;
    }

    /**
     * @param int $templateOptionTranslateId
     * @return \Exto\Sarp\Api\Data\TemplateOptionTranslateInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($templateOptionTranslateId)
    {
        $templateOptionTranslateModel = $this->templateOptionTranslateRegistry->retrieve($templateOptionTranslateId);
        return $templateOptionTranslateModel->getDataModel();
    }

    /**
     * @param int $templateOptionTranslateId
     * @param int $storeId
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByIdAndStore($templateOptionTranslateId, $storeId)
    {
        $templateOptionTranslateModel = $this
            ->templateOptionTranslateRegistry
            ->retrieveByStore($templateOptionTranslateId, $storeId);
        return $templateOptionTranslateModel->getDataModel();
    }

    /**
     * @param \Exto\Sarp\Api\Data\TemplateOptionTranslateInterface $templateOptionTranslate
     * @return $this
     */
    public function delete(TemplateOptionTranslateInterface $templateOptionTranslate)
    {
        $this->templateOptionTranslateRegistry->remove($templateOptionTranslate->getId());
        return $this;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $collection = $this->templateOptionTranslateFactory->create()->getCollection();
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $fields[] = $filter->getField();
                $conditions[] = [$condition => $filter->getValue()];

            }
            if ($fields) {
                $collection->addFieldToFilter($fields, $conditions);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        $templateOptionTranslates = [];
        foreach ($collection as $templateOptionTranslateModel) {
            $templateOptionTranslates[] = $templateOptionTranslateModel->getDataModel();
        }
        $searchResults->setItems($templateOptionTranslates);
        return $searchResults;
    }

    /**
     * @param int $optionId
     * @return \Exto\Sarp\Api\Data\TemplateOptionTranslateSearchResultsInterface
     */
    public function getListByOptionId($optionId)
    {
        $criteria = $this->searchBuilder
            ->addFilter('option_id', $optionId, 'eq')
            ->create();
        return $this->getList($criteria);
    }
}
