<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\ResourceModel;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Exto\Sarp\Api\Data\ProfileInterface;
use Exto\Sarp\Api\ProfileRepositoryInterface;
use Exto\Sarp\Model\ProfileFactory;
use Exto\Sarp\Model\ProfileRegistry;
use Exto\Sarp\Api\Data\ProfileSearchResultsInterfaceFactory;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Exto\Sarp\Model\ResourceModel\Profile;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Class ProfileRepository
 */
class ProfileRepository implements ProfileRepositoryInterface
{
    /**
     * @var \Exto\Sarp\Model\ProfileFactory
     */
    protected $profileFactory;

    /**
     * @var \Exto\Sarp\Model\ProfileRegistry
     */
    protected $profileRegistry;

    /**
     * @var \Exto\Sarp\Api\Data\ProfileSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var \Magento\Framework\Api\ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;

    /**
     * @var Profile
     */
    protected $profileResource;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchBuilder;

    /**
     * ProfileRepository constructor.
     * @param ProfileFactory $profileFactory
     * @param ProfileRegistry $profileRegistry
     * @param ProfileSearchResultsInterfaceFactory $searchResultsFactory
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param \Exto\Sarp\Model\ResourceModel\Profile $profileResource
     * @param SearchCriteriaBuilder $searchBuilder
     */
    public function __construct(
        ProfileFactory $profileFactory,
        ProfileRegistry $profileRegistry,
        ProfileSearchResultsInterfaceFactory $searchResultsFactory,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        Profile $profileResource,
        SearchCriteriaBuilder $searchBuilder
    ) {
        $this->profileFactory = $profileFactory;
        $this->profileRegistry = $profileRegistry;
        $this->profileResource = $profileResource;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->searchBuilder = $searchBuilder;
    }

    /**
     * @param \Exto\Sarp\Api\Data\ProfileInterface $profile
     * @return \Exto\Sarp\Api\Data\ProfileInterface
     * @throws \Exception
     */
    public function save(ProfileInterface $profile)
    {
        $profileData = $this->extensibleDataObjectConverter->toNestedArray(
            $profile,
            [],
            ProfileInterface::class
        );
        $profileData[ProfileInterface::QUOTE_DATA] = $profile->getQuoteData();
        $profileData[ProfileInterface::QUOTE_ITEMS_DATA] = $profile->getQuoteItemsData();
        $profileData[ProfileInterface::BILLING_ADDRESS_DATA] = $profile->getBillingAddressData();
        $profileData[ProfileInterface::SHIPPING_ADDRESS_DATA] = $profile->getShippingAddressData();
        $profileModel = $this->profileFactory->create();
        $profileModel->addData($profileData);
        $profileModel->setId($profile->getId());
        $this->profileResource->save($profileModel);
        $this->profileRegistry->push($profileModel);
        $savedObject = $this->get($profileModel->getId());
        return $savedObject;
    }

    /**
     * @param int $profileId
     * @return \Exto\Sarp\Api\Data\ProfileInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($profileId)
    {
        $profileModel = $this->profileRegistry->retrieve($profileId);
        return $profileModel->getDataModel();
    }

    /**
     * @param string $externalProfileId
     * @return \Exto\Sarp\Api\Data\ProfileInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByExternalProfileId($externalProfileId)
    {
        $profileModel = $this->profileRegistry->retrieve($externalProfileId, 'external_service_profile_id');
        return $profileModel->getDataModel();
    }

    /**
     * @param \Exto\Sarp\Api\Data\ProfileInterface $profile
     * @return $this
     */
    public function delete(ProfileInterface $profile)
    {
        $this->profileRegistry->remove($profile->getId());
        return $this;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $collection = $this->profileFactory->create()->getCollection();
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $fields[] = $filter->getField();
                $conditions[] = [$condition => $filter->getValue()];

            }
            if ($fields) {
                $collection->addFieldToFilter($fields, $conditions);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        $profiles = [];
        foreach ($collection as $profileModel) {
            $profiles[] = $profileModel->getDataModel();
        }
        $searchResults->setItems($profiles);
        return $searchResults;
    }

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function getListByIds($ids)
    {
        $criteria = $this->searchBuilder
            ->addFilter('id', $ids, 'in')
            ->create()
        ;
        return $this->getList($criteria);
    }
}
