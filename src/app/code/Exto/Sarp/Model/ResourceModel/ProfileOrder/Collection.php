<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\ResourceModel\ProfileOrder;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Exto\Sarp\Model\ProfileOrder as ProfileOrderModel;
use Exto\Sarp\Model\ResourceModel\ProfileOrder as ProfileOrderResourceModel;
use Magento\Framework\DB\Select;

/**
 * Class Collection
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    
    /**
     * Init collection and determine table names
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ProfileOrderModel::class, ProfileOrderResourceModel::class);
    }

    /**
     * @param \DateTime|null $from
     * @param \DateTime|null $to
     * @return string
     */
    public function getTotalForSubscriptionInPeriod(\DateTime $from = null, \DateTime $to = null)
    {
        if (null === $from) {
            $from = new \DateTime();
        }
        if (null === $to) {
            $to = new \DateTime();
        }
        $select = clone $this->getSelect();
        $fromDate = $from->format('Y-m-d H:i:s');
        $toDate = $to->format('Y-m-d H:i:s');
        $select->join(
            ['sales_order' => $this->getTable('sales_order')],
            'main_table.order_id = sales_order.entity_id',
            null
        );
        $fromCondition = $this->getConnection()
            ->prepareSqlCondition('sales_order.created_at', [['gt' => $fromDate], ['null' => $fromDate]]);
        $toCondition = $this->getConnection()
            ->prepareSqlCondition('sales_order.created_at', [['lt' => $toDate], ['null' => $toDate]]);
        $select->reset(Select::COLUMNS);
        $select->columns(['total' => 'SUM(sales_order.base_grand_total)']);
        $select->where($fromCondition);
        $select->where($toCondition);
        return $this->getConnection()->fetchOne($select);
    }
}
