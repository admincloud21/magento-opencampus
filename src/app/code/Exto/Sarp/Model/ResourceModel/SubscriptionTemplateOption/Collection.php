<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\ResourceModel\SubscriptionTemplateOption;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Exto\Sarp\Model\SubscriptionTemplateOption as SubscriptionTemplateOptionModel;
use Exto\Sarp\Model\ResourceModel\SubscriptionTemplateOption as SubscriptionTemplateOptionResourceModel;
use Magento\Framework\DB\Select;

/**
 * Class Collection
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    
    /**
     * Init collection and determine table names
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(SubscriptionTemplateOptionModel::class, SubscriptionTemplateOptionResourceModel::class);
    }

    /**
     * @param int $templateId
     * @param int $storeId
     * @return array
     */
    public function getTemplateOptionsByTemplateId($templateId, $storeId)
    {
        $select = clone $this->getSelect();
        $select->joinLeft(
            ['option_translate' => $this->getTable('exto_sarp_template_option_translate')],
            "main_table.id = option_translate.option_id AND option_translate.store_id = {$storeId}",
            null
        );
        $condition = $this->getConnection()
            ->prepareSqlCondition('main_table.template_id', ['eq' => $templateId]);
        $select->where($condition);
        $select->reset(Select::COLUMNS);
        $select->columns(
            [
                'id' => 'main_table.id',
                'label' => 'COALESCE(option_translate.value, main_table.title)',
            ]
        );

        return $this->getConnection()->fetchPairs($select);
    }
}
