<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Checkout\Session;

/**
 * Class SuccessValidator
 */
class SuccessValidator extends \Magento\Checkout\Model\Session\SuccessValidator
{
    /**
     * @return bool
     */
    public function isValid()
    {
        if ($this->checkoutSession->getLastRecurringProfileIds()) {
            return true;
        }
        return parent::isValid();
    }
}
