<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Exto\Sarp\Model\ProfileOrderFactory;
use Exto\Sarp\Model\ResourceModel\ProfileOrder as ProfileOrderResourceModel;

/**
 * Class ProfileOrder
 */
class ProfileOrderRegistry
{
    /**
     * @var ProfileOrderFactory
     */
    private $profileOrderFactory;

    /**
     * ProfileOrder registry by id
     * @var array
     */
    private $profileOrderRegistryById = [];

    /**
     * ProfileOrder resource
     * @var ResourceModel\ProfileOrder
     */
    private $profileOrderResource;

    /**
     * ProfileOrderRegistry constructor.
     * @param \Exto\Sarp\Model\ProfileOrderFactory $profileOrderFactory
     * @param ProfileOrderResourceModel $profileOrderResource
     */
    public function __construct(
        ProfileOrderFactory $profileOrderFactory,
        ProfileOrderResourceModel $profileOrderResource
    ) {
        $this->profileOrderResource = $profileOrderResource;
        $this->profileOrderFactory = $profileOrderFactory;
    }

    /**
     * @param int $profileOrderId
     * @return ProfileOrder
     * @throws NoSuchEntityException
     */
    public function retrieve($profileOrderId)
    {
        if (!isset($this->profileOrderRegistryById[$profileOrderId])) {
            /** @var ProfileOrder $profileOrder */
            $profileOrder = $this->profileOrderFactory->create();
            $this->profileOrderResource->load($profileOrder, $profileOrderId);
            if (!$profileOrder->getId()) {
                throw NoSuchEntityException::singleField('profileOrderId', $profileOrderId);
            } else {
                $this->profileOrderRegistryById[$profileOrderId] = $profileOrder;
            }
        }
        return $this->profileOrderRegistryById[$profileOrderId];
    }

    /**
     * @param int $profileOrderId
     * @return void
     */
    public function remove($profileOrderId)
    {
        if (isset($this->profileOrderRegistryById[$profileOrderId])) {
            unset($this->profileOrderRegistryById[$profileOrderId]);
        }
    }

    /**
     * @param ProfileOrder $profileOrder
     * @return $this
     */
    public function push(ProfileOrder $profileOrder)
    {
        $this->profileOrderRegistryById[$profileOrder->getId()] = $profileOrder;
        return $this;
    }
}
