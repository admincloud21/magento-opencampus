<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Paypal;

use Magento\Paypal\Model as PaypalModel;
use Exto\Sarp\Model as SarpModel;
use Exto\Sarp\Model\Source\Profile\Status as ProfileStatus;
use Psr\Log\LoggerInterface;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
use Exto\Sarp\Api\Data\ProfileMessageInterfaceFactory;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\Order\Payment\Transaction as PaymentTransactionModel;
use Exto\Sarp\Model\ResourceModel\ProfileOrderRepository;

/**
 * Class Ipn
 */
class Ipn extends PaypalModel\AbstractIpn implements PaypalModel\IpnInterface
{
    /** @var \Exto\Sarp\Model\ResourceModel\ProfileRepository */
    protected $profileRepository;

    /** @var \Exto\Sarp\Model\ProfileService */
    protected $profileService;

    /** @var \Magento\Sales\Api\OrderRepositoryInterface */
    protected $orderRepository;

    /** @var \Exto\Sarp\Api\Data\ProfileMessageInterfaceFactory */
    protected $profileMessageFactory;

    /** @var \Exto\Sarp\Model\ResourceModel\ProfileMessageRepository */
    protected $profileMessageRepository;

    /** @var \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender */
    protected $orderSender;

    /** @var PaymentTransactionModel */
    protected $paymentTransactionModel;

    /** @var ProfileOrderRepository */
    protected $profileOrderRepository;

    /** @var \Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory */
    protected $orderStatusCollectionFactory;

    /**
     * Ipn constructor.
     * @param PaypalModel\ConfigFactory $configFactory
     * @param LoggerInterface $logger
     * @param CurlFactory $curlFactory
     * @param OrderRepositoryInterface $orderRepository
     * @param SarpModel\ResourceModel\ProfileRepository $profileRepository
     * @param SarpModel\ProfileService $profileService
     * @param ProfileMessageInterfaceFactory $profileMessageFactory
     * @param SarpModel\ResourceModel\ProfileMessageRepository $profileMessageRepository
     * @param OrderSender $orderSender
     * @param PaymentTransactionModel $paymentTransactionModel
     * @param ProfileOrderRepository $profileOrderRepository
     * @param array $data
     */
    public function __construct(
        PaypalModel\ConfigFactory $configFactory,
        LoggerInterface $logger,
        CurlFactory $curlFactory,
        OrderRepositoryInterface $orderRepository,
        SarpModel\ResourceModel\ProfileRepository $profileRepository,
        SarpModel\ProfileService $profileService,
        ProfileMessageInterfaceFactory $profileMessageFactory,
        SarpModel\ResourceModel\ProfileMessageRepository $profileMessageRepository,
        OrderSender $orderSender,
        PaymentTransactionModel $paymentTransactionModel,
        ProfileOrderRepository $profileOrderRepository,
        \Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory $orderStatusCollectionFactory,
        array $data = []
    ) {
        parent::__construct($configFactory, $logger, $curlFactory, $data);
        $this->profileRepository = $profileRepository;
        $this->profileService = $profileService;
        $this->orderRepository = $orderRepository;
        $this->profileMessageFactory = $profileMessageFactory;
        $this->profileMessageRepository = $profileMessageRepository;
        $this->orderSender = $orderSender;
        $this->paymentTransactionModel = $paymentTransactionModel;
        $this->profileOrderRepository = $profileOrderRepository;
        $this->orderStatusCollectionFactory = $orderStatusCollectionFactory;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function processIpnRequest()
    {
        try {
            $profile = $this->getRecurringProfile();
        } catch (NoSuchEntityException $e) {
            $internalReferenceId = $this->getRequestData('recurring_payment_id');
            throw new \Exception(
                sprintf('Wrong recurring profile INTERNAL_REFERENCE_ID: "%s".', $internalReferenceId)
            );
        }
        /** @var \Exto\Sarp\Api\Data\ProfileMessageInterface $profileMessage */
        $profileMessage = $this->profileMessageFactory->create();
        $profileMessage->setProfileId($profile->getId());
        $profileMessage->setMessage(print_r($this->getRequestData(), 1));
        $this->profileMessageRepository->save($profileMessage);

        $this->postBack($profile);

        $txnType = $this->getRequestData('txn_type');
        if ($txnType === 'recurring_payment') {
            $paymentStatus = $this->filterPaymentStatus($this->getRequestData('payment_status'));
            switch ($paymentStatus) {
                case PaypalModel\Info::PAYMENTSTATUS_COMPLETED:
                    $this->registerRecurringProfilePaymentCapture($profile);
                    break;
                default:
                    throw new \Exception("Cannot handle payment status '{$paymentStatus}'.");
            }
        }
        $this->processProfileData($profile);
        return $this;
    }

    /**
     * @param \Exto\Sarp\Api\Data\ProfileInterface $profile
     * @return void
     */
    protected function registerRecurringProfilePaymentCapture($profile)
    {
        $txnId = $this->getRequestData('txn_id');
        $transactionModel = $this->paymentTransactionModel->load($txnId, 'txn_id');
        if (null !== $transactionModel->getId()) {
            $orderId = $transactionModel->getOrderId();
            $list = $this->profileOrderRepository->getListByOrderId($orderId);
            if ($list->getTotalCount() > 0) {
                return;
            }
        }
        $itemListTotalData = [];
        $numCartItems = $this->getRequestData('num_cart_items');
        if (null !== $numCartItems) {
            for ($i = 1; $i <= $numCartItems; $i++) {
                $itemListTotalData[] = [
                    'shipping' => $this->getRequestData('mc_shipping' . $i),
                    'tax' => $this->getRequestData('tax' . $i),
                    'total' => $this->getRequestData('mc_gross' . $i),
                ];
            }
        } else {
            $itemListTotalData[] = [
                'shipping' => $this->getRequestData('shipping'),
                'tax' => $this->getRequestData('tax'),
                'total' => $this->getRequestData('mc_gross'),
            ];
        }
        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->profileService->createOrder($profile, $itemListTotalData);

        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $payment = $order->getPayment();
        $payment->setTransactionId($this->getRequestData('txn_id'))
            ->setCurrencyCode($this->getRequestData('mc_currency'))
            ->setIsTransactionClosed(0);
        $this->orderRepository->save($order);
        $this->profileService->addOrderRelation($profile, $order);
        $payment->registerCaptureNotification($this->getRequestData('mc_gross'), true);
        $order->setStatus($profile->getDefaultOrderStatus());
        $order->setState($this->getOrderStateByStatus($profile->getDefaultOrderStatus()));
        $this->orderRepository->save($order);
        if ($order->getCanSendNewEmailFlag()) {
            try {
                $this->orderSender->send($order);
            } catch (\Exception $e) {
                $this->logger->critical($e);
            }
        }
    }

    /**
     * @param \Exto\Sarp\Api\Data\ProfileInterface $profile
     * @return void
     */
    protected function processProfileData($profile)
    {
        $profileStatus = $this->filterProfileStatus($this->getRequestData('profile_status'));
        $profile->setStatus($profileStatus);
        $orderList = $this->profileOrderRepository->getListByProfileId($profile->getId());
        if ($profile->getStatus() === ProfileStatus::ACTIVE_VALUE && $orderList->getTotalCount() == 0) {
            $profile->setStatus(ProfileStatus::PENDING_VALUE);
        }
        if (null !== $this->getRequestData('payment_date')) {
            $paymentDate = new \DateTime($this->getRequestData('payment_date'));
            $profile->setLastSuccessfulDate($paymentDate->format('Y-m-d H:i:s'));
        }
        $profile->setCompletedDate($this->profileService->estimateCompleteDate($profile));
        $this->profileRepository->save($profile);
        if ($profile->getStatus() === ProfileStatus::COMPLETED_VALUE) {
            $this->profileService->sendProfileEndNotification($profile);
        }
    }

    /**
     * @return \Exto\Sarp\Api\Data\ProfileInterface
     */
    protected function getRecurringProfile()
    {
        $externalReferenceId = $this->getRequestData('recurring_payment_id');
        return $this->profileRepository->getByExternalProfileId($externalReferenceId);
    }

    /**
     * @param \Exto\Sarp\Api\Data\ProfileInterface $profile
     * @throws \Exception
     * @throws \Magento\Framework\Exception\RemoteServiceUnavailableException
     * @return void
     */
    protected function postBack($profile)
    {
        $quoteData = $profile->getQuoteData();
        $parameters = ['params' => [PaypalModel\Config::METHOD_WPP_EXPRESS, $quoteData['store_id']]];
        $this->_config = $this->_configFactory->create($parameters);
        parent::_postBack();
    }

    /**
     * @param string $ipnPaymentStatus
     *
     * @return string
     */
    protected function filterPaymentStatus($ipnPaymentStatus)
    {
        switch ($ipnPaymentStatus) {
            case 'Created':
            case 'Completed':
                return PaypalModel\Info::PAYMENTSTATUS_COMPLETED;
            case 'Denied':
                return PaypalModel\Info::PAYMENTSTATUS_DENIED;
            case 'Expired':
                return PaypalModel\Info::PAYMENTSTATUS_EXPIRED;
            case 'Failed':
                return PaypalModel\Info::PAYMENTSTATUS_FAILED;
            case 'Pending':
                return PaypalModel\Info::PAYMENTSTATUS_PENDING;
            case 'Refunded':
                return PaypalModel\Info::PAYMENTSTATUS_REFUNDED;
            case 'Reversed':
                return PaypalModel\Info::PAYMENTSTATUS_REVERSED;
            case 'Canceled_Reversal':
                return PaypalModel\Info::PAYMENTSTATUS_UNREVERSED;
            case 'Processed':
                return PaypalModel\Info::PAYMENTSTATUS_PROCESSED;
            case 'Voided':
                return PaypalModel\Info::PAYMENTSTATUS_VOIDED;
        }
        return '';
    }

    /**
     * @param string $ipnProfileStatus
     *
     * @return string
     */
    protected function filterProfileStatus($ipnProfileStatus)
    {
        switch ($ipnProfileStatus) {
            case 'Suspended':
                return ProfileStatus::PAUSED_VALUE;
            case 'Cancelled':
                return ProfileStatus::CANCELED_VALUE;
            case 'Expired':
                return ProfileStatus::COMPLETED_VALUE;
            case 'Active':
                return ProfileStatus::ACTIVE_VALUE;
            case 'Pending':
                return ProfileStatus::PENDING_VALUE;
        }
        return '';
    }

    /**
     * @param string $status
     *
     * @return string
     */
    protected function getOrderStateByStatus($status)
    {
        /** @var \Magento\Sales\Model\ResourceModel\Order\Status\Collection $orderStatusCollection */
        $orderStatusCollection = $this->orderStatusCollectionFactory->create();
        $orderStatusCollection->joinStates();
        $orderStatusCollection->addFieldToFilter('main_table.status', $status);
        $orderStatusModel = $orderStatusCollection->getFirstItem();
        return $orderStatusModel->getState();
    }
}
