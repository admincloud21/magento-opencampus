<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Paypal\Api;

use Magento\Payment\Model\Method\Logger;
use Magento\Customer\Helper\Address;
use Psr\Log\LoggerInterface;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Directory\Model\RegionFactory;
use Magento\Directory\Model\CountryFactory;
use Magento\Paypal\Model\Api\ProcessableExceptionFactory;
use Magento\Framework\Exception\LocalizedExceptionFactory;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use Exto\Sarp\Model\Quote\VirtualManagement;
use Exto\Sarp\Model\QuoteHelper;

/**
 * Class Nvp
 */
class Nvp extends \Magento\Paypal\Model\Api\Nvp
{
    /** @var array */
    protected $paymentRequestFieldList = [
        'SHIPTONAME',
        'SHIPTOCOUNTRYCODE',
        'SHIPTOSTATE',
        'SHIPTOCITY',
        'SHIPTOSTREET',
        'SHIPTOSTREET2',
        'SHIPTOZIP',
        'SHIPTOPHONENUM',
        'AMT',
        'CURRENCYCODE',
        'ITEMAMT',
        'TAXAMT',
        'SHIPPINGAMT',
        'INVNUM',
        'PAYMENTACTION',
    ];

    /** @var array */
    protected $paymentRequestItemFieldList = [
        'L_NUMBER',
        'L_NAME',
        'L_QTY',
        'L_AMT',
    ];

    /** @var bool */
    protected $recurringSetExpressCheckoutFlag = false;

    /** @var \Exto\Sarp\Model\Quote\VirtualManagement */
    protected $virtualManagement;

    /** @var \Exto\Sarp\Model\QuoteHelper */
    protected $quoteHelper;

    /**
     * Nvp constructor.
     * @param Address $customerAddress
     * @param LoggerInterface $logger
     * @param Logger $customLogger
     * @param ResolverInterface $localeResolver
     * @param RegionFactory $regionFactory
     * @param CountryFactory $countryFactory
     * @param ProcessableExceptionFactory $processableExceptionFactory
     * @param LocalizedExceptionFactory $frameworkExceptionFactory
     * @param CurlFactory $curlFactory
     * @param VirtualManagement $virtualManagement
     * @param QuoteHelper $quoteHelper
     * @param array $data
     */
    public function __construct(
        Address $customerAddress,
        LoggerInterface $logger,
        Logger $customLogger,
        ResolverInterface $localeResolver,
        RegionFactory $regionFactory,
        CountryFactory $countryFactory,
        ProcessableExceptionFactory $processableExceptionFactory,
        LocalizedExceptionFactory $frameworkExceptionFactory,
        CurlFactory $curlFactory,
        VirtualManagement $virtualManagement,
        QuoteHelper $quoteHelper,
        array $data = []
    ) {
        parent::__construct(
            $customerAddress,
            $logger,
            $customLogger,
            $localeResolver,
            $regionFactory,
            $countryFactory,
            $processableExceptionFactory,
            $frameworkExceptionFactory,
            $curlFactory,
            $data
        );
        $this->virtualManagement = $virtualManagement;
        $this->quoteHelper = $quoteHelper;
    }

    /**
     * {@inheritdoc}
     * @return $this
     */
    public function callSetExpressCheckout()
    {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->getAddress()->getQuote();
        if (!$this->quoteHelper->isQuoteContainRecurringItems($quote)) {
            parent::callSetExpressCheckout();
            return $this;
        }
        $this->recurringSetExpressCheckoutFlag = true;
        parent::callSetExpressCheckout();
        $this->recurringSetExpressCheckoutFlag = false;
        return $this;
    }

    /**
     * @param array $request
     * @param int $i
     * @return null|true
     */
    protected function _exportLineItems(array &$request, $i = 0)
    {
        $result = parent::_exportLineItems($request, $i);
        if ($this->recurringSetExpressCheckoutFlag) {
            $this->processRequestDataForRecurring($request);
        }
        return $result;
    }

    /**
     * @param array $request
     * @return $this
     */
    protected function processRequestDataForRecurring(&$request)
    {
        $newRequestData = [];
        $virtualQuoteListPerPayments = $this->virtualManagement->getVirtualQuoteList($this->getAddress()->getQuote());
        $recurringPaymentId = 0;
        foreach ($virtualQuoteListPerPayments as $virtualQuote) {
            /** @var \Magento\Quote\Model\Quote $virtualQuote */
            if ($this->quoteHelper->isQuoteContainRecurringItems($virtualQuote)) {
                $request['L_BILLINGTYPE' . $recurringPaymentId] = 'RecurringPayments';
                $allVisibleItems = $virtualQuote->getAllVisibleItems();
                $billingAgreement = $this->quoteHelper->getBillingAgreementForQuoteItem(
                    array_shift($allVisibleItems)
                );
                $request['L_BILLINGAGREEMENTDESCRIPTION' . $recurringPaymentId] = $billingAgreement;
                $recurringPaymentId++;
            } else {
                $newRequestData = array_merge(
                    $newRequestData,
                    $this->convertRequestKeysToRecurring($request, $virtualQuote)
                );
            }
        }
        $this->removeNonRecurringKeys($request);
        $request = array_merge($request, $newRequestData);
        return $this;
    }

    /**
     * @param array $initialRequest
     * @param \Magento\Quote\Model\Quote $quote
     * @return array
     */
    protected function convertRequestKeysToRecurring($initialRequest, $quote)
    {
        $request = [];
        $paypalPaymentId = 0;
        if (array_key_exists('BILLINGTYPE', $initialRequest)) {
            $request['L_BILLINGTYPE' . $paypalPaymentId] = $initialRequest['BILLINGTYPE'];
        }
        $keyPrefix = 'PAYMENTREQUEST_' . $paypalPaymentId . '_';
        foreach ($this->paymentRequestFieldList as $fieldKey) {
            if (!array_key_exists($fieldKey, $initialRequest)) {
                continue;
            }
            $newKey = $keyPrefix . $fieldKey;
            $request[$newKey] = $initialRequest[$fieldKey];
        }

        foreach ($quote->getAllVisibleItems() as $i => $item) {
            $itemData = [
                'L_' . $keyPrefix . 'NUMBER' . $i => $i,
                'L_' . $keyPrefix . 'NAME' . $i => $item->getName(),
                'L_' . $keyPrefix . 'QTY' . $i => $item->getQty(),
                'L_' . $keyPrefix . 'AMT' . $i => $this->formatPrice($item->getPrice()),
            ];
            $request = array_merge($request, $itemData);
        }
        $address = $quote->getBillingAddress();
        if (!$quote->isVirtual()) {
            $address = $quote->getShippingAddress();
        }
        $request[$keyPrefix . 'ITEMAMT'] = $address->getBaseSubtotal() - $address->getBaseDiscountAmount();
        $request[$keyPrefix . 'TAXAMT'] = $address->getBaseTaxAmount();
        $request[$keyPrefix . 'SHIPPINGAMT'] = $address->getBaseShippingAmount();
        $request[$keyPrefix . 'AMT']
            = $address->getBaseSubtotal() - $address->getBaseDiscountAmount() + $address->getBaseTaxAmount()
            + $address->getBaseShippingAmount();
        return $request;
    }

    /**
     * @param array $request
     * @return void
     */
    protected function removeNonRecurringKeys(&$request)
    {
        $request = array_diff_key(
            $request,
            array_combine($this->paymentRequestFieldList, $this->paymentRequestFieldList)
        );
        foreach ($this->paymentRequestItemFieldList as $field) {
            foreach ($request as $key => $value) {
                if (strpos($key, $field) === 0) {
                    unset($request[$key]);
                }
            }
        }
    }
}
