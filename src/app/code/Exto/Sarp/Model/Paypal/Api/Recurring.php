<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Paypal\Api;

use Magento\Customer\Helper\Address;
use Psr\Log\LoggerInterface;
use Magento\Payment\Model\Method\Logger;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Directory\Model\RegionFactory;
use Magento\Directory\Model\CountryFactory;
use Magento\Paypal\Model\Api\ProcessableExceptionFactory;
use Magento\Framework\Exception\LocalizedExceptionFactory;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use Magento\Paypal\Model\ProFactory;
use Magento\Paypal\Model\Config;

/**
 * Class Recurring
 *
 * @method setToken
 * @method setProfileId
 * @method setAction
 * @method setAmount
 * @method setCurrencyCode
 * @method getAddress
 * @method setAddress
 * @method setBillingAddress
 * @method setSuppressShipping
 * @method getProfileId
 * @method getProfileStatus
 * @method setBillingPeriod
 * @method setBillingFrequency
 * @method setTotalBillingCycles
 * @method setMaxFailedAttempts
 * @method setProfileStartDate
 */
class Recurring extends \Magento\Paypal\Model\Api\Nvp
{
    const DO_CREATE_RECURRING_PAYMENTS_PROFILE = 'CreateRecurringPaymentsProfile';
    const DO_MANAGE_RECURRING_PAYMENTS_PROFILE_STATUS = 'ManageRecurringPaymentsProfileStatus';

    /**
     * @var array
     */
    protected $additionalGlobalMap = [
        'PROFILESTARTDATE' => 'profile_start_date',
        'BILLINGPERIOD' => 'billing_period',
        'BILLINGFREQUENCY' => 'billing_frequency',
        'TOTALBILLINGCYCLES' => 'total_billing_cycles',
        'MAXFAILEDPAYMENTS' => 'max_failed_attempts',
        'PROFILEID' => 'profile_id',
        'PROFILESTATUS' => 'profile_status',
        'DESC' => 'profile_description'
    ];

    /** @var array */
    protected $createRecurringPaymentsProfile = [
        'TOKEN',
        'PROFILESTARTDATE',
        'DESC',
        'BILLINGPERIOD',
        'BILLINGFREQUENCY',
        'TOTALBILLINGCYCLES',
        'AMT',
        'CURRENCYCODE',
        'SHIPPINGAMT',
        'TAXAMT',
        'EMAIL',
        'MAXFAILEDPAYMENTS'
    ];

    /** @var array */
    protected $createRecurringPaymentsProfileResponse = [
        'PROFILEID',
        'PROFILESTATUS'
    ];

    /** @var array */
    protected $manageRecurringPaymentsProfileStatus = [
        'PROFILEID',
        'ACTION'
    ];

    /** @var array */
    protected $manageRecurringPaymentsProfileStatusResponse = [
        'PROFILEID'
    ];

    /**
     * Recurring constructor.
     * @param Address $customerAddress
     * @param LoggerInterface $logger
     * @param Logger $customLogger
     * @param ResolverInterface $localeResolver
     * @param RegionFactory $regionFactory
     * @param CountryFactory $countryFactory
     * @param ProcessableExceptionFactory $processableExceptionFactory
     * @param LocalizedExceptionFactory $frameworkExceptionFactory
     * @param CurlFactory $curlFactory
     * @param ProFactory $proFactory
     * @param array $data
     */
    public function __construct(
        Address $customerAddress,
        LoggerInterface $logger,
        Logger $customLogger,
        ResolverInterface $localeResolver,
        RegionFactory $regionFactory,
        CountryFactory $countryFactory,
        ProcessableExceptionFactory $processableExceptionFactory,
        LocalizedExceptionFactory $frameworkExceptionFactory,
        CurlFactory $curlFactory,
        ProFactory $proFactory,
        array $data = []
    ) {
        parent::__construct(
            $customerAddress,
            $logger,
            $customLogger,
            $localeResolver,
            $regionFactory,
            $countryFactory,
            $processableExceptionFactory,
            $frameworkExceptionFactory,
            $curlFactory,
            $data
        );
        /** @var \Magento\Paypal\Model\Pro $pro */
        $pro = $proFactory->create();
        $pro->setMethod(Config::METHOD_WPP_EXPRESS);
        $this->setConfigObject($pro->getConfig());
    }

    /**
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    public function callCreateRecurringPaymentsProfile()
    {
        $request = $this->_exportToRequest($this->createRecurringPaymentsProfile);
        $this->_exportLineItems($request);
        if ($this->getAddress()) {
            $request = $this->_importAddresses($request);
        }
        $response = $this->call(self::DO_CREATE_RECURRING_PAYMENTS_PROFILE, $request);
        $this->_importFromResponse($this->createRecurringPaymentsProfileResponse, $response);
    }

    /**
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    public function callManageRecurringPaymentsProfileStatus()
    {
        $request = $this->_exportToRequest($this->manageRecurringPaymentsProfileStatus);
        $response = $this->call(self::DO_MANAGE_RECURRING_PAYMENTS_PROFILE_STATUS, $request);
        $this->_importFromResponse($this->manageRecurringPaymentsProfileStatusResponse, $response);
    }

    /**
     * @param array $privateResponseMap
     * @param array $response
     *
     * @return void
     */
    protected function _importFromResponse(array $privateResponseMap, array $response)
    {
        $this->_globalMap = array_merge($this->_globalMap, $this->additionalGlobalMap);
        parent::_importFromResponse($privateResponseMap, $response);
    }

    /**
     * @param array $privateRequestMap
     * @param array $request
     *
     * @return array
     */
    protected function &_exportToRequest(array $privateRequestMap, array $request = [])
    {
        $this->_globalMap = array_merge($this->_globalMap, $this->additionalGlobalMap);
        return parent::_exportToRequest($privateRequestMap, $request);
    }

    /**
     * @return string
     */
    protected function getProfileStartDate()
    {
        $startDate = new \DateTime($this->getData('profile_start_date'));
        $utcTimezone = new \DateTimeZone("UTC");
        $startDate->setTimezone($utcTimezone);
        return $startDate->format('Y-m-d') . 'T' . $startDate->format('G:i:s') . 'Z';
    }
}
