<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Product;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Exto\Sarp\Api\ProductManagementInterface;
use Exto\Sarp\Api\ProfileOrderRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * Class ProductManagement
 */
class ProductManagement implements ProductManagementInterface
{
    const TEMPLATE_PRODUCT_ATTRIBUTE_CODE = 'exto_sarp_template';

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * OrderManagement constructor.
     * @param ProductRepositoryInterface $productRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @return \Magento\Catalog\Api\Data\ProductSearchResultsInterface
     */
    public function getProductList()
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->create();
        return $this->productRepository->getList($searchCriteria);
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     *
     * @return bool
     */
    public function hasRecurringOptions(\Magento\Catalog\Model\Product $product)
    {
        return !!$product->getData(self::TEMPLATE_PRODUCT_ATTRIBUTE_CODE);
    }
}
