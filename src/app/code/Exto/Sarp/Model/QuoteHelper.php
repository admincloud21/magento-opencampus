<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model;

use Magento\Quote\Model\Quote as QuoteEntity;
use Magento\Quote\Model\Quote\Item as QuoteItem;
use Exto\Sarp\Model\Product\Type\Subscription as SarpProductType;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class QuoteHelper
 */
class QuoteHelper
{
    const BUY_REQUEST_FIRST_BILLING_DATE = 'exto_sarp_first_billing_date';
    const BUY_REQUEST_TEMPLATE_OPTION = 'exto_sarp_template_option';

    /** @var ResourceModel\SubscriptionTemplateRepository */
    protected $subscriptionTemplateRepository;

    /** @var ResourceModel\SubscriptionTemplateOptionRepository */
    protected $subscriptionTemplateOptionRepository;

    /**
     * QuoteHelper constructor.
     * @param ResourceModel\SubscriptionTemplateRepository $subscriptionTemplateRepository
     * @param ResourceModel\SubscriptionTemplateOptionRepository $subscriptionTemplateOptionRepository
     */
    public function __construct(
        ResourceModel\SubscriptionTemplateRepository $subscriptionTemplateRepository,
        ResourceModel\SubscriptionTemplateOptionRepository $subscriptionTemplateOptionRepository
    ) {
        $this->subscriptionTemplateRepository = $subscriptionTemplateRepository;
        $this->subscriptionTemplateOptionRepository = $subscriptionTemplateOptionRepository;
    }

    /**
     * @param QuoteEntity $quote
     *
     * @return bool
     */
    public function isQuoteContainRecurringItems($quote)
    {
        foreach ($quote->getAllVisibleItems() as $item) {
            if ($this->isRecurringQuoteItem($item)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param QuoteItem\AbstractItem $quoteItem
     *
     * @return bool
     */
    public function isRecurringQuoteItem($quoteItem)
    {
        $subscriptionTemplateOption = $this->getQuoteItemSubscriptionTemplateOption($quoteItem);
        if (null === $subscriptionTemplateOption) {//then item has w\o sarp option
            return false;
        }
        return true;
    }

    /**
     * @param QuoteItem\AbstractItem $quoteItem
     *
     * @return Data\SubscriptionTemplateOption|null
     */
    public function getQuoteItemSubscriptionTemplateOption($quoteItem)
    {
        $option = $quoteItem->getOptionByCode(self::BUY_REQUEST_TEMPLATE_OPTION);
        if (null === $option) {
            return null;
        }
        $templateOptionId = intval($option->getValue());
        try {
            return $this->subscriptionTemplateOptionRepository->get($templateOptionId);
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * @param QuoteItem\AbstractItem $quoteItem
     *
     * @return string
     */
    public function getQuoteItemSubscriptionTitle($quoteItem)
    {
        $subscriptionTemplateOption = $this->getQuoteItemSubscriptionTemplateOption($quoteItem);
        if (null === $subscriptionTemplateOption) {
            return null;
        }
        return $subscriptionTemplateOption->getTitle();
    }

    /**
     * @param QuoteItem\AbstractItem $quoteItem
     *
     * @return int|null
     */
    public function getQuoteItemMaxPaymentsFailures($quoteItem)
    {
        $subscriptionTemplateOption = $this->getQuoteItemSubscriptionTemplateOption($quoteItem);
        if (null === $subscriptionTemplateOption) {
            return null;
        }
        $subscriptionTemplate = $this->subscriptionTemplateRepository->get(
            $subscriptionTemplateOption->getTemplateId()
        );
        return $subscriptionTemplate->getMaxPaymentFailures();
    }

    /**
     * @param QuoteItem\AbstractItem $quoteItem
     *
     * @return int|null
     */
    public function getQuoteItemDefaultOrderStatus($quoteItem)
    {
        $subscriptionTemplateOption = $this->getQuoteItemSubscriptionTemplateOption($quoteItem);
        if (null === $subscriptionTemplateOption) {
            return null;
        }
        $subscriptionTemplate = $this->subscriptionTemplateRepository->get(
            $subscriptionTemplateOption->getTemplateId()
        );
        return $subscriptionTemplate->getDefaultOrderStatus();
    }

    /**
     * @param QuoteItem\AbstractItem $quoteItem
     *
     * @return int|null
     */
    public function getQuoteItemFrequencyOption($quoteItem)
    {
        $subscriptionTemplateOption = $this->getQuoteItemSubscriptionTemplateOption($quoteItem);
        if (null === $subscriptionTemplateOption) {
            return null;
        }
        return intval($subscriptionTemplateOption->getBillingFrequency());
    }

    /**
     * @param QuoteItem\AbstractItem $quoteItem
     *
     * @return string|null
     */
    public function getQuoteItemPeriodOption($quoteItem)
    {
        $subscriptionTemplateOption = $this->getQuoteItemSubscriptionTemplateOption($quoteItem);
        if (null === $subscriptionTemplateOption) {
            return null;
        }
        return $subscriptionTemplateOption->getBillingPeriod();
    }

    /**
     * @param QuoteItem\AbstractItem $quoteItem
     *
     * @return int|null
     */
    public function getQuoteItemCyclesOption($quoteItem)
    {
        $subscriptionTemplateOption = $this->getQuoteItemSubscriptionTemplateOption($quoteItem);
        if (null === $subscriptionTemplateOption) {
            return null;
        }
        return intval($subscriptionTemplateOption->getBillingCycles());
    }

    /**
     * @param QuoteItem\AbstractItem $quoteItem
     *
     * @return \DateTime|null
     */
    public function getQuoteItemStartDateOption($quoteItem)
    {
        $option = $quoteItem->getOptionByCode(self::BUY_REQUEST_FIRST_BILLING_DATE);
        if (null === $option) {
            return null;
        }
        return $this->getQuoteItemStartDateByOptionValue($option->getValue());
    }

    /**
     * @param string $value
     *
     * @return \DateTime|null
     */
    public function getQuoteItemStartDateByOptionValue($value)
    {
        $startDate = new \DateTime($value);
        $now = new \DateTime();
        if ($startDate < $now) {
            return null;
        }
        return $startDate;
    }

    /**
     * @param QuoteItem\AbstractItem $quoteItem
     *
     * @return string
     */
    public function getBillingAgreementForQuoteItem($quoteItem)
    {
        $billingFrequency = $this->getQuoteItemFrequencyOption($quoteItem);
        $billingPeriod = $this->getQuoteItemPeriodOption($quoteItem);
        $billingCycles = $this->getQuoteItemCyclesOption($quoteItem);
        $startDateAsString = $this->getSubscriptionStartDateDescription($quoteItem);
        return __(
            'Bill each %1 %2 for %3 times. Starts at %4',
            $billingFrequency,
            $billingPeriod,
            $billingCycles,
            $startDateAsString
        )->render();
    }

    /**
     * @param QuoteItem\AbstractItem $quoteItem
     *
     * @return string
     */
    public function getSubscriptionStartDateDescription($quoteItem)
    {
        $startDate = $this->getQuoteItemStartDateOption($quoteItem);
        if (null === $startDate) {
            $startDate = new \DateTime();
        }
        return $this->getSubscriptionStartDateDescriptionByDatetime($startDate);
    }

    /**
     * @param QuoteItem\AbstractItem $quoteItem
     *
     * @return string
     */
    public function getSubscriptionPeriodDescription($quoteItem)
    {
        $period = $this->getQuoteItemPeriodOption($quoteItem);
        $frequency = $this->getQuoteItemFrequencyOption($quoteItem);
        $cycles = $this->getQuoteItemCyclesOption($quoteItem);
        return $this->getSubscriptionPeriodDescriptionByOptions($period, $frequency, $cycles);
    }

    /**
     * @param \DateTime $startDate
     *
     * @return string
     */
    public function getSubscriptionStartDateDescriptionByDatetime(\DateTime $startDate)
    {
        $startDateAsString = __('tomorrow');
        if (null !== $startDate) {
            $startDateAsString = $startDate->format('d M Y');
        }
        return $startDateAsString;
    }

    /**
     * @param string $period
     * @param int $frequency
     * @param int $cycles
     *
     * @return \Magento\Framework\Phrase
     */
    public function getSubscriptionPeriodDescriptionByOptions($period, $frequency, $cycles)
    {
        $total = $cycles * $frequency;
        $unitName = '';
        $multiUnitName = '';
        switch ($period) {
            case Source\Template\BillingPeriod::DAY_VALUE:
                $unitName = __('day');
                $multiUnitName = __('day(s)');
                break;
            case Source\Template\BillingPeriod::WEEK_VALUE:
                $unitName = __('week');
                $multiUnitName = __('week(s)');
                break;
            case Source\Template\BillingPeriod::MONTH_VALUE:
                $unitName = __('month');
                $multiUnitName = __('month(s)');
                break;
            case Source\Template\BillingPeriod::YEAR_VALUE:
                $unitName = __('year');
                $multiUnitName = __('year(s)');
                break;
        }
        $total .= ' ' . $multiUnitName;
        return __('Each %1 %2, %3', $frequency, $unitName, $total);
    }
}
