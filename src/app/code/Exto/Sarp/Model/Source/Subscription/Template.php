<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Source\Subscription;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Exto\Sarp\Model\ResourceModel\SubscriptionTemplate\CollectionFactory as SubscriptionTemplateCollectionFactory;

/**
 * Class Template
 */
class Template extends AbstractSource implements OptionSourceInterface
{
    const NO_VALUE = 0;

    /** 
     * @var  \Exto\Sarp\Model\ResourceModel\SubscriptionTemplate\Collection
     */
    protected $templateCollection;

    /**
     * Template constructor.
     * @param \Exto\Sarp\Model\ResourceModel\SubscriptionTemplate\CollectionFactory $templateCollectionFactory
     */
    public function __construct(SubscriptionTemplateCollectionFactory $templateCollectionFactory)
    {
        $this->templateCollection = $templateCollectionFactory->create();
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $label = __('No');
        $templates = $this->templateCollection->toOptionArray('id', 'title');
        $noTemplate = [
            ['value' => self::NO_VALUE, 'label' => $label],
        ];
        return array_merge($noTemplate, $templates);
    }

    /**
     * @return array
     */
    public static function getNoSubscriptionOption()
    {
        return [self::NO_VALUE => __('No subscription')];
    }
}
