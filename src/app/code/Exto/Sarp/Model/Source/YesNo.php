<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class YesNo
 */
class YesNo implements OptionSourceInterface
{
    const YES_VALUE = 1;
    const NO_VALUE = 0;
    
    const DEFAULT_VALUE = self::YES_VALUE;

    /**
     * @return array
     */
    public static function getOptionArray()
    {
        return [
            self::YES_VALUE => __('Yes'),
            self::NO_VALUE => __('No'),
        ];
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::YES_VALUE,  'label' => __('Yes')],
            ['value' => self::NO_VALUE,  'label' => __('No')],
        ];
    }
}
