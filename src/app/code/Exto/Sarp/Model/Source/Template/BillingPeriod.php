<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Source\Template;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class BillingPeriod
 */
class BillingPeriod implements OptionSourceInterface
{
    const DAY_VALUE = 'day';
    const WEEK_VALUE = 'week';
    const MONTH_VALUE = 'month';
    const YEAR_VALUE = 'year';
    
    /**
     * @return array
     */
    public static function getOptionArray()
    {
        return [
            self::DAY_VALUE => __('Day'),
            self::WEEK_VALUE => __('Week'),
            self::MONTH_VALUE => __('Month'),
            self::YEAR_VALUE => __('Year')
        ];
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::DAY_VALUE,  'label' => __('Day')],
            ['value' => self::WEEK_VALUE,  'label' => __('Week')],
            ['value' => self::MONTH_VALUE,  'label' => __('Month')],
            ['value' => self::YEAR_VALUE,  'label' => __('Year')],
        ];
    }
}
