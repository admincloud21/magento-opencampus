<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Source\Template;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class OptionStyle
 */
class OptionStyle implements OptionSourceInterface
{
    const SELECT_VALUE = 'select';
    const RADIO_VALUE = 'radio';
    
    /**
     * @return array
     */
    public static function getOptionArray()
    {
        return [
            self::SELECT_VALUE => __('Select'),
            self::RADIO_VALUE => __('Radio Buttons'),
        ];
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::SELECT_VALUE,  'label' => __('Select')],
            ['value' => self::RADIO_VALUE,  'label' => __('Radio Buttons')],
        ];
    }
}
