<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Source\Template;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class OrderStatus
 */
class OrderStatus implements OptionSourceInterface
{
    const COMPLETE_STATUS = 'complete';
    const PROCESSING_STATUS = 'processing';
    const PENDING_STATUS = 'pending';
    
    /**
     * @return array
     */
    public static function getOptionArray()
    {
        return [
            self::COMPLETE_STATUS => __('Complete'),
            self::PROCESSING_STATUS => __('Processing'),
            self::PENDING_STATUS => __('Pending'),
        ];
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::COMPLETE_STATUS,  'label' => __('Complete')],
            ['value' => self::PROCESSING_STATUS,  'label' => __('Processing')],
            ['value' => self::PENDING_STATUS,  'label' => __('Pending')],
        ];
    }
}
