<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Source\Profile;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 */
class Status implements OptionSourceInterface
{
    const PENDING_VALUE = 'pending';
    const FAILED_VALUE = 'failed';
    const ACTIVE_VALUE = 'active';
    const PAUSED_VALUE = 'paused';
    const CANCELED_VALUE = 'canceled';
    const COMPLETED_VALUE = 'completed';
    
    const SUSPEND_ACTION = 'suspend';
    const RESUME_ACTION = 'resume';

    const DEFAULT_VALUE = self::PENDING_VALUE;

    /**
     * @return array
     */
    public static function getOptionArray()
    {
        return [
            self::PENDING_VALUE => __('Pending'),
            self::FAILED_VALUE => __('Failed'),
            self::ACTIVE_VALUE => __('Active'),
            self::PAUSED_VALUE => __('Paused'),
            self::CANCELED_VALUE => __('Canceled'),
            self::COMPLETED_VALUE => __('Completed'),
        ];
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::PENDING_VALUE,  'label' => __('Pending')],
            ['value' => self::FAILED_VALUE,  'label' => __('Failed')],
            ['value' => self::ACTIVE_VALUE,  'label' => __('Active')],
            ['value' => self::PAUSED_VALUE,  'label' => __('Paused')],
            ['value' => self::CANCELED_VALUE,  'label' => __('Canceled')],
            ['value' => self::COMPLETED_VALUE,  'label' => __('Completed')],
        ];
    }

    /**
     * @param string $value
     * @return mixed|string
     */
    public function getStatusLabelByCode($value)
    {
        $allOptions = $this->getOptionArray();
        if (isset($allOptions[$value])) {
            $label = $allOptions[$value];
        } else {
            $label = '';
        }
        return $label;
    }
}
