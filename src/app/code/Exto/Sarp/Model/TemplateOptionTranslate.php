<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model;

use Magento\Framework\Profiler;
use Magento\Framework;
use Exto\Sarp\Model\ResourceModel\TemplateOptionTranslate as TemplateOptionTranslateResourceModel;
use Exto\Sarp\Api\Data;

/**
 * Class TemplateOptionTranslate
 */
class TemplateOptionTranslate extends Framework\Model\AbstractModel
{
    /**
     * @var \Exto\Sarp\Api\Data\TemplateOptionTranslateInterfaceFactory
     */
    private $templateOptionTranslateDataFactory;

    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * TemplateOptionTranslate constructor.
     * @param Framework\Model\Context $context
     * @param Framework\Registry $registry
     * @param TemplateOptionTranslateResourceModel $resource
     * @param TemplateOptionTranslateResourceModel\Collection $resourceCollection
     * @param Data\TemplateOptionTranslateInterfaceFactory $templateOptionTranslateDataFactory
     * @param Framework\Api\DataObjectHelper $dataObjectHelper
     * @param array $data
     */
    public function __construct(
        Framework\Model\Context $context,
        Framework\Registry $registry,
        TemplateOptionTranslateResourceModel $resource,
        TemplateOptionTranslateResourceModel\Collection $resourceCollection,
        Data\TemplateOptionTranslateInterfaceFactory $templateOptionTranslateDataFactory,
        Framework\Api\DataObjectHelper $dataObjectHelper,
        array $data = []
    ) {
        $this->templateOptionTranslateDataFactory = $templateOptionTranslateDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Initialize resource mode
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(TemplateOptionTranslateResourceModel::class);
    }

    /**
     * Retrieve TemplateOptionTranslate model with data
     *
     * @return \Exto\Sarp\Api\Data\TemplateOptionTranslateInterface
     */
    public function getDataModel()
    {
        $data = $this->getData();
        $dataObject = $this->templateOptionTranslateDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $dataObject,
            $data,
            Data\TemplateOptionTranslateInterface::class
        );
        return $dataObject;
    }
}
