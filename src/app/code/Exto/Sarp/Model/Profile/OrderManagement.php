<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Profile;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Exto\Sarp\Api\OrderManagementInterface;
use Exto\Sarp\Api\ProfileOrderRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\OrderFactory;

/**
 * Class OrderManagement
 */
class OrderManagement implements OrderManagementInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var ProfileOrderRepositoryInterface
     */
    protected $profileOrderRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * OrderManagement constructor.
     * @param ProfileOrderRepositoryInterface $profileOrderRepository
     * @param OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        ProfileOrderRepositoryInterface $profileOrderRepository,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        OrderFactory $orderFactory
    ) {
        $this->profileOrderRepository = $profileOrderRepository;
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->orderFactory = $orderFactory;
    }

    /**
     * @param int $profileId
     * @return \Magento\Sales\Api\Data\OrderSearchResultInterface
     */
    public function getOrderListByProfileId($profile)
    {
        $profileId = $profile->getId();
        $orderItems = $this->profileOrderRepository->getListByProfileId($profileId)->getItems();
        $orderIds = [];
        /** @var \Exto\Sarp\Api\Data\ProfileOrderInterface $profileOrder */
        foreach ($orderItems as $profileOrder) {
            $orderIds[] = $profileOrder->getOrderId();
        }

        $quoteData = $profile->getQuoteData();
        if (empty($orderIds) && isset($quoteData['reserved_order_id'])) {
            $order = $this->orderFactory->create()->loadByIncrementId($quoteData['reserved_order_id']);
            $orderIds[] = $order->getId();
        }

        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('entity_id', $orderIds, 'in')
            ->create();
        return $this->orderRepository->getList($searchCriteria);
    }
}
