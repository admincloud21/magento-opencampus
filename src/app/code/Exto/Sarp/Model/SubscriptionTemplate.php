<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model;

use Magento\Framework\Profiler;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework;
use Exto\Sarp\Model\ResourceModel\SubscriptionTemplate as SubscriptionTemplateResourceModel;
use Exto\Sarp\Api\Data;

/**
 * Class SubscriptionTemplate
 */
class SubscriptionTemplate extends AbstractModel
{
    /**
     * @var \Exto\Sarp\Api\Data\SubscriptionTemplateInterfaceFactory
     */
    private $subscriptionTemplateDataFactory;

    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * SubscriptionTemplate constructor.
     * @param Framework\Model\Context $context
     * @param Framework\Registry $registry
     * @param SubscriptionTemplateResourceModel $resource
     * @param SubscriptionTemplateResourceModel\Collection $resourceCollection
     * @param Data\SubscriptionTemplateInterfaceFactory $subscriptionTemplateDataFactory
     * @param Framework\Api\DataObjectHelper $dataObjectHelper
     * @param array $data
     */
    public function __construct(
        Framework\Model\Context $context,
        Framework\Registry $registry,
        SubscriptionTemplateResourceModel $resource,
        SubscriptionTemplateResourceModel\Collection $resourceCollection,
        Data\SubscriptionTemplateInterfaceFactory $subscriptionTemplateDataFactory,
        Framework\Api\DataObjectHelper $dataObjectHelper,
        array $data = []
    ) {
        $this->subscriptionTemplateDataFactory = $subscriptionTemplateDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Initialize resource mode
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(SubscriptionTemplateResourceModel::class);
    }

    /**
     * Retrieve SubscriptionTemplate model with data
     *
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateInterface
     */
    public function getDataModel()
    {
        $data = $this->getData();
        $dataObject = $this->subscriptionTemplateDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $dataObject,
            $data,
            Data\SubscriptionTemplateInterface::class
        );
        return $dataObject;
    }
}
