<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model;

use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class TemplateOptionTranslate
 */
class TemplateOptionTranslateRegistry
{
    /**
     * @var TemplateOptionTranslateFactory
     */
    private $templateOptionTranslateFactory;

    /**
     * TemplateOptionTranslate registry by id
     * @var array
     */
    private $templateOptionTranslateRegistryById = [];

    /**
     * TemplateOptionTranslate registry by id and store id
     * @var array
     */
    private $templateOptionTranslateRegistryByIdAndStore = [];

    /**
     * TemplateOptionTranslate resource
     * @var ResourceModel\TemplateOptionTranslate
     */
    private $templateOptionTranslateResource;

    /**
     * TemplateOptionTranslateRegistry constructor.
     * @param TemplateOptionTranslateFactory $templateOptionTranslateFactory
     * @param ResourceModel\TemplateOptionTranslate $templateOptionTranslateResource
     */
    public function __construct(
        TemplateOptionTranslateFactory $templateOptionTranslateFactory,
        ResourceModel\TemplateOptionTranslate $templateOptionTranslateResource
    ) {
        $this->templateOptionTranslateResource = $templateOptionTranslateResource;
        $this->templateOptionTranslateFactory = $templateOptionTranslateFactory;
    }

    /**
     * @param int $templateOptionTranslateId
     * @return TemplateOptionTranslate
     * @throws NoSuchEntityException
     */
    public function retrieve($templateOptionTranslateId)
    {
        if (!isset($this->templateOptionTranslateRegistryById[$templateOptionTranslateId])) {
            /** @var TemplateOptionTranslate $templateOptionTranslate */
            $templateOptionTranslate = $this->templateOptionTranslateFactory->create();
            $this->templateOptionTranslateResource->load($templateOptionTranslate, $templateOptionTranslateId);
            if (!$templateOptionTranslate->getId()) {
                throw NoSuchEntityException::singleField('templateOptionTranslateId', $templateOptionTranslateId);
            } else {
                $this->templateOptionTranslateRegistryById[$templateOptionTranslateId] = $templateOptionTranslate;
            }
        }
        return $this->templateOptionTranslateRegistryById[$templateOptionTranslateId];
    }

    public function retrieveByStore($templateOptionTranslateId, $storeId)
    {
        if (!isset($this->templateOptionTranslateRegistryByIdAndStore[$templateOptionTranslateId][$storeId])) {
            /** @var TemplateOptionTranslate $templateOptionTranslate */
            $templateOptionTranslate = $this->templateOptionTranslateFactory->create();
            $this->templateOptionTranslateResource->loadByStore(
                $templateOptionTranslate,
                $templateOptionTranslateId,
                $storeId
            );
            if (!$templateOptionTranslate->getId()) {
                throw NoSuchEntityException::singleField('templateOptionTranslateId', $templateOptionTranslateId);
            } else {
                $this->templateOptionTranslateRegistryByIdAndStore[$templateOptionTranslateId][$storeId]
                    = $templateOptionTranslate;
            }
        }
        return $this->templateOptionTranslateRegistryByIdAndStore[$templateOptionTranslateId][$storeId];
    }

    /**
     * @param int $templateOptionTranslateId
     *
     * @return void
     */
    public function remove($templateOptionTranslateId)
    {
        if (isset($this->templateOptionTranslateRegistryById[$templateOptionTranslateId])) {
            unset($this->templateOptionTranslateRegistryById[$templateOptionTranslateId]);
        }
        if (isset($this->templateOptionTranslateRegistryByIdAndStore[$templateOptionTranslateId])) {
            unset($this->templateOptionTranslateRegistryByIdAndStore[$templateOptionTranslateId]);
        }
    }

    /**
     * @param TemplateOptionTranslate $templateOptionTranslate
     * @return $this
     */
    public function push(TemplateOptionTranslate $templateOptionTranslate)
    {
        $this->templateOptionTranslateRegistryById[$templateOptionTranslate->getId()] = $templateOptionTranslate;
        return $this;
    }
}
