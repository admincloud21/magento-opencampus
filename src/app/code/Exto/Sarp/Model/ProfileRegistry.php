<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Exto\Sarp\Model\ProfileFactory;
use Exto\Sarp\Model\ResourceModel\Profile as ProfileResourceModel;

/**
 * Class Profile
 */
class ProfileRegistry
{
    /**
     * @var ProfileFactory
     */
    private $profileFactory;

    /**
     * Profile registry by id
     * @var array
     */
    private $profileRegistryById = [];

    /**
     * Profile resource
     * @var ResourceModel\Profile
     */
    private $profileResource;

    /**
     * ProfileRegistry constructor.
     * @param ProfileFactory $profileFactory
     * @param ResourceModel\Profile $profileResource
     */
    public function __construct(
        ProfileFactory $profileFactory,
        ProfileResourceModel $profileResource
    ) {
        $this->profileResource = $profileResource;
        $this->profileFactory = $profileFactory;
    }

    /**
     * @param mixed $profileId
     * @param string $field
     * @return Profile
     * @throws NoSuchEntityException
     */
    public function retrieve($profileId, $field = null)
    {
        if (!isset($this->profileRegistryById[$profileId])) {
            /** @var Profile $profile */
            $profile = $this->profileFactory->create();
            $this->profileResource->load($profile, $profileId, $field);
            if (!$profile->getId()) {
                throw NoSuchEntityException::singleField('profileId', $profileId);
            } else {
                $this->profileRegistryById[$profileId] = $profile;
            }
        }
        return $this->profileRegistryById[$profileId];
    }

    /**
     * @param int $profileId
     * @return void
     */
    public function remove($profileId)
    {
        if (isset($this->profileRegistryById[$profileId])) {
            unset($this->profileRegistryById[$profileId]);
        }
    }

    /**
     * @param Profile $profile
     * @return $this
     */
    public function push(Profile $profile)
    {
        $this->profileRegistryById[$profile->getId()] = $profile;
        return $this;
    }
}
