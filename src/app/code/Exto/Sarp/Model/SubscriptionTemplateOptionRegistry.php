<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Exto\Sarp\Model;

/**
 * Class SubscriptionTemplateOption
 */
class SubscriptionTemplateOptionRegistry
{
    /**
     * @var SubscriptionTemplateOptionFactory
     */
    private $subscriptionTemplateOptionFactory;

    /**
     * SubscriptionTemplateOption registry by id
     * @var array
     */
    private $subscriptionTemplateOptionRegistryById = [];

    /**
     * SubscriptionTemplateOption resource
     * @var ResourceModel\SubscriptionTemplateOption
     */
    private $subscriptionTemplateOptionResource;

    /**
     * SubscriptionTemplateOptionRegistry constructor.
     * @param SubscriptionTemplateOptionFactory $subscriptionTemplateOptionFactory
     * @param ResourceModel\SubscriptionTemplateOption $subscriptionTemplateOptionResource
     */
    public function __construct(
        Model\SubscriptionTemplateOptionFactory $subscriptionTemplateOptionFactory,
        Model\ResourceModel\SubscriptionTemplateOption $subscriptionTemplateOptionResource
    ) {
        $this->subscriptionTemplateOptionResource = $subscriptionTemplateOptionResource;
        $this->subscriptionTemplateOptionFactory = $subscriptionTemplateOptionFactory;
    }

    /**
     * @param int $subscriptionTemplateOptionId
     * @return SubscriptionTemplateOption
     * @throws NoSuchEntityException
     */
    public function retrieve($subscriptionTemplateOptionId)
    {
        if (!isset($this->subscriptionTemplateOptionRegistryById[$subscriptionTemplateOptionId])) {
            /** @var SubscriptionTemplateOption $subscriptionTemplateOption */
            $subscriptionTemplateOption = $this->subscriptionTemplateOptionFactory->create();
            $this->subscriptionTemplateOptionResource->load($subscriptionTemplateOption, $subscriptionTemplateOptionId);
            if (!$subscriptionTemplateOption->getId()) {
                throw NoSuchEntityException::singleField('subscriptionTemplateOptionId', $subscriptionTemplateOptionId);
            } else {
                $this->subscriptionTemplateOptionRegistryById[$subscriptionTemplateOptionId]
                    = $subscriptionTemplateOption;
            }
        }
        return $this->subscriptionTemplateOptionRegistryById[$subscriptionTemplateOptionId];
    }

    /**
     * @param int $subscriptionTemplateOptionId
     * @return void
     */
    public function remove($subscriptionTemplateOptionId)
    {
        if (isset($this->subscriptionTemplateOptionRegistryById[$subscriptionTemplateOptionId])) {
            unset($this->subscriptionTemplateOptionRegistryById[$subscriptionTemplateOptionId]);
        }
    }

    /**
     * @param SubscriptionTemplateOption $subscriptionTemplateOption
     * @return $this
     */
    public function push(SubscriptionTemplateOption $subscriptionTemplateOption)
    {
        $this->subscriptionTemplateOptionRegistryById[$subscriptionTemplateOption->getId()]
            = $subscriptionTemplateOption;
        return $this;
    }
}
