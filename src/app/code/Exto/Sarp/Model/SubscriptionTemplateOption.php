<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model;

use Magento\Framework\Profiler;
use Magento\Framework;
use Exto\Sarp\Model\ResourceModel\SubscriptionTemplateOption as SubscriptionTemplateOptionResourceModel;
use Exto\Sarp\Api\Data;

/**
 * Class SubscriptionTemplateOption
 */
class SubscriptionTemplateOption extends Framework\Model\AbstractModel
{
    /**
     * @var \Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterfaceFactory
     */
    private $subscriptionTemplateOptionDataFactory;

    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * SubscriptionTemplateOption constructor.
     * @param Framework\Model\Context $context
     * @param Framework\Registry $registry
     * @param SubscriptionTemplateOptionResourceModel $resource
     * @param SubscriptionTemplateOptionResourceModel\Collection $resourceCollection
     * @param Data\SubscriptionTemplateOptionInterfaceFactory $subscriptionTemplateOptionDataFactory
     * @param Framework\Api\DataObjectHelper $dataObjectHelper
     * @param array $data
     */
    public function __construct(
        Framework\Model\Context $context,
        Framework\Registry $registry,
        SubscriptionTemplateOptionResourceModel $resource,
        SubscriptionTemplateOptionResourceModel\Collection $resourceCollection,
        Data\SubscriptionTemplateOptionInterfaceFactory $subscriptionTemplateOptionDataFactory,
        Framework\Api\DataObjectHelper $dataObjectHelper,
        array $data = []
    ) {
        $this->subscriptionTemplateOptionDataFactory = $subscriptionTemplateOptionDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Initialize resource mode
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(SubscriptionTemplateOptionResourceModel::class);
    }

    /**
     * Retrieve SubscriptionTemplateOption model with data
     *
     * @return \Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterface
     */
    public function getDataModel()
    {
        $data = $this->getData();
        $dataObject = $this->subscriptionTemplateOptionDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $dataObject,
            $data,
            Data\SubscriptionTemplateOptionInterface::class
        );
        return $dataObject;
    }
}
