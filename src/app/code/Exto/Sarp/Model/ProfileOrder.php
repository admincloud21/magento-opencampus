<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model;

use Magento\Framework\Profiler;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Exto\Sarp\Model\ResourceModel\ProfileOrder as ProfileOrderResourceModel;
use Exto\Sarp\Api\Data\ProfileOrderInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Exto\Sarp\Api\Data\ProfileOrderInterface;

/**
 * Class ProfileOrder
 */
class ProfileOrder extends AbstractModel
{
    /**
     * @var \Exto\Sarp\Api\Data\ProfileOrderInterfaceFactory
     */
    private $profileOrderDataFactory;

    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * ProfileOrder constructor.
     * @param Context $context
     * @param Registry $registry
     * @param ProfileOrderResourceModel $resource
     * @param ProfileOrderResourceModel\Collection $resourceCollection
     * @param ProfileOrderInterfaceFactory $profileOrderDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ProfileOrderResourceModel $resource,
        ProfileOrderResourceModel\Collection $resourceCollection,
        ProfileOrderInterfaceFactory $profileOrderDataFactory,
        DataObjectHelper $dataObjectHelper,
        array $data = []
    ) {
        $this->profileOrderDataFactory = $profileOrderDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Initialize resource mode
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ProfileOrderResourceModel::class);
    }

    /**
     * Retrieve ProfileOrder model with data
     *
     * @return \Exto\Sarp\Api\Data\ProfileOrderInterface
     */
    public function getDataModel()
    {
        $data = $this->getData();
        $dataObject = $this->profileOrderDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $dataObject,
            $data,
            ProfileOrderInterface::class
        );
        return $dataObject;
    }
}
