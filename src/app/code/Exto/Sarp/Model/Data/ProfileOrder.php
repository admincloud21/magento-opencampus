<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Data;

use Magento\Framework\Api\AbstractExtensibleObject;
use Exto\Sarp\Api\Data\ProfileOrderInterface;

/**
 * Class ProfileOrder
 */
class ProfileOrder extends AbstractExtensibleObject implements ProfileOrderInterface
{
    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->_get(self::ID);
    }

    /**
     * @param int|null $id
     * @return $this
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @return int|null
     */
    public function getProfileId()
    {
        return $this->_get(self::PROFILE_ID);
    }

    /**
     * @param int|null $profileId
     * @return $this
     */
    public function setProfileId($profileId)
    {
        return $this->setData(self::PROFILE_ID, $profileId);
    }

    /**
     * @return int|null
     */
    public function getOrderId()
    {
        return $this->_get(self::ORDER_ID);
    }

    /**
     * @param int|null $orderId
     * @return $this
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }
    
    /**
     * @api
     * @return array
     */
    public function toArray()
    {
        return $this->__toArray();
    }
}
