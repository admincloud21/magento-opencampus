<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Data;

use Magento\Framework\Api\AbstractExtensibleObject;
use Exto\Sarp\Api\Data\SubscriptionTemplateOptionInterface;

/**
 * Class SubscriptionTemplateOption
 */
class SubscriptionTemplateOption extends AbstractExtensibleObject implements SubscriptionTemplateOptionInterface
{
    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->_get(self::ID);
    }

    /**
     * @param int|null $id
     * @return $this
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @return int|null
     */
    public function getTemplateId()
    {
        return $this->_get(self::TEMPLATE_ID);
    }

    /**
     * @param int|null $templateId
     * @return $this
     */
    public function setTemplateId($templateId)
    {
        return $this->setData(self::TEMPLATE_ID, $templateId);
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        return $this->_get(self::TITLE);
    }

    /**
     * @param string|null $title
     * @return $this
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * @return int|null
     */
    public function getBillingFrequency()
    {
        return $this->_get(self::BILLING_FREQUENCY);
    }

    /**
     * @param int|null $billingFrequency
     * @return $this
     */
    public function setBillingFrequency($billingFrequency)
    {
        return $this->setData(self::BILLING_FREQUENCY, $billingFrequency);
    }

    /**
     * @return string|null
     */
    public function getBillingPeriod()
    {
        return $this->_get(self::BILLING_PERIOD);
    }

    /**
     * @param string|null $billingPeriod
     * @return $this
     */
    public function setBillingPeriod($billingPeriod)
    {
        return $this->setData(self::BILLING_PERIOD, $billingPeriod);
    }

    /**
     * @return int|null
     */
    public function getBillingCycles()
    {
        return $this->_get(self::BILLING_CYCLES);
    }

    /**
     * @param int|null $billingCycles
     * @return $this
     */
    public function setBillingCycles($billingCycles)
    {
        return $this->setData(self::BILLING_CYCLES, $billingCycles);
    }
    
    /**
     * @api
     * @return array
     */
    public function toArray()
    {
        return $this->__toArray();
    }
}
