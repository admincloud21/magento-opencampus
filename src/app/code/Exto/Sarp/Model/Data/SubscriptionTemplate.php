<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Data;

use Magento\Framework\Api\AbstractExtensibleObject;
use Exto\Sarp\Api\Data\SubscriptionTemplateInterface;

/**
 * Class SubscriptionTemplate
 */
class SubscriptionTemplate extends AbstractExtensibleObject implements SubscriptionTemplateInterface
{
    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->_get(self::ID);
    }

    /**
     * @param int|null $id
     * @return $this
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        return $this->_get(self::TITLE);
    }

    /**
     * @param string|null $title
     * @return $this
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * @return int|null
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsSubscriptionOnly()
    {
        return $this->_get(self::IS_SUBSCRIPTION_ONLY);
    }

    /**
     * @param int|null $isSubscriptionOnly
     * @return $this
     */
    public function setIsSubscriptionOnly($isSubscriptionOnly)
    {
        return $this->setData(self::IS_SUBSCRIPTION_ONLY, $isSubscriptionOnly);
    }

    /**
     * @return int|null
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsChooseBillingDate()
    {
        return $this->_get(self::IS_CHOOSE_BILLING_DATE);
    }

    /**
     * @param int|null $isChooseBillingDate
     * @return $this
     */
    public function setIsChooseBillingDate($isChooseBillingDate)
    {
        return $this->setData(self::IS_CHOOSE_BILLING_DATE, $isChooseBillingDate);
    }

    /**
     * @return int|null
     */
    public function getMaxPaymentFailures()
    {
        return $this->_get(self::MAX_PAYMENT_FAILURES);
    }

    /**
     * @param int|null $maxPaymentFailures
     * @return $this
     */
    public function setMaxPaymentFailures($maxPaymentFailures)
    {
        return $this->setData(self::MAX_PAYMENT_FAILURES, $maxPaymentFailures);
    }

    /**
     * @return string|null
     */
    public function getDefaultOrderStatus()
    {
        return $this->_get(self::DEFAULT_ORDER_STATUS);
    }

    /**
     * @param string|null $defaultOrderStatus
     * @return $this
     */
    public function setDefaultOrderStatus($defaultOrderStatus)
    {
        return $this->setData(self::DEFAULT_ORDER_STATUS, $defaultOrderStatus);
    }

    /**
     * @return string|null
     */
    public function getOptionStyle()
    {
        return $this->_get(self::OPTION_STYLE);
    }

    /**
     * @param string|null $optionStyle
     * @return $this
     */
    public function setOptionStyle($optionStyle)
    {
        return $this->setData(self::OPTION_STYLE, $optionStyle);
    }

    /**
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * @param string|null $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsDeleted()
    {
        $result = $this->_get(self::IS_DELETED);
        if (null === $result) {
            return 0;
        }
        return $result;
    }

    /**
     * @param bool $isDeleted
     * @return $this
     */
    public function setIsDeleted($isDeleted)
    {
        if (null === $isDeleted) {
            $isDeleted = 0;
        }
        return $this->setData(self::IS_DELETED, $isDeleted);
    }
    
    /**
     * @api
     * @return array
     */
    public function toArray()
    {
        return $this->__toArray();
    }
}
