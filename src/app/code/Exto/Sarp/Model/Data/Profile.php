<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Sarp\Model\Data;

use Magento\Framework\Api\AbstractExtensibleObject;
use Exto\Sarp\Api\Data\ProfileInterface;

/**
 * Class Profile
 */
class Profile extends AbstractExtensibleObject implements ProfileInterface
{
    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->_get(self::ID);
    }

    /**
     * @param int|null $id
     * @return $this
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @return string|null
     */
    public function getProfileId()
    {
        return $this->_get(self::PROFILE_ID);
    }

    /**
     * @param string|null $profileId
     * @return $this
     */
    public function setProfileId($profileId)
    {
        return $this->setData(self::PROFILE_ID, $profileId);
    }

    /**
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->_get(self::CUSTOMER_ID);
    }

    /**
     * @param int|null $customerId
     * @return $this
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * @return string|null
     */
    public function getCustomerName()
    {
        return $this->_get(self::CUSTOMER_NAME);
    }

    /**
     * @param string|null $customerName
     * @return $this
     */
    public function setCustomerName($customerName)
    {
        return $this->setData(self::CUSTOMER_NAME, $customerName);
    }

    /**
     * @return string|null
     */
    public function getCustomerEmail()
    {
        return $this->_get(self::CUSTOMER_EMAIL);
    }

    /**
     * @param string|null $customerEmail
     * @return $this
     */
    public function setCustomerEmail($customerEmail)
    {
        return $this->setData(self::CUSTOMER_EMAIL, $customerEmail);
    }

    /**
     * @return string|null
     */
    public function getExternalServiceProfileId()
    {
        return $this->_get(self::EXTERNAL_SERVICE_PROFILE_ID);
    }

    /**
     * @param string|null $externalServiceProfileId
     * @return $this
     */
    public function setExternalServiceProfileId($externalServiceProfileId)
    {
        return $this->setData(self::EXTERNAL_SERVICE_PROFILE_ID, $externalServiceProfileId);
    }

    /**
     * @return string|null
     */
    public function getStatus()
    {
        return $this->_get(self::STATUS);
    }

    /**
     * @param string|null $status
     * @return $this
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        return $this->_get(self::TITLE);
    }

    /**
     * @param string|null $title
     * @return $this
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * @return string|null
     */
    public function getCreatedDate()
    {
        return $this->_get(self::CREATED_DATE);
    }

    /**
     * @param string|null $createdDate
     * @return $this
     */
    public function setCreatedDate($createdDate)
    {
        return $this->setData(self::CREATED_DATE, $createdDate);
    }

    /**
     * @return string|null
     */
    public function getCompletedDate()
    {
        return $this->_get(self::COMPLETED_DATE);
    }

    /**
     * @param string|null $completedDate
     * @return $this
     */
    public function setCompletedDate($completedDate)
    {
        return $this->setData(self::COMPLETED_DATE, $completedDate);
    }

    /**
     * @return string|null
     */
    public function getBillingStartDate()
    {
        return $this->_get(self::BILLING_START_DATE);
    }

    /**
     * @param string|null $billingStartDate
     * @return $this
     */
    public function setBillingStartDate($billingStartDate)
    {
        return $this->setData(self::BILLING_START_DATE, $billingStartDate);
    }
    
    /**
     * @return string|null
     */
    public function getLastSuccessfulDate()
    {
        return $this->_get(self::LAST_SUCCESSFUL_DATE);
    }

    /**
     * @param string|null $lastSuccessfulDate
     * @return $this
     */
    public function setLastSuccessfulDate($lastSuccessfulDate)
    {
        return $this->setData(self::LAST_SUCCESSFUL_DATE, $lastSuccessfulDate);
    }

    /**
     * @return int|null
     */
    public function getMaxPaymentFailures()
    {
        return $this->_get(self::MAX_PAYMENT_FAILURES);
    }

    /**
     * @param int|null $maxPaymentFailures
     * @return $this
     */
    public function setMaxPaymentFailures($maxPaymentFailures)
    {
        return $this->setData(self::MAX_PAYMENT_FAILURES, $maxPaymentFailures);
    }

    /**
     * @return string|null
     */
    public function getDefaultOrderStatus()
    {
        return $this->_get(self::DEFAULT_ORDER_STATUS);
    }

    /**
     * @param string|null $defaultOrderStatus
     * @return $this
     */
    public function setDefaultOrderStatus($defaultOrderStatus)
    {
        return $this->setData(self::DEFAULT_ORDER_STATUS, $defaultOrderStatus);
    }

    /**
     * @return int|null
     */
    public function getBillingFrequency()
    {
        return $this->_get(self::BILLING_FREQUENCY);
    }

    /**
     * @param int|null $billingFrequency
     * @return $this
     */
    public function setBillingFrequency($billingFrequency)
    {
        return $this->setData(self::BILLING_FREQUENCY, $billingFrequency);
    }

    /**
     * @return string|null
     */
    public function getBillingPeriod()
    {
        return $this->_get(self::BILLING_PERIOD);
    }

    /**
     * @param string|null $billingPeriod
     * @return $this
     */
    public function setBillingPeriod($billingPeriod)
    {
        return $this->setData(self::BILLING_PERIOD, $billingPeriod);
    }

    /**
     * @return int|null
     */
    public function getBillingCycles()
    {
        return $this->_get(self::BILLING_CYCLES);
    }

    /**
     * @param int|null $billingCycles
     * @return $this
     */
    public function setBillingCycles($billingCycles)
    {
        return $this->setData(self::BILLING_CYCLES, $billingCycles);
    }

    /**
     * @return mixed[]|null
     */
    public function getQuoteData()
    {
        return $this->_get(self::QUOTE_DATA);
    }

    /**
     * @param mixed[]|null $quoteData
     * @return $this
     */
    public function setQuoteData($quoteData)
    {
        return $this->setData(self::QUOTE_DATA, $quoteData);
    }

    /**
     * @return mixed[]|null
     */
    public function getQuoteItemsData()
    {
        return $this->_get(self::QUOTE_ITEMS_DATA);
    }

    /**
     * @param mixed[]|null $quoteItemsData
     * @return $this
     */
    public function setQuoteItemsData($quoteItemsData)
    {
        return $this->setData(self::QUOTE_ITEMS_DATA, $quoteItemsData);
    }

    /**
     * @return mixed[]|null
     */
    public function getBillingAddressData()
    {
        return $this->_get(self::BILLING_ADDRESS_DATA);
    }

    /**
     * @param mixed[]|null $billingAddressData
     * @return $this
     */
    public function setBillingAddressData($billingAddressData)
    {
        return $this->setData(self::BILLING_ADDRESS_DATA, $billingAddressData);
    }

    /**
     * @return mixed[]|null
     */
    public function getShippingAddressData()
    {
        return $this->_get(self::SHIPPING_ADDRESS_DATA);
    }

    /**
     * @param mixed[]|null $shippingAddressData
     * @return $this
     */
    public function setShippingAddressData($shippingAddressData)
    {
        return $this->setData(self::SHIPPING_ADDRESS_DATA, $shippingAddressData);
    }

    /**
     * @return string
     */
    public function getProductIds()
    {
        return $this->_get(self::PRODUCT_IDS);
    }

    /**
     * @param string $productIds
     * @return $this
     */
    public function setProductIds($productIds)
    {
        return $this->setData(self::PRODUCT_IDS, $productIds);
    }

    /**
     * @api
     * @return array
     */
    public function toArray()
    {
        return $this->__toArray();
    }

    /**
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsDeleted()
    {
        $result = $this->_get(self::IS_DELETED);
        if (null === $result) {
            return 0;
        }
        return $result;
    }

    /**
     * @param bool $isDeleted
     * @return $this
     */
    public function setIsDeleted($isDeleted)
    {
        if (null === $isDeleted) {
            $isDeleted = 0;
        }
        return $this->setData(self::IS_DELETED, $isDeleted);
    }
}
