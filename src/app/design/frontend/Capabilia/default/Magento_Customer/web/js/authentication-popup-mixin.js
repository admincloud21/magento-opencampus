define([
    'uiComponent',
    'jquery',
    'Magento_Ui/js/modal/modal',
    'mage/url',
    'mage/translate'
], function (Component, $, modal, url) {
    'use strict';

    return function (Component) {
        return Component.extend({
            sendRegisterToOctavio: function(){
                //validate
                $('#register-form .mage-error').hide();
                var passed= true;

                //validate empty first name
                if ($('#customer-fname').val()=='') {
                    passed = false;
                    $('#customer-fname-error').html($.mage.__('This is a required field.'));
                    $('#customer-fname-error').show();
                } else {
                    //validate unallowed characters on first name

                }

                //validate empty last name
                if ($('#customer-lname').val()=='') {
                    passed = false;
                    $('#customer-lname-error').html($.mage.__('This is a required field.'));
                    $('#customer-lname-error').show();
                } else {
                    //validate unallowed characters on last name

                }

                //validate email
                var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
                if (!pattern.test($('#reg-customer-email').val())) {
                    passed = false;
                    $('#reg-customer-email-error').html($.mage.__('The email entered is not valid.'));
                    $('#reg-customer-email-error').show();
                }

                //validate pass empty
                if ($('#reg-pass').val()=='') {
                    passed = false;
                    $('#reg-pass-error').html($.mage.__('This is a required field.'));
                    $('#reg-pass-error').show();
                } else {
                    //validate pass length
                    if ($('#reg-pass').val().length < 8) {
                        passed = false;
                        $('#reg-pass-error').html($.mage.__('Passwords must have at least 8 characters.'));
                        $('#reg-pass-error').show();
                    }
                }

                //validate pass2 empty
                if ($('#pass2').val()=='') {
                    passed = false;
                    $('#pass2-error').html($.mage.__('This is a required field.'));
                    $('#pass2-error').show();
                } else {
                    //validate pass2 length
                    if ($('#pass2').val().length < 8) {
                        passed = false;
                        $('#pass2-error').html($.mage.__('Passwords must have at least 8 characters.'));
                        $('#pass2-error').show();
                    }
                }

                //validate different passwords
                if ($('#reg-pass').val()!= $('#pass2').val()) {
                    passed = false;
                    $('#general-pass-error').html($.mage.__('Passwords do not match.'));
                    $('#general-pass-error').show();
                } else {
                    //validate uppercase on pass
                    var passs1 = $('#reg-pass').val();
                    var patron=/[A-Z]/g;
                    var resultado = passs1.match(patron);
                    if (resultado === null) {
                        passed = false;
                        $('#general-pass-error').html($.mage.__('Password must contain at least one uppercase letter.'));
                        $('#general-pass-error').show();
                    }
                }

                //validate privacy policies
                if (!$('#privacy-policies').prop('checked')) {
                    passed = false;
                    $('#privacy-error').html($.mage.__('Please accept the privacy policies.'));
                    $('#privacy-error').show();
                }

                if (passed) {
                    //your code to send ajax request here
                    var ajaxRequest;
                    ajaxRequest = jQuery.ajax({
                        url: url.build('bw-capabilia/account/RegisterOctavioPost') + '?isAjax=true',
                        type: 'POST',
                        data: {
                            fname: $('#customer-fname').val(),
                            lname: $('#customer-lname').val(),
                            pass: $('#reg-pass').val(),
                            email: $('#reg-customer-email').val()
                        },
                        dataType: 'json',
                        showLoader: true
                    });

                    //Show successfully for submit message
                    ajaxRequest.done(function (response, textStatus, jqXHR) {
                        if (response.response=='ok') {
                            location.reload();
                        } else{
                            if(response.error) {
                                alert(response.error);
                            } else {
                                console.log(response);
                            }
                        }
                    });

                    //On failure of request this function will be called
                    ajaxRequest.fail(function () {
                        alert($.mage.__('Oops, An error occured, please try again later!'));
                    });
                }
            }
        });
    };
});