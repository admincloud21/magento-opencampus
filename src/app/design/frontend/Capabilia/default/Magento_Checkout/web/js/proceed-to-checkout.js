/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'Magento_Customer/js/model/authentication-popup',
    'Magento_Customer/js/customer-data'
], function ($, authenticationPopup, customerData) {
    'use strict';

    return function (config, element) {
        $(element).click(function (event) {
            if(!($('#form-validate').validation() && $('#form-validate').validation('isValid'))) {
                return false;
            }

            var cart = customerData.get('cart'),
            customer =  window.customerData;

            event.preventDefault();

            if (!customer.firstname && cart().isGuestCheckoutAllowed === false) {
                authenticationPopup.showModal();
                return false;
            }

            $(element).attr('disabled', true);
            location.href = config.checkoutUrl;
        });

    };
});
