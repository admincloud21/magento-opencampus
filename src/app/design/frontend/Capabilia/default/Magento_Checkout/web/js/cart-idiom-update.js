define([
    'jquery',
], function ($) {
    $(document).ready(function(){
        $(document).on('change', '.item_impartido_en', function(){
            console.log(this.name + "=" + $(this).val() + "&isAjax=1");
            var urlPrefix = window.location.href.split('/');
            urlPrefix = (urlPrefix[3] == 'es' || urlPrefix[3] == 'pt') ? urlPrefix[3] + '/' : '';
            $.ajax({
                url: '/' + urlPrefix + 'bw-capabilia/cart/update',
                data: this.name + "=" + $(this).val() + "&isAjax=1",
                showLoader: true,
                success: function (res) {
                    //console.log(res);
                },
                error: function (xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        });
    });
});