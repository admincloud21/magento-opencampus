/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'underscore',
        'jquery',
        'Magento_Checkout/js/model/quote',
        'mage/translate',
        'Magento_Catalog/js/price-utils',
        'Magento_Paypal/js/view/payment/method-renderer/paypal-express-abstract',
        'Magento_Paypal/js/action/set-payment-method',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Ui/js/lib/view/utils/dom-observer',
        'paypalInContextExpressCheckout',
        'Magento_Customer/js/customer-data',
        'Magento_Ui/js/model/messageList'
    ],
    function (
        _,
        $,
        quote,
        $t,
        priceUtils,
        Component,
        setPaymentMethodAction,
        additionalValidators,
        domObserver,
        paypalExpressCheckout,
        customerData,
        messageList
    ) {
        'use strict';

        // State of PayPal module initialization
        var clientInit = false;

        return Component.extend({

            defaults: {
                template: 'Magento_Paypal/payment/paypal-express-in-context',
                installment_qty: '',
                clientConfig: {
                    /**
                     * @param {Object} event
                     */
                    click: function (event) {
                        event.preventDefault();

                        if (additionalValidators.validate()) {
                            paypalExpressCheckout.checkout.initXO();

                            this.selectPaymentMethod();
                            setPaymentMethodAction(this.messageContainer).done(function () {
                                $('body').trigger('processStart');

                                $.getJSON(this.path, {
                                    button: 0
                                }).done(function (response) {
                                    var message = response && response.message;

                                    if (message) {
                                        if (message.type === 'error') {
                                            messageList.addErrorMessage({
                                                message: message.text
                                            });
                                        } else {
                                            messageList.addSuccessMessage({
                                                message: message.text
                                            });
                                        }
                                    }

                                    if (response && response.url) {
                                        paypalExpressCheckout.checkout.startFlow(response.url);

                                        return;
                                    }

                                    paypalExpressCheckout.checkout.closeFlow();
                                }).fail(function () {
                                    paypalExpressCheckout.checkout.closeFlow();
                                }).always(function () {
                                    $('body').trigger('processStop');
                                    customerData.invalidate(['cart']);
                                });
                            }.bind(this)).fail(function () {
                                paypalExpressCheckout.checkout.closeFlow();
                            });
                        }
                    }
                }
            },

            /**
             * @returns {Object}
             */
            initialize: function () {
                this._super();
                this.initClient();

                return this;
            },
            getData: function() {
                return {
                    'method': this.item.method,
                    'additional_data': {
                        'installment_qty': this.installment_qty()
                    }
                };
            },
            getInstallments: function() {
                var grandTotal = quote.totals().grand_total;
                var portionAmount = 0;
                var installlmentsValue = [];
                installlmentsValue[1] = grandTotal;

                for (var i = 2; i <= this.getCcNumberOfInstallments(); i++) {
                    portionAmount = grandTotal / i;
                    // confere se a parcela nao esta abaixo do minimo
                    if (this.getCcMaxInstallmentQty() >= 0 && i > this.getCcMaxInstallmentQty()) {
                        break;
                    }

                    installlmentsValue[i] = portionAmount;
                }

                installlmentsValue.splice(0,1);

                return installlmentsValue;
            },
            getCcMaxInstallmentQty: function () {
                return 3;
            },
            getCcNumberOfInstallments: function () {
                return 6;
            },
            initObservable: function () {
                this._super()
                    .observe([
                        'installment_qty'
                    ]);

                return this;
            },
            getTemplateOptionsValues: function() {
                return window.checkoutConfig.payment.extoRecurrence.template_options_values;
                return _.map(this.getInstallments(), function (value, key) {
                    return {
                        'value': key+1,
                        'template_option_id': $t('%1x of %2 no taxes').replace('%1', key+1).replace('%2', priceUtils.formatPrice(value, quote.getPriceFormat()))
                    };
                });
            },
            /**
             * @returns {Object}
             */
            initClient: function () {
                var selector = '#' + this.getButtonId();

                _.each(this.clientConfig, function (fn, name) {
                    if (typeof fn === 'function') {
                        this.clientConfig[name] = fn.bind(this);
                    }
                }, this);

                if (!clientInit) {
                    domObserver.get(selector, function () {
                        paypalExpressCheckout.checkout.setup(this.merchantId, this.clientConfig);
                        clientInit = true;
                        domObserver.off(selector);
                    }.bind(this));
                } else {
                    domObserver.get(selector, function () {
                        $(selector).on('click', this.clientConfig.click);
                        domObserver.off(selector);
                    }.bind(this));
                }

                return this;
            },

            /**
             * @returns {String}
             */
            getButtonId: function () {
                return this.inContextId;
            }
        });
    }
);
