/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'Magento_Paypal/js/view/payment/method-renderer/paypal-express-abstract',
    'Magento_Checkout/js/model/quote',
    'mage/translate',
    'Magento_Catalog/js/price-utils'
], function (Component, quote, $t, priceUtils) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Magento_Paypal/payment/paypal-express',
            installment_qty: ''
        },
        getData: function() {
            return {
                'method': this.item.method,
                'additional_data': {
                    'installment_qty': this.installment_qty()
                }
            };
        },
        initObservable: function () {
            this._super()
                .observe([
                    'installment_qty'
                ]);

            return this;
        },
        getInstallments: function() {
            var grandTotal = quote.totals().grand_total;
            var portionAmount = 0;
            var installlmentsValue = [];
            installlmentsValue[1] = grandTotal;

            for (var i = 2; i <= this.getCcNumberOfInstallments(); i++) {
                portionAmount = grandTotal / i;
                // confere se a parcela nao esta abaixo do minimo
                if (this.getCcMaxInstallmentQty() >= 0 && i > this.getCcMaxInstallmentQty()) {
                    break;
                }

                installlmentsValue[i] = portionAmount;
            }

            installlmentsValue.splice(0,1);

            return installlmentsValue;
        },
        getCcMaxInstallmentQty: function () {
            return 3;
        },
        getCcNumberOfInstallments: function () {
            return 6;
        },
        getTemplateOptionsValues: function() {
            return window.checkoutConfig.payment.extoRecurrence.template_options_values;
            return _.map(this.getInstallments(), function (value, key) {
                return {
                    'value': key+1,
                    'template_option_id': $t('%1x of %2 no taxes').replace('%1', key+1).replace('%2', priceUtils.formatPrice(value, quote.getPriceFormat()))
                };
            });
        }
    });
});
