# Magento 2018 Final

Magento

## Configuring Magento

First you need to run composer to install all packages

    composer install

Configure your env.php file in `app/etc/env.php` path with content like:

```php
<?php
return [
    'backend' => [
        'frontName' => 'admin_2014wg'
    ],
    'crypt' => [
        'key' => '8cfaa6dd5555257e97b8f7628792e525'
    ],
    'db' => [
        'table_prefix' => '',
        'connection' => [
            'default' => [
                'host' => 'localhost',
                'dbname' => 'capabilia',
                'username' => 'root',
                'password' => 'root',
                'active' => '1'
            ]
        ]
    ],
    'resource' => [
        'default_setup' => [
            'connection' => 'default'
        ]
    ],
    'x-frame-options' => 'SAMEORIGIN',
    'MAGE_MODE' => 'developer',
    'session' => [
        'save' => 'files'
    ],
    'cache_types' => [
        'config' => 1,
        'layout' => 1,
        'block_html' => 1,
        'collections' => 1,
        'reflection' => 1,
        'db_ddl' => 1,
        'eav' => 1,
        'customer_notification' => 1,
        'config_integration' => 1,
        'config_integration_api' => 1,
        'full_page' => 1,
        'translate' => 1,
        'config_webservice' => 1
    ],
    'install' => [
        'date' => 'Tue, 11 Sep 2018 18:12:21 +0000'
    ]
];

```

## @TODO

Setup Environment to `production` when create production server

## Capistrano

Add SSH Key to you agent ssh:

   ssh-add ~/.ssh/andre

To install capistraon on MacOs:

    gem install capistrano
    gem install bundler
    gem install capistrano-magento2
    cd tools/cap
    bundle install
    cap staging deploy 
