namespace :mage2 do
    desc 'Set the necessary ownership / permissions'
    task :fix_permission do
      on roles(:all) do |host|
        execute :sudo, :chmod, "-R 777 #{shared_path}/var #{shared_path}/pub/media #{release_path}/app/etc #{release_path}/vendor #{release_path}/pub/static"
        execute :sudo, :chmod, "+x #{release_path}/bin/magento"
        execute :sudo, :chown, "-R www-data.www-data #{release_path}/"
        execute :sudo, :chown, "-R www-data.www-data #{current_path}"
      end
    end

end
