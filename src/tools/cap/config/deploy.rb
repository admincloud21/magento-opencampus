# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"

set :application, "capabilia"
set :repo_url, "andre_bestworlds@bitbucket.org:admincloud21/magento-2018-final.git"

set :magento_auth_public_key, '4f7e82741316e52fdb3b5f42961c0984'
set :magento_auth_private_key, '8bbc0cd9a98986b37332fed176996eea'

after "deploy:symlink:release", "mage2:fix_permission"

# append :linked_dirs, 'vendor'

# Default value for keep_releases is 5
set :keep_releases, 3

set :magento_deploy_languages, ['en_US', 'es_ES', 'pt_BR', 'ca_ES']

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml"

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure
