-- SELECT * FROM core_config_data WHERE path in ('web/unsecure/base_url', 'web/secure/base_url', 'web/unsecure/base_static_url', 'web/secure/base_static_url', 'capabilia/general/redirect_success_url', 'capabilia/general/redirect_success_url_event');
UPDATE core_config_data SET value = replace(value, 'opencampus.capabilia.org', 'local.opencampus.com')
WHERE path in ('web/unsecure/base_url', 'web/secure/base_url', 'web/unsecure/base_static_url', 'web/secure/base_static_url', 'capabilia/general/redirect_success_url', 'capabilia/general/redirect_success_url_event');

-- SELECT * FROM core_config_data WHERE path = 'web/cookie/cookie_domain';
UPDATE core_config_data SET value = 'local.opencampus.com' WHERE path = 'web/cookie/cookie_domain';

-- SELECT * FROM core_config_data WHERE path LIKE '%popupwindowmessage/popup_extension/popup_mod_enable%';
UPDATE core_config_data SET value = 0 WHERE path LIKE '%popupwindowmessage/popup_extension/popup_mod_enable%';


-- SELECT * FROM customer_entity WHERE email NOT LIKE '%TEST@BW_%' AND email NOT LIKE '%@bestworlds%' ;
UPDATE customer_entity SET email = CONCAT(SUBSTRING_INDEX(email,'@',1),'_TEST@BW_',SUBSTRING_INDEX(email,'@',-1)) WHERE email NOT LIKE '%TEST@BW_%' AND email NOT LIKE '%@bestworlds%' ;
-- SELECT * FROM sales_order WHERE customer_email NOT LIKE '%TEST@BW_%' AND customer_email NOT LIKE '%@bestworlds%' ;
UPDATE sales_order SET customer_email = CONCAT(SUBSTRING_INDEX(customer_email,'@',1),'_TEST@BW_',SUBSTRING_INDEX(customer_email,'@',-1)) WHERE customer_email NOT LIKE '%TEST@BW_%' AND customer_email NOT LIKE '%@bestworlds%' ;
-- SELECT * FROM sales_order_grid WHERE customer_email NOT LIKE '%TEST@BW_%' AND customer_email NOT LIKE '%@bestworlds%' ;
UPDATE sales_order_grid SET customer_email = CONCAT(SUBSTRING_INDEX(customer_email,'@',1),'_TEST@BW_',SUBSTRING_INDEX(customer_email,'@',-1)) WHERE customer_email NOT LIKE '%TEST@BW_%' AND customer_email NOT LIKE '%@bestworlds%' ;
-- SELECT * FROM newsletter_subscriber WHERE subscriber_email NOT LIKE '%TEST@BW_%' AND subscriber_email NOT LIKE '%@bestworlds%' ;
UPDATE newsletter_subscriber SET subscriber_email = CONCAT(SUBSTRING_INDEX(subscriber_email,'@',1),'_TEST@BW_',SUBSTRING_INDEX(subscriber_email,'@',-1)) WHERE subscriber_email NOT LIKE '%TEST@BW_%' AND subscriber_email NOT LIKE '%@bestworlds%' ;

