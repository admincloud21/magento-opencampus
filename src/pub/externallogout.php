<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use Magento\Framework\App\Bootstrap;

//require __DIR__ . 'app/bootstrap.php';
require '../app/bootstrap.php';

require_once 'jwt/BeforeValidException.php';
require_once 'jwt/ExpiredException.php';
require_once 'jwt/SignatureInvalidException.php';
require_once 'jwt/JWT.php';


use \Firebase\JWT\JWT;

$JWTkey = "Sr9LDr5iG7X2IlFzBIHTpGQNUbd0EbVelQ7h/9U/ljRIBsKXGWjnB8SLtGQ3czqi7dCDOET/E1cF+W90hCcBVc2ouEiT+R682drxf8A5EGDlEv+snHRoEc8lNmAg8iYLgZwh3VlJM2XxAM4fXqhRpORSK3YzsOTUhYfxhA37dxfTODRPl36C+vPa4TpdWXAr/Wn8wt+9KLIrieC8ARJdyweutT181cARxT/LwpGQgNkrns2/Qg7KrR/Dy/SPDU2F5ivgyVDq1+So6ylgNRcHocPfRim26UyO7tSre31+9lfvSi6mnu9/ichcDOdipKZ9bhUG4zmkzFfvQr3PZnGb9Q==";

$params = $_SERVER;

$bootstrap = Bootstrap::create(BP, $params);

$obj = $bootstrap->getObjectManager();

$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();

$request = $objectManager->get('Magento\Framework\App\RequestInterface');
$jwt = $request->getParam('jwt');
$token = $request->getParam('token');

$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$websiteId = $storeManager->getStore()->getWebsiteId();
/*
 *
Funcionamiento:
    -el wordpress hace un post a este archivo con el jwt
*/
try {
    $decoded = JWT::decode($jwt, $JWTkey, array('HS256'));
    $customerSession = $objectManager->create('Magento\Customer\Model\Session');
    if (
        !$customerSession->isLoggedIn() 
        || ($decoded->userName != $customerSession->getCustomer()->getEmail())
    ) {
        throw new Exception('Could\'nt logout the user');
    }

    $customerSession->logout();

    if(!$customerSession->isLoggedIn()) {
        echo "OK";
    }else{
        echo "ERROR";
    }

} catch (Exception $e) {
    echo $e->getMessage();
}
