#!/bin/bash

set -eo pipefail

TAG=$3
IMAGE_NAME=$2
GCR_PATH=$1

# image specifics:
REPO=$4
BRANCH=$5
DB_NAME=$6
DB_USER=$7
DB_PASS=$8
DB_HOST=$9
CDN_CLIENT_FOLDER=${10}
CDN_ENV_FOLDER=${11}
SERVER_NAME=${12}
REDIS_HOST=${13}
CLUSTER=${14}
ZONE=${15}
NAMESPACE=${16}

# copy media from storage so it is available at build time
echo "removing remote client cache"
gsutil -m rm -r gs://static.capabiliaserver.com/frontend/clients/${CDN_CLIENT_FOLDER}/${CDN_ENV_FOLDER}/media/catalog/product/cache/ 2> /dev/null || true

echo "copying client media folder"
mkdir MAGENTO_MEDIA_STORAGE
gsutil -m rsync -r gs://static.capabiliaserver.com/frontend/clients/${CDN_CLIENT_FOLDER}/${CDN_ENV_FOLDER}/media MAGENTO_MEDIA_STORAGE 2> /dev/null || true


# connect to redis instance
echo "connecting builder to cluster: "$CLUSTER" at "$ZONE" (ns: "$NAMESPACE")"
gcloud container clusters get-credentials $CLUSTER --zone $ZONE
REDIS_SVC=${REDIS_HOST%"."$NAMESPACE".svc.cluster.local"}
echo "forwarding redis svc: "$REDIS_SVC
kubectl port-forward svc/$REDIS_SVC 6379:6379 -n $NAMESPACE & 

# build
echo "starting build"
docker build . \
-t $GCR_PATH/$IMAGE_NAME:$TAG \
--network=host \
--build-arg BRANCH=$BRANCH \
--build-arg REPO=$REPO \
--build-arg DB_NAME=$DB_NAME \
--build-arg DB_USER=$DB_USER \
--build-arg DB_PASS=$DB_PASS \
--build-arg DB_HOST=$DB_HOST \
--build-arg CDN_CLIENT_FOLDER=$CDN_CLIENT_FOLDER \
--build-arg CDN_ENV_FOLDER=$CDN_ENV_FOLDER \
--build-arg SERVER_NAME=$SERVER_NAME \
--build-arg REDIS_HOST=$REDIS_HOST

# upload
echo "uploading image"
docker push $GCR_PATH/$IMAGE_NAME:$TAG

# after we built, we copy the static media files back to gcp storage
# creamos un writable container con create. luego usamos docker cp para copiar el contenido.
echo "copying new media and static files to storage"
mkdir MAGENTO_MEDIA_STORAGE_NEW
docker create -ti --name dummy $GCR_PATH/$IMAGE_NAME:$TAG bash
docker cp dummy:/var/www/html/pub/media/. MAGENTO_MEDIA_STORAGE_NEW
docker rm -f dummy

gsutil -m rsync -rd MAGENTO_MEDIA_STORAGE_NEW gs://static.capabiliaserver.com/frontend/clients/${CDN_CLIENT_FOLDER}/${CDN_ENV_FOLDER}/media

kill $( jobs -p )

echo "build finished! ;)"
exit 0