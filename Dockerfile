FROM gcr.io/infra-capabilia-v2/stretch-php7.1-fpm-nginx:prod

# build args
ARG BRANCH
ARG REPO
ARG DB_NAME
ARG DB_USER
ARG DB_PASS
ARG DB_HOST
ARG CDN_CLIENT_FOLDER
ARG CDN_ENV_FOLDER
ARG SERVER_NAME
ARG REDIS_HOST

# cloning base repo...
RUN mkdir -p /tmp/code/; \
    cd /tmp/code; \
    git clone --quiet --depth=1 --single-branch --branch $BRANCH $REPO .; \
    rm -rf /var/www/html/* ; \
    rsync -avq --exclude ".git" /tmp/code/src/ /var/www/html;

COPY auth.json /var/www/html/auth.json

# setup magento env
COPY env.php /var/www/html/app/etc/env.php
RUN sed -i 's/##DB_HOST##/'${DB_HOST}'/g' /var/www/html/app/etc/env.php; \
    sed -i 's/##DB_NAME##/'${DB_NAME}'/g' /var/www/html/app/etc/env.php; \
    sed -i 's/##DB_USER##/'${DB_USER}'/g' /var/www/html/app/etc/env.php; \
    sed -i 's/##DB_PASS##/'${DB_PASS}'/g' /var/www/html/app/etc/env.php; 

# setup magento nginx config
RUN sed -i 's/##SERVER_NAME##/'${SERVER_NAME}'/g' /etc/nginx/sites-available/magento.conf;

# installing magento...
RUN export COMPOSER_HOME=/root; \
    cd /var/www/html; \
    composer install --no-dev --quiet

# preparing magento...
# para este paso se requiere el remote builder. para que el build se haga en una VM con acceso a la ip interna de la base de datos
RUN chmod +x /var/www/html/bin/magento; \
    rm -rf var/di/* var/generation/* var/cache/* var/page_cache/* var/view_preprocessed/* var/composer_home/cache/*; \
    chmod 777 var -R; \
    chmod 777 pub -R; \
    bin/magento cache:flush; \
    bin/magento setup:upgrade; \
    bin/magento setup:di:compile

# copy media existing on gcp storage
COPY MAGENTO_MEDIA_STORAGE/ /var/www/html/pub/media

# generating static content
RUN bin/magento setup:static-content:deploy -j16 en_US; \
    bin/magento setup:static-content:deploy -j16 es_ES; \
    bin/magento setup:static-content:deploy -j16 ca_ES; \
    bin/magento setup:static-content:deploy -j16 pt_BR

# setup redis
RUN echo "127.0.0.1  localhost $REDIS_HOST" >> /etc/hosts; \
    bin/magento setup:config:set --cache-backend=redis --cache-backend-redis-server=$REDIS_HOST --cache-backend-redis-db=0; \
    bin/magento setup:config:set --page-cache=redis --page-cache-redis-server=$REDIS_HOST --page-cache-redis-db=1; \
    rm -rf var/cache/* var/page_cache/*; 
    
# preparing copied files and configuring permissions...
RUN rm -rf /var/www/code; \
    chown -R www-data:www-data /var/www/html/; \
    chmod -R 777 /var/www/html/var /var/www/html/pub/media /var/www/html/app/etc /var/www/html/vendor /var/www/html/pub/static /var/www/html/generated; \
    chown -R www-data:www-data /var/www/html/


    