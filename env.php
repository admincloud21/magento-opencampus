<?php
return [
    "backend" => [
        "frontName" => "admin_1aohdg"
    ],
    "crypt" => [
        "key" => "b58f4a8dd333a5dfa8ff137f3ae5dc1f"
    ],
    "db" => [
        "table_prefix" => "",
        "connection" => [
            "default" => [
                "host" => "##DB_HOST##",
                "dbname" => "##DB_NAME##",
                "username" => "##DB_USER##",
                "password" => "##DB_PASS##",
                "model" => "mysql4",
                "engine" => "innodb",
                "initStatements" => "SET NAMES utf8;",
                "active" => "1"
            ]
        ]
    ],
    "resource" => [
        "default_setup" => [
            "connection" => "default"
        ]
    ],
    "x-frame-options" => "SAMEORIGIN",
    "MAGE_MODE" => "production",
    "session" => [
        "save" => "files"
    ],
    "cache_types" => [
        "config" => 1,
        "layout" => 1,
        "block_html" => 1,
        "collections" => 1,
        "reflection" => 1,
        "db_ddl" => 1,
        "eav" => 1,
        "customer_notification" => 1,
        "config_integration" => 1,
        "config_integration_api" => 1,
        "full_page" => 1,
        "translate" => 1,
        "config_webservice" => 1,
        "compiled_config" => 1
    ],
    "install" => [
        "date" => "Tue, 21 Aug 2018 20:04:12 +0000"
    ],
    "system" => [
        "default" => [
            "dev" => [
                "debug" => [
                    "debug_logging" => "0"
                ]
            ]
        ]
    ]
];